#09/15/2015: Created by Nicole Czakon
#
#The purpose of this file is keep all of the items that need to be uniformly soft-coded in one location.

# DIRECTORIES

homeDirectory = '/array/users/czakon/'
plotDirectory = homeDirectory + 'figures/'
dataDirectory = homeDirectory + 'data/'
catalogDirectory = dataDirectory + 'catalogs/'
ucacDirectory = catalogDirectory + 'UCAC4/ucac4/'
hscDataDirectory = dataDirectory + 'HSC/'
sspPatchDirectory = hscDataDirectory + 'sspPatches/'
sqlDirectory = dataDirectory + 'sql_db/'
analysisDirectory = dataDirectory + 'analysis/'
maskPlotDirectory = analysisDirectory + 'plots/hsc_pipeline/masking/'
characterizationPlotDirectory = analysisDirectory + 'plots/hsc_pipeline/characterization/'
characterizationSavDirectory = analysisDirectory + 'sav_files/hsc_pipeline/characterization/'
maskRegDirectory = dataDirectory + 'bright_star_mask/'
maskSavDirectory = analysisDirectory + 'sav_files/hsc_pipeline/masking/'

# FILES
fullField = 'D-S15b-DR-2'
# STEP 1: Downloaded from the data archive, needs to be converted to an SQLite database.
downloadXMMLSS_sql = sqlDirectory + 'SSP/XMM_Wide_11042015.csv'
#'XMM_Wide_10022015_v2.csv'#'8524_8525_8766_8767_w_psf.csv'#'8524_8525_8766_8767_w_psf_full.csv'
# STEP 2: Downloaded from the data archive.
sqlSourceBase = sqlDirectory + 'SSP/XMM_LSS_sql_psf'
sqlSourceDb = sqlSourceBase + '.db'#XMM_LSS_sql.db'#'XMM_LSS_sql_psf_mod.db'#'XMM_WIDE.db'
altSqlSourceBase = sqlDirectory + 'SPP/XMM_WIDE'
altSqlSourceDb = altSqlSourceBase + '.db'
#This is corrupted
#tycho_fits_file = catalogDirectory + 'TYCHO/tycho_cat.fits'
#
tychoDatFile = catalogDirectory + 'TYCHO/catalog.dat'
ucacDirFile = ucacDirectory + 'listAll'
# STEP 3: Created by Nicole specifically for the bright star analysis. 
brightStarDb = 'BrightStar.db' #sqlDirectory +
#And this is the copy back up 
#brightStarDb = 'BrightStarCopyAll.db' #sqlDirectory +
#sourcesDb = 'SSP/gband11661.sqlite3'
sourcesDb = {'g':'SSP/gband11661.sqlite3','r':'SSP/rband11669.sqlite3','i':'SSP/iband11670.sqlite3','z':'SSP/zband11674.sqlite3', 'y':'SSP/yband11675.sqlite3'}
             
#This is a new database created on 12/10/2015
#bsDb = sqlDirectory + 'bs.db'
randomsDb = 'randoms.db' #sqlDirectory + 
#bright_star_p = sqlDirectory + 'BrightStar.p'
# STEP 4: Output of the correlation analysis as a function of bright-star and source magnitude.
corrFilename = maskSavDirectory + 'Corr_'

#########################################################################
#The pixel scale can be obtained via the following:
#import loadTestExposure
#testExposure.exposure.getWcs().pixelScale().asArcseconds()
pixScale = 0.168#arcsec/pixel
patchSide = 4200#pixels
# Mask size
# SSP-derived: steeper at the bright end, shallower at the faint end
bands = ['G','R','I','Z','Y']
fields = {'W-XMM':{'RA':[25,43],'Dec':[-10,1]}, \
          'W-GAMA09H':{'RA':[126,144],'Dec':[-5.0,7.0]}, \
          'W-GAMA15H':{'RA':[206,228],'Dec':[-6.0,6.0]}, \
          'W-VVDS':{'RA':[326,346],'Dec':[-4.0,6.0]}, \
          'W-WIDE12H':{'RA':[172,186],'Dec':[-5.0,5.0]}, \
          'W-HECTMAP':{'RA':[236,252],'Dec':[37.0,49.0]}}

# CORRELATION FUNCTION/BRIGHT-STAR RADIUS

corrCutoff = {'large':[-0.2,+0.2], \
              'medium':[-0.5,+0.2], \
              'small':[-0.8,+0.2]}              
#
# bf = 150.*10**(0.30*(7.0-self.medNomad)) + 12.*10**(0.05*(16.0-self.medNomad))
# New data release Feb 17, 2016
# Maximum set to 1.0
# bf = A0   *10**(B0* (C0-self.medNomad))       + A1 *10**(B1*(C1-self.medNomad))
# bf = 1.0E3*10**(1.2*(7.0-self.medBrightStar)) + 12.*10**(0.1*(16.0-self.medBrightStar))
# bf2 = 12.*10**(0.1*(16.0-self.medBrightStar))

brightStarRadius = {'small': {'maxRadius':0.25,'A0':  0.05,  'B0':1.00,'C0':7.0,'A1':6.0,'B1':0.1, 'C1':16.0}, \
                    'medium':{'maxRadius':0.50,'A0': 12.00,  'B0':1.00,'C0':7.0,'A1':12.0,'B1':0.1, 'C1':16.0}, \
                    'large': {'maxRadius':1.00,'A0': 50.00,  'B0':1.00,'C0':7.0,'A1':12.0,'B1':0.1,'C1':16.0}}
#bf = 1.0E3*10**(1.2*(7.0-self.medBrightStar)) + 12.*10**(0.1*(16.0-self.medBrightStar))
#bf2 = 12.*10**(0.1*(16.0-self.medBrightStar))
#                    'medium':{'A0':1.0E2,  'B0':1.00,'C0':7.0,'A1':24.0,'B1':0.1, 'C1':16.0}, \
#                    'large': {'A0':2.0E2,  'B0':0.25,'C0':7.0,'A1':12.0,'B1':0.05,'C1':16.0}}
#brightStarRadius = {'A0':150.,'B0':0.30,'C0':7.0,'A1':12.0,'B1':0.05,'C1':16.0}
# Jean Coupon
# bf_jp = (self.medNomad/26.0)**(-3.4)

#########################################################################
# This is for the single exposures
testVisit = {'G':11712,'R':1206,'I':1246,'Z':1186,'Y':11726}
#Check to make sure this doesn't choke because I removed the leading zeroes...
#testVisit = {'G':11712,'R':01206,'I':01246,'Z':01186,'Y':11726}
testCcd = 58
# This is for the coadds.
testTract = 8524#0
testPatch = '1,7'#'5,5'
################
# 1: This is the format needed if I'm running the code from IPMU
#setenv runDir "/lustre/Subaru/SSP/rerun/yasuda/SSP3.7.3_20150518"
# 4: This is for the data release, I can't manage to get this to work, think about it in the future.
#setenv runDir "/array/users/czakon/data/s15a_deep"
sspDir = hscDataDirectory + 'lustre/Subaru/SSP/'#'/array2/SSP/'
#runDir = sspDir + 'rerun/s15_pre_sumire'#/hsc_ssp/dr1/s15b_pre/data/s15b_deep'
runDir = sspDir + 'rerun/s15b_wide/'#/hsc_ssp/dr1/s15b_pre/data/s15b_deep'
randomsDir = sspDir + 'rerun/s15b_wide/randoms/HSC-G/'
#OLD keep
#randomFitsFile = catalogDirectory + 'HSC/WIDE_20151216_2_randoms.fits'
randomFitsFile = runDir + '10055/0,0/ran-HSC-G-10055-0,0.fits'
#runDir = sspDir + 'rerun/yasuda/SSP3.8.5_20150725'
# 4a: This doesn't work directly because I have to include the _mapper files, etc.
#setenv runDir "/array/SSP/133.40.7.159:4443/hsc_ssp/dr_early/s15a/data/s15a_deep"
# 2: This is a randomly chosen ccd in SSP 3.7.3 (Obsolete?)
#setenv runDir "/array/users/czakon/lustre/Subaru/SSP/rerun/yasuda/SSP3.7.3_20150518"
# 3: This is the directory for COSMOS, not necessary the best one to use to identify bright stars. (Obsolete?)
#setenv runDir "/array/users/czakon/lustre/Subaru/SSP/rerun/yasuda/SSP3.8.5_20150810_cosmos"

