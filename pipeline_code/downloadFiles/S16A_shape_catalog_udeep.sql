SELECT
  meas.object_id
, meas.parent_id
, meas.ira
, meas.idec

, meas.imag_kron
, meas.imag_kron_err

, meas.icmodel_mag
, meas.icmodel_mag_err

-- forced Kron magnitudes
, forced.gmag_kron        as gmag_forced_kron
, forced.gmag_kron_err    as gmag_forced_kron_err
, forced.rmag_kron  	      as rmag_forced_kron
, forced.rmag_kron_err    as rmag_forced_kron_err
, forced.imag_kron        as imag_forced_kron
, forced.imag_kron_err    as imag_forced_kron_err
, forced.zmag_kron        as zmag_forced_kron
, forced.zmag_kron_err    as zmag_forced_kron_err
, forced.ymag_kron        as ymag_forced_kron
, forced.ymag_kron_err    as ymag_forced_kron_err

-- forced CModel magnitudes
, forced.gcmodel_mag        as gmag_forced_cmodel
, forced.gcmodel_mag_err    as gmag_forced_cmodel_err
, forced.rcmodel_mag        as rmag_forced_cmodel
, forced.rcmodel_mag_err    as rmag_forced_cmodel_err
, forced.icmodel_mag        as imag_forced_cmodel
, forced.icmodel_mag_err    as imag_forced_cmodel_err
, forced.zcmodel_mag        as zmag_forced_cmodel
, forced.zcmodel_mag_err    as zmag_forced_cmodel_err
, forced.ycmodel_mag        as ymag_forced_cmodel
, forced.ycmodel_mag_err    as ymag_forced_cmodel_err

-- shapes
, meas2.ishape_hsm_regauss_e1
, meas2.ishape_hsm_regauss_e2
, meas2.ishape_hsm_regauss_sigma
, meas2.ishape_hsm_regauss_resolution
, meas2.ishape_sdss_11                  as ishape_sdss_ixx
, meas2.ishape_sdss_22                  as ishape_sdss_iyy
, meas2.ishape_sdss_12                  as ishape_sdss_ixy
, meas2.ishape_sdss_psf_11              as ishape_sdss_psf_ixx
, meas2.ishape_sdss_psf_22              as ishape_sdss_psf_iyy
, meas2.ishape_sdss_psf_12              as ishape_sdss_psf_ixy

-- columns which can be used for selection
, meas.tract

-- columns which can be used to be more conservative
, meas.iflags_negative
, meas.ideblend_too_many_peaks
, meas.ideblend_parent_too_big
, meas2.icentroid_naive_flags
, meas.iflags_pixel_interpolated_any
, meas.iflags_pixel_saturated_any
, meas.iflags_pixel_cr_any
, meas.iflags_pixel_suspect_any

FROM
s16a_udeep.meas as meas
LEFT JOIN s16a_udeep.forced as forced using (object_id)
LEFT JOIN s16a_udeep.meas2 as meas2 using (object_id)

WHERE
-- Please uncomment to get a field you want

-- UDEEP (COSMOS)	g,r,i,z,y,NB0816,NB0921	
-- tractSearch(meas.patch_id,9570,9571) OR
-- tractSearch(meas.patch_id,9812,9814) OR
-- tractSearch(meas.patch_id,10054,10055) AND

-- UDEEP (SXDS)	g,r,i,z,y,NB0816,NB0921	
 tractSearch(meas.patch_id,8523,8524) OR
 tractSearch(meas.patch_id,8765,8766) AND

 NOT meas.ideblend_skipped                  AND
 NOT meas.iflags_badcentroid                AND
 NOT meas2.icentroid_sdss_flags          AND
 NOT meas.iflags_pixel_edge                 AND
 NOT meas.iflags_pixel_interpolated_center  AND
 NOT meas.iflags_pixel_saturated_center     AND
 NOT meas.iflags_pixel_cr_center            AND
 NOT meas.iflags_pixel_bad                  AND
 NOT meas.iflags_pixel_suspect_center       AND
 NOT meas.iflags_pixel_clipped_any          AND
 meas.idetect_is_primary             	        AND		   
 NOT meas2.ishape_hsm_regauss_flags    	    AND
 meas.iclassification_extendedness != 0     AND
 -- In postgres, all numbers are comparable including NaN
 meas2.ishape_hsm_regauss_sigma != 'NaN'

 ORDER BY meas.object_id
;
