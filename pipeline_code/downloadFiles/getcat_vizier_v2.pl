#!/usr/bin/perl

use strict;
use Getopt::Long;

my($i, $j, $k) = 0;

my $vizier_conf = "";
my $ra = 0.0;
my $dec = 0.0;
my $ssize = 1.0;     # arcmin
#my $bin = "http://vizier.u-strasbg.fr/viz-bin/asu-tsv?";
#my $bin = "http://vizier.cfa.harvard.edu/viz-bin/asu-tsv?";
my $bin = "http://vizier.nao.ac.jp/viz-bin/asu-tsv?";
my $tsvfile = "/tmp/tsv.txt";
my $filepatt = "";
my $filename = "";

my $help = 0;
my $re = GetOptions("i=s"    => \$vizier_conf,
		    "f=s"    => \$filename,
		    "ra=f"   => \$ra,
		    "dec=f"  => \$dec,
		    "s=f"    => \$ssize,
		    "name=s" => \$filepatt,
		    "bin=s"  => \$bin,
		    "help"   => \$help,
		    "h"      => \$help);
if ($help == 1) {
    print STDERR "\n";
    print "Usage :\n";
    print "   -i     a vizier config file (e.g. error output from vizier)\n";
    print "          (use comma to separate two or more config files)\n";
    print "   -ra    R.A. in deg\n";
    print "   -dec   Dec. in deg\n";
    print "   -s     searching size in arcmin\n";
    print "         (N.B. the shape of region is defined in the config file)\n";
    print "   -name  keyword of output filename\n";
    print "   -bin   if you would like to change the downloading site\n";
    print "         (default site is CfA)\n";
    print "\n";
    print "\n";
    exit;
}

if ($vizier_conf eq "") {
    die "please give me a config file\n";
}

my $query;
my @args = ("-4",
	    "-from=-3",
	    "-out.max=unlimited",
	    "-out.form=Tab-Separated-Values",
	    "-order=I");
if ($filename ne "") {
    push @args, "-list -file=".$filename;
    $filepatt = $filename;
} else {
    push @args, sprintf("-c=%.6f%+.6f", $ra, $dec);
if ($filepatt eq "") {
    $filepatt = sprintf("%010.6f%+010.6f", $ra, $dec);
}
}
push @args, sprintf("-c.r=%.2f", $ssize);



my @conffiles = split(/\,/, $vizier_conf);
foreach my $cfg (@conffiles) {
    my @cfgpath = split(/\//, $cfg);
    my @patcfg = split(/\./, $cfgpath[@cfgpath - 1]);

    print STDERR "$patcfg[0]\n";

    if (!-e $cfg) {
	print STDERR "config file \"$cfg\" does NOT exist\n";
	next;
    }

    my @a = @args;
    open(CONF, "$cfg");
    while(<CONF>) {
	chomp;

	$_ =~ s/^\s+//;
	$_ =~ s/\s+$//;

	push @a, $_;
    }
    $query = join("\&", @a);

    system("wget --connect-timeout=300 --read-timeout=300 \"$bin$query\" --output-document=$tsvfile >& /dev/null");

    my $output = uc($patcfg[0]) ."_". $filepatt ."_vizier.cat";

    fill999($tsvfile, $output);
}

exit;

##############################################################################
# we fill out the empty columns with 999999999999
sub fill999 {
    my($tsvfile, $outputfile) = @_;
    my @filled999 = ();
    my $ncol = 0;
    open(TXT, ">$outputfile");
    open(TSV, "$tsvfile");
    while(<TSV>) {
	chomp;

	if ($_ =~ m/^#Column/) {
	    my ($junk1, $junk2, $format, @junks) = split(/\s+/, $_);

	    $format =~ s/\(//;
	    $format =~ s/\)//;
	    my $str999 = "";
	    if ($format =~ m/^F/) {
		my($d1,$d0) = split(/\./, substr($format, 1));
		$d1 = $d1 - $d0 - 1;
		for ($i = 0; $i < $d1; $i++) {
		    $str999 = $str999 ."9";
		}
		$str999 = $str999 . ".";
		for ($i = 0; $i < $d0; $i++) {
		    $str999 = $str999 ."9";
		}
	    } elsif ($format =~ m/^I/) {
		my $d1 = int(substr($format, 1));
		for ($i = 0; $i < $d1; $i++) {
		    $str999 = $str999 ."9";
		}
	    } elsif ($format =~ m/^[a|A]/) {
		my $d1 = int(substr($format, 1));
		for ($i = 0; $i < $d1; $i++) {
		    $str999 = $str999 ."X";
		}
	    } else {
		die "ERROR: invalid string format on $format($_)\n";
	    }
	    push @filled999, $str999;
	    print(TXT "$_\n");

	    $ncol++;
	    next;
	}
	print(TXT "$_\n"),next if ($_ =~ m/^\#/ || $_ eq "");
	print(TXT "#$_\n"),next if ($_ =~ m/^\_r/);
	print(TXT "#$_\n"),next if ($_ =~ m/^arcsec/);
	print(TXT "#$_\n"),next if ($_ =~ m/^arcmin/);
	print(TXT "#$_\n"),next if ($_ =~ m/^deg/);
	print(TXT "#$_\n"),next if ($_ =~ m/^---/);

	my @cols = split(/\t/, $_);
	for ($i = 0; $i < $ncol; $i++) {
	    if ($cols[$i] =~ m/^\s+$/ || $cols[$i] eq "") {
		$cols[$i] = $filled999[$i];
	    }
	}
	print TXT join("  ", @cols)."\n";
    }
    close(TSV);
    close(TXT);
}


exit;
