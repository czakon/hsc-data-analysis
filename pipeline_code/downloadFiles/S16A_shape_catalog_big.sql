-- This has all of the original fields from H. Miyatake's SQL procedure on the S15 data.
-- This ended up being too large and I simplified this is a bit in the subsequent files.
SELECT
  meas.object_id
, meas.parent_id
, meas.ira
, meas.idec

, meas.iflux_kron
, meas.iflux_kron_err
, meas.iflux_kron_flags
, meas.imag_kron
, meas.imag_kron_err

, meas.icmodel_mag
, meas.icmodel_mag_err
, meas.icmodel_flux_flags
, meas.icmodel_flux
, meas.icmodel_flux_err

-- forced measurement
, forced.merge_measurement_i
, forced.a_g
, forced.a_r
, forced.a_i
, forced.a_z
, forced.a_y

-- forced Kron magnitudes
, forced.gmag_kron        as gmag_forced_kron
, forced.gmag_kron_err    as gmag_forced_kron_err
, forced.gflux_kron_flags as gflux_forced_kron_flags
, forced.rmag_kron  	    as rmag_forced_kron
, forced.rmag_kron_err    as rmag_forced_kron_err
, forced.rflux_kron_flags as rflux_forced_kron_flags
, forced.imag_kron        as imag_forced_kron
, forced.imag_kron_err    as imag_forced_kron_err
, forced.iflux_kron_flags as iflux_forced_kron_flags
, forced.zmag_kron        as zmag_forced_kron
, forced.zmag_kron_err    as zmag_forced_kron_err
, forced.zflux_kron_flags as zflux_forced_kron_flags
, forced.ymag_kron        as ymag_forced_kron
, forced.ymag_kron_err    as ymag_forced_kron_err
, forced.yflux_kron_flags as yflux_forced_kron_flags

-- forced CModel magnitudes
, forced.gcmodel_mag        as gmag_forced_cmodel
, forced.gcmodel_mag_err    as gmag_forced_cmodel_err
, forced.gcmodel_flux_flags as gflux_forced_cmodel_flags
, forced.rcmodel_mag        as rmag_forced_cmodel
, forced.rcmodel_mag_err    as rmag_forced_cmodel_err
, forced.rcmodel_flux_flags as rflux_forced_cmodel_flags
, forced.icmodel_mag        as imag_forced_cmodel
, forced.icmodel_mag_err    as imag_forced_cmodel_err
, forced.icmodel_flux_flags as iflux_forced_cmodel_flags
, forced.zcmodel_mag        as zmag_forced_cmodel
, forced.zcmodel_mag_err    as zmag_forced_cmodel_err
, forced.zcmodel_flux_flags as zflux_forced_cmodel_flags
, forced.ycmodel_mag        as ymag_forced_cmodel
, forced.ycmodel_mag_err    as ymag_forced_cmodel_err
, forced.ycmodel_flux_flags as yflux_forced_cmodel_flags

-- shapes
, meas2.ishape_hsm_regauss_e1
, meas2.ishape_hsm_regauss_e2
, meas2.ishape_hsm_regauss_sigma
, meas2.ishape_hsm_regauss_resolution
, meas2.ishape_sdss_11                  as ishape_sdss_ixx
, meas2.ishape_sdss_22                  as ishape_sdss_iyy
, meas2.ishape_sdss_12                  as ishape_sdss_ixy
, meas2.ishape_sdss_psf_11              as ishape_sdss_psf_ixx
, meas2.ishape_sdss_psf_22              as ishape_sdss_psf_iyy
, meas2.ishape_sdss_psf_12              as ishape_sdss_psf_ixy

-- columns which can be used for selection
, meas.tract
, meas.merge_peak_g
, meas.merge_peak_r
, meas.merge_peak_i
, meas.merge_peak_z
, meas.merge_peak_y
, meas.icountInputs
, meas.ideblend_has_stray_flux
, meas.iflags_pixel_bright_object_center
, meas.iflags_pixel_bright_object_any
, meas.iblendedness_flags
, meas.iblendedness_flags_noCentroid
, meas.iblendedness_flags_noShape
, meas.iblendedness_old
, meas.iblendedness_raw_flux
, meas.iblendedness_raw_flux_child
, meas.iblendedness_raw_flux_parent
, meas.iblendedness_abs_flux
, meas.iblendedness_abs_flux_child
, meas.iblendedness_abs_flux_parent
, meas.iblendedness_raw_shape_child_11  as iblendedness_raw_shape_child_ixx
, meas.iblendedness_raw_shape_child_22  as iblendedness_raw_shape_child_iyy
, meas.iblendedness_raw_shape_child_12  as iblendedness_raw_shape_child_ixy
, meas.iblendedness_raw_shape_parent_11 as iblendedness_raw_shape_parent_ixx
, meas.iblendedness_raw_shape_parent_22 as iblendedness_raw_shape_parent_iyy
, meas.iblendedness_raw_shape_parent_12 as iblendedness_raw_shape_parent_ixy
, meas.iblendedness_abs_shape_child_11  as iblendedness_abs_shape_child_ixx
, meas.iblendedness_abs_shape_child_22  as iblendedness_abs_shape_child_iyy
, meas.iblendedness_abs_shape_child_12  as iblendedness_abs_shape_child_ixy
, meas.iblendedness_abs_shape_parent_11 as iblendedness_abs_shape_parent_ixx
, meas.iblendedness_abs_shape_parent_22 as iblendedness_abs_shape_parent_iyy
, meas.iblendedness_abs_shape_parent_12 as iblendedness_abs_shape_parent_ixy

-- columns which can be used to be more conservative
, meas.iflags_negative
, meas.ideblend_too_many_peaks
, meas.ideblend_parent_too_big
, meas2.icentroid_naive_flags
, meas.iflags_pixel_interpolated_any
, meas.iflags_pixel_saturated_any
, meas.iflags_pixel_cr_any
, meas.iflags_pixel_suspect_any

FROM
s16a_wide.meas as meas
LEFT JOIN s16a_wide.forced as forced using (object_id)
LEFT JOIN s16a_wide.meas2 as meas2 using (object_id)

WHERE
-- Please uncomment to get a field you want

-- UDEEP (COSMOS)	g,r,i,z,y,NB0816,NB0921	
-- tractSearch(meas.patch_id,9570,9571) OR
-- tractSearch(meas.patch_id,9812,9814) OR
-- tractSearch(meas.patch_id,10054,10055) AND

-- UDEEP (SXDS)	g,r,i,z,y,NB0816,NB0921	
-- tractSearch(meas.patch_id,8523,8524) OR
-- tractSearch(meas.patch_id,8765,8766) AND

-- DEEP (COSMOS) g,r,i,z,y,NB0921
-- tractSearch(meas.patch_id,9569,9572) OR
-- tractSearch(meas.patch_id,9812,9814) OR
-- tractSearch(meas.patch_id,10054,10056) AND
 
-- DEEP (XMM-LSS) g,r,i,z,y
-- tractSearch(meas.patch_id,8282,8284) OR
-- tractSearch(meas.patch_id,8523,8525) OR
-- tractSearch(meas.patch_id,8765,8767) AND

-- DEEP (DEEP2-3) g,r,i,z,y,NB0816,NB0921
-- tractSearch(meas.patch_id,9220,9221) OR
-- tractSearch(meas.patch_id,9462,9465) OR
-- tractSearch(meas.patch_id,9706,9708) AND

-- DEEP (ELAIS-N1) g,r,i,z,y,NB0816,NB0921
-- tractSearch(meas.patch_id,16984,16985) OR
-- tractSearch(meas.patch_id,17129,17131) OR
-- tractSearch(meas.patch_id,17270,17272) OR
-- tractSearch(meas.patch_id,17406,17407) AND

-- WIDE (WIDE01H) g,r
-- tractSearch(meas.patch_id,8995-8999) OR
-- tractSearch(meas.patch_id,9236-9242) OR
-- tractSearch(meas.patch_id,9479-9485) OR
-- tractSearch(meas.patch_id,9722-9728) OR
-- tractSearch(meas.patch_id,9965-9969) AND

-- WIDE (XMM) g,r,i,z,y
-- tractSearch(meas.patch_id,8278,8286)             OR
-- tractSearch(meas.patch_id,8519,8527)             OR
-- tractSearch(meas.patch_id,8761,8769)             OR
-- tractSearch(meas.patch_id,9003,9011)             OR
-- tractSearch(meas.patch_id,9245,9253)             OR
-- tractSearch(meas.patch_id,9489,9493)             OR
-- tractSearch(meas.patch_id,9732,9736)             AND

-- WIDE (GAMA09H) g,r,i,z,y
-- tractSearch(meas.patch_id,9070-9079) OR
-- tractSearch(meas.patch_id,9313-9322) OR
-- tractSearch(meas.patch_id,9556-9565) OR
-- tractSearch(meas.patch_id,9798-9808) OR
-- tractSearch(meas.patch_id,10041-10049) OR
-- tractSearch(meas.patch_id,10287-10291) AND

-- WIDE (WIDE12H) g,r,i,z,y
-- tractSearch(meas.patch_id,9102,9107)             OR
-- tractSearch(meas.patch_id,9344,9350)             OR
-- tractSearch(meas.patch_id,9587,9593)             OR
-- tractSearch(meas.patch_id,9830,9835)           AND

-- WIDE (GAMA15H) g,r,i,z,y
-- tractSearch(meas.patch_id,9125,9135)             OR
-- tractSearch(meas.patch_id,9368,9378)             OR
-- tractSearch(meas.patch_id,9611,9621)             OR
-- tractSearch(meas.patch_id,9854,9862)           AND

-- WIDE (HECTOMAP) g,r,i,z,y
-- tractSearch(meas.patch_id,15825-15834) OR
-- tractSearch(meas.patch_id,16003-16012) OR
-- tractSearch(meas.patch_id,16178-16186) AND

-- WIDE (VVDS) g,r,i,z,y
-- tractSearch(meas.patch_id,9206,9218)             OR
-- tractSearch(meas.patch_id,9448,9462)             OR
-- tractSearch(meas.patch_id,9691,9705)             OR
-- tractSearch(meas.patch_id,9933,9946)             OR
-- tractSearch(meas.patch_id,10176,10188)           AND

-- SPECIAL (AEGIS) g,r,i,z,y
-- tractSearch(meas.patch_id,16972-16973) OR
-- tractSearch(meas.patch_id,16821-16822) AND
 

 NOT meas.ideblend_skipped                  AND
 NOT meas.iflags_badcentroid                AND
 NOT meas2.icentroid_sdss_flags          AND
 NOT meas.iflags_pixel_edge                 AND
 NOT meas.iflags_pixel_interpolated_center  AND
 NOT meas.iflags_pixel_saturated_center     AND
 NOT meas.iflags_pixel_cr_center            AND
 NOT meas.iflags_pixel_bad                  AND
 NOT meas.iflags_pixel_suspect_center       AND
 NOT meas.iflags_pixel_clipped_any          AND
 meas.idetect_is_primary             	    AND
 NOT meas2.ishape_hsm_regauss_flags    	    AND
 meas.iclassification_extendedness != 0     AND
 -- In postgres, all numbers are comparable including NaN
 meas2.ishape_hsm_regauss_sigma != 'NaN'
 ORDER BY meas.object_id
;
