#!/usr/bin/env python
# Created by Nicole Gisela Czakon: 01/08/2016
# The purpose of this program is to download various catalogs predominantly from the Vizier database.
# USAGE:

from astroquery.vizier import Vizier
import astropy.units as u
import astropy.coordinates as coord
from pipeline_code.loadFiles import loadTractInfoFile
import pipeline_code.hscConfigurationFile as hscConfig
from code import interact
import time

allTractInfo = loadTractInfoFile.data(fieldName=hscConfig.fullField)

for tract,tractInfo in allTractInfo.items():

    CenterRA = tractInfo['Center']['RA']
    CenterDec = tractInfo['Center']['Dec']
    widthRA = tractInfo['Corner3']['RA'] - tractInfo['Corner1']['RA']
    heightDec = tractInfo['Corner3']['Dec'] - tractInfo['Corner1']['Dec']

    new_map_coord =  coord.SkyCoord(ra = float(CenterRA), \
                                    dec=float(CenterDec), \
                                    unit=(u.deg,u.deg),frame='fk5')#icrs)

    nomad_vizier_a = Vizier().query_region(new_map_coord, \
                                           width = widthRA*u.deg, height = heightDec*u.deg, \
                                           catalog=["I/297"])
    print(len(nomad_vizier_a))

    interact(local=locals())
    
