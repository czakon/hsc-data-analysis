SELECT
  meas.object_id
, meas.parent_id
, meas.ira
, meas.idec

, meas.iflux_kron
, meas.iflux_kron_err
, meas.iflux_kron_flags
, meas.imag_kron
, meas.imag_kron_err

-- Unforced cmodel is not available in meas table.
-- However, we can get it from mosaic_measphoto__deepcoadd and mosaic_measflag_i__deepcoadd.
, measphoto.mag_cmodel         as imag_cmodel
, measphoto.mag_cmodel_err     as imag_cmodel_err
, measflag_i.cmodel_flux_flags as iflux_cmodel_flags
, measphoto.flux_cmodel        as iflux_cmodel
, measphoto.flux_cmodel_err    as iflux_cmodel_err

-- forced measurement
, forced.merge_measurement_i
, forced.a_g
, forced.a_r
, forced.a_i
, forced.a_z
, forced.a_y
-- forced Kron magnitudes
, forced.gmag_kron        as gmag_forced_kron
, forced.gmag_kron_err    as gmag_forced_kron_err
, forced.gflux_kron_flags as gflux_forced_kron_flags
, forced.rmag_kron  	    as rmag_forced_kron
, forced.rmag_kron_err    as rmag_forced_kron_err
, forced.rflux_kron_flags as rflux_forced_kron_flags
, forced.imag_kron        as imag_forced_kron
, forced.imag_kron_err    as imag_forced_kron_err
, forced.iflux_kron_flags as iflux_forced_kron_flags
, forced.zmag_kron        as zmag_forced_kron
, forced.zmag_kron_err    as zmag_forced_kron_err
, forced.zflux_kron_flags as zflux_forced_kron_flags
, forced.ymag_kron        as ymag_forced_kron
, forced.ymag_kron_err    as ymag_forced_kron_err
, forced.yflux_kron_flags as yflux_forced_kron_flags
-- forced CModel magnitudes
, forced.gmag_cmodel        as gmag_forced_cmodel
, forced.gmag_cmodel_err    as gmag_forced_cmodel_err
, forced.gcmodel_flux_flags as gflux_forced_cmodel_flags
, forced.rmag_cmodel        as rmag_forced_cmodel
, forced.rmag_cmodel_err    as rmag_forced_cmodel_err
, forced.rcmodel_flux_flags as rflux_forced_cmodel_flags
, forced.imag_cmodel        as imag_forced_cmodel
, forced.imag_cmodel_err    as imag_forced_cmodel_err
, forced.icmodel_flux_flags as iflux_forced_cmodel_flags
, forced.zmag_cmodel        as zmag_forced_cmodel
, forced.zmag_cmodel_err    as zmag_forced_cmodel_err
, forced.zcmodel_flux_flags as zflux_forced_cmodel_flags
, forced.ymag_cmodel        as ymag_forced_cmodel
, forced.ymag_cmodel_err    as ymag_forced_cmodel_err
, forced.ycmodel_flux_flags as yflux_forced_cmodel_flags

-- shapes
, meas.ishape_hsm_regauss_e1
, meas.ishape_hsm_regauss_e2
, meas.ishape_hsm_regauss_sigma
, meas.ishape_hsm_regauss_resolution
, meas.ishape_sdss[1]                  as ishape_sdss_ixx
, meas.ishape_sdss[2]                  as ishape_sdss_iyy
, meas.ishape_sdss[3]                  as ishape_sdss_ixy
, meas.ishape_sdss_psf[1]              as ishape_sdss_psf_ixx
, meas.ishape_sdss_psf[2]              as ishape_sdss_psf_iyy
, meas.ishape_sdss_psf[3]              as ishape_sdss_psf_ixy

-- columns which can be used for selection
, meas.tract
, meas.merge_peak_g
, meas.merge_peak_r
, meas.merge_peak_i
, meas.merge_peak_z
, meas.merge_peak_y
, meas.icountInputs
, meas.ideblend_has_stray_flux
, meas.iflags_pixel_bright_object_center
, meas.iflags_pixel_bright_object_any
, meas.iblendedness_flags
, meas.iblendedness_flags_noCentroid
, meas.iblendedness_flags_noShape
, meas.iblendedness_old
, meas.iblendedness_raw_flux
, meas.iblendedness_raw_flux_child
, meas.iblendedness_raw_flux_parent
, meas.iblendedness_abs_flux
, meas.iblendedness_abs_flux_child
, meas.iblendedness_abs_flux_parent
, meas.iblendedness_raw_shape_child[1]  as iblendedness_raw_shape_child_ixx
, meas.iblendedness_raw_shape_child[2]  as iblendedness_raw_shape_child_iyy
, meas.iblendedness_raw_shape_child[3]  as iblendedness_raw_shape_child_ixy
, meas.iblendedness_raw_shape_parent[1] as iblendedness_raw_shape_parent_ixx
, meas.iblendedness_raw_shape_parent[2] as iblendedness_raw_shape_parent_iyy
, meas.iblendedness_raw_shape_parent[3] as iblendedness_raw_shape_parent_ixy
, meas.iblendedness_abs_shape_child[1]  as iblendedness_abs_shape_child_ixx
, meas.iblendedness_abs_shape_child[2]  as iblendedness_abs_shape_child_iyy
, meas.iblendedness_abs_shape_child[3]  as iblendedness_abs_shape_child_ixy
, meas.iblendedness_abs_shape_parent[1] as iblendedness_abs_shape_parent_ixx
, meas.iblendedness_abs_shape_parent[2] as iblendedness_abs_shape_parent_iyy
, meas.iblendedness_abs_shape_parent[3] as iblendedness_abs_shape_parent_ixy

-- columns which can be used to be more conservative
, meas.iflags_negative
, meas.ideblend_too_many_peaks
, meas.ideblend_parent_too_big
, meas.icentroid_naive_flags
, meas.iflags_pixel_interpolated_any
, meas.iflags_pixel_saturated_any
, meas.iflags_pixel_cr_any
, meas.iflags_pixel_suspect_any

FROM
s15b_wide.meas as meas
LEFT JOIN s15b_wide.forced as forced using (object_id)
LEFT JOIN s15b_wide.mosaic_measphoto__deepcoadd as measphoto on (meas.object_id = measphoto.id)
LEFT JOIN s15b_wide.mosaic_measflag_i__deepcoadd as measflag_i on (meas.object_id = measflag_i.id)
WHERE
-- Please uncomment to get a field you want

-- AEGIS
-- s15b_search_aegis(meas.patch_id)           AND

-- HECTOMAP
-- s15b_search_hectomap(meas.patch_id)        AND

-- GAMA09H
-- s15b_search_gama09h(meas.patch_id)         AND

-- WIDE12H
-- s15b_search_wide12h(meas.patch_id)         AND

-- GAMA15H
-- s15b_search_gama15h(meas.patch_id)         AND

-- VVDS
-- s15b_search_vvds(meas.patch_id)            AND

-- XMM
 s15b_search_xmm(meas.patch_id)             AND

 NOT meas.ideblend_skipped                  AND
 NOT meas.iflags_badcentroid                AND
 NOT meas.icentroid_sdss_flags          AND
 NOT meas.iflags_pixel_edge                 AND
 NOT meas.iflags_pixel_interpolated_center  AND
 NOT meas.iflags_pixel_saturated_center     AND
 NOT meas.iflags_pixel_cr_center            AND
 NOT meas.iflags_pixel_bad                  AND
 NOT meas.iflags_pixel_suspect_center       AND
 NOT meas.iflags_pixel_clipped_any          AND
 meas.idetect_is_primary             	    AND
 NOT meas.ishape_hsm_regauss_flags    	    AND
 meas.iclassification_extendedness != 0     AND
 -- In postgres, all numbers are comparable including NaN
 meas.ishape_hsm_regauss_sigma != 'NaN'    AND
 measphoto.filter01 = 'HSC-I'
 ORDER BY meas.object_id
;
