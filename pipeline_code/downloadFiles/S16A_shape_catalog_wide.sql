SELECT
  meas.object_id
, meas.parent_id
, meas.ira
, meas.idec

, meas.imag_kron
, meas.imag_kron_err

, meas.icmodel_mag
, meas.icmodel_mag_err

-- forced Kron magnitudes
, forced.gmag_kron        as gmag_forced_kron
, forced.gmag_kron_err    as gmag_forced_kron_err
, forced.rmag_kron  	      as rmag_forced_kron
, forced.rmag_kron_err    as rmag_forced_kron_err
, forced.imag_kron        as imag_forced_kron
, forced.imag_kron_err    as imag_forced_kron_err
, forced.zmag_kron        as zmag_forced_kron
, forced.zmag_kron_err    as zmag_forced_kron_err
, forced.ymag_kron        as ymag_forced_kron
, forced.ymag_kron_err    as ymag_forced_kron_err

-- forced CModel magnitudes
, forced.gcmodel_mag        as gmag_forced_cmodel
, forced.gcmodel_mag_err    as gmag_forced_cmodel_err
, forced.rcmodel_mag        as rmag_forced_cmodel
, forced.rcmodel_mag_err    as rmag_forced_cmodel_err
, forced.icmodel_mag        as imag_forced_cmodel
, forced.icmodel_mag_err    as imag_forced_cmodel_err
, forced.zcmodel_mag        as zmag_forced_cmodel
, forced.zcmodel_mag_err    as zmag_forced_cmodel_err
, forced.ycmodel_mag        as ymag_forced_cmodel
, forced.ycmodel_mag_err    as ymag_forced_cmodel_err

-- shapes
, meas2.ishape_hsm_regauss_e1
, meas2.ishape_hsm_regauss_e2
, meas2.ishape_hsm_regauss_sigma
, meas2.ishape_hsm_regauss_resolution
, meas2.ishape_sdss_11                  as ishape_sdss_ixx
, meas2.ishape_sdss_22                  as ishape_sdss_iyy
, meas2.ishape_sdss_12                  as ishape_sdss_ixy
, meas2.ishape_sdss_psf_11              as ishape_sdss_psf_ixx
, meas2.ishape_sdss_psf_22              as ishape_sdss_psf_iyy
, meas2.ishape_sdss_psf_12              as ishape_sdss_psf_ixy

-- columns which can be used for selection
, meas.tract

-- columns which can be used to be more conservative
, meas.iflags_negative
, meas.ideblend_too_many_peaks
, meas.ideblend_parent_too_big
, meas2.icentroid_naive_flags
, meas.iflags_pixel_interpolated_any
, meas.iflags_pixel_saturated_any
, meas.iflags_pixel_cr_any
, meas.iflags_pixel_suspect_any

FROM
s16a_wide.meas as meas
LEFT JOIN s16a_wide.forced as forced using (object_id)
LEFT JOIN s16a_wide.meas2 as meas2 using (object_id)

WHERE

-- Please uncomment to get a field you want

-- WIDE (WIDE01H) g,r
-- tractSearch(meas.patch_id,8995-8999) OR
-- tractSearch(meas.patch_id,9236-9242) OR
-- tractSearch(meas.patch_id,9479-9485) OR
-- tractSearch(meas.patch_id,9722-9728) OR
-- tractSearch(meas.patch_id,9965-9969) AND

-- WIDE (XMM) g,r,i,z,y
-- tractSearch(meas.patch_id,8278,8286)             OR
-- tractSearch(meas.patch_id,8519,8527)             OR
-- tractSearch(meas.patch_id,8761,8769)             OR
-- tractSearch(meas.patch_id,9003,9011)             OR
-- tractSearch(meas.patch_id,9245,9253)             OR
-- tractSearch(meas.patch_id,9489,9493)             OR
-- tractSearch(meas.patch_id,9732,9736)             AND

-- WIDE (GAMA09H) g,r,i,z,y
-- tractSearch(meas.patch_id,9070-9079) OR
-- tractSearch(meas.patch_id,9313-9322) OR
-- tractSearch(meas.patch_id,9556-9565) OR
-- tractSearch(meas.patch_id,9798-9808) OR
-- tractSearch(meas.patch_id,10041-10049) OR
-- tractSearch(meas.patch_id,10287-10291) AND

-- WIDE (WIDE12H) g,r,i,z,y
-- tractSearch(meas.patch_id,9102,9107)             OR
-- tractSearch(meas.patch_id,9344,9350)             OR
-- tractSearch(meas.patch_id,9587,9593)             OR
-- tractSearch(meas.patch_id,9830,9835)           AND

-- WIDE (GAMA15H) g,r,i,z,y
-- tractSearch(meas.patch_id,9125,9135)             OR
-- tractSearch(meas.patch_id,9368,9378)             OR
-- tractSearch(meas.patch_id,9611,9621)             OR
-- tractSearch(meas.patch_id,9854,9862)           AND

-- WIDE (HECTOMAP) g,r,i,z,y
-- tractSearch(meas.patch_id,15825-15834) OR
-- tractSearch(meas.patch_id,16003-16012) OR
-- tractSearch(meas.patch_id,16178-16186) AND

-- WIDE (VVDS) g,r,i,z,y
-- tractSearch(meas.patch_id,9206,9218)             OR
-- tractSearch(meas.patch_id,9448,9462)             OR
-- tractSearch(meas.patch_id,9691,9705)             OR
-- tractSearch(meas.patch_id,9933,9946)             OR
-- tractSearch(meas.patch_id,10176,10188)           AND

-- SPECIAL (AEGIS) g,r,i,z,y
-- tractSearch(meas.patch_id,16972-16973) OR
-- tractSearch(meas.patch_id,16821-16822) AND
 
 NOT meas.ideblend_skipped                  AND
 NOT meas.iflags_badcentroid                AND
 NOT meas2.icentroid_sdss_flags          AND
 NOT meas.iflags_pixel_edge                 AND
 NOT meas.iflags_pixel_interpolated_center  AND
 NOT meas.iflags_pixel_saturated_center     AND
 NOT meas.iflags_pixel_cr_center            AND
 NOT meas.iflags_pixel_bad                  AND
 NOT meas.iflags_pixel_suspect_center       AND
 NOT meas.iflags_pixel_clipped_any          AND
 meas.idetect_is_primary             	        AND		   
 NOT meas2.ishape_hsm_regauss_flags    	    AND
 meas.iclassification_extendedness != 0     AND
 -- In postgres, all numbers are comparable including NaN
 meas2.ishape_hsm_regauss_sigma != 'NaN'

 ORDER BY meas.object_id
;
