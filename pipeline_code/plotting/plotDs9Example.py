#!/usr/bin/env python
from code import interact
import hscConfigurationFile as hscConfig
########### Pipeline-Related Packages #####################################
import lsst.daf.persistence as dafPersist
import lsst.afw.display.ds9 as ds9

dataDir = hscConfig.runDir#os.environ['runDir']
#runDir = '/array/users/czakon/lustre/Subaru/SSP/rerun/yasuda/SSP3.8.5_20150725'
butler = dafPersist.Butler(dataDir)

#tract =  int(os.environ['test_tract'])
#patch =  int(os.environ['test_patch'])
#9813,
tract = 8524
patch = '5,2'
dataId = {'filter':'HSC-G','tract':tract,'patch':patch}
exposure = butler.get("deepCoadd",dataId,immediate=True)

coadd = butler.get('deepCoadd',dataId)
settings = {'scale':'zscale','zoom':'to fit','mask':'transparency 60'}
ds9.mtv(exposure,frame=1,title='My Data',settings=settings)
#for source in optional_catalogs[cat_x]:
#    ds9.dot(symbol,source.getX(),source.getY(),ctype=ds9_color,size=mag_radius,frame=1,silent=True)
interact(local=locals())
