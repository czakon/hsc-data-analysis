# Created by Nicole Czakon 09/22/2015
# The purpose of this program is to quickly make the characterizatin plots for
# a particular field.
#
# STEP 1: Open the SQL database.
# The purpose of this program is to read in all of the values created in
# "create_sources..."
#
# PROGRAMS:
#
# General Functions:
# scatterPlot
# densityPlot
# seeingPlot
# doHist
# plotTractsPatches
#
# Functions That Generate Output Image Files:
# plotRaDec - This allows you to plot the data in the fields and the patches. 
# characterizationPlots - not sure what this is currently doing.
#
# USAGE:
# from pipeline_code.plotting.makeCharacterizationPlots import MakePlotsTask
# MakePlotsTask().raDecPlots()
#
########### Pipeline-Related Packages #####################################
import lsst.daf.persistence as dafPersist
import lsst.afw.display.ds9 as ds9
import lsst.afw.coord as afwCoord
import lsst.afw.geom as afwGeom
import lsst.afw.image as afwImage

#### Standard Python Packages ####
from datetime import date
import numpy as np
from code import interact
import matplotlib
import matplotlib.patches as mpatches
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib.backends.backend_pdf import PdfPages
from scipy import ndimage
from scipy.interpolate import griddata,Rbf,interp2d
from scipy.stats import gaussian_kde,norm
import pickle
from copy import copy
import time
from astropy.coordinates import SkyCoord
from astropy import units as u
import sqlite3
### USER PACKAGES ###
import pipeline_code.hscConfigurationFile as hscConfig
from pipeline_code.loadFiles import loadTractInfoFile
from pipeline_code.loadFiles.loadBrightStarSql \
    import LoadBrightStarSqlTask as bsSql


__all__ = ["MakePlotsTask"]


class MakePlotsTask:

    _DefaultName = "makeCharacterizationPlots"

    def __init__(self,
                 plotRandoms=None,plotSources=None, \
                 plotNomad=None,plotTycho=None,plotUcac=None, \
                 plotPatch=None,do_sn=None, \
                 dbFilename=None,doPDF=None,final_plots=None,\
                 redo_downsample=None,fields=None):

        # If you're running the function plotRaDec, it will plot the
        # specific field or patch, with the option of three major components:
        # plotRandoms - Plot the randoms.
        # plotSources - Plot the HSC sources.
        #
        # And three different flavors of bright star masks.
        # plotNomad - Plot the bright-star mask.
        # plotTycho - Plots the tycho mask in addition to the nomad.
        # plotUcac - Plots the ucac mask in addition to the nomad. 
        # plotBsmReg - Not currently implemented.
        #              Loads the bright star mask from the .reg files
        #              (this is a sanity check for the final data product).
        #
        # plotPatch - This is useful if you are only interested in plotting the bright star masks for one patch.
        # dbFilename - this is the location for the database. It's a little bit weird that it has to be inherited in so many different locations
        # do_sn - This is only important if you're running the characterization plots.
        #
        # doPdf - Default False.
        #        plot's the file to a pdf, this makes very large files and should only be performed if you're really desperate for resolution.
        #
        # final_plots - This has something to do with speeding up the plotting process when I'm debugging the code, however, I don't quite
        #        understand the implementation.
        #
        # redo_downsample - this is something that I don't quite understand because it interfaces with the pickle file
        #        consider getting rid of this in the future.
        
        if fields is None:
            self.fields = hscConfig.fields.keys()
        else:
            self.fields = fields
            
        if dbFilename is not None:
            print('\ndbFilename is set to: ' + dbFilename)
            self.dbFilename = dbFilename

        self.pixScale = hscConfig.pixScale#arcsec/pixel

        bsSql.downsample = 100

            #Right now, only plot the files that we take from the reg file.
            # I believe that this code is obsolete, consider getting rid of it.
            #if plotBsmReg is None:
            #
            #    self.plotBsmUcac = True
            #    #self.plotBsmReg = False
            #    #self.plotBsmNomad = True
            #else:
            #
            #    self.plotBsmUcac = False
            #    #self.plotBsmReg = True
            #    #self.plotBsmNomad = False
            
        if plotRandoms is None:
            self.plotRandoms = False
        else:
            self.plotRandoms = True

        if plotNomad:# is None:
            self.plotNomad = True#False
        else:
            self.plotNomad = False#True
            
        if plotTycho:# is None:
            self.plotTycho = True#False
        else:
            self.plotTycho = False#True

        if plotUcac:# is None:
            self.plotUcac = True#False
        else:
            self.plotUcac = False#True
        
        if plotSources is None:
            self.plotSources = False
            self.plotSourcesFromDR = False#True#
            self.plotSourcesPreDR = False
        else:
            self.plotSources = True
            self.plotSourcesFromDR = True#False
            self.plotSourcesPreDR = False#True

        #This is just to intialize the values to False.
        self.plotBsm_only = False
        self.plotRandoms_only = False
        self.plotSourcesPreDR_only = False
        self.plotSourcesFromDR_only = False

        #This is the actual logic that is involved
        if self.plotNomad or self.plotTycho or self.plotUcac:
            self.plotBsm = True
        else:
            self.plotBsm = False
            
        if self.plotBsm and not self.plotRandoms and not self.plotSourcesPreDR and not self.plotSourcesFromDR:

            self.plotBsm_only = True

        if not self.plotBsm and self.plotRandoms and not self.plotSourcesPreDR and not self.plotSourcesFromDR:

            self.plotRandoms_only = True

        if not self.plotBsm and not self.plotRandoms and self.plotSourcesPreDR and not self.plotSourcesFromDR:

            self.plotSourcesPreDR_only = True

        if not self.plotBsm and not self.plotRandoms and not self.plotSourcesPreDR and self.plotSourcesFromDR:

            self.plotSourcesFromDR_only = True
            
        self.nomadColor = 'blue'
        self.tychoColor = 'red'
        self.ucacColor = 'magenta'
        self.sourcesColor = 'yellow'#'red'
        self.randomsColor = 'red'
        self.archiveColor = self.sourcesColor

        self.nomadSymbol='o'
        self.sourcesSymbol = 'o'
        self.randomsSymbol = 'o'

        if plotPatch is None:
            
            self.plotPatch = False
            self.plotDs9 = False

        else:

            self.plotPatch = True
            self.plotDs9 = True
            
        #Keep this as a reminder in the future to help speed up reading the cluster database.
        if False:
            self.sql_pickle_file_downsample = hscConfig.characterizationSavDirectory + 'characterization_' + \
                                          str(bsSql.downsample) + '.p'
            
        todays_date = str(date.today())

        if doPDF==None:
            self.doPDF = False
        else:

            self.doPDF = True

        self.plot_filename_base = hscConfig.characterizationPlotDirectory + \
                                    'xmm_lss_sky_coverage_'+ str(bsSql.downsample)+ '_' + todays_date

        if final_plots:
            #This opens up the full version of the pickle file, since it takes a while, it is recommended to unselect this
            #when debugging the code.
            self.final_plots = True
            #10/27/2015 Will comment this out for the time being.
            if False:
                print('Starting to open the pickle file, this takes a while....')
                self.sql_pickle_file = hscConfig.characterizationSavDirectory + 'characterization.p'
                RA,Dec,shapeTemp = pickle.load(open(self.sql_pickle_file,"rb"))
                shapeTemp = np.array(shapeTemp)
                shapeTemp  = pixScale*shapeTemp
        else:
            self.final_plots = False


        if redo_downsample:
            self.redo_downsample = True
            #This probably still needs to be modified a bit.
            RA = bsSql.downsampleArray(RA,downsample)
            Dec = bsSql.downsampleArray(Dec,downsample)
        else:
            self.redo_downsample = False
            if False:
                print('Loading: '+self.sql_pickle_file_downsample)
                RA_new, Dec_new, shapeTemp_new, downsample = pickle.load(open(self.sql_pickle_file_downsample, 'rb'))
                print('Finished Loading: '+self.sql_pickle_file_downsample)

        if do_sn:
            self.do_sn = True
        else:
            self.do_sn = False            

    if __name__ == '__main__':
        print('main')
        interact(local=locals())

    def startImage(xlims, ylims):
        #This program sets up the default plotting parameters. 
        plt.clf()
        font = {'size' : 10}
        matplotlib.rc('font', **font)#'ytick',labelsize=10
        ax1 = plt.subplot2grid((6,5), (0,0), rowspan = 4, colspan = 5)
        plt.xlabel('RA (J2000) (deg)')
        plt.ylabel('Dec (J2000) (deg)')
        plt.xlim(xlims)
        plt.ylim(ylims)
        plt.axis('equal')

    def scatterPlot(xlims,ylims,entry, hist_in, name):
        s_size = 5.0
        hist_max = 1E4
        plt.scatter(entry[0:hist_max], hist_in[0:hist_max], edgecolor = 'None', alpha = 0.1, s = s_size)
        plt.xlabel(name)
        plt.ylabel(name + ' S/N')
        plt.xlim(xlims)
        plt.ylim(ylims)
        plt.yscale('log')

    def densityPlot():
        plt.colorbar(label = 'A.U.', format = '%2.1f')

    def seeingPlot():
        plt.colorbar(label = 'SDSS PSF Shape $r_{det}$\nFWHM')    

    def doHist(hist_in, plot_title, full_plot = False, SN_in = None):

        if full_plot:
            plt.clf()
            font = {'size' : 20}
            matplotlib.rc('font', **font)
        else:
            if SN_in == None:
                ax2 = plt.subplot2grid((6, 5),(5,0),rowspan=2,colspan=4)
            else:
                ax2 = plt.subplot2grid((6, 5),(5,0),rowspan=2,colspan=5)

        if SN_in == None:
            n_histbins = 1E2
            tempX = plt.hist(hist_in, bins = n_histbins)
        else:
            n_histbins = 1E4
            tempX = plt.hist(SN_in, bins = n_histbins)

        #Find the gaussian best-fit
        param = norm.fit(hist_in)
        #Define out the 3-sigma mark, or three-times the median
        if SN_in==None:
            hist_xlims = [0, param[0]+3*param[1]]
        else:
            hist_xlims = [1, 6*np.median(SN_in)]
            plt.xscale('log')
        hist_x = np.linspace(hist_xlims[0], hist_xlims[1], n_histbins)
        plt.plot(hist_x, norm.pdf(hist_x, loc = param[0], scale = param[1]), 'r--')
        plt.xlim(hist_xlims)
        plt.ylim([0, 1.25*np.max(tempX[0][1::])])
        plt.xlabel(plot_title)
        plt.ylabel('Counts')
        #Convert to scientific notation after a bit.
        plt.ticklabel_format(style = 'sci', axis = 'y', scilimits = (-3,3))
        if full_plot:
            #plt.subplots_adjust(left=0.1,right=0.9,top=0.9,bottom=0.1)
            plt.tight_layout()
        return
    
    def plotTractsPatches(self,field_key,field_stats):

        #Open the folder with all of the tract information.
        tractInfo = loadTractInfoFile.data(fieldName=field_key)
        for tract_key,tract_stats in tractInfo.items():
            square1 = plt.Rectangle((tract_stats['Corner1']['RA'],tract_stats['Corner1']['Dec']), \
                                    tract_stats['Corner3']['RA'] - tract_stats['Corner1']['RA'], \
                                    tract_stats['Corner3']['Dec'] - tract_stats['Corner1']['Dec'], \
                                    fill=False)
            patch_label_offset = -0.75
            plt.text(tract_stats['Center']['RA'],tract_stats['Corner3']['Dec']+patch_label_offset,tract_key,horizontalalignment='center', \
                     verticalalignment='bottom', \
                     size='xx-small')
            plt.gcf().gca().add_artist(square1)
            for patch_key,patch_stats in tract_stats.items():
                if ',' in  patch_key:
                    square1 = plt.Rectangle((patch_stats['Corner1']['RA'],patch_stats['Corner1']['Dec']), \
                                            patch_stats['Corner3']['RA'] - patch_stats['Corner1']['RA'], \
                                            patch_stats['Corner3']['Dec'] - patch_stats['Corner1']['Dec'], \
                                            fill=False,alpha=0.1,linewidth=0.1)

                    #plt.text(patch_stats['Center']['RA'],patch_stats['Corner3']['Dec']+patch_label_offset,tract_key,horizontalalignment='center', \
                    #         verticalalignment='bottom', size='xx-small')
                             
                    plt.gcf().gca().add_artist(square1)


        return
    
    def plotRaDec(self):

        print('\nIn it\'s current state, it takes approximately 50 minutes to run all of the fields.')
        time1 = time.time()

        todaysDate = str(date.today())

        if not self.doPDF:
            
            imageFilename = hscConfig.characterizationPlotDirectory + 'CharacterizationPlots_'+todaysDate
            imageFilename += '.png'
                    
        print('\nStarting to plot...')

        for field_name in self.fields:
            field_stats = hscConfig.fields[field_name]

            # Keep this code as an example of the type of fields that we can plot.
            #if field_name != 'W-XMM':
            #    continue
            
            addString = ''
            if self.doPDF == True:

                if self.plotUcac:
                    addString += '_Ucac'
                    
                self.plot_filename = hscConfig.characterizationPlotDirectory \
                                     + 'CharacterizationPlots_' \
                                     + field_name + '_' + todaysDate \
                                     + addString +'.pdf'
                
                print('\nPlotting to: ' + self.plot_filename + '\n')
                # Figure out how to plot different types of catalogs.
                self.pdf_plot = PdfPages(self.plot_filename)
            else:
                if self.plotUcac:
                    addString += '_Ucac'
                self.plot_filename = hscConfig.characterizationPlotDirectory \
                                     + 'CharacterizationPlots_' \
                                     + field_name + '_' + todaysDate \
                                     + addString +'.png'
                
                print('\nPlotting to: ' + self.plot_filename)
                
            if self.plotPatch:

                #Extract the nomad data.
                # 0 8524 8766 8767
                tract = ('9450',)
                patchA = '6'
                patchB = '6'
                patch0 = (patchA + ',' + patchB,)
                patch1 = (patchA + ', ' + patchB,)

                if self.plotBsm:

                    #raBsm,decBsm = bsSql().loadBsmSqlTractPatch(tract,patch)
                    raBsm,decBsm = bsSql().loadUcacSqlTractPatch(tract,patch0)

                if self.plotRandoms:
                    print('loading randoms...')
                    
                    #raRandoms,decRandoms = bsSql().loadRandomsSqlTractPatch(tract,patch0[0])
                    raRandoms,decRandoms = bsSql().loadRandomsSqlRaDec(field_stats['RA'][0],field_stats['RA'][1],\
                                                                       field_stats['Dec'][0],field_stats['Dec'][1])
                    print('finished loading randoms')

            #If self.plotFullArea
            else:

                minRA = field_stats['RA'][0]
                maxRA = field_stats['RA'][1]
                minDec = field_stats['Dec'][0]
                maxDec = field_stats['Dec'][1]
            
                if self.plotBsm:

                    if self.plotNomad:
                        raNomad, decNomad, bsrNomad, bnMag, vnMag, rnMag = bsSql().loadNomadSqlRaDec(minRA,maxRA,minDec,maxDec)
                        bnMag = []
                        vnMag = []
                        rnMag = []
                    if self.plotTycho:
                        raTycho, decTycho, bsrTycho, btMag, vtMag = bsSql().loadTychoSqlRaDec(minRA,maxRA,minDec,maxDec)
                        btMag = []
                        vtMag = []
                    if self.plotUcac:
                        idUcac,raUcac,decUcac,bsrUcac,buMag,vuMag,ruMag, minuMag = bsSql().loadUcacSqlRaDec(minRA,maxRA,minDec,maxDec)
                        buMag = []
                        vuMag = []
                        ruMag = []

                if self.plotRandoms:
                    #randomsDb
                    raRandoms,decRandoms = bsSql(randomsDb=self.dbFilename).loadRandomsSqlRaDec(minRA,maxRA,minDec,maxDec)

                    randomDS = 1

                    if randomDS > 1:

                        print('\nDownsampling the random arrays by a factor of ' + str(randomDS) + '...')
                        raRandoms = bsSql().downsampleArray(raRandoms,randomDS)
                        decRandoms = bsSql().downsampleArray(decRandoms,randomDS)
                    
            if self.plotDs9:

                #newRunDir = '/array/users/czakon/data/HSC/lustre/Subaru/SSP/rerun/hsc-1361/20151104/wide'
                #runDir = '/array/users/czakon/data/HSC/lustre/Subaru/SSP/rerun/yasuda/SSP3.8.5_20150725'#hscConfig.runDirrunDir#
                #runDir = '/array/users/czakon/lustre/Subaru/SSP/rerun/yasuda/SSP3.7.3_20150518_cosmos'#hscConfig.runDir
                dataDir = hscConfig.runDir
                butler = dafPersist.Butler(dataDir)
                newButler = dafPersist.Butler(dataDir)
            
                dataId = {'filter':'HSC-G','tract':int(tract[0]),'patch':patch0[0]}
                #newDataId = {'filter':'HSC-G','tract':int(tract[0]),'patch':patch0[0]}

                if True:
                    exposure = butler.get("deepCoadd_calexp",dataId,immediate=True)
                    coadd = butler.get('deepCoadd_calexp',dataId)
                else:
                    exposure = butler.get("deepCoadd",dataId,immediate=True)
                    coadd = butler.get('deepCoadd',dataId)
                
                wcs = exposure.getWcs()
                bbox = exposure.getBBox(afwImage.PARENT).getCorners()
                bboxIcrs = [wcs.pixelToSky(bbox[i].getX(),bbox[i].getY()).toIcrs() for i in range(4)]
                Decs  =[bboxIcrs[i].getDec().asDegrees() for i in range(4)]

                RAs = [bboxIcrs[i].getRa().asDegrees() for i in range(4)]
                settings = {'scale':'zscale','zoom':'to fit','mask':'transparency 100'}
                ds9.mtv(exposure,frame=1,title='My Data',settings=settings)
                mag_radius = 20#2.0*(-1.860*nomadmag[i]+60.00)#*afwGeom.arcminutes#From Anderson, 2012

                if self.plotNomad:
                    
                    for d_x in range(len(raNomad)):
                        tempCoord = wcs.skyToPixel(afwCoord.IcrsCoord(raNomad[d_x]*afwGeom.degrees, \
                                                                       decNomad[d_x]*afwGeom.degrees))
                        plot_x = tempCoord.getX() - exposure.getX0()
                        plot_y = tempCoord.getY() - exposure.getY0()
                        ds9.dot(nomadSymbol,plot_x,plot_y,ctype=nomadColor,size=mag_radius,frame=1,silent=True)

                if self.plotRandoms:

                    for d_x in range(len(raRandoms)):

                        tempCoord = wcs.skyToPixel(afwCoord.IcrsCoord(raRandoms[d_x]*afwGeom.degrees, \
                                                                       decRandoms[d_x]*afwGeom.degrees))
                        plot_x = tempCoord.getX() - exposure.getX0()
                        plot_y = tempCoord.getY() - exposure.getY0()
                        scale_radius = 2.0
                        ds9.dot(self.randomsSymbol,plot_x,plot_y,ctype=self.randomsColor,size=mag_radius/scale_radius,frame=1)#,silent=True)


                ### Get the sources from the exposure
                sources = newButler.get("deepCoadd_forced_src",dataId)#"deepCoadd_src"
                #sources = newButler.get("deepCoadd_forced_src",dataId)#"deepCoadd_src"
                if self.plotSourcesPreDR:
                    
                    for src in sources:

                        tempCoord = wcs.skyToPixel(afwCoord.IcrsCoord(src.getRa().asDegrees()*afwGeom.degrees, \
                                                                       src.getDec().asDegrees()*afwGeom.degrees))
                        plot_x = tempCoord.getX() - exposure.getX0()
                        plot_y = tempCoord.getY() - exposure.getY0()
                        
                        ds9.dot(self.sourcesSymbol,plot_x,plot_y,ctype=self.sourcesColor,size=mag_radius/6.0,frame=1,silent=True)

                plot_ssp = False

                if plot_ssp:
                    print('Plotting the sources extracted using the butler and local data')

                    for row in c.execute('''SELECT * FROM ssp WHERE (RA_J2000_degrees > ?) AND (RA_J2000_degrees < ?) AND 
                                           (Dec_J2000_degrees > ?) AND (Dec_J2000_degrees < ?)''',(RAs[1],RAs[0],Decs[0],Decs[3])):
                        tempCoord = wcs.skyToPixel(afwCoord.IcrsCoord(row[4]*afwGeom.degrees, \
                                                                   row[5]*afwGeom.degrees))
                        plot_x = tempCoord.getX() - exposure.getX0()
                        plot_y = tempCoord.getY() - exposure.getY0()
                        
                        ds9.dot(self.sourcesSymbol,plot_x,plot_y,ctype=self.sourcesColor,size=mag_radius/2.0,frame=1,silent=True)

                #src_sql_filename = hscConfig.alt_sql_source_db
                #conn = sqlite3.connect(src_sql_filename)
                #c = conn.cursor()

                # This counts all of the rows in the database downloaded from the data archive.
                #start here 12/28/2015
                if self.plotSourcesFromDR:

                    #Need to make sure that I get the patch here
                    print('Plotting the sources downloaded from the SQL database.')
                    sourceRa, sourceDec, MagCmodelSources = bsSql(verbose=True).loadSourcesSspDownload(\
                                                                             min(RAs),max(RAs),min(Decs),max(Decs))


                    #These also need to be inserted into an sql database.
                    
                    for tempX in range(len(sourceRa)):
                        tempCoord = wcs.skyToPixel(afwCoord.IcrsCoord(sourceRa[tempX]*afwGeom.degrees, \
                                                                      sourceDec[tempX]*afwGeom.degrees))
                        plot_x = tempCoord.getX() - exposure.getX0()
                        plot_y = tempCoord.getY() - exposure.getY0()

                        ds9.dot(self.sourcesSymbol,plot_x,plot_y,ctype=self.sourcesColor,size=mag_radius/4.0,frame=1,silent=True)

                #conn.close()
                print('Stop Plotting A.')
                interact(local=locals())

            if self.plotSourcesFromDR:

                src_sql_filename = hscConfig.altSqlSourceDb
                conn = sqlite3.connect(src_sql_filename)
                c = conn.cursor()
                RA_archive = []
                Dec_archive = []

                for row in c.execute('''SELECT * FROM xmm_lss'''):

                    RA_archive.append(row[1])
                    Dec_archive.append(row[2])

                downsample = 100
                print('Downsampling the archive data by a factor of ' + str(downsample) + '...')
                new_size = len(RA_archive) - len(RA_archive) % downsample
                # Downsample RA_archive
                RA_archive = np.array(RA_archive)
                RA_archive = RA_archive[0:new_size]
                RA_archive = RA_archive.reshape(new_size / downsample, downsample)
                RA_archive = RA_archive[:,0]

                # Downsample Dec_archive
                Dec_archive = np.array(Dec_archive)
                Dec_archive = Dec_archive[0:new_size]
                Dec_archive = Dec_archive.reshape(new_size / downsample, downsample)
                Dec_archive = Dec_archive[:,0]

                xlims = [max(RA_archive),min(RA_archive)]
                ylims = [min(Dec_archive),max(Dec_archive)]
                print('A: Make sure that you want to close the connection here')
                interact(local=locals())
                #conn.close()

            time_2 = time.time()
            time_tot = (time_2 - time1) / 60.
            print('This program has already been running for a total of ' + \
                  str(time_tot) + ' minutes, not done yet!')
            print('\nStarting to plot the full area...')

            if not self.plotPatch:

                #This program sets up the default plotting parameters. 
                plt.clf()
                font = {'size' : 20}
                matplotlib.rc('font', **font)#'ytick',labelsize=10
                #ax1 = plt.subplot2grid((6,5), (0,0), rowspan = 4, colspan = 5)
                #plt.figure(figsize=(20,10))
                plt.xlabel('RA (J2000) (deg)')
                plt.ylabel('Dec (J2000) (deg)')
                xlims = list(reversed(field_stats['RA']))
                ylims = field_stats['Dec']
                plt.xlim(xlims)
                plt.ylim(ylims)
                plt.axis('equal')
            
                s_size = 0.1
                title_string = field_name + ': '
                junk = self.plotTractsPatches(field_name,field_stats)

                if self.plotSourcesFromDR:
                    plt.scatter(RA_archive,Dec_archive, facecolor = archiveColor, \
                                edgecolor = 'None', alpha = 0.1, s = s_size)

                if self.plotSourcesFromDR_only:
                    plt.title(title_string + 'SSP sources from the data archive.')

                if self.plotRandoms:
                    scale_radius = 1E-1
                    plt.scatter(raRandoms,decRandoms, facecolor = self.randomsColor , \
                                edgecolor = 'None', alpha = 0.1, s = s_size*scale_radius)

                patchX0 = 0.91
                patchY0 = 0.95
                deltaX0 = 0.0
                deltaY0 = -0.08
                patchW = 0.05
                patchH = 0.05
                tSize = 15
                uAlpha = 0.2
                tAlpha = 0.4

                if self.plotNomad:

                    for tempX in range(len(raNomad)):

                        circleN = plt.Circle((raNomad[tempX],decNomad[tempX]),radius=bsrNomad[tempX],alpha=0.2, \
                                             edgecolor='none',facecolor=self.nomadColor,label='NOMAD')
                        plt.gcf().gca().add_artist(circleN)

                    plt.text(patchX0,patchY0,'Nomad', size = tSize,ha="center",va="center", \
                             bbox=dict(boxstyle="square",ec='None',fc=self.nomadColor,alpha=0.2), \
                             transform=plt.gca().transAxes,color='k')
                    patchX0 += deltaX0
                    patchY0 += deltaY0

                print('\nFinished plotting the Nomad circles....')
                time_2 = time.time()
                time_tot = (time_2 - time1) / 60.
                print('This program has been running for a total of ' + str(time_tot) + ' minutes.')

                if self.plotTycho:
                    for tempx in range(len(raTycho)):
                        tEdgeColor = 'k'
                        circleT = plt.Circle((raTycho[tempx],decTycho[tempx]),radius=bsrTycho[tempx],alpha=tAlpha, \
                                             edgecolor=tEdgeColor,facecolor=self.tychoColor,label='TYCHO')
                        plt.gcf().gca().add_artist(circleT)
                    plt.text(patchX0,patchY0,'Tycho', size = tSize,ha="center",va="center", \
                             bbox=dict(boxstyle="square",ec='None',fc=self.tychoColor,alpha=tAlpha), \
                             transform=plt.gca().transAxes,color=tEdgeColor)
                    #plt.gcf().gca().add_artist(mpatches.Rectangle((patchX0,patchY0),patchW,patchH,color=self.tychoColor,label='Tycho', \
                    #                                              transform=plt.gca().transAxes,alpha=0.2))
                    patchX0 += deltaX0
                    patchY0 += deltaY0

                print('Finished plotting the Tycho circles....')
                time_2 = time.time()
                time_tot = (time_2 - time1) / 60.
                print('This program has been running for a total of ' + str(time_tot) + ' minutes.')

                if self.plotUcac:
                    for tempx in range(len(raUcac)):
                        circleU = plt.Circle((raUcac[tempx],decUcac[tempx]),radius=bsrUcac[tempx], \
                                             alpha=uAlpha,edgecolor='none',facecolor=self.ucacColor,label='UCAC')
                        plt.gcf().gca().add_artist(circleU)
                    plt.text(patchX0,patchY0,'Ucac', size = tSize,ha="center",va="center", \
                             bbox=dict(boxstyle="square",ec='None',fc=self.ucacColor,alpha=uAlpha), \
                             transform=plt.gca().transAxes,color='k')
                    #plt.gcf().gca().add_artist(mpatches.Rectangle((patchX0,patchY0),patchW,patchH,color=self.ucacColor,label='Ucac', \
                    #                                              transform=plt.gca().transAxes,alpha=0.2))
                    patchX0 += deltaX0
                    patchY0 += deltaY0

                print('Finished plotting the Ucac circles....')
                time_2 = time.time()
                time_tot = (time_2 - time1) / 60.
                print('This program has been running for a total of ' + str(time_tot) + ' minutes.')

                if self.plotRandoms_only:
                    plt.title(title_string + 'Randoms inserted into the database.')

                if self.plotBsm_only:
                    plt.title(title_string + 'Bright-Star Mask')

                if self.plotSourcesPreDR:
                    plt.scatter(RA_ssp,Dec_ssp, facecolor = self.sourcesColor, edgecolor = 'None', alpha = 0.1, s = s_size)

                if self.plotSourcesPreDR_only:
                    plt.title(title_string + 'SSP sources inserted into the database.')

                ax1 = plt.gcf().gca()
                ax1.set_xlim(xlims)
                ax1.set_ylim(ylims)
                plt.gcf().tight_layout()#subplots_adjust(bottom=0.15)

                if self.doPDF:

                    self.pdf_plot.savefig()
                    self.pdf_plot.close()
                else:
                    plt.savefig(self.plot_filename,dpi=300)
                    print('\a')
                    print('Finished plotting to: ' + self.plot_filename)
                    time_2 = time.time()
                    time_tot = (time_2 - time1) / 60.
                    print('\nThis program has been running for a total of ' + str(time_tot) + ' minutes.')
        print('\nFinished plotting the RADec plots...')

    def compareCatMags(self):

        todaysDate = str(date.today())
        #The purpose of this program is to compare the overlap of various catalogs.
        print('Starting to compare the magnitudes in the various catalogs... '
              'This takes about 2-3 minutes.')
        
        dbFilename = hscConfig.sqlDirectory + hscConfig.brightStarDb
        
        if self.plotNomad and self.plotTycho and self.plotUcac:
            print('Error: Can only only have two catalogs set for this option, please check your input parameters and try again.')
            interact(local=locals())
            
        if not self.plotNomad and not self.plotTycho and not self.plotUcac:
            print('Error, none of the bright-star catalogs are set.')
            interact(local=locals())
            
        if self.plotNomad:
            primaryCat = 'nomad'
            if self.plotTycho:
                secondaryCat = 'tycho'
            else:
                secondaryCat = 'ucac'
        else:
            primaryCat = 'tycho'
            secondaryCat = 'ucac'

        for field,stat in hscConfig.fields.items():

            #This only needs to be performed for one field....
            if field != 'W-GAMA09H':
                continue

            #field = 'W-GAMA09H'#'W-XMM'#'W-HECTMAP'#
            minRA = stat['RA'][0]
            maxRA = stat['RA'][1]
            minDec = stat['Dec'][0]
            maxDec = stat['Dec'][1]
            print('\nField: ' + field)
            print('RA/Dec Limits:')
            print(stat)
            if self.plotNomad:
                raNomad, decNomad, bsrNomad, bnMag, vnMag, rnMag = \
                    bsSql().loadNomadSqlRaDec(minRA,maxRA,minDec,maxDec)
                nomadRADec = SkyCoord(ra=raNomad*u.degree,dec=decNomad*u.degree)

            if self.plotTycho:
                #The Tycho database does not have any Rmags.
                raTycho, decTycho, bsrTycho, btMag, vtMag = \
                    bsSql().loadTychoSqlRaDec(minRA,maxRA,minDec,maxDec)
                tychoRADec = SkyCoord(ra=raTycho*u.degree,dec=decTycho*u.degree)

            if self.plotUcac:
                idUcac,raUcac,decUcac,bsrUcac,buMag,vuMag,ruMag, minuMag = \
                    bsSql().loadUcacSqlRaDec(minRA,maxRA,minDec,maxDec)
                ucacRADec = SkyCoord(ra=raUcac*u.degree,dec=decUcac*u.degree)

            if primaryCat == 'nomad' and secondaryCat == 'tycho':

                idx,d2d,d3d = tychoRADec.match_to_catalog_sky(nomadRADec)
                goodx = np.where(d2d.degree < 0.001)
                idx = idx[goodx]
                d2d = d2d[goodx]
                bMag1 = bnMag[idx]
                bMag2 = btMag[goodx]
                vMag1 = vnMag[idx]
                vMag2 = vtMag[goodx]
                #rMag1 = rnMag[idx]
                #rMag2 = rtMag
                bsr1 = bsrNomad[idx]
                bsr2 = bsrTycho[goodx]
                    
            elif primaryCat == 'nomad' and secondaryCat == 'ucac':
                
                idx,d2d,d3d = ucacRADec.match_to_catalog_sky(nomadRADec)
                goodx = np.where(d2d.degree < 0.001)
                idx = idx[goodx]
                d2d = d2d[goodx]
                bMag1 = bnMag[idx]
                bMag2 = buMag[goodx]
                vMag1 = vnMag[idx]
                vMag2 = vuMag[goodx]
                rMag1 = rnMag[idx]
                rMag2 = ruMag[goodx]
                bsr1 = bsrNomad[idx]
                bsr2 = bsrUcac[goodx]
                    
            else:
                    
                idx,d2d,d3d = ucacRADec.match_to_catalog_sky(tychoRADec)
                goodx = np.where(d2d.degree < 0.001)
                idx = idx[goodx]
                d2d = d2d[goodx]
                bMag1 = btMag[idx]
                bMag2 = buMag[goodx]
                vMag1 = vtMag[idx]
                vMag2 = vuMag[goodx]
                #rMag1 = rtMag[idx]
                #rMag2 = ruMag
                bsr1 = bsrTycho[idx]
                bsr2 = bsrUcac[goodx]
                    
            # Note: Make a decision if you want to include this additional cut
            #       onto the catalog.
            #       This will make sure that the sources are matched better.
                
            plt.figure(1,figsize=(8,8))

            plot_filename = hscConfig.characterizationPlotDirectory \
                            + 'CharacterizationPlots_' \
                            + field + '_' + todaysDate + '_closest_' + \
                            secondaryCat+'_to_'+primaryCat+'_sources.pdf'
            
            print('\nPlotting to: ' + plot_filename + '\n')
            # Figure out how to plot different types of catalogs.
                                                                
            pdf_plot = PdfPages(plot_filename) 
            cbLabel = 'distance between matches'
            plt.subplot(2,2,1)
            plt.scatter(bMag2,bMag1,c=d2d*3600)
            plt.scatter(bMag2,bMag1,c=d2d*3600)
            plt.plot([0,20],[0,20])
            plt.xlim([0,20])
            plt.ylim([0,20])
            plt.ylabel(primaryCat + ' B')
            plt.title(field + ' '+secondaryCat+' BT')
            plt.colorbar(label=cbLabel)

            plt.subplot(2,2,2)
            plt.scatter(vMag2,vMag1,c=d2d*3600)
            plt.plot([0,20],[0,20])
            plt.xlim([0,20])
            plt.ylim([0,20])
            plt.title(secondaryCat + ' VT')
            plt.ylabel(primaryCat + ' V')
            plt.colorbar(label=cbLabel)
            #plt.colorbar()

            if primaryCat != 'tycho' and secondaryCat != 'tycho':
                plt.subplot(2,2,3)
                plt.scatter(vMag2,rMag1,c=d2d*3600)
                plt.plot([0,20],[0,20])
                plt.xlim([0,20])
                plt.ylim([0,20])
                plt.colorbar(label=cbLabel)
                plt.ylabel(secondaryCat + ' R')
                plt.title(primaryCat + ' VT')

            plt.subplot(2,2,4)
            plt.scatter(bsr2,bsr1,c=d2d*3600)
            plt.colorbar()
            plt.ylabel(primaryCat + ' BSR')
            plt.title(secondaryCat + ' BSR')
            plt.tight_layout()

            pdf_plot.savefig()
            pdf_plot.close()
            plt.close()
            #print('\nCheck to make sure that the plot was plotted '
            #      'appropriately....\n')
            #interact(local=locals())
        
            #Note: This compares the various Tycho magnitudes with each other.
            #Note: Some of this is obsolete code, it needs to be looked at and
            #      seen if it is still needed.
            if False:
                plt.figure(1,figsize=(8,8))
                plot_filename = hscConfig.characterizationPlotDirectory + \
                                'CharacterizationPlots_' + \
                                field + '_' + todaysDate + '_tycho_BV.pdf'
            
                print('\nPlotting to: ' + plot_filename + '\n')
                pdf_plot = PdfPages(plot_filename)
                # Figure out how to plot different types of catalogs.
                plt.scatter(btMag,vtMag)
                plt.xlabel('Tycho Btmag')
                plt.ylabel('Tycho Vtmag')
                #plt.hlines(11,1,1)
                plt.axhline(y=11,linestyle='--')
                plt.axvline(x=11,linestyle='--')
                plt.title('Tycho magnitude comparison')
                plt.tight_layout()
                pdf_plot.savefig()
                pdf_plot.close()

                plot_filename = hscConfig.characterizationPlotDirectory \
                                + 'CharacterizationPlots_' + field + '_' \
                                + todaysDate + '_'+primaryCat+'_' \
                                + secondaryCat+'.pdf'
            
                print('\nPlotting to: ' + plot_filename + '\n')
                # Figure out how to plot different types of catalogs.
                                                                
                pdf_plot = PdfPages(plot_filename) 

                idx,d2d,d3d = nomadRADec.match_to_catalog_sky(tychoRADec)

                if False:
                    goodx = np.where(d2d.degree > 0.001)
                    idx = idx[goodx]
                    d2d = d2d[goodx]
                    Bmag = Bmag[goodx]
                    Vmag = Vmag[goodx]
                    Rmag = Rmag[goodx]
                    nomadBsr = nomadBsr[goodx]
                                                                                
                plt.subplot(2,2,1)
                plt.scatter(btMag[idx],Bmag,c=d2d*3600)
                plt.colorbar()
                plt.ylabel('NOMAD B')
                plt.title(field + ' Tycho BT')
                 
                plt.subplot(2,2,2)
                plt.scatter(vtMag[idx],Vmag,c=d2d*3600)
                plt.colorbar()
                plt.ylabel('NOMAD V')
                plt.title(field + ' Tycho VT')
                                 
                plt.subplot(2,2,3)
                plt.scatter(vtMag[idx],Rmag,c=d2d*3600)
                plt.colorbar()
                plt.ylabel('NOMAD R')
                plt.title(field + ' Tycho VT')

                plt.subplot(2,2,4)
                plt.scatter(tychoBsr[idx],nomadBsr,c=d2d*3600)
                plt.colorbar()
                plt.ylabel('NOMAD BSR')
                plt.title(field + ' Tycho BSR')
                plt.tight_layout()
                pdf_plot.savefig()

                pdf_plot.close()
                plt.close()

        return
    ###############################################################
    ##################### MAIN PROGRAM ############################
    ###############################################################
    def characterizationPlots(self):

        # This plots all of the files that were output to the pickle.
        if self.do_sn:
            add_string = '_SN'
        else:
            add_string = ''

        # If this is not set, then each of the files will be output to a
        # different place.
        if self.doPDF:
            pdf_plot = PdfPages(plot_filename_base + add_string + '.pdf')
            
        if self.do_sn:
            xlims = [15,30]
            ylims = [1,1E5]#min(entry/err_entry),max(entry/err_entry)]
            startImage(xlims,ylims)
            scatterPlot(xlims,ylims,entry, entry/err_entry, entry_name)
            doHist(entry, entry_name + ' S/N', full_plot = False,
                   SN_in = entry/err_entry)
        else:
            doHist(entry, entry_name, full_plot = True)
            pdf_plot.savefig()
            pdf_plot.close()

        xy = np.vstack([RA_new, Dec_new])
        new_pixelization = 512j
        x_flat = np.r_[RA_new.min() : RA_new.max() : new_pixelization]
        y_flat = np.r_[Dec_new.min() : Dec_new.max() : new_pixelization]
        x,y = np.meshgrid(x_flat, y_flat)
        grid_coords = np.append(x.reshape(-1, 1), y.reshape(-1, 1), axis = 1)

        # Below is the density function.
        if downsample == 100:
            z_kde = gaussian_kde(xy, bw_method = 0.01)#(xy)#'scott'
        else:
            z_kde = gaussian_kde(xy, bw_method = 0.05)#(xy)#'scott'
        z2_kde = z_kde(grid_coords.T)

        #Remove all of the values that correspond to zero density function.
        z3_kde = copy(z2_kde)

        #This histogram below is used for characterization.
        if False:
            plt.hist(z3, bins = 20)
            plt.show()

        kde_x = z2_kde < 1E-2
        z3_kde[kde_x] = 0.0
        z3_kde = z3_kde.reshape(new_pixelization, new_pixelization)

        # We need to make randoms based on the seeinf
        # nearest')
        z_gd = griddata(xy.T, shapeTemp_new, grid_coords, method = 'cubic')
        z_gd[kde_x] = 0.0
        z_gd = z_gd.reshape(new_pixelization, new_pixelization)
        xlims = [RA_new.max(), RA_new.min()]
        ylims = [-6.2, -3.0]#[Dec_new.min(),Dec_new.max()]
        #fig,((ax1,ax2,ax3),(ax4,ax5,ax6)) = plt.subplots(2,3, \
            #sharex='col',sharey='row')
        #plt.figure(1,figsize=(20,20))
        #gs = GridSpec(12,8)#,bottom=0.1,left=0.1,right=0.1)
        if True:
            startImage(xlims, ylims)
            # PLOT 1A: Show the histogram results
            print('Making a 2d histogram of the distribution of sources...')
            #First, find where the density is zero in the image.
            vmin1 = 0.0
            vmax1 = 5.0
            nhist_bins = 200
            if final_plots:
                vmax1 *=downsample
                z_hist2d = plt.hist2d(RA, Dec, (nhist_bins, nhist_bins), \
                                  vmin = vmin1,vmax = vmax1)
            else:
                z_hist2d = plt.hist2d(RA_new, Dec_new,
                                      (nhist_bins, nhist_bins), \
                                      vmin = vmin1, vmax = vmax1)
            densityPlot()
            plt.title('XMM Deep\n(2d Histogram)')
            plt.gca().invert_xaxis()
            # PLOT 1B: Show the histogram
            doHist(z_hist2d[0].flatten(), 'Pixel Values')
            if self.doPDF:
                pdf_plot.savefig()
            else:
                plt.savefig(plot_filename_base + '_2dhist.png')
            #plt.axes('equal','datalim')

        if False:
            # PLOT 2 (KDE)
            plt.clf()
            print('Making the kde approximation of the seeing data.')
            #plt.subplot(322)
            #vmax1 = 0.5*downsample
            #I'm not sure where this estimate came from...
            plt.imshow(20 * downsample * z3_kde[::-1, ::-1], \
                       extent = [x_flat[-1], x_flat[0],
                                 y_flat[0], y_flat[-1]], \
                       vmin = vmin1,vmax = vmax1)
            #cbar_ax = plt.add_axes([0.85,0.15,0.05,0.7])
            densityPlot()
            plt.title('XMM Deep\n(KDE)')
            startImage(xlims, ylims)
            #plt.gca().invert_xaxis()
            #plt.gca().invert_yaxis()
            plt.axis('equal')
            #plt.axis('scaled')
            if self.doPDF:
                pdf_plot.savefig()
            else:
                plt.savefig(plot_filename_base + '_kde.png')

        if True:
            # PLOT 3: Finding a better way to interpolate the data
            print('Making the 2d interp')
            f_interp = interp2d(z_hist2d[1][0:-1], z_hist2d[2][0:-1], \
                                z_hist2d[0], kind = 'cubic')
            z_interp = f_interp(x_flat, y_flat)
            z_interp = z_interp.reshape(new_pixelization, new_pixelization)
            bad_z = z_interp < 0.0
            z_interp[bad_z] = 0.0
            #plt.subplot(323)
            #vmax1 = 5.0*downsample
            startImage(xlims, ylims)
            plt.title('XMM Deep\n(2d Spline Interp)')
            plt.imshow(z_interp[::-1,::-1].T, extent = [x_flat[-1], x_flat[0],
                                                        y_flat[0], y_flat[-1]],
                       vmin = vmin1,vmax = vmax1)
            densityPlot()
            doHist(z_interp.flatten(), 'Pixel Values')
            if self.doPDF:
                pdf_plot.savefig()
            else:
                plt.savefig(plot_filename_base + '_spline.png')
        vmax1 = 8.0 * pixScale

        if True:
            # PLOT 1: Show the distribution of these points.
            print('Making the scatter plot including the seeing as a '
                  'third dimension')
            #plt.subplot(324)
            #plt.scatter(RA_new,Dec_new,s=2,alpha=0.5,edgecolor='None')
            startImage(xlims, ylims)
            if final_plots:
                plt.scatter(RA, Dec, c = shapeTemp, s = 1, edgecolor = '', \
                            vmin = vmin1, vmax = vmax1)
            else:
                plt.scatter(RA_new, Dec_new, c = shapeTemp_new, s = 1,
                            edgecolor = '', vmin = vmin1, vmax = vmax1)
                            
            seeingPlot()
            plt.title('XMM Deep\n(Scatter plot)')
            #plt.gca().invert_xaxis()
            if final_plots:
                doHist(shapeTemp, 'Pixel Values')
            else:
                doHist(shapeTemp_new, 'Pixel Values')
            if self.doPDF:
                pdf_plot.savefig()
            else:
                plt.savefig(plot_filename_base + '_scatter.png')

        if True:
            # PLOT 4: Show the griddata results
            print('Making the grid data interpolation of the seeing data.')
            startImage(xlims, ylims)
            vmax1 = 500
            #350:Perhaps 300 a bit too high and 400 perhaps a bit too high.
            #exponential factor, 1.1 didn't do anything
            norm_factor = 350.
            exp_factor = 1.0
            plot_z = norm_factor*z_gd[::-1, ::-1] ** exp_factor
            plt.imshow(plot_z, \
                       extent = [x_flat[-1], x_flat[0],
                                 y_flat[0], y_flat[-1]], \
                       vmin = vmin1,vmax = vmax1)
            plt.colorbar(label = 'SDSS PSF Shape Scaled to Density (Grid Data)')
            plt.title('XMM Deep\n(GridData)')
            doHist(plot_z.flatten(), 'Pixel Values')
            if self.doPDF:
                pdf_plot.savefig()
            else:
                plt.savefig(plot_filename_base + '_GridData.png')

        if False:
            #This takes a little bit of a long time
            z_rbf = Rbf(RA_new, Dec_new, shapeTemp_new, function = 'linear')
            di = z_rbf(x, y)
            plt.imshow(di)

        if False:
            sx = ndimage.sobel(im, axis = 0, mode = 'mirror')
            sy = ndimage.sobel(im, axis = 1, mode = 'mirror')
            sob = np.hypot(sx, sy)
            startImage(xlims, ylims)
            #plt.subplot(131)
            plt.imshow(im)#,cmap=plt.cm.gray)
            #plt.subplot(132)
            plt.imshow(sx)
            #plt.subplot(133)
            plt.imshow(sob)
            plt.title('XMM Deep')
            plt.savefig(plot_filename)
'''
                #I got rid of this database because I didn't know how to calibrate the fluxes...
                if False:
                    
                    for row in c.execute('SELECT * FROM ssp'):
                        ##(RA_J2000_degrees > ?) AND (RA_J2000_degrees < ?) AND
                        #print(row)
                        # Only take those sources that are not masked.
                        if row[6]:
                            RA_ssp.append(row[4])
                            Dec_ssp.append(row[5])
            
                    downsample = 50
                    new_size = len(RA_ssp) - len(RA_ssp) % downsample
                    # Downsample RA_ssp
                    RA_ssp = np.array(RA_ssp)
                    RA_ssp = RA_ssp[0:new_size]
                    RA_ssp = RA_ssp.reshape(new_size / downsample, downsample)
                    RA_ssp = RA_ssp[:,0]

                    # Downsample Dec_ssp
                    Dec_ssp = np.array(Dec_ssp)
                    Dec_ssp = Dec_ssp[0:new_size]
                    Dec_ssp = Dec_ssp.reshape(new_size / downsample, downsample)
                    Dec_ssp = Dec_ssp[:,0]


'''
