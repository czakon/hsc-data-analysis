#!/usr/bin/env python
# Created by Nicole Czakon 10/05/2015
# The purpose of this script is to run the bright star mask pipeline from beginning to end
#
# USAGE:
# ./runBrightStarMaskPipeline.py
import argparse
from code import interact
#My self-made packages
from plotting import makeCharacterizationPlots
from convertToPickle import convertSQLToPickle
from pipeline_code.masking import newRetrieveBrightStarsFromNomad

if __name__ == '__main__':
    # The description below can be attained by calling. 
    #./runBrightStarMaskPipeline.py -h
    parser = argparse.ArgumentParser(description= 'The purpose of this script '+ \
                                     'is to run the bright star mask pipeline from beginning to end')
    # 1. Need to add an option to convert to SQL.
    # 2. Need to load from sql convert to Pickle (although I might get rid of the pickle version later.....)
    parser.add_argument('--sql',help='Load the data from the SQL file.',action='store_true',required=False)
    # This will run the characterization plots.
    parser.add_argument('--cp',help='Generate data characterization plots',action='store_true',required=False)
    args = parser.parse_args()

    # STEP 1 IMPORT THE DATA FOR THE BRIGHT STAR MASKS FROM VIZIER:
    #def __init__(self,overwrite=None,searchWidth=None,exposure=None,mapCoord=None, \
    #             test=None,insertSQL=None,overwriteSQL=None,overwriteReg=None,loadOnly=None,fields=None):
    brightStars = newRetrieveBrightStarsFromNomad.FindNomadTask(test=True,overwrite=True,fields=['D-S15b-DR-2'])
    #,insertSQL=True)#overwriteReg=True,
    print('\nDone with inserting the stars from nomad, please check the rest of the code before continuing to run the script.')
    interact(local=locals())
    # KEEP THE STUFF BELOW!
    bands = ['g','r','i','z','y']
    mag_models = ['kron','cmodel']
    test_entries = [band + 'mag_' + mag_model for band in bands for mag_model in mag_models]
    test_entries.append('shape_sdss_psf_r_det_fwhm')
    for test_entry in test_entries:
        if args.sql:
            junk = convertSQLToPickle.MakePickleTask(srcParam=test_entry)
        if args.cp:
            #(loadFromSQL=False,do_plot=None,do_pdf=False,final_plots=None,redo_downsample=None,do_sn=None):
            junk = makeCharacterizationPlots.MakePlotsTask(srcParam=test_entry)

# STEP 1: Create and SQL database from CSV files that we downloaded via STARS
#

# Open the SQL database.
# The purpose of this program is to read in all of the values created in "create_sources..."
# from astropy.io import ascii
#
# STEP X: Make the configuration plots


