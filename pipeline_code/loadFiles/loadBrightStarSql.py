# Created by Nicole Gisela Czakon: 12/28/2015
# The purpose of this program is to load the data for the bright star masks from the sql files.
# USAGE: from pipeline_code.loadFiles import loadBrightStarSql
# randoms = loadBrightStarSql.LoadBrightStarSqlTask().loadRandoms()
#import os,re
import pipeline_code.hscConfigurationFile as hscConfig
from astropy.io import fits
from code import interact
from copy import copy
import numpy as np
import sqlite3
import sys, math
bsr = hscConfig.brightStarRadius['medium']


__all__ = ["LoadBrightStarSqlTask"]

# Programs:
#
# loadTycho (this is the raw data)
# loadUcac (this is the raw data)
# loadRandomsFitsTractPatch
# downsampleArray
# sourcesInformation
# loadSourcesSqlRaDec
# loadNomadSqlTractPatch
# loadNomadSqlRaDec
# loadUcacSqlRaDec
# loadTycho
# loadTychoSqlRaDec
# loadBsmRegRaDec
# loadBsmRegTractPatch
# randomInformation
# loadRandomsSqlTractPatch
# loadRandomsSqlRaDec
#
class LoadBrightStarSqlTask:

    _DefaultName = "loadBrightStarSql"
        
    def __init__(self,brightStarDb=None,sourcesDb=None,randomsDb=None,verbose=None,band=None):

        if band is None:
            self.band = 'g'
        else:
            self.band = band
            
        #Step 1: Open the bright star database.
        if brightStarDb is None:
            brightStarDb = hscConfig.sqlDirectory + hscConfig.brightStarDb
        else:
            brightStarDb = hscConfig.sqlDirectory + brightStarDb

        #/asiaa/home/czakon/data/HSC/SqlDl
        self.connBrightStar = sqlite3.connect(brightStarDb)
        
        #Create a cursor object
        self.cBrightStar = self.connBrightStar.cursor()

        #Step 2: Open the SSP sources database.
        if sourcesDb is None:
            sourcesDb = hscConfig.sqlDirectory + hscConfig.sourcesDb[self.band]
        else:
            sourcesDb = hscConfig.sqlDirectory + sourcesDb

        #/asiaa/home/czakon/data/HSC/SqlDl
        self.connSources = sqlite3.connect(sourcesDb)
        
        #Create a cursor object
        self.cSources = self.connSources.cursor()

        #Step 3: Open the SSP sources database.
        if randomsDb is None:
            randomsDb = hscConfig.sqlDirectory + hscConfig.randomsDb
        else:
            randomsDb = hscConfig.sqlDirectory + randomsDb

        #/asiaa/home/czakon/data/HSC/SqlDl
        self.connRandoms = sqlite3.connect(randomsDb)
        
        #Create a cursor object
        self.cRandoms = self.connRandoms.cursor()

        if verbose is not None:
            print('\nOpening the following bright star database: ' + brightStarDb + '\n')
            print(self.cBrightStar.execute('''SELECT name FROM sqlite_master WHERE type='table' ''').fetchall())
            print('Opening the following SPP source database: ' + sourcesDb + '\n')
            print(self.cSources.execute('''SELECT name FROM sqlite_master WHERE type='table' ''').fetchall())

            print('Opening the following randoms database: ' + randomsDb + '\n')
            print(self.cRandoms.execute('''SELECT name FROM sqlite_master WHERE type='table' ''').fetchall())
        #self.conn.close()
        #allTables = self.cSources.execute('''SELECT name FROM sqlite_master WHERE type='table' ''').fetchall()

        return

    def loadUcac(self,binfile):

        BINNAMES = [ 'ra',
                     'spd',
                     'magm',
                     'maga',
                     'sigmag',
                     'objtype',
                     'cdf',
                     'sigra',
                     'sigdec',
                     'na1',
                     'nu1',
                     'cu1',
                     'cepra',
                     'cepdec',
                     'pmracos',
                     'pmdec',
                     'sigpmra',
                     'sigpmdec',
                     'pts_key',
                     '2mass-J',
                     '2mass-H',
                     '2mass-K',
                     'icqflag_J',
                     'icqflag_H',
                     'icqflag_K',
                     '2mass-Jerr',
                     '2mass-Herr',
                     '2mass-Kerr',
                     'APASS-B',
                     'APASS-V',
                     'APASS-g',
                     'APASS-r',
                     'APASS-i',
                     'APASS-Berr',
                     'APASS-Verr',
                     'APASS-gerr',
                     'APASS-rerr',
                     'APASS-ierr',
                     'gcflag',
                     'icflag',
                     'leda',
                     '2mass-Ext',
                     'starid',
                     'u2-zone',
                     'u2-starid' ]

        BINFORMATS = [ "i4" ] * 2 + \
                     [ "i2" ] * 2 + \
                     [ "i1" ] * 8 + \
                     [ "i2" ] * 4 + \
                     [ "i1" ] * 2 + \
                     [ "i4" ] * 1 + \
                     [ "i2" ] * 3 + \
                     [ "i1" ] * 6 + \
                     [ "i2" ] * 5 + \
                     [ "i1" ] * 6 + \
                     [ "i4" ] * 1 + \
                     [ "i1" ] * 2 + \
                     [ "i4", "i2", "i4" ]

        BINDTYPE = { 'names' : BINNAMES, 'formats' : BINFORMATS }


        data = np.fromfile(hscConfig.ucacDirectory + binfile, dtype=np.dtype(BINDTYPE))

        #ra, dec  = data['ra']  / 3600000., data['spd'] / 3600000. - 90.

        colnames = ['starid', 'ra', 'spd', 'magm', 'maga',
                    'APASS-B', 'APASS-V', 'APASS-g', 'APASS-r', 'APASS-i']

        colfmt = 1* '%6i' + '  '  + 2 * '%12.6f' + '  ' + 7 * ' %6.3f'
        rows = np.empty((len(data), len(colnames)), dtype=np.float64)

        for i, tup in enumerate(data[colnames]):
            vals = np.array(list(tup), dtype=np.float64) / 1000.
            vals[0] *= 1000.
            vals[1], vals[2] = vals[1] / 3600., vals[2] / 3600. - 90.
            rows[i] = vals

            #print(colfmt % tuple(vals))
            #print('See what this is doing and what kind of data that you want to return.')
        
        return rows, colnames
    
    def loadTycho(self):
        #01/04/2016 Need to find out how to load the Tycho data.

        tychoId = []
        raTycho = []
        decTycho = []
        btMag = []
        vtMag = []

        with open(hscConfig.tycho_dat_file,'r') as f:

            for line in f:

                tychoId.append(line[0:12].replace(' ','-'))

                try:
                    raTycho.append(float(line[15:27]))
                except:
                    raTycho.append(float(line[152:164]))

                try:
                    decTycho.append(float(line[28:40]))
                except:
                    decTycho.append(float(line[165:177]))

                try:
                    btMag.append(float(line[110:116]))
                except:
                    btMag.append(np.nan)

                try:
                    vtMag.append(float(line[123:129]))
                except:
                    vtMag.append(np.nan)
                
        f.close()
        #raTycho = hdulist[1].data['_RAJ2000']
        #decTycho = hdulist[1].data['_DEJ2000']
        #btMag = hdulist[1].data['BTmag']
        #vtMag = hdulist[1].data['VTmag']
        return tychoId, raTycho, decTycho, btMag, vtMag
    
    def loadRandomsFitsTractPatch(self,randomFitsFile,onlyInner=None):

        if onlyInner is None:
            onlyInner = False
            
        hdulist = fits.open(randomFitsFile)
        coords = hdulist[1].data['coord']#['col1','col2']
        flags = hdulist[1].data['flags']#['col1','col2']
        raRandom = np.array([])
        decRandom = np.array([])
        for index, value in enumerate(coords):
            if onlyInner or (flags[index][0] and flags[index][1]):
                raRandom = np.append(raRandom,value[0])#*180./np.pi
                decRandom = np.append(decRandom,value[1])#*180./np.pi

        hdulist.close()
        return raRandom*180/np.pi, decRandom*180/np.pi
    
    def downsampleArray(self,arrayIn,dsIndex):

        #dsIndex is the amount by which to downsample the data.

        if len(arrayIn)==0:

            print('Cannot downsample an empty array, skipping...')
            
            return arrayIn
                        
        newSize = len(arrayIn) - len(arrayIn) % dsIndex
        
        arrayNew = copy(arrayIn[0:new_size])
        
        arrayNew = arrayNew.reshape(new_size / dsIndex,dsIndex)
    
        arrayNew = arrayNew[:,0]
    
        return arrayNew

    def sourcesInformation(self):

        #12/28/2015 This code is deprecated and needs to be improved.
        for row in self.cSources.execute('''SELECT Count(*) FROM xmm_lss'''):
            print(str('%4.3e' % row[0])+' rows in the xmm_lss.\n')

        # This counts all of the different tracts in the database.
        for row in self.cSources.execute('''SELECT Count(DISTINCT tract) FROM xmm_lss'''):
            print(str(row[0])+' distinct tracts.\n')

        # Only two distinct tracts for now, hopefully this changes when I make a new query.
        for row in self.cSources.execute('''SELECT DISTINCT tract FROM xmm_lss'''):
            print('Tract '+str(row[0])+' is included.\n')

    #This function only downloads the sources that were directly downloaded
    #from the SSP data release.
    
    def loadSourcesSspDownload(self,minRa,maxRa,minDec,maxDec):
        #sourceRa, sourceDec, sourceGFluxCmodel, sourceGFluxPsf = loadSourcesSspDownload()
        #self.cSources.execute('''PRAGMA table_info(table_1)''').fetchall()
        #c.execute('SELECT Count(*) FROM table_1').fetchall()
        sourceRa = []
        sourceDec = []
        sourceMagCmodel = []
        #sourceGMagPsf = []
        
        for row in self.cSources.execute('''SELECT ra2000, decl2000, ''' + self.band + '''mag_cmodel FROM table_1 
                                            WHERE (ra2000 > ?) AND (ra2000 < ?) AND 
                                                  (decl2000 > ?) AND (decl2000 < ?)''', \
                                         (minRa,maxRa,minDec,maxDec)):
            sourceRa.append(row[0])
            sourceDec.append(row[1])
            sourceMagCmodel.append(row[2])
            #sourceGMagPsf.append(row[3])
        print(str('%4.3e' % len(sourceRa)) +
              ' sources extracted from the downloaded source catalog.')
        return sourceRa, sourceDec, sourceMagCmodel
        
    def loadSourcesSqlRaDec(self,minRa,maxRa,minDec,maxDec):

        sourceRa = []
        sourceDec = []

        column_names = np.array([])
        for row in c.execute('PRAGMA table_info(xmm_lss)'):
            column_names = np.append(column_names,row[1])
        print('SSP archive column names:')
        print(column_names)
        for row in c.execute('SELECT Count(*) FROM xmm_lss'):
            print(str(row[0])+' rows in the XMM_LSS_sql.')
        for row in c.execute('SELECT Count(DISTINCT tract) FROM xmm_lss'):
            print(str(row[0])+' distinct tracts.')
        for row in c.execute('SELECT DISTINCT tract FROM xmm_lss'):
            print('Tract '+str(row[0])+' is included.')


        for row in self.cSources.execute('''SELECT * FROM xmm_lss WHERE (RA_J2000_degrees > ?) AND (RA_J2000_degrees < ?) AND 
                                                            (Dec_J2000_degrees > ?) AND (Dec_J2000_degrees < ?)''', \
                               (minRa,maxRa,minDec,maxDec)):

            tempCoord = wcs.skyToPixel(afwCoord.IcrsCoord(row[1]*afwGeom.degrees, \
                                                          row[2]*afwGeom.degrees))
            sourceRa.append(tempCoord.getX() - exposure.getX0())
            sourceDec.append(tempCoord.getY() - exposure.getY0())

        sourceRa = np.array(sourceRa)
        sourceDec = np.array(sourceDec)
        
        return sourceRa,sourceDec

    def loadNomadSqlTractPatch(self,tract,patch):

        column_names = np.array([])
        for row in self.cBrightStar.execute('PRAGMA table_info(nomad)'):
            column_names = np.append(column_names,row[1])
        print('NOMAD column names:')
        print(column_names)

        #This is a deprecated version of the code.
        #Where is the nomad catalog made?
        print('Extracting the NOMAD catalog that was inserted in insertNomadIntoSql.')

        raNomad = []
        decNomad = []

        for row in self.cBrightStar.execute('''SELECT * FROM nomad WHERE (tract=?) AND (patch= ?)''',(tract[0],patch0[0])):
            raNomad.append(row[1])
            decNomad.append(row[2])

        print(str(len(raNomad)) + ' sources found in NOMAD catalog\n')# for ' + field_name + '\n')

        return raNomad,decNomad

    def loadNomadSqlRaDec(self,minRA,maxRA,minDec,maxDec):

        #for row in self.c.execute('''SELECT Count(*) FROM nomad_all'''):
        raNomad = []
        decNomad = []
        bsrNomad = []
        bNomad = []
        vNomad = []
        rNomad = []
        
        print('\nQuerying the Nomad database....')
        for row in self.cBrightStar.execute('''SELECT DISTINCT nomad_id, RA_J2000_degrees, Dec_J2000_degrees, 
                                                bright_star_radius, Bmag, Vmag, Rmag FROM nomad_all 
                                WHERE (RA_J2000_degrees > ?) AND (RA_J2000_degrees < ?) AND 
                                      (Dec_J2000_degrees > ?) AND (Dec_J2000_degrees < ?)''', \
                                (minRA,maxRA,minDec,maxDec)):

            raNomad.append(row[1])
            decNomad.append(row[2])
            bsrNomad.append(row[3])
            bNomad.append(row[4])
            vNomad.append(row[5])
            rNomad.append(row[6])
            
        raNomad = np.array(raNomad)
        decNomad = np.array(decNomad)
        bsrNomad = np.array(bsrNomad)
        bNomad = np.array(bNomad)
        vNomad = np.array(vNomad)
        rNomad = np.array(rNomad)
        print(str('%4.3e' % len(raNomad)) +
              ' sources extracted from the nomad bright-star catalog.')
        
        self.connBrightStar.close()
        return raNomad,decNomad,bsrNomad, bNomad, vNomad, rNomad
    
    def loadUcacSqlTractPatch(self,tract,patch):

        print('Error: Ucac does not insert the tract and patch information, and this still needs to be fixed.')
        interact(local=locals())
        column_names = np.array([])
        for row in self.cBrightStar.execute('PRAGMA table_info(ucac_all)'):
            column_names = np.append(column_names,row[1])
        print('UCAC column names:')
        print(column_names)

        #This is a deprecated version of the code.
        #Where is the nomad catalog made?
        print('Extracting the UCAC catalog that was inserted in insertUcacIntoSql.')

        raUcac = []
        decUcac = []

        for row in self.cBrightStar.execute('''SELECT RA_J2000_degrees, Dec_J2000_degrees FROM ucac_all WHERE (tract=?) AND (patch= ?)''',(tract[0],patch[0])):
            raUcac.append(row[0])
            decUcac.append(row[1])

        print(str(len(raUcac)) + ' sources found in UCAC catalog\n')# for ' + field_name + '\n')
        raUcac = np.array(raUCac)
        decUcac = np.array(decUCac)
        return raUcac,decUcac

    def loadUcacSqlRaDec(self,minRA,maxRA,minDec,maxDec):

        idUcac = []
        raUcac = []
        decUcac = []
        magUcac = []
        bUcac = []
        vUcac = []
        rUcac = []
        
        print('\nQuerying the Ucac database....')
        for row in self.cBrightStar.execute(
                '''SELECT DISTINCT ucac_id, RA_J2000_degrees, Dec_J2000_degrees,
                                   gMag,rMag,iMag, Bmag, Vmag FROM ucac_all
                   WHERE (RA_J2000_degrees > ?) AND (RA_J2000_degrees < ?) AND
                   (Dec_J2000_degrees > ?) AND (Dec_J2000_degrees < ?)''', \
                (minRA,maxRA,minDec,maxDec)):
            idUcac.append(row[0])
            raUcac.append(row[1])
            decUcac.append(row[2])
            rUcac.append(row[4])
            bUcac.append(row[6])
            vUcac.append(row[7])
            magUcac.append(min([row[3],row[4],row[5],row[6],row[7]]))

        idUcac = np.array(idUcac)
        raUcac = np.array(raUcac)
        decUcac = np.array(decUcac)
        magUcac = np.array(magUcac)
        bUcac = np.array(bUcac)
        vUcac = np.array(vUcac)
        rUcac = np.array(rUcac)
        bsrUcac = bsr['A0'] * 10**(bsr['B0'] * (bsr['C0'] - magUcac))/3600. + \
                  bsr['A1'] * 10**(bsr['B1'] * (bsr['C1'] - magUcac))/3600.

        print(str('%4.3e' % len(raUcac)) +
              ' sources extracted from the ucac bright-star catalog.')

        self.connBrightStar.close()
        return idUcac,raUcac,decUcac,bsrUcac,bUcac,vUcac,rUcac,magUcac
    
    def loadTychoSqlRaDec(self,minRA,maxRA,minDec,maxDec):

        raTycho = []
        decTycho = []
        btMag = []
        vtMag = []
        minMag = []

        print('\nQuerying the Tycho database....')
        for row in self.cBrightStar.execute(
                '''SELECT DISTINCT RA_J2000_degrees, Dec_J2000_degrees, 
                                   btMag, vtMag FROM tycho_all
                   WHERE (RA_J2000_degrees > ?) AND (RA_J2000_degrees < ?) AND
                         (Dec_J2000_degrees > ?) AND (Dec_J2000_degrees < ?)''', \
                                                                                                  (minRA,maxRA,minDec,maxDec)):
            raTycho.append(row[0])
            decTycho.append(row[1])
            if row[2] is None:
                minMag.append(row[3])
            elif row[3] is None:
                minMag.append(row[2])
            else:
                minMag.append(np.nanmin([row[2],row[3]]))
            btMag.append(row[2])
            vtMag.append(row[3])

        raTycho = np.array(raTycho)
        decTycho = np.array(decTycho)
        minMag = np.array(minMag)
        btMag = np.array(btMag)
        vtMag = np.array(vtMag)
        bsrTycho = bsr['A0'] * 10**(bsr['B0'] * (bsr['C0'] - minMag))/3600. + \
                                     bsr['A1'] * 10**(bsr['B1'] * (bsr['C1'] - minMag))/3600.
        bsrTycho = np.array(bsrTycho)
        print(str('%4.3e' % len(raTycho)) +
              ' sources extracted from the tycho bright-star catalog.')

        self.connBrightStar.close()
        return raTycho, decTycho, bsrTycho, btMag, vtMag
                                                                                                            
    def loadBsmRegRaDec(self,minRA,maxRA,minDec,maxDec):

        raNomad = []
        decNomad = []
        bsrNomad = []

        for row in self.c.execute(
                '''SELECT DISTINCT RA_J2000_degrees, Dec_J2000_degrees, 
                                   bright_star_radius FROM reg_all 
                   WHERE (RA_J2000_degrees > ?) AND (RA_J2000_degrees < ?) AND 
                         (Dec_J2000_degrees > ?) AND (Dec_J2000_degrees < ?)''', \
                                (minRA,maxRA,minDec,maxDec)):

            raNomad.append(row[0])
            decNomad.append(row[1])
            bsrNomad.append(row[2])
            
        raNomad = np.array(raNomad)
        decNomad = np.array(decNomad)
        bsrNomad = np.array(bsrNomad)

        return raNomad,decNomad,bsrNomad


    def randomInformation(self):

        # This counts all of the rows in the database
        #self.cRandoms.execute('''SELECT name FROM sqlite_master
        #                         WHERE type='table' ''').fetchall()

        for row in self.cRandoms.execute('''SELECT Count(*) FROM randoms_all'''):
            print(str('%4.3e' % row[0])+' rows in the randoms.\n')
            
        # This counts all of the different tracts in the database.
        # Only two distinct tracts for now, hopefully this changes when
        # I make a new query.
        # for row in self.c.execute('''SELECT Count(DISTINCT tract)
        #                              FROM randoms_all'''):
        #    print(str(row[0])+' distinct tracts in the randoms.\n')
        # Only two distinct tracts for now, hopefully this changes when I
        # make a new query.
        # for row in self.c.execute('''SELECT DISTINCT tract
        #                              FROM randoms_all'''):
        #    print('Tract '+str(row[0])+' is included in the randoms.')
            
        return
    
    def loadRandomsSqlTractPatch(self,tract,patch):
    
        self.randomInformation()
        raRandoms = []
        decRandoms = []
        
        for row in self.cRandoms.execute(
                '''SELECT * FROM randoms_all  WHERE (tract=?) AND (patch= ?)''',
                             (tract[0],patch[0])):

            #Make sure that this is in the inner tract and the mask is not set.
            #inner_tract = row[2]
            unmasked = row[6]
            #if inner_tract and unmasked:
            if unmasked:
                raRandoms.append(row[3])
                decRandoms.append(row[4])
                
        raRandoms = np.array(raRandoms)
        decRandoms = np.array(decRandoms)
        
        return raRandoms,decRandoms

    def loadRandomsSqlRaDec(self,minRa,maxRa,minDec,maxDec):

        self.randomInformation()
        
        raRandoms =[]
        decRandoms = []
        for row in self.cRandoms.execute(
                '''SELECT RA_J2000_degrees, Dec_J2000_degrees 
                   FROM randoms_all WHERE (RA_J2000_degrees > ?) AND (RA_J2000_degrees < ?) AND 
                                 (Dec_J2000_degrees > ?) AND (Dec_J2000_degrees < ?) ''' , \
                                 (minRa,maxRa,minDec,maxDec)):
            #AND (tract_inner=1) AND (patch_inner=1)''', \

            raRandoms.append(row[0])
            decRandoms.append(row[1])

        raRandoms = np.array(raRandoms)
        decRandoms = np.array(decRandoms)
        
        return raRandoms,decRandoms
