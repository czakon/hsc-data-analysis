# Created by Nicole Gisela Czakon: 10/26/2015
# The purpose of this program is to load the tract and patches from the data file.
# USAGE: from pipeline_code.readFiles import readTractInfoFile
import os,re
import pipeline_code.hscConfigurationFile as hscConfig
from code import interact

def data(tract_info_file=None,fieldName=None):

    if tract_info_file != None and fieldName != None:

        raise ValueError('Error, both the tract_info_file and the fieldName cannot be set simultaneously...')

    elif tract_info_file == None and fieldName == None:

        raise ValueError('Error, either tract_info_file or the fieldName must be set...')

    elif tract_info_file==None:

        all_folders = os.listdir(hscConfig.sspPatchDirectory)

        if fieldName in all_folders:

            tract_info_file = hscConfig.sspPatchDirectory + fieldName + '/tracts_patches_' + fieldName + '.txt'

        else:

            err_string = 'Error, ' + fieldName + ' directory not found in: ' + hscConfig.sspPatchDirectory
            raise ValueError(err_string)

    tract_patch_complete = []

    with open(tract_info_file,'r') as f:

        line = f.readline()

        tracts_patches_all = {}
        
        while line:
            
            #First, get rid of any commented lines
            while line[0]=='*':
                line = f.readline()

            #Read the first line and get the tract
            t_line = re.findall('[^(, ):]+',line)
            tract_x = t_line[1]

            #Then, identify if the tract information or the patch information is given.
            #If there is no tract information given, then just leave those entries blank.
            
            if 'Patch' in t_line:

                tracts_patches_all[tract_x] = {}

            else:
                #print(t_line)
                #interact(local=locals())
                tracts_patches_all[tract_x] = {'Center':{'RA':float(t_line[5]),'Dec':float(t_line[6])}}

                for t_x in range(4):

                    line = f.readline()
                    t_line = re.findall('[^(, ):]+',line)
                    tracts_patches_all[tract_x]['Corner'+str(t_x)] = {'RA':float(t_line[5]),'Dec':float(t_line[6])}

                line = f.readline()

            #Remove any commented lines.
            while line[0]=='*':
                line = f.readline()
                t_line = re.findall('[^(, ):]+',line)

            #Now cycle over all of the patches
            for p_x in range(81):

                ### Keep this at the top
                #print(line)
                #If the tract changes, then we have to continue
                if tract_x != t_line[1]:
                    tract_x = t_line[1]
                    continue

                patch_x = t_line[3]+','+t_line[4]
                tracts_patches_all[tract_x][patch_x] = {'Center':{'RA':float(t_line[8]),'Dec':float(t_line[9])}}
                #print('A')
                #print(t_line)
                #print('B')
                line = f.readline()
                t_line = re.findall('[^(, ):]+',line)

                for p_xx in range(5):

                    #print(t_line)
                    
                    if 'Center' in t_line or t_line==[]:
                        #print('Only 4 corners are defined. Reached a center, breaking loop')
                        #print(tracts_patches_all[tract_x][patch_x].keys())
                        #interact(local=locals())
                        tracts_patches_all[tract_x][patch_x]['Corner4'] = tracts_patches_all[tract_x][patch_x]['Corner0']
                        break
                    else:
                        tracts_patches_all[tract_x][patch_x]['Corner'+str(p_xx)] = {'RA':float(t_line[8]),'Dec':float(t_line[9])}
                        line = f.readline()
                        t_line = re.findall('[^(, ):]+',line)

                #line = f.readline()
                #t_line = re.findall('[^(, ):]+',line)
                
                #print('\nFinished reading one patch\n')
                #interact(local=locals())

            #print('\nFinished reading one tract\n')
            #interact(local=locals())
            if 'Center' not in tracts_patches_all[tract_x].keys():
                tracts_patches_all[tract_x]['Center'] = tracts_patches_all[tract_x]['4,4']['Center']
                tracts_patches_all[tract_x]['Corner0'] = tracts_patches_all[tract_x]['0,0']['Corner0']
                tracts_patches_all[tract_x]['Corner1'] = tracts_patches_all[tract_x]['8,0']['Corner1']
                tracts_patches_all[tract_x]['Corner2'] = tracts_patches_all[tract_x]['8,8']['Corner2']
                tracts_patches_all[tract_x]['Corner3'] = tracts_patches_all[tract_x]['0,8']['Corner3']

    return tracts_patches_all

