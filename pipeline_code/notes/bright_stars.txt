for row in c.execute('SELECT DISTINCT * FROM reg_all WHERE bright_star_radius>0.35 ORDER BY bright_star_radius DESC'):
                    print(row)
#    RA         Dec       BsRadius  Field    Name     Tract Patch
1.  (247.16062, 41.88168, 1.191454) HECTMAP  *gHer    15832 (2,1)
2.  (331.44598, -0.31985, 1.033584) VVDS     *alfAqr  09450 (6,6)
3.  ( 34.83662, -2.97765, 0.675692) XMM-WIDE Mir      08765 (1,8)
4.  (177.67383,  1.76472, 0.583454) WIDE12H  *betVir  09831 (0,2),(1,2),(0,1),(1,1)
5.  ( 35.48595,  0.39567, 0.557731) XMM-WIDE *69Cet   09493 (~4,2) don't have the data for this.
6.  (238.65771, 43.13857, 0.539197) HECTMAP  *2Her    15828 (4,8) 
9.  (129.68932,  3.34144, 0.476442) GAMA-09H *sig Hya
10. (221.56218,  1.89288, 0.409485) GAMA-15H (Tract 9861)
11. (244.93516, 46.31337, 0.391554) HECTMAP
12. (138.59111,  2.31427, 0.382892) GAMA-09H (Tract 9805)
13. (336.96468,  4.69566, 0.374425) (edge of field)
14. (221.29878, -1.41753, 0.370264) GAMA-15H
15. (251.83228, 42.23891, 0.356070) (edge of field)
16. (238.16893, 42.45152, 0.352117) (Tract 15828)
XX. ( 29.5    , -2.05               (XMM-WIDE, Tract 9000,5,5)
#(128.43116,  4.75700, 0.586754) GAMA-09H (off of survey area)
#(349.29141, 3.28229, 0.634984)  (off of survey area on VVDS)
#(240.47228, 58.56525, 0.463249) *tet Dra (off of survey)
#(216.29915, 51.85074, 0.435521) (off of survey)
#6.  ( 30.11176, -8.52387, 0.548384) XMM-WIDE V* Ar Cet (Off of the field)
#8.  (174.23721, -0.82375, 0.490018) WIDE12H  *ups Leo 9344 outside of the field
