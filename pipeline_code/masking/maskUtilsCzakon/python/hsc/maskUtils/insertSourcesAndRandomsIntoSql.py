#!/usr/bin/env python
#
# LSST Data Management System
# Copyright 2008, 2009, 2010, 2011, 2012 LSST Corporation.
#
# This product includes software developed by the
# LSST Project (http://www.lsst.org/).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
# GNU General Public License for more details.
#
# You should have received a copy of the LSST License Statement and
# the GNU General Public License along with this program.  If not,
# see <http://www.lsstcorp.org/LegalNotices/>.
#
import numpy as np 
import os
import sqlite3
from pipeline_code import hscConfigurationFile as hscConfig
from code import interact

import lsst.pex.config   as pexConfig
import lsst.pipe.base    as pipeBase
import lsst.afw.image    as afwImage
from   lsst.afw.geom     import Box2D
import lsst.afw.table    as afwTable
import lsst.meas.algorithms as measAlg

import lsst.afw.geom as afwGeom
import lsst.afw.detection as afwDetection

from lsst.pipe.tasks.coaddBase import CoaddBaseTask
from lsst.pipe.tasks.setPrimaryFlags import SetPrimaryFlagsTask

#NGC
#__all__ = ["DrawRandomsTask"]
__all__ = ["InsertSourcesAndRandomsTask"]
#NGC
#class DrawRandomsConfig(CoaddBaseTask.ConfigClass):
class InsertSourcesAndRandomsConfig(CoaddBaseTask.ConfigClass):

    N           = pexConfig.Field("Number of random points per patch (supersedes Nden)", int, -1)
    Nden        = pexConfig.Field("Random number density per sq arcmin", float, 30)
    clobber     = pexConfig.Field("To overwrite existing file [default: True]", bool, True)
    folderOutName = pexConfig.Field("Name of output folder", str, "ran/")
    test        = pexConfig.Field("To write a test table", bool, False)

    setPrimaryFlags = pexConfig.ConfigurableField(target=SetPrimaryFlagsTask, doc="Set flags for primary source in tract/patch")

    def setDefaults(self):
        pexConfig.Config.setDefaults(self)

class InsertSourcesAndRandomsTask(CoaddBaseTask):
    _DefaultName = "insertSourcesAndRandomsIntoSql"
    ConfigClass  = InsertSourcesAndRandomsConfig

    def __init__(self, schema=None, *args, **kwargs):

        #ProcessCoaddTask.__init__(self, **kwargs)
        CoaddBaseTask.__init__(self,  *args, **kwargs)

        if schema is None:
            schema = afwTable.SourceTable.makeMinimalSchema()
        self.schema = schema

        self.makeSubtask("setPrimaryFlags", schema=self.schema)

    def run(self, dataRef, selectDataList=[]):

        """
        Draw randoms for a given patch, at the same time, insert the sources into the database....
        See http://hsca.ipmu.jp/hscsphinx_test/scripts/print_coord.html
        for coordinate routines.
        """
        # verbose
        self.log.info("Processing %s" % (dataRef.dataId))

        self.log.info("Opening the SQL database...")

        # get coadd, coadd info and coadd psf object
        coadd   = dataRef.get(self.config.coaddName + "Coadd_calexp")
        psf = coadd.getPsf()

        skyInfo = self.getSkyInfo(dataRef)

        #skyInfo = self.getSkyInfo(coaddName=self.config.coaddName, patchRef=dataRef)
        # Setup the SQL datafile
        insertSQL = True
        insertRandoms = True
        insertSsp = False#True
        
        if insertSQL:
            dbOutFilename = hscConfig.brightStarDb
            conn = sqlite3.connect(dbOutFilename)
            
            #Create a cursor object
            c = conn.cursor()
            #print('self.overwriteSQL is set to TRUE, erasing the old database...')
            # I think that I need a better method to determine whether or not to overwrite.
            #This doesn't seem to be giving me the correct answers, I will rely on the database downloads....

            #Check to see if the randoms table exists, if not, then create it....
            allTables = c.execute('''SELECT name FROM sqlite_master WHERE type='table' ''').fetchall()

            if insertRandoms:
                #c.execute('''DROP TABLE randoms''')

                if (u'randoms',) not in allTables:
                    self.log.info("Starting to create the database table from scratch...")
                    # Create table for both the randoms and the ssp data.
                    c.execute('''CREATE TABLE randoms (tract int, tract_inner bool, patch string, patch_inner bool,
                    RA_J2000_degrees float, Dec_J2000_degrees float,
                    shape_sdss_psf float, unmasked bool)''')
                    if doSsp:
                        c.execute('''DROP TABLE ssp''')
                        c.execute('''CREATE TABLE ssp (id long, filter string, tract int, patch string,
                        RA_J2000_degrees float, Dec_J2000_degrees float, unmasked bool,
                        flux_kron float, flux_kron_err float, flux_cmodel float, flux_cmodel_err float)''')

        # wcs and reference point (wrt tract)
        wcs  = coadd.getWcs()
        xy0  = coadd.getXY0()

        # dimension in pixels
        dim  = coadd.getDimensions()

        # create table and catalog
        # copied from meas_algorithms/HSC-3.9.0/tests/countInputs.py
        measureSourcesConfig = measAlg.SourceMeasurementConfig()
        measureSourcesConfig.algorithms.names = ["flags.pixel", "centroid.naive", "countInputs"]
        measureSourcesConfig.slots.centroid = "centroid.naive"
        measureSourcesConfig.slots.psfFlux = None
        measureSourcesConfig.slots.apFlux = None
        measureSourcesConfig.slots.modelFlux = None
        measureSourcesConfig.slots.instFlux = None
        measureSourcesConfig.slots.calibFlux = None
        measureSourcesConfig.slots.shape =  None
        measureSourcesConfig.validate()

        # add PSF_size column
        shape_sdss_psf = self.schema.addField("shape_sdss_psf", type="MomentsD", doc="PSF moments from SDSS algorithm", units="Pixels")
        #PSF_size       = self.schema.addField("PSF_size", type=np.float32, doc="Size of the PSF from shape_sdss_psf (=sigma of gaussian)", units="Pixels")

        ms      = measureSourcesConfig.makeMeasureSources(self.schema)
        catalog = afwTable.SourceCatalog(self.schema)
        measureSourcesConfig.slots.setupTable(catalog.getTable())

        if self.config.N == -1:
            # constant random number density
            # Compute the area in degree
            pixel_area =  coadd.getWcs().pixelScale().asDegrees()**2
            area       = pixel_area * dim[0] * dim[1]
            N          = self.iround(area*self.config.Nden*60.0*60.0)
        else:
            # fixed number if random points
            N = self.config.N

        # inner patch
        innerFloatBBox = Box2D(skyInfo.patchInfo.getInnerBBox())
        # loop over N random points

        # extract mask and convert into array
        mask = coadd.getMaskedImage().getMask()
        dim  = mask.getDimensions()
        mask_array = mask.getArray()
        #Keep this asa reference for debugging
        mask_keys = mask.getMaskPlaneDict().keys()
        mask_values = mask.getMaskPlaneDict().values()
        bad_keys = ['BAD','CR','NOT_DEBLENDED','UNMASKEDNAN','SAT']
        bad_mask = 0
        for bad_key in bad_keys:
            bad_mask += 1 << mask_values[mask_keys.index(bad_key)]

        if insertRandoms:
            # verbose

            for i in range(N):

                # draw a random point
                x = np.random.random()*(dim[0]-1)
                y = np.random.random()*(dim[1]-1)

                # coordinates
                radec = wcs.pixelToSky(afwGeom.Point2D(x + xy0.getX(), y + xy0.getY()))
                xy    = wcs.skyToPixel(radec)

                ###
                # inner patch
                isPatchInner   = innerFloatBBox.contains(xy)

                # inner tract
                sourceInnerTractId = skyInfo.skyMap.findTract(radec).getId()
                isTractInner       = sourceInnerTractId == skyInfo.tractInfo.getId()
                if mask_array[y,x]:
                    unmasked = bool(int(mask_array[y,x]) & bad_mask)
                else:
                    #Set the mask to True, if it's equal to 0
                    unmasked = True
                ###
                # add record in table
                record = catalog.addNew()
                record.setCoord(radec)

                # get PSF moments and evaluate size
                # NGC 01/19/2016
                try:
                    psf.computeShape(afwGeom.Point2D(xy))
                    record.set(shape_sdss_psf, psf.computeShape(afwGeom.Point2D(xy)))
                except:
                    print('Cannot obtain psf information, will skip this step....')
                    continue
            
            #record.set(PSF_size, psf.computeShape(afwGeom.Point2D(xy)).getDeterminantRadius())

            # looks like defining footprint isn't necessary
            # foot = afwDetection.Footprint(afwGeom.Point2I(xy), 1)
            # record.setFootprint(foot)

            # required for setPrimaryFlags
            record.set(catalog.getCentroidKey(), afwGeom.Point2D(xy))

            # do measurements (flagging and countINputs)
            ms.apply(record, coadd, afwGeom.Point2D(xy))

            #insert catalog into sql
            try:
                temp_psf = psf.computeShape(afwGeom.Point2D(xy))
                shape_sdss_psf_det = temp_psf.getIxx()*temp_psf.getIyy() - temp_psf.getIxy()**2 
                                 
                if shape_sdss_psf_det > 0.0:
                    t_shape_sdss_psf = 2.0*np.sqrt(2.0*np.log(2.0)) * (shape_sdss_psf_det)**0.25
                else:
                    t_shape_sdss_psf = None
            except:
                print('Skipping this as well...')

            if insertRandoms:
                c.execute('''INSERT INTO randoms VALUES (?,?,?,?,?,?,?,?)''', \
                          [skyInfo.tractInfo.getId(),isTractInner,str(skyInfo.patchInfo.getIndex())[1:-1], \
                           isPatchInner,
                           radec.toIcrs().getRa().asDegrees(), radec.toIcrs().getDec().asDegrees(), \
                           t_shape_sdss_psf,unmasked])

        self.setPrimaryFlags.run(catalog, skyInfo.skyMap, skyInfo.tractInfo, skyInfo.patchInfo, includeDeblend=False)
        if insertSsp:
            ### Now, insert the ssp sources, as of Nov 2, 2015, this isn't working, I'll rely on the database downloaded from the data archive. ###
            sources = dataRef.get("deepCoadd_meas")#dataRef.get("deepCoadd_forced_src")
            self.log.info("Starting to insert the source catalogs into the database... ")

            for i, source in enumerate(sources):

                if not source.get('flags.pixel.saturated.any') and not source.get('flags.pixel.cr.any') and not source.get('flags.pixel.bad'):
                    unmasked = True
                else:
                    unmasked = False
                kron_flux, kron_flux_err = source.get('flux.kron'),source.get('flux.kron.err')
                kron_mag, kron_mag_err = 2.5*np.log10(kron_flux), 2.5/np.log(10)*(kron_flux_err/kron_flux)
                cmodel_flux, cmodel_flux_err = source.get('cmodel.flux'),source.get('cmodel.flux.err')
                cmodel_mag, cmodel_mag_err = 2.5*np.log10(cmodel_flux), 2.5/np.log(10)*(cmodel_flux_err/cmodel_flux)

                if butler.datasetExists('fcr_md', dataId):
                    print('STOP A')
                    interact(local=locals())
                    fcr_md = butler.get('fcr_md', dataId)
                    ffp = measMosaic.Flux.FitParams(fc_md)
                    correction = ffp.eval(source.getX(),source.getY())
                    zeropoint = 2.5*np.log10(fcr_md.get('FLUXMAG0')) + correction
                else:
                    print('STOP B')
                    interact(local=locals())
                    metadata = butler.get('calexp_md', dataId)
                    zeropoint = 2.5*np.log10(fcr_md.get('FLUXMAG0'))

                kron_mag = zeropoint - kron_mag
                cmodel_mag = zeropoint - cmodel_mag
            
                c.execute('''INSERT INTO ssp VALUES (?,?,?,?,?,?,?,?,?,?,?)''', \
                          [source.get('id'),dataRef.dataId['filter'],skyInfo.tractInfo.getId(),str(skyInfo.patchInfo.getIndex())[1:-1], \
                           source.getRa().asDegrees(), source.getDec().asDegrees(),unmasked, \
                       ])
            
        ######
        if insertSQL:
            conn.commit()
            conn.close()

        # write catalog
        if insertRandoms:
            fullDirectoryOutName = self.config.folderOutName + '/' + dataRef.dataId['filter'] + '/' + str(dataRef.dataId['tract'])
            if not os.path.isdir(fullDirectoryOutName):
                os.makedirs(fullDirectoryOutName)
            fileOutName = fullDirectoryOutName + '/ran-' + dataRef.dataId['patch'] + '.fits'
            print('Writing to: ' + fileOutName)
            catalog.writeFits(fileOutName)

        # to do. Define output name in init (not in paf) and
        # allow parallel processing
        # write sources
        # if self.config.doWriteSources:
        #   dataRef.put(result.sources, self.dataPrefix + 'src')

        return

    # Overload these if your task inherits from CmdLineTask
    def _getConfigName(self):
        return None
    def _getEupsVersionsName(self):
        return None
    def _getMetadataName(self):
        return None



    """
    *******************************************************************
    *******************************************************************
                        BELOW IS NOT USED ANYMORE
    *******************************************************************
    *******************************************************************
    """


    def run_Old(self, dataRef, selectDataList=[]):

        import pyfits as fits

        if self.config.test:
            self.testTable(read=True)
            return

        # verbose
        self.log.info("Processing %s" % (dataRef.dataId))

        # get coadd and coadd info
        coadd   = dataRef.get(self.config.coaddName + "Coadd")

        #coadd.setPsf(None)
        #print coadd.getPsf()

        skyInfo = self.getSkyInfo(dataRef)
        #skyInfo = self.getSkyInfo(coaddName=self.config.coaddName, patchRef=dataRef)

        # wcs and reference point (wrt tract)
        wcs = coadd.getWcs()
        xy0 = coadd.getXY0()

        # extract mask and convert into array
        mask = coadd.getMaskedImage().getMask()
        dim  = mask.getDimensions()
        mask_array = mask.getArray()

        # Compute the area in degree
        pixel_area =  coadd.getWcs().pixelScale().asDegrees()**2
        area       = pixel_area * dim[0] * dim[1]

        # print "total area = ", area

        if self.config.N  == -1:
            N = self.iround(area*self.config.Nden*60.0*60.0)
        else:
            N = self.config.N

        # get mask labels and bit values
        mask_labels = mask.getMaskPlaneDict()
        keys        = mask_labels.keys()
        values      = mask_labels.values()

        # initialize arrays
        ra           = [0.0]*N
        dec          = [0.0]*N
        isPatchInner = [True]*N
        isTractInner = [True]*N
        flags        = [[True]*N for i in range(len(keys))]

        # loop over N random points
        for i in range(N):
            ra[i], dec[i], bitmask, isPatchInner[i], isTractInner[i] = self.drawOnePoint(mask_array, dim, wcs, xy0, skyInfo)

            for j, value in enumerate(values):
                flags[j][i] = self.check_bit(bitmask, value)

        # column definition
        cols = fits.ColDefs([fits.Column(name="ra",           format="E", array=ra)])
        cols.add_col(        fits.Column(name="dec",          format="E", array=dec))
        cols.add_col(        fits.Column(name="isPatchInner", format="L", array=isPatchInner))
        cols.add_col(        fits.Column(name="isTractInner", format="L", array=isTractInner))
        for j, key in enumerate(keys):
            cols.add_col(        fits.Column(name=key,            format="L", array=flags[j]))

        # create table object - method depends on pyfits version
        pyfits_v = [self.int_robust(v) for v in (fits.__version__).split('.')]
        if (pyfits_v[0] > 3) | (pyfits_v[0] == 3 & pyfits_v[1] >= 3):
            tbhdu = fits.BinTableHDU.from_columns(cols)
        else:
            tbhdu = fits.new_table(cols)

        # add info in header
        header = fits.Header()
        header['AREA'] = (area, "in sq. degrees")

        # write table
        hdulist = fits.HDUList([fits.PrimaryHDU(header=header), tbhdu])

        #tbhdu.writeto(self.config.fileOutName, clobber=self.config.clobber)
        hdulist.writeto(self.config.fileOutName, clobber=self.config.clobber)

        return

    def check_bit(self, bitmask, bit):
        return ((bitmask&(1<<bit))!=0)

    def iround(self, x):
        """
        iround(number) -> integer
        Round a number to the nearest integer.
        From https://www.daniweb.com/software-development/python/threads/299459/round-to-nearest-integer-
        """
        return int(round(x) - .5) + (x > 0)

    def drawOnePointOLD(self, mask_array, dim, wcs, xy0, skyInfo):

        x = np.random.random()*(dim[0]-1)
        y = np.random.random()*(dim[1]-1)

        bitmask = mask_array[y, x]

        radec = wcs.pixelToSky(x + xy0.getX(), y + xy0.getY())
        xy    = wcs.skyToPixel(radec)


        #self.exp.getWcs().pixelToSky(afwGeom.Point2D(self.x, self.y))

        # inner patch
        innerFloatBBox = Box2D(skyInfo.patchInfo.getInnerBBox())
        isPatchInner   = innerFloatBBox.contains(xy)

        # inner tract
        sourceInnerTractId = skyInfo.skyMap.findTract(radec).getId()
        isTractInner       = sourceInnerTractId == skyInfo.tractInfo.getId()

        # ra and dec in decimal degrees
        ra  = radec.getLongitude().asDegrees()
        dec = radec.getLatitude ().asDegrees()

        return ra, dec, bitmask, isPatchInner, isTractInner


    def testTable(self, read=False):

        #import lsst.afw.geom as afwGeom


        #print afwGeom.Point2D(100.23, 100)


        #exit(-1)

        # to do
        # this table cannot be read by Topcat and I have no clue why


#        schema = afwTable.SourceTable.makeMinimalSchema()
#        task = measAlg.SourceMeasurementTask(schema, config=config)
#        source = catalog.addNew()
#        source.set("id", 12345)



        # create table
#        schema = afwTable.Schema()
        schema = afwTable.SourceTable.makeMinimalSchema()

        # define table fields
        fields = [schema.addField("ra", type="F", doc="ra")]
        fields.append(schema.addField("dec", type="F", doc="dec"))
        fields.append(schema.addField("isPatchInner", type="Flag", doc="True if inside patch inner area"))
        fields.append(schema.addField("isTractInner", type="Flag", doc="True if inside tract inner area"))

        # create table object
#        catalog = afwTable.BaseCatalog(schema)
        catalog = afwTable.SourceCatalog(schema)


        # fill the table
        record = catalog.addNew()
#        help(record)


        record.set(fields[0], 1.0)
        record.set(fields[1], 2.0)
        for i in range(2, 4):
            record.set(fields[i], True)

        for source in record:
            print source.get("isPatchInner")

        # save the table
        catalog.writeFits("test.fits")

        if read:

            #FILE = fits.open("/Users/coupon/data/HSC/SSP/rerun/CLAUDS/deepCoadd-results/HSC-Y/1/5,5/src-HSC-Y-1-5,5.fits")
            FILE = fits.open("test.fits")
            d    = FILE[1].data

            print FILE.info()
            print FILE[0].header

            FILE.close()

        return


    def int_robust(self, s):
        try:
            return int(s)
        except ValueError:
            return 0
