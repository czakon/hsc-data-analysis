#!/usr/bin/env python
#
# Created by Nicole Czkon 09212015
# Description of the Code:
# The purpose of this code is to retrieve the bright-stars from NOMAD
# And save them to a file.
#
# KEYWORDS:
#
# overwrite (I'm not sure if this has become obsolete)
# overwriteReg (This should overwrite the .Reg file)
# overwriteSQL (This should reinsert the data into the SQL (but I'm not sure if it clears the datbase or not).)
# insertSQL: this inserts the data into the SQL, by default selected if overwrite SQL is set. 
#
# USAGE:
#
# from pipeline_code.masking import retrieveBrightStarsFromNomad
#
# The default setting, loads the results from file.
# brightStars = retrieveBrightStarsFromNomad.FindNomadTask()
#
# The "test" option returns the dictionary
# brightStars = retrieveBrightStarsFromNomad.FindNomadTask(test=True)
#
# IF YOU ACTUALLY WANT TO ADD MORE STARS TO THE DATABASE.
# brightStars = retrieveBrightStarsFromNomad.FindNomadTask(test=True,insertSQL=True)
# The configuration phase, calls Vizier remotely.
# brightStars = retrieveBrightStarsFromNomad.FindNomadTask(exposure=exposure,
#                                                          mapCoord=mapCoord)
#
# Then, the catalog can be retrieved like this.
# nomadOut = brightStars.nomadOut
#
# OUTPUT:
# 
# TODO:
# Basically, you want to save the mapCoord's to confirm that you're opening
# the correct file.
#
import os
################# For Debugging ###########################################
from code import interact
#import time
########### Pipeline-Related Packages #####################################
import lsst.daf.persistence as dafPersist
import lsst.afw.coord as afwCoord
import lsst.afw.geom as afwGeom
import lsst.pex.config as pexConfig
############ General Python Packages #####################################
import astropy.units as u
import astropy.coordinates as coord
import numpy as np
from astropy.io import ascii,fits
from astropy.table import vstack
from datetime import date
## Additional package needed in order to download the data from Vizier ###
from astroquery.vizier import Vizier
from copy import deepcopy
import cPickle as pickle
import sqlite3
########## My Own Python Packages #######################################
from pipeline_code import hscConfigurationFile as hscConfig
import loadTestExposure

__all__ = ["FindNomadTask"]

########## FINDING NOMAD SOURCES FOR VERY BRIGHT STARS  #################
class FindNomadTask:
    _DefaultName = "findNomad"

    def __init__(self,overwrite=None,searchWidth=None,exposure=None,mapCoord=None, \
                 test=None,insertSQL=None,overwriteSQL=None,overwriteReg=None,loadOnly=None,fields=None):

        self.folderout = hscConfig.ssp_patch_folder

        self.folderout_exists = os.path.isdir(self.folderout)

        if fields is None:
            self.fields = ['D-COSMOS']
        else:
            self.fields = fields
            
        if loadOnly is None:
            self.loadOnly = False
        else:
            self.loadOnly = True
            self.load()
            
        if overwriteReg is None:
            self.overwriteReg = False
        else:
            self.overwriteReg = True

        if insertSQL is None:
            self.insertSQL = False
        else:
            self.insertSQL = True

        if overwriteSQL is None:
            self.overwriteSQL = False
        else:
            self.overwriteSQL = True
            if self.insertSQL is False:
                print('overwriteSQL keyword is, setting the insertSQL keyword to TRUE.')
                self.insertSQL = False

        if overwrite is None:
            self.overwrite = True
            if test:
                self.nomadOut = self.test()
            #else:
                #if self.folderout_exists:
                #    print('Overwrite keyword is set to False, loading data from file: ' + self.fileout)
                #    self.nomadOut = pickle.load(open(self.fileout,"rb"))
                #else:
                #    print('Output file: ' + self.fileout + ' does not exist, if you would like to create this,' + \
                #          ' please include "overwrite=True" as a keyword.')
        else:

            if test:
                print('The test configuration by design cannot overwrite the output file....')
                self.nomadOut = self.test()
                return self.nomadOut

            else:

                if exposure is None:
                    print('You must specify an exposure before querying Vizier.')
                    return
                else:
                    self.exposure = exposure

                if mapCoord is None:
                    print('You must specify an exposure before querying Vizier.')
                    return
                else:
                    self.mapCoord = mapCoord

                self.exposure = exposure    
                self.overwrite = True

        #The default search width is the size of the XMM-LSS field for now.
        if searchWidth is None:
            self.searchWidth = 2.8
        else:
            self.searchWidth = searchWidth

    def run(self):

        reg_file_open = False

        c = self.load()
        print('\nInserting all of the entries into the database, this will take a while...')
        tract_patch_completed = []
        if self.overwriteSQL:
            print('\nself.overwriteSQL is set to TRUE, erasing the old database...press Ctrl-D to continue...')
            interact(local=locals())
            c.execute('''DROP TABLE nomad''')
            # Create table
            c.execute('''CREATE TABLE nomad (nomad_id string, 
                                             RA_J2000_degrees float, Dec_J2000_degrees float,
                                             Bmag float, Vmag float, Rmag float,
                                             Jmag float, Hmag float, Kmag float,
                                             tract string, patch string,
                                             bright_star_radius float)''')
        if self.insertSQL and not self.overwriteSQL:
            # This counts all of the rows in the database
            for row in c.execute('''SELECT Count(*) FROM nomad'''):
                print(str(row[0])+' rows in the nomad sql database.')
            # This counts all of the different tracts in the database.
            # Only two distinct tracts for now, hopefully this changes when I make a new query.
            for row in c.execute('''SELECT Count(DISTINCT tract) FROM nomad'''):
                print(str(row[0])+' distinct tracts.')
            # Only two distinct tracts for now, hopefully this changes when I make a new query.
            for row in c.execute('''SELECT DISTINCT tract,patch FROM nomad GROUP BY tract,patch'''):
                tract_patch = {'tract':str(row[0]),'patch':str(row[1])}
                tract_patch_completed.append(tract_patch)
            #    print('Tract: '+ str(row[0]) + ' and Patch: ' + str(row[1]) + ' is included.')
            for row in c.execute('''SELECT DISTINCT tract FROM nomad GROUP BY tract'''):
                #tract_patch_completed.append(tract_patch)
                print('Tract: '+ str(row[0]) + ' is included.')
            
        for field in self.fields:
            tract_info_filename = self.folderout + field + '/tracts_patches_' + field + '.txt'
            tract_info_file = readTractInfoFile.data(tract_info_filename)

            if field[0]=='W':

                sub_directory = 'WIDE/'

            elif field[0]=='D':

                sub_directory = 'DEEP/'

            elif field[0]=='UD':

                sub_directory = 'ULTRADEEP/'

            else:

                print('Invalid format for the field name, must start with \'W\', \'D\' or \'UD\'.')
                interact(local=locals())
                
            for tract_patch_full in tract_info_file:

                tract_patch = {'tract':tract_patch_full['tract'],'patch':tract_patch_full['patch']}

                if tract_patch in tract_patch_completed:
                    tract_patch_in_sql = True

                else:
                    tract_patch_in_sql = False
                #Start here!!! Search the directory and make sure that the file doesn't already exist.
                #We want to still perform the search if the reg file doesn't exist.
                # for band in hscConfig.bands: (this needs to go further down.)
                band = hscConfig.bands[0]

                directory_out = self.folderout + sub_directory + \
                                'deepCoadd/BrightObjectMasks/'+tract_patch['tract'] +'/'

                try:
                    os.stat(directory_out)
                except:
                    print(directory_out + ' does not exist, will create it....')
                    os.mkdir(directory_out)
                    
                reg_fileout = directory_out + 'BrightObjectMask-' + \
                              tract_patch['tract'] + '-' + tract_patch['patch'] + '-HSC-' + band + '.reg'

                print('\nWriting star mask to to reg_fileout: ' + reg_fileout)

                reg_file_exists = os.path.isfile(reg_fileout)

                if tract_patch_in_sql and self.insertSQL and reg_file_exists:

                    print('\nTract: ' + tract_patch['tract'] + \
                          ' Patch: ' + tract_patch['patch'] + \
                          ' already inserted in SQL and the .reg file exists, skipping...')

                elif not tract_patch_in_sql and not self.insertSQL and reg_file_exists:

                    print('\nTract: ' + tract_patch['tract'] + \
                          ' Patch: ' + tract_patch['patch'] + \
                          ' .reg file exists but the patch is not yet inserted into the SQL.'+ \
                          'As the \'insertSQL\' is set to False, will skip this tract patch...')

                else:
                    
                    Corner1RA = tract_patch_full['Corner1RA']
                    Corner1Dec = tract_patch_full['Corner1Dec']
                    Corner3RA = tract_patch_full['Corner3RA']
                    Corner3Dec = tract_patch_full['Corner3Dec']
                    n_searches = 20#11
                    extra_width = 0.2
                    total_width = np.max(np.abs(Corner3RA - Corner1RA),np.abs(Corner3Dec - Corner1Dec))*(1.0+ extra_width)
                    #t_width = hscConfig.patch_side * hscConfig.pix_scale/(n_searches+1.0)
                    t_width = total_width/n_searches

                    nomad_vizier = []
                    nomad_vizier_a = []
                    for j in range(n_searches):

                        for k in range(n_searches):
                            new_map_coord =  coord.SkyCoord(ra = float(Corner3RA) - \
                                                            (j+0.5)*t_width + 0.9*extra_width*total_width/2.0/(1.0+extra_width), \
                                                            dec=float(Corner3Dec) - \
                                                            (k+0.5)*t_width + extra_width*total_width/2.0/(1.0+extra_width), \
                                                            unit=(u.deg,u.deg),frame='fk5')#icrs)

                            nomad_vizier_a = Vizier().query_region(new_map_coord,width = \
                                                                   str(t_width) + 'd', \
                                                                   catalog=["I/297"])

                            if nomad_vizier_a.keys() == []:
                                print('No bright stars found in this segment, skipping...')
                                continue

                            nomad_vizier_a = nomad_vizier_a['I/297/out']

                            if len(nomad_vizier_a) >= 50:

                                print('\a')
                                print('Sample range is too large, decrease this a bit...')
                                interact(local=locals())

                            nomad_vizier_a['tract'] = tract_patch['tract'] 
                            nomad_vizier_a['patch'] = tract_patch['patch'] 
                            nomad_vizier_a['bright_star_radius'] = 0.0
                            nomad_vizier_a.remove_column('_x')
                            nomad_vizier_a.remove_column('_y')
                            nomad_vizier_a.remove_column('_p')
                            nomad_vizier_a.remove_column('_r')
                            nomad_vizier_a.remove_column('USNO-B1')# Identifier in USNO-B1
                            nomad_vizier_a.remove_column('UCAC2')# Number in UCAC2
                            # Identifier in Tycho-2 or Hipparcos
                            nomad_vizier_a.remove_column('Tycho-2')
                            nomad_vizier_a.remove_column('f_Tycho-2')
                            # Source of position. This could potentially be useful
                            nomad_vizier_a.remove_column('r')
                            # Lower precision (ICRS)
                            nomad_vizier_a.remove_column('_RAJ2000')
                            nomad_vizier_a.remove_column('_DEJ2000')
                            nomad_vizier_a.remove_column('e_RAJ2000')# Mean error on RAdeg
                            nomad_vizier_a.remove_column('e_DEJ2000')# Mean error on DEdeg
                            # Various flags regarding the reliability of the source.
                            # This could potentially be useful. 
                            nomad_vizier_a.remove_column('Xflags')
                            # Origin of mags (potentially useful)
                            nomad_vizier_a.remove_column('r_Bmag')
                            nomad_vizier_a.remove_column('r_Vmag')
                            nomad_vizier_a.remove_column('r_Rmag')
                            nomad_vizier_a.remove_column('YM')
                            nomad_vizier_a.remove_column('pmRA')
                            nomad_vizier_a.remove_column('e_pmRA')
                            nomad_vizier_a.remove_column('pmDE')
                            nomad_vizier_a.remove_column('e_pmDE')
                            # Define the catalog as to only the recommended stars.
                            # Recommended (R) Problematic (*)
                            nomad_vizier_a.remove_column('R')

                            if self.overwrite:
                                if not reg_file_exists or self.overwriteReg:

                                    if reg_file_open:
                                        total_len += len(nomad_vizier_a)
                                    
                                    if not reg_file_open: 
                                        print('Writing to: ' + reg_fileout)
                                        f_t = open(reg_fileout,'w')
                                        reg_file_open = True
                                        f_t.write('# BRIGHT STAR CATALOG: Nicole G. Czakon\n')
                                        f_t.write('# GENERATED ON: ' + str(date.today()) + '\n')
                                        f_t.write('# TRACT: ' + tract_patch['tract']  + '\n')
                                        f_t.write('# PATCH: ' + tract_patch['patch']  + '\n')
                                        f_t.write('# FILTER: HSC-' + band + '\n')
                                        f_t.write('wcs; fk5 \n')
                                        total_len = 0
                                        
                                for star in nomad_vizier_a:

                                    #Perform the bitwise or if the Xflag is 00001 or 00002 indicating that it
                                    #has failed Blaise's test for a star #| (int(star['Xflags'][-1]) & 2)
                                    #not (int(star['Xflags'][-1]) & 1):
                                    if True:

                                        if not reg_file_exists or self.overwriteReg:
                                            min_mag = np.nanmin([star['Bmag'], \
                                                                 star['Vmag'], \
                                                                 star['Rmag']])

                                            if np.isnan(min_mag) or min_mag > 17.5:
                                                #print('Outside of magnitude range...')
                                                continue
     
                                            bsr = hscConfig.brightStarRadius['moderatelyConservative']
                                            radius = bsr['A0'] * 10**(bsr['B0'] * (bsr['C0'] - min_mag))/3600. + \
                                                     bsr['A1'] * 10**(bsr['B1'] * (bsr['C1'] - min_mag))/3600.
                                            #print(str(star['Bmag']) + ' ' + str(star['Vmag']) + ' ' + str(star['Rmag']))
                                            star['bright_star_radius'] = radius

                                            nomad1 = star['NOMAD1'].split('-')
                                            star['NOMAD1'] = nomad1[0] + nomad1[1]

                                            f_t.write('circle(' + '{0:.7f}'.format(star['RAJ2000']) + ',' + \
                                                      '{0:.7f}'.format(star['DEJ2000']) + ',' + \
                                                      '{0:.6f}'.format(star['bright_star_radius']) + \
                                                      'd) # ID: ' + star['NOMAD1'] + '\n')
                                            
                                        if not tract_patch_in_sql and self.insertSQL:

                                            #if 'Xflags' in star.colnames:
                                            #    star.colnames
                                            #    interact(local=locals())
                                            #    star.remove_column('Xflags')

                                            print('Inserting into the database....')
                                            c.execute('''INSERT INTO nomad VALUES (?,?,?,
                                            ?,?,?,
                                            ?,?,?,
                                            ?,?,?)''', star.as_void().tolist())
                                            #This is only counting items that are inserted in the database.
                                            tract_patch_completed.append(tract_patch)
                                    #else:
                                       #print('Not a star')        


                    print(str(len(nomad_vizier_a)) + ' sources found in this segment...')

                    if self.insertSQL:
                        self.conn.commit()
                    if reg_file_open:
                        f_t.close()
                        reg_file_open = False
                        print('Total length: ' + str(total_len))
                        print('f_t close')
                                # TRACT: XXXX
                                # PATCH: X,X
                                # FILTER HSC-I
                                # wcs; icrs
            f.close()
        if self.overwriteSQL:
            self.conn.commit()
            # Close the connection
            self.conn.close()

    def old_nomad(self):
        #see vizierNotes.txt for more information.
        print('\nStarting to query Vizier...')
        Vizier.VIZIER_SERVER = u'vizier.nao.ac.jp'
        nomad_cat = u'I/297' 
        #Even though I set this here, the magnitude does not change
        Vizier.ROW_LIMIT = -1#00
        #Alternative Filters: Bmag,Vmag,Rmag,Jmag,Hmag,Kmag
        nomad_vizier = []
        n_searches = 14
        #There is a bug in astroquery and it can only retrieve a maximum of 50 entries at a time.
        for i in range(n_searches):#Search through the RAs
            for j in range(n_searches):#Search through the Decs
                new_map_coord = deepcopy(self.mapCoord)
                new_map_coord.ra.deg = self.mapCoord.ra.deg + (i-n_searches/2+0.75)*0.2
                new_map_coord.dec.deg = self.mapCoord.dec.deg + (j-n_searches/2+0.75)*0.2
                new_map_coord = coord.SkyCoord(ra=float(new_map_coord.ra.deg),dec=float(new_map_coord.dec.deg), \
                                               unit=(u.deg,u.deg),frame='fk5')

                nomad_vizier_a = Vizier(column_filters={"Rmag":"5..19.0"}).query_region(new_map_coord,width="0.2d",catalog=["I/297"])
                                                                                         
                nomad_vizier_a = nomad_vizier_a['I/297/out']
                nomad_vizier_a.remove_column('_x')
                nomad_vizier_a.remove_column('_y')
                nomad_vizier_a.remove_column('_p')
                nomad_vizier_a.remove_column('_r')

                if j is 0 and i is 0:
                    nomad_vizier = deepcopy(nomad_vizier_a)
                else:
                    nomad_vizier = vstack([nomad_vizier,nomad_vizier_a])
        #In case you want more information
        print("NOMAD CATALOG INFORMATION RETRIEVED FROM VIZIER:")
        print(nomad_vizier)
        print('\r')
        #Now, we need to put the nomad catalog in the proper format to plot later on.
        nomadRA = nomad_vizier['_RAJ2000'].tolist()
        nomadDec = nomad_vizier['_DEJ2000'].tolist()
        nomadBmag = nomad_vizier['Bmag'].tolist()
        nomadVmag = nomad_vizier['Vmag'].tolist()
        nomadRmag = nomad_vizier['Rmag'].tolist()
        wcs = self.exposure.getWcs()
        interact(local=locals())
        nomad_pixel_out = [wcs.skyToPixel(afwCoord.IcrsCoord(nomadRA[i]*afwGeom.degrees, \
                                                             nomadDec[i]*afwGeom.degrees)) \
                           for i in range(len(nomadDec))]
        nomadOut = {'RA':nomadRA,'Dec':nomadDec,'pix_out':nomad_pixel_out,\
                     'Bmag':nomadBmag,'Vmag':nomadVmag,'Rmag':nomadRmag}
        if self.overwrite:
            interact(local=locals())
            pickle.dump(nomadOut,open(nomad_filename,'wb'))
        return nomadOut

    def load(self):
        print('\nLoading the SQL database...')

        # Setup the SQL datafile
        db_out_filename = hscConfig.bright_star_db
        self.conn = sqlite3.connect(db_out_filename)
        #Create a cursor object
        self.c = self.conn.cursor()
        return self.c
    
    def test(self):
        #START HERE!!! Make sure that the appropriate code is deleted. 
        #testExposure = loadTestExposure.LoadExposureTask(loadCoadd=True,filter='G',doPlot=False)
            #End of it single exposure or coadd conditional statement.
        #self.mapCoord = testExposure.mapCoord
        #self.exposure = testExposure.exposure
        print('\nStarting the test file...')
        self.overwrite = True
        nomadOut = self.run()
        return nomadOut

if __name__ == 'main':
    print('Made it to main')
    brightStars = FindNomadTask(test=True)


                #The following below is old code and can be removed once we verify that the program
                #is working.
#            with open(tract_info_file,'r') as f:

#                for line in f:

#                    if line.find('Patch:') is not -1 and line.find('Corner1') is not -1:

#                        t_line = re.findall('[^(, )]+',line)
#                        Corner1RA = float(t_line[9])
#                        Corner1Dec = float(t_line[10])

#                    if line.find('Patch:') is -1 or line.find('Corner3') is -1:
#                        continue

#                    t_line = re.findall('[^(, )]+',line)
#                    tract = t_line[1]
#                    patch = t_line[3] + ',' + t_line[4]
