#!/usr/bin/env python
#Ceated by Nicole Czkon 09102015
#This is modified from the original code in order to take into account the coadds that
# Jean felt would be better to include.
# Description of the Code:
# There are several modules that are defined in the top part of this section:
# 1. find_nomad(map_coord)
# 2. find_sat_sources
# 3. sat_star_mask
# 4. plot_cat_psf_sources(ds9,sources,zeropoint)
# 5. find_halo_sources
import os
########### For Debugging #################################################
from code import interact
import time
########### Pipeline-Related Packages #####################################
import lsst.daf.persistence as dafPersist
import lsst.afw.display.ds9 as ds9
import lsst.afw.image as afwImage
import lsst.afw.coord as afwCoord
import lsst.afw.geom as afwGeom
from lsst.coadd.utils.coadd import Coadd
import lsst.meas.astrom.astrom as measAst
import lsst.pex.config as pexConfig
### Another package in order to load the astrometry.net data #############
import lsst.meas.astrom as measAstrom
### General Python Packages ##############################################
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import astropy.units as u
import astropy.coordinates as coord
import numpy as np
from astropy.io import fits
from astropy.table import vstack
## Additional package needed in order to download the data from Vizier ##
from astroquery.vizier import Vizier
#I might not need the astroML version below.
from astroML.correlation import two_point_angular
from sklearn.neighbors import KDTree
import sqlite3
from copy import copy,deepcopy
import cPickle as pickle
from matplotlib.backends.backend_pdf import PdfPages
from scipy.interpolate import UnivariateSpline
import hscConfigurationFile as hscConfig
# PURPOSE: to calculate the radius at which the sources counts fall off.
# ADDITIONAL INFORMATION: "sources" refers to the sources detected in the HSC images.
#                         "nomad" refers to the bright-stars extracted from the nomad catalog.
#                         see "find_nomad" function. 
# OPTIONAL KEYWORDS:
# plot_2pt: This is a kludge because I want to save the plots in different files and I
#           can't figure out how to do this without saving the correlation functions in
#           a different structure.
# verbose: this gives a little more output in case we're interested in more
#          details.
# overwrite: Since it takes a while to do these calculations, it's better to
#            pickle the results whenever you decide on a configuration.
########## FINDING NOMAD SOURCES FOR VERY BRIGHT STARS  #################
def find_nomad(exposure,map_coord,overwrite=False):
    nomad_filename = hscConfig.nomad_filename
    nomad_exists = os.path.isfile(nomad_filename)
    #nomad_exists = False
    if nomad_exists and not overwrite:
        nomad_all = pickle.load(open(nomad_filename,"rb"))
    else:
        print('Starting to query Vizier...')
        Vizier.VIZIER_SERVER = u'vizier.nao.ac.jp'
        nomad_cat = u'I/297' #: u'I/259' u'The Tycho-2 Catalogue (Hog+ 2000)' "BTmag"
        #Even though I set this here, the magnitude does not change
        Vizier.ROW_LIMIT = -1#00
        #Alternative Filters: Bmag,Vmag,Rmag,Jmag,Hmag,Kmag
        nomad_vizier = []
        #This is necessary in case we need to scale the step size at all.
        search_radius = 0.04#in degrees
        n_searches = int(np.ceil(2.8/search_radius))#np.ceil(14*search_factor)
        for i in range(n_searches):#Search through the RAs
            for j in range(n_searches):#Search through the Decs
                new_map_coord = deepcopy(map_coord)
                new_map_coord.ra.deg = map_coord.ra.deg + (i-n_searches/2 + 0.75)*search_radius
                new_map_coord.dec.deg = map_coord.dec.deg + (j-n_searches/2 + 0.75)*search_radius
                new_map_coord = coord.SkyCoord(ra=float(new_map_coord.ra.deg),dec=float(new_map_coord.dec.deg), \
                                               unit=(u.deg,u.deg),frame='icrs')
                nomad_vizier_a = Vizier(column_filters={"Rmag":"5..20.0"}).query_region(new_map_coord,width=str(search_radius)+'d', \
                                                                                         catalog=["I/297"])
                if nomad_vizier_a.keys() != []:
                    nomad_vizier_a = nomad_vizier_a['I/297/out']
                    nomad_vizier_a.remove_column('_x')
                    nomad_vizier_a.remove_column('_y')
                    nomad_vizier_a.remove_column('_p')
                    nomad_vizier_a.remove_column('_r')
                    if j is 0 and i is 0:
                        nomad_vizier = deepcopy(nomad_vizier_a)
                    else:
                        nomad_vizier = vstack([nomad_vizier,nomad_vizier_a])
                    #In case you want more information
                    print(str(len(nomad_vizier_a))+' entries.')
                    if len(nomad_vizier_a) == 50:
                        print('Your search is maxed out, you should consider reducing the values in search_radius.')
                        print('\a')
                        interact(local=locals())
                else:
                    print('No bright stars found in this area, continuing...')
        print("NOMAD CATALOG INFORMATION RETRIEVED FROM VIZIER:")
        print(nomad_vizier)
        print('\r')
        #Now, we need to put the nomad catalog in the proper format to plot later on.
        nomadRA = nomad_vizier['_RAJ2000'].tolist()
        nomadDec = nomad_vizier['_DEJ2000'].tolist()
        nomadBmag = nomad_vizier['Bmag'].tolist()
        nomadVmag = nomad_vizier['Vmag'].tolist()
        nomadRmag = nomad_vizier['Rmag'].tolist()
        wcs = exposure.getWcs()
        nomad_pixel_out = [wcs.skyToPixel(afwCoord.IcrsCoord(nomadRA[i]*afwGeom.degrees, \
                                                             nomadDec[i]*afwGeom.degrees)) \
                           for i in range(len(nomadDec))]
        nomad_all = {'RA':nomadRA,'Dec':nomadDec,'pix_out':nomad_pixel_out,\
                     'Bmag':nomadBmag,'Vmag':nomadVmag,'Rmag':nomadRmag}
        pickle.dump(nomad_all,open(nomad_filename,'wb'))
    return nomad_all
#########################################################################
####### FINDING THE SSP CATALOG SOURCES THAT ARE SATURATED  #############
#########################################################################
def find_sat_sources(sources):
    sat_index = sources.get("flags.pixel.saturated.center")
    sat_sources = sources[sat_index]
    print "We found this many sources that are saturated in the center:", len(sat_sources)
    print '\r'    
    return sat_sources
#########################################################################
# This finds and plots the faint sources in the catalog #################
#########################################################################
def find_faint_sources(sources,zeropoint):
    #Other options: getPsfFlux()
    cmodel = sources.getModelFlux()
    good_cmodel = ~np.isnan(cmodel)
    #For one one of the sample images, this cmodel_range = [17.6,28.9] mag
    cmodel = zeropoint-2.5*np.log10(cmodel[good_cmodel])
    #I think that I have this additional line here as a "deepcopy" proxy.
    fs_sources = sources
    fs_sources = fs_sources[good_cmodel]
    min_mag = 25.4
    fs_sources = fs_sources[cmodel > min_mag]
    #This is just a reminder in case we want to make a separate diagnostic plot.
    #rect1 = Rectangle((x,y),width,height,alpha=0.5,color='red')
    return fs_sources
##########################################################################
# Generate the star mask by combining the different cuts on the catalogs #
# ** Optional catalogs currently includes nomad_pixel_out,psf_out. #############
##########################################################################
def sat_star_mask(exposure,**optional_catalogs):
    maskedImage = exposure.getMaskedImage()
    mask = maskedImage.getMask()
    #Make sure that we still have a plane free to use.
    original_number_of_planes = mask.getNumPlanesUsed()
    if original_number_of_planes > 15:
        print('Number of planes used is already at the maximum of 16, no more bits left, exiting...')
        raise SystemExit
    else:
        print('Number of planes used: '+str(original_number_of_planes))

    print('Creating the BRIGHT_STAR mask....')
    mask.addMaskPlane('BRIGHT_STAR')
    #Confirm the an additional mask plane has been created
    if mask.getNumPlanesUsed() is original_number_of_planes+1:
        print('New number of planes used: '+ str(mask.getNumPlanesUsed()))
        print('\r') 
    else:
        print('Unable to add new plane, exiting.....')
        raise SystemExit

    myBit =  mask.getMaskPlane("BRIGHT_STAR")

    #Find the bits for the other planes...
    SAT =  {'bit':mask.getMaskPlane("SAT"),'int':mask.getPlaneBitMask("SAT")}
    BRIGHT_STAR = {'bit':mask.getMaskPlane("BRIGHT_STAR"), \
                   'int':mask.getPlaneBitMask("BRIGHT_STAR")}
    NOT_DEBLENDED_bit = mask.getMaskPlane("NOT_DEBLENDED")
    NOT_DEBLENDED_int = mask.getPlaneBitMask("NOT_DEBLENDED")

    #We need to initialize the format of the new mask.
    mask_y =mask.getWidth()
    mask_x =mask.getHeight()

    temp_mask = afwImage.MaskU(mask_y,mask_x)
    temp_plane = temp_mask.getArray()
    
    #This is a numpy NDarray of the mask plane 
    mask_plane = mask.getArray()
    temp_mask_plane = mask_plane.copy()

    #First, find where the saturated and non-deblended bits are, as they are usually associated with bright objects.
    sat_plane = np.bitwise_and(mask_plane,SAT['int']) << (BRIGHT_STAR['bit'] - SAT['bit'])
    not_deblended_plane = np.bitwise_and(mask_plane,NOT_DEBLENDED_int) << (BRIGHT_STAR['bit'] - NOT_DEBLENDED_bit)

    #The bitwise_or is the equivalent of taking the union of the two planes.
    my_plane = np.bitwise_or(sat_plane,not_deblended_plane)
    my_plane = np.bitwise_or(mask_plane,my_plane)
    
    ### This is where you want to add in a mask for each of the sources
    temp_size = 50
    max_X = my_plane.shape[1]
    max_Y = my_plane.shape[0]
    mag_radius = 20#2.0*(-1.860*nomadmag[i]+60.00)#*afwGeom.arcminutes#From Anderson, 2012
    for cat_x in optional_catalogs:
        for source in optional_catalogs[cat_x]:
            temp_X = int(source.getX())
            temp_Y = int(source.getY())
            if temp_X<max_X and temp_Y<max_Y and temp_X>0 and temp_Y>0:
                    if temp_X+temp_size > max_X:
                        size_XB = max_X - temp_X
                    else:
                        size_XB = temp_size
                    if temp_X-temp_size < 0:
                        size_XA = temp_X
                    else:
                        size_XA = temp_size
                    if temp_Y+temp_size > max_Y:
                        size_YB = max_Y - temp_Y
                    else:
                        size_YB = temp_size
                    if temp_Y-temp_size < 0:
                        size_YA = temp_Y
                    else:
                        size_YA = temp_size
                        #2**BRIGHT_STAR['bit']  
                        my_plane[temp_Y-size_YA:temp_Y+size_YB,temp_X-size_XA:temp_X+size_XB] =BRIGHT_STAR['int']
    temp_plane |=  my_plane
    mask <<=  temp_mask
    #mask_plane = np.bitwise_or(mask_plane,sat_plane)
    check_plane = np.bitwise_and(temp_plane,BRIGHT_STAR['int'])
    # This is optional information to print to the command line.
    #print('Proposed mask[0,0]:      '+str(check_plane[0,0]))
    #print(mask.getPlaneBitMask("BRIGHT_STAR"))
    #print(myBit)
    maskedImage = exposure.getMaskedImage()
    mask = maskedImage.getMask()# OK
    mask_plane = mask.getArray()#this is a numpy NDarray (OK!)
    check_plane = np.bitwise_and(mask_plane,myBit)
    # This is optional information to print to the command line.
    #print('New mask[0,0]: '+str(check_plane[0,0]))
    #print(mask.getPlaneBitMask("BRIGHT_STAR"))
    #print(myBit)
    # NGC 09092015, I'm not sure if the stuff that I've commented below is still necessary.
    # figure out what it is and then remove it.
    if 0:
        #    print('Pixel ['+str(mask_x)+']')
        for mask_yy in range(mask_y):
            if 0:
                print('Check to see if this is true, then it will work faster')
                print(satMask[mask_xx,mask_yy])
                #print(mask.getMaskPlane("BRIGHT_STAR"))
                #mask.setMaskPlaneValues(BS_bit,mask_xx,mask_yy,mask_xx)
                #print(mask_plane[mask_xx,mask_yy])
                mask[mask_xx,mask_yy] |= myBit
                #print(myBit)
                #print(mask.getMaskPlane("BRIGHT_STAR"))
                #print(mask_plane[mask_x,mask_y])
                #if 1: #For debugging
                #print(mask_plane[mask_xx,mask_yy])
                #print(len(mask_plane_binary2))
                #if 1:#len(mask_plane_binary2) > 11:
                #    print(str(BS_bit))
                #    print(bool(int(mask_plane_binary2[-BS_bit-1])))
                #### We have to think what the ra and decs are....I think that this needs to be the parent. 
    ## rad = 0.05*afwGeom.degrees
    ## temp_center = wcs.pixelToSky(mask_y/2,mask_x/2)
    #remove the saturated bit just for debugging purposes.
    #cat = astrom.getReferenceSources(temp_center.toIcrs().getRa(),temp_center.toIcrs().getDec(), rad, "r")
    ############################################################################
    # Clear the other maskplanes to make sure that the bright-star mask was created properly
    mask.clearMaskPlane(mask.getMaskPlane("SAT"))
    mask.clearMaskPlane(mask.getMaskPlane("INTRP"))
    mask.clearMaskPlane(mask.getMaskPlane("DETECTED"))
    mask.clearMaskPlane(mask.getMaskPlane("SUSPECT"))
    mask.clearMaskPlane(mask.getMaskPlane("BAD"))
    mask.clearMaskPlane(mask.getMaskPlane("CROSSTALK"))
    #This has an element of the star mask!
    mask.clearMaskPlane(mask.getMaskPlane("NOT_DEBLENDED"))
    ######################## END OF BRIGHT STAR MASKING #########################
    return mask_y,mask_x
def plot_star_mask_sources(ds9,**optional_catalogs):
    ######################## PLOTTING NOMAD SOURCES (keep)  ###################################
    mag_radius = 20#2.0*(-1.860*nomadmag[i]+60.00)#*afwGeom.arcminutes#From Anderson, 2012
    symbol='o'
    #tyc2RA = result['I/259/tyc2']['_RAJ2000'].tolist()
    #tyc2Dec = result['I/259/tyc2']['_DEJ2000'].tolist()
    for cat_x in optional_catalogs:
        if cat_x is 'nomad_pixel_out':
            ds9_color = 'green'
        if cat_x is 'ssp_sat':
            ds9_color = 'red'#'blue','cyan','magenta','orange','red','yellow'
#        nomadRA = optional_catalogs[cat_x]['I/297/out']['_RAJ2000'].tolist()
#        nomadDec = optional_catalogs[cat_x]['I/297/out']['_DEJ2000'].tolist()
#        nomadmag = optional_catalogs[cat_x]['I/297/out']['Rmag'].tolist()
        #nomadRA = nomad_pixel_out['I/297/out']['_RAJ2000'].tolist()
        #nomadDec = nomad_pixel_out['I/297/out']['_DEJ2000'].tolist()
        #nomadmag = nomad_pixel_out['I/297/out']['Rmag'].tolist()
        #sample_icrs_coord = [afwCoord.IcrsCoord(nomadRA[i]*afwGeom.degrees,nomadDec[i]*afwGeom.degrees) \
        #                     for i in range(len(nomadDec))]
        #sample_pixel_coord = [wcs.skyToPixel(sample_icrs_coord[i]) for i in range(len(nomadDec))]
 #       for i in range(len(nomadDec)):
        for source in optional_catalogs[cat_x]:
            ds9.dot(symbol,source.getX(),source.getY(),ctype=ds9_color,size=mag_radius,frame=1,silent=True)

#    if 'ssp_out' in optional_catalogs:
#        for source in ssp_out:
#            ds9.dot(symbol,source.getX(),source.getY(),ctype=ds9.RED,size=15,frame=1,silent=True)
    
    return ds9
###################################################################################
########################## MAIN PROGRAGM ##########################################
###################################################################################
dataDir = hscConfig.runDir#os.environ['runDir']
#runDir = '/array/users/czakon/lustre/Subaru/SSP/rerun/yasuda/SSP3.8.5_20150725'
butler = dafPersist.Butler(dataDir)
single_exposure = 0
#This is for a single CCD
if single_exposure:
    #Something wrong with the g & r_visit?
    visit =  int(os.environ['z_visit'])#works: g,r,i,z,y
#    visit =  int(os.environ['test_visit'])
    ccd =  int(os.environ['test_ccd'])
    dataId = {"visit" : visit, "ccd": ccd}
    exposure = butler.get("calexp",dataId)
#This is for a coadd, which is where we are going to begin our science
else:
    #tract =  int(os.environ['test_tract'])
    #patch =  int(os.environ['test_patch'])
    #9813,
    tract = 8524
    patch = '1,7'
    dataId = {'filter':'HSC-G','tract':tract,'patch':patch}
    exposure = butler.get("deepCoadd",dataId,immediate=True)
    #exposure = butler.get("deepCoadd",dataId,immediate=True)
#### Now query Vizier for the objects in the catalog ###
#This needs to be included when running this at IPMU because the server gets confused with the French address for some reason.
#The issue with Tycho catalog is that it is only within 9.78 and 13.198 magnitude and we go much deeper than that.
#temporarily comment this out until it's installed
#
#The folowing is some demo code on how I discovered the name of the Tycho-2 catalog
#catalog_list = Vizier.find_catalogs('Hog');from the Hog (2000) paper
#sdss_list = Vizier.find_catalogs('Ivezic+ 2007')#Does not exist?
#sdss_list = Vizier.find_catalogs('Ivezic+ 2007')u'J/ApJ/644/829'
#print(catalog_list)
#print({k:v.description for k,v in catalog_list.iteritems()})
#sdss_bs_cat = u'J/AJ/134/2398'

#This does not include a cut on magnitude
if 0:
########################################################
# Since the Tycho2 Catalog Doesn't seem extensive enough, let's see if the Ivezic SDSS catalog has more sources
# The declination is |Dec(J2000)|<1.26 for stripe 82
    hdulist = fits.open('/array/users/czakon/ivezic2007.fits')#memmap=True#rather than all into memory at once
    #hdulist = fits.open('/home/nicole/local_data/ivezic2007.fits')#memmap=True#rather than all into memory at once
    hdu_data = hdulist[1].data
    iv_gmag = hdu_data['g_mmed']
    iv_mask = (hdu_data['RA(J2000 degrees)'] > min(RAs)) & (hdu_data['RA(J2000 degrees)'] < max(RAs)) & (hdu_data['Dec(J2000 degrees)'] > min(Decs)) & (hdu_data['Dec(J2000 degrees)'] < max(Decs))
    hdulist.close()
#start here, include call to bright star mask

if single_exposure:
    sources = butler.get('src',dataId)
    metadata = butler.get('calexp_md',dataId)
    zeropoint = 2.5*np.log10(metadata.get('FLUXMAG0'))
else:
    coadd = butler.get('deepCoadd',dataId)
    #ccdInputs = coadd.getInfo().getCoaddInputs().ccds
    #visitInputs = coadd.getInfo().getCoaddInputs().visits
    #print ccdInputs.schema
    #print ccdInputs.get("visit")
    #config_astrom = pexConfig.ConfigField(dtype=measAst.MeasAstromConfig,doc="Configuration for readMatches")
    #config_astrom.filterMap = {'B':'g','V':'r','R':'r','I':'i','y':'z'}
    #astrom = measAst.Astrometry(config_astrom)
    #Need to download the data for this field still
    sources = butler.get('deepCoadd_meas',dataId,immediate=True)
    ######################## PLOTTING IN DS9  ###################################
settings = {'scale':'zscale','zoom':'to fit','mask':'transparency 60'}
##############################################################################################
######################## STEP 1: FIND THE CORRELATION MATRIX  ################################
##############################################################################################
#ds9 = plot_panstarrs(ds9,mask_y,mask_x)
######################## PLOTTING IN MATPLOTLIB  ###################################
#ds9 = plot_cat_psf_sources(ds9,sources,zeropoint)
#sources_RADec =  np.array([[0,0]])
sources_RA = []
sources_Dec = []
for source in sources:
    sources_RA.append(source.getRa().asDegrees())
    sources_Dec.append(source.getDec().asDegrees())
    #sources_RADec = np.append(sources_RADec,[[source.getRa().asDegrees(),source.getDec().asDegrees()]],axis=0)
# Find the edges of the bounding box, we will use this information to search other catalogs with
# For some reason, the values on the bbox are not correct
use_bbox = 0
if use_bbox:
    wcs = exposure.getWcs()
    bbox = exposure.getBBox().getCorners()
    bbox_icrs = [wcs.pixelToSky(bbox[i].getX(),bbox[i].getY()).toIcrs() for i in range(4)]
    Decs  =[bbox_icrs[i].getDec().asDegrees() for i in range(4)]
    RAs = [bbox_icrs[i].getRa().asDegrees() for i in range(4)]
    centerRA = np.mean(RAs)
    centerDec = np.mean(Decs)
else:
    centerRA = np.mean(sources_RA)
    centerDec = np.mean(sources_Dec)
map_coord = coord.SkyCoord(ra=float(centerRA),dec=float(centerDec),unit=(u.deg,u.deg),frame='icrs')
#RA_nomad,Dec_nomad,nomad_pixel_out = find_nomad(exposure,map_coord,overwrite=1)
nomad_all = find_nomad(exposure,map_coord,overwrite=True)
#This code was moved to: brightStarCorrelationFunction.py
#find_name = two_pt_correlation_fxn(nomad_all)
ssp_sat = find_sat_sources(sources)
#fs_sources = find_faint_sources(sources,zeropoint)
mask_y,mask_x =  sat_star_mask(exposure,nomad_pixel_out=nomad_all['pix_out'],ssp_sat=ssp_sat)
#write_source_cutout is temporary code that I'm using in order to figure out how to stack images.
#temp_out = write_source_cutout(ds9,exposure,ssp_sat)
temp_out = write_source_cutout(ds9,exposure,nomad_all['pix_out'])
ds9.setMaskPlaneColor('BRIGHT_STAR',color=ds9.BLUE)
ds9.mtv(exposure,frame=1,title='My Data',settings=settings)
ds9 = plot_star_mask_sources(ds9,nomad_pixel_out=nomad_all['pix_out'],ssp_out=ssp_sat)#,fs_out=fs_sources)
#This is repeated, 08/21/2015, I got rid of this so that it is not necessary.
#ds9,nomad_pixel_out = plot_nomad(ds9,map_coord)
#ds9,ssp_out = find_sat_sources(ds9,sources,zeropoint)
################ END OF MAIN PROGRAM, THE REST ARE EXAMPLES OF OLD CODE ############
'''
# def two_pt_correlation_fxn: THIS HAS MOVED TO "brightStarCorrelationFunction.py"
def two_pt_correlation_fxn(nomad_all,verbose=False,overwrite=False):
    mag_models = ['cmodel','kron']
    j_model = 0
    plot_folder = hscConfig.mask_plot_folder
    corr_filename = hscConfig.corr_filename + mag_models[j_model] + '.p'
    core_file_exists = os.path.isfile(corr_filename)
    corr_cutoff = 0.2
    if core_file_exists and not overwrite:
        corr_all = pickle.load(open(corr_filename,"rb"))
        min_mags = corr_all['min_mags']
        max_mags = corr_all['max_mags']
        nsrc_mags = len(min_mags)
        min_mag_nomad_bins = corr_all['min_mag_nomad_bins']
        nnomad_bins = len(min_mag_nomad_bins)-1
        bins =  corr_all['bins']
        nbins = len(bins)
        bands =  corr_all['bands']
        #Below is only temporary!
        #if mag_models[j_model] == 'kron':
            #I'm not sure why there is no 'g'
        #    bands = ['g','r','i','z','y']
        #else:
        #    bands = ['g','r','i','z','y']            
        nbands = len(bands)
        nsources =  corr_all['nsources']
        nnomad =  corr_all['nnomad']
        med_nomad =  corr_all['med_nomad']
        counts_sources_sq = corr_all['corr']['counts_sources_sq']
        counts_nomad_sq =  corr_all['corr']['counts_nomad_sq']
        counts_sources_nomad = corr_all['corr']['counts_sources_nomad']
        best_fit_arcmin = corr_all['corr']['best_fit_arcmin']
        corr_sources_nomad =  corr_all['corr']['corr_sources_nomad']
    else:
        if mag_models[j_model] == 'kron':
            #I'm not sure why there is no 'g'
            bands = ['r','i','z','y']
        else:
            bands = ['g','r','i','z','y']            
        #Let's start at one arcsec and then build up to 30 arcsec.
        min_bin_arcsec = 1.0
        max_bin_arcsec = 300.0
        n_bins = 20
        #Need both a min and a max, otherwise, the datasets become too large.
        min_mags = [21.0,22.0,23.00,24.00,25.00,26.00,26.50]
        #max_mags = [22.5,23.0,23.75,24.30,25.25,26.15,27.15]
        max_mags = {'kron':{'g':[23.0,24.0,23.75,24.50,25.25,26.15,27.15], \
                            'r':[23.0,23.5,23.75,24.30,25.25,26.25,27.15], \
                            'i':[23.0,23.0,23.75,24.30,25.25,26.5,27.5], \
                            'z':[22.5,23.0,23.75,24.30,25.40,26.5,27.5], \
                            'y':[22.5,23.0,23.75,24.30,25.40,26.6,28.0]}, \
                    'cmodel':{'g':[22.5,23.0,23.75,24.30,25.25,26.5,27.15], \
                              'r':[23.0,23.5,23.75,24.40,25.25,26.25,27.15], \
                              'i':[22.5,23.0,23.75,24.30,25.25,26.25,27.15], \
                              'z':[22.25,22.8,23.50,24.30,25.20,26.5,27.5], \
                              'y':[22.25,22.8,23.50,24.30,25.25,26.5,27.5]}}
                                                
        #For the bright-star mags, we only need to take the difference b/n adjacent bins.
        min_mag_nomad_bins = [5.0,7.5,10.0,12.5,14.5,17.5]
        #Convert the nomad values to something that's easier to manipulate in numpy.
        print('Finding the brightest NOMAD magnitude in the B/V/ and R bands.')
        Bmag_nomad = np.array(nomad_all['Bmag'])
        Vmag_nomad = np.array(nomad_all['Vmag'])
        Rmag_nomad = np.array(nomad_all['Rmag'])
        all_nomad = np.vstack((Bmag_nomad,Vmag_nomad,Rmag_nomad))
        min_mag_nomad = np.amin(all_nomad,axis=0)
        RA_nomad = np.array(nomad_all['RA'])
        Dec_nomad = np.array(nomad_all['Dec'])
        sql_source_db = hscConfig.sql_source_db
        ##############################################################################
        # Open up the SQL database that I built elsewhere
        ##############################################################################
        #Create a connection object, this needs to be soft-coded!
        conn = sqlite3.connect(sql_source_db) 
        #Create a cursor object
        c = conn.cursor()
        column_names = np.array([])
        for row in c.execute('PRAGMA table_info(xmm_lss)'):
            column_names = np.append(column_names,row[1])
        print(column_names)
        print('Figure out how to extract the PFS information.')
        interact(local=locals())
        indices = {'RA':np.where(column_names == 'RA_J2000_degrees')[0][0], \
            'Dec':np.where(column_names == 'Dec_J2000_degrees')[0][0], \
            #                   'shape_sdss_psf':np.where(column_names == 'shape_sdss_psf')[0][0], \
            'gmag_cmodel':np.where(column_names == 'gmag_cmodel')[0][0]}
        # and if you want to know a little more about the database, here is some more
        # output. Select verbose=True in the keywords.
        if verbose:
            for row in c.execute(SELECT Count(*) FROM xmm_lss'):
                print(str(row[0])+' rows in the XMM_LSS_sql.')
            # This counts all of the different tracts in the database.
            # Only two distinct tracts for now, hopefully this changes when I make a new query.
            for row in c.execute('SELECT Count(DISTINCT tract) FROM xmm_lss'):
                print(str(row[0])+' distinct tracts.')
            # There should be tracks 8524,8525, 8766, and 8767 in this field.
            for row in c.execute('SELECT DISTINCT tract FROM xmm_lss'):
                print('Tract '+str(row[0])+' is included.')
            #This is a good way to peruse the SQL database.
            print('Extracting all of the RA and Dec information from the table, this takes a while....')
            # Kind of chokes after 500,000, so, you'll have to be more clever with that dataset.
            # Create the bin array for the correlation function.
        bins = np.array(0)
        bins = np.append(bins,np.logspace(np.log10(min_bin_arcsec/3600.),np.log10(max_bin_arcsec/3600.),n_bins))
        bins_sqr = bins**2
        bins_area = np.pi*(bins_sqr[1::]-bins_sqr[0:-1])*(3600)**2
        nbins = len(bins)
        nbands = len(bands)
        nnomad_bins = len(min_mag_nomad_bins)-1
        nsrc_mags = len(min_mags)
        data_type = type(np.ndarray(nbins-1))
        #Technically, this doesn't have to be counted for every nomad bin. Consider removing if slowing the program down.
        counts_sources_sq = np.ndarray(shape=(nbands,nnomad_bins,nsrc_mags),dtype=data_type)
        #Technically, this doesn't have to be counted for every band bin or src bin.
        counts_nomad_sq = np.ndarray(shape=(nbands,nnomad_bins,nsrc_mags),dtype=data_type)
        counts_sources_nomad = np.ndarray(shape=(nbands,nnomad_bins,nsrc_mags),dtype=data_type)
        corr_sources_nomad = np.ndarray(shape=(nbands,nnomad_bins,nsrc_mags),dtype=data_type)
        best_fit_arcmin =  np.ndarray(shape=(nbands,nnomad_bins,nsrc_mags),dtype=float)
        nnomad =  np.ndarray(shape=(nnomad_bins),dtype=float)
        med_nomad =  np.ndarray(shape=(nnomad_bins),dtype=float)
        nsources =  np.ndarray(shape=(nbands,nsrc_mags),dtype=float)
        #Cycle through the GRIZY bands
        for j_band in range(nbands):
            #Cycle individually through the Rmags of the bright-sources and the mag bins of the HSC sources.
            for j_mag in range(nsrc_mags):        
                RADec_sources = np.array([[0,0]])
                RA_sources = []
                Dec_sources = []
                #for row in c.execute('SELECT * FROM xmm_lss WHERE rmag_cmodel>=? AND rmag_cmodel<=?', \
                    ###
                sql_command = 'SELECT * FROM xmm_lss WHERE '+bands[j_band]+'mag_'+mag_models[j_model]+'>=? AND ' + \
                              bands[j_band]+'mag_'+mag_models[j_model]+'<=? '
                for row in c.execute(sql_command,[min_mags[j_mag], \
                                                  max_mags[mag_models[j_model]][bands[j_band]][j_mag]]):
                    if (len(RA_sources) % 10000) is 0:
                        print(str(len(RA_sources))+' entries inserted.')
                    RA_sources.append(row[indices['RA']])
                    Dec_sources.append(row[indices['Dec']])
                    RADec_sources = np.append(RADec_sources,[[row[indices['RA']],row[indices['Dec']]]],axis=0)
                #END OF: for row in c.execute(sql_command,[min_mags[j_mag],max_mags[j_mag]])
                RADec_sources_dict = {'RA':RA_sources,'Dec':Dec_sources}
                nsources[j_band][j_mag] = len(RA_sources)
                print('A total of '+ str(nsources[j_band][j_mag])+' entries have been inserted.')
                if nsources[j_band][j_mag] == 0:
                    print('No valid sources returned...skipping to next set of parameters...')
                else:
                    #Remove the first entry.
                    RADec_sources = RADec_sources[1::]
                    ##############################################################################
                    # Use the scikit to get started, this can be sophisticated if we look at
                    # the examples in astroML (although it doesn't have the full functionality).
                    ##############################################################################
                    for j_nomad in range(nnomad_bins):
                        good_nomad = np.where((min_mag_nomad >= min_mag_nomad_bins[j_nomad]) & \
                                              (min_mag_nomad < min_mag_nomad_bins[j_nomad+1]))
                        med_nomad[j_nomad] = np.median(min_mag_nomad[good_nomad])
                        nnomad[j_nomad] = len(good_nomad[0])
                        RA_nomad_bin = RA_nomad[good_nomad]
                        Dec_nomad_bin = Dec_nomad[good_nomad]
                        RADec_nomad_bin = np.vstack((RA_nomad_bin,Dec_nomad_bin)).T
                        R_RADec_sources = copy(RADec_sources)
                        R_RADec_nomad_bin = copy(RADec_nomad_bin)
                        #setup -v -j -r /path/to/maskUtils
#                        drawRandoms.py DATADIR --rerun RERUNDIR --id tract=TRACT patch=PATCH filter=HSC-{G,R,I,Z,Y} --config Nden=100 fileOutName=ran.fits
                        #START HERE! FIGURE OUT A WAY TO MAKE BETTER RANDOMS.
                        np.random.shuffle(R_RADec_sources[:,0])
                        np.random.shuffle(R_RADec_nomad_bin[:,0])
                        #from sklearn.neighbors import KDTree
                        kdtree_sources = KDTree(RADec_sources)
                        R_kdtree_sources = KDTree(R_RADec_sources)
                        kdtree_nomad = KDTree(RADec_nomad_bin)
                        R_kdtree_nomad = KDTree(RADec_nomad_bin)
                        #Now, perform the two_point correlation fxn
                        #max 3.3E10
                        counts_sources_sq[j_band][j_nomad][j_mag] =  \
                                np.diff(kdtree_sources.two_point_correlation(RADec_sources,bins)) / bins_area
                        #max 3.0E8
                        counts_nomad_sq[j_band][j_nomad][j_mag] =  \
                                np.diff(kdtree_nomad.two_point_correlation(RADec_nomad_bin,bins)) / bins_area
                        #max 1.8E10
                        counts_sources_nomad[j_band][j_nomad][j_mag] = \
                                np.diff(kdtree_sources.two_point_correlation(RADec_nomad_bin,bins)) / bins_area
                        #max 4.6E8
                        # I don't think that I need to store the randoms right now.
                        counts_rsources_nomad = \
                                np.diff(R_kdtree_sources.two_point_correlation(RADec_nomad_bin,bins)) / bins_area
                        #max 3.3E8
                        #This is simple used as a cross-check.
                        #counts_sources_rnomad = \
                            #       np.diff(kdtree_sources.two_point_correlation(R_RADec_nomad_bin,bins)) / bins_area
                        #W = D1*D2/(D2*R1)-1
                        corr_sources_nomad[j_band][j_nomad][j_mag] =  \
                                counts_sources_nomad[j_band][j_nomad][j_mag]/counts_rsources_nomad-1
                        #I've verified that this gives the same answer as well
                        #W2 = counts_sources_nomad/counts_sources_rnomad-1
                        #I've confirmed that these give the same outputs.
                        #counts_nomad_sources = np.diff(kdtree_nomad.two_point_correlation(RADec_sources,bins))
                        #Keep the following plot for the time being
                        #print('Stop here to examine how to find the spline fit.')
                        #Find the spot where the source counts equal approximately 80% of their overall value.
                        #source_fraction = 0.55
                        temp = bins*3600.>3.0
                        x_bins = np.where(temp==True)[0]
                        good_bins = bins[x_bins]
                        #x_halo = np.abs(counts_sources_nomad - source_fraction*counts_sources_nomad[-1])
                        temp_min = good_bins[np.argmax(counts_sources_nomad[j_band][j_nomad][j_mag][x_bins[0:-1]])+1]*3600
                        best_fit_arcmin[j_band][j_nomad][j_mag] = temp_min
                        corr_all = {'bins':bins, 'bands':bands, \
                                    'min_mags':min_mags,'max_mags':max_mags,'nsources':nsources, \
                                    'min_mag_nomad_bins':min_mag_nomad_bins,'nnomad':nnomad,'med_nomad':med_nomad, \
                                    'corr':{'best_fit_arcmin':best_fit_arcmin, \
                                            'counts_nomad_sq':counts_nomad_sq, \
                                            'counts_sources_sq':counts_sources_sq, \
                                            'counts_sources_nomad':counts_sources_nomad, \
                                            'corr_sources_nomad':corr_sources_nomad, \
                                    }}
                        #Hopefully the above works properly.
                        #corr_all = {'min_mags':min_mags,'max_mag':max_mags,''}    
                    #END OF: for j_nomad in range(nnomad_bins)
                #END OF: if nsources[j_band][j_mag] != 0
            #END OF: for j_mag in range(nsrc_mags):
        #END OF: for j_band in range(nbands):
        pickle.dump(corr_all,open(corr_filename,'wb'))
        ################################################################################################
        # Close the sql connection
        conn.close()
    #END OF: if ~core_file_exists or  overwrite
    ########################################################################
    # PLOT 1: Plot the correlationfxn.
    # Generate a random version of both RADec_sources and alt_RADec_nomad
    # min_mag_nomad_bins = [12.0,14.4,15.6,16.4,17.0,17.0,17.51]
    # Cycle through the magnitudes again
    # This is currently a kludge because I am too lazy to figure out how to plot
    # this to different pdf files simulatanteously.
    ########################################################################
    # Start here, we need to configure this to read in the pickles file.
    all_plots = [False,True]
    halo_radius_final =  np.ndarray(shape=(nbands,nnomad_bins,nsrc_mags),dtype=float)
    false_detection_radius =  np.ndarray(shape=(nbands,nnomad_bins,nsrc_mags),dtype=float)
    false_detection_max =  np.ndarray(shape=(nbands,nnomad_bins,nsrc_mags),dtype=float)
    for plot_2pt in all_plots:
        #Set up the pdf files.
        if plot_2pt:
            two_pt_filename = plot_folder + mag_models[j_model] + '_xmm_lss_2pt_corr.pdf'
            two_pt_pdf = PdfPages(two_pt_filename)
        else:
            cross_corr_filename = plot_folder + mag_models[j_model] + '_xmm_lss_cross_correlation.pdf'
            cross_corr_pdf = PdfPages(cross_corr_filename)
        for j_band in range(nbands):
            all_mag_string = np.array([])
            for j_mag in range(nsrc_mags):
                check_corr = corr_sources_nomad[j_band][0][j_mag]
                if check_corr==None:
                    break
                #Make a separate page for every range of source magnitudes.
                plt.clf()
                plt.xlabel('arcseconds')
                temp_title = ' sources with mag = ['+str(min_mags[j_mag])+','+ \
                             str(max_mags[mag_models[j_model]][bands[j_band]][j_mag])+']'
                alt_title = '['+str(min_mags[j_mag])+','+ \
                             str(max_mags[mag_models[j_model]][bands[j_band]][j_mag])+']('+str(nsources[j_band][j_mag])+')'
                all_mag_string = np.append(all_mag_string,alt_title)
                plot_title_j_mag = mag_models[j_model] + ' ' + bands[j_band]+' band: '+ \
                                   str(nsources[j_band][j_mag])+ temp_title
                plt.title(plot_title_j_mag)
                if plot_2pt:
                    plt.ylabel('2_pt correlation function: counts/bin/arcsec$^2$')
                    plt.ylim([1E-4,1E2])
                else:
                    plt.ylabel('cross-correlation function: D1D2/D2R1 - 1')
                    plt.ylim([-1.3,3.0])
                    #Plot horizontal lines to see where we make the cutoff.
                    plt.axhline(y=corr_cutoff,ls='--',c='k')#,label='')
                    plt.axhline(y=-corr_cutoff,ls='--',c='k')#,label='')
                    #Cycle through the different bright-star magnitude bins
                for j_nomad in range(nnomad_bins):
                    xvalues = bins[1::]*3600
                    yvalues = corr_sources_nomad[j_band][j_nomad][j_mag]
                    max_mag_string = '['+ str(min_mag_nomad_bins[j_nomad])+','+ \
                                  str(min_mag_nomad_bins[j_nomad+1])+']'
                    max_mag_string = max_mag_string + '(' + str(nnomad[j_nomad]) + ')'
                    rmag_legend_title = ' Nomad(Max B,V,R)\nmag [min,max](nsources)'
                    if plot_2pt:
                        plt.loglog(bins[1::]*3600,counts_sources_nomad[j_band][j_nomad][j_mag], \
                                   label=max_mag_string,marker='o')
                        #plt.axvline(x=best_fit_arcmin[j_nomad][j_mag])#,label='Estimated halo extent.')
                        #Keep these below in case they are interesting later on.
                        #plt.loglog(bins[1::]*3600,counts_sources_sq[j_nomad][j_mag], \
                            #           label='2 pt: counts$^2$',marker='o')
                        #plt.loglog(bins[1::]*3600,counts_nomad_sq[j_nomad][j_mag], \
                            #           label='2 pt: bright-stars$^2$',marker='o')
                        #plt.loglog(bins[1::]*3600,counts_nomad_sources,'--')
                    else:
                        if len(xvalues) != len(yvalues):
                            print('The number of radial bins does not match the number of correlation measurements.')
                            interact(local=locals())
                        line = plt.semilogx(xvalues,yvalues, \
                                     label=max_mag_string,marker='o')
                        do_spline_fit = True
                        good_y = np.isfinite(yvalues)
                        if do_spline_fit:
                            new_x = np.logspace(0,3,200)
                            spl = UnivariateSpline(xvalues[good_y],yvalues[good_y],k=4,s=0.025,check_finite=True)
                            spl_roots = spl.derivative().roots()
                            if len(spl_roots) > 0:
                                max_root = np.argmax(spl(spl_roots))
                                #plt.plot(new_x,spl(new_x),c='k')
                                plt.scatter(spl_roots[max_root],spl(spl_roots[max_root]),marker='x',c='k',s=60)
                                false_detection_radius[j_band][j_nomad][j_mag] = spl_roots[max_root]
                                false_detection_max[j_band][j_nomad][j_mag] = spl(spl_roots[max_root])
                            else:
                                false_detection_radius[j_band][j_nomad][j_mag] = 300.
                                false_detection_max[j_band][j_nomad][j_mag] = np.max(yvalues[good_y])
                        halo_radius_x = len(yvalues[good_y])
                        if np.max(yvalues[good_y]) > -corr_cutoff:
                            halo_radius_x = np.max(np.where(np.abs(yvalues[good_y]) > corr_cutoff))
                            while halo_radius_x > len(yvalues[good_y])-2 and halo_radius_x > 5:
                                halo_radius_x = np.max(np.where(np.abs(yvalues[good_y]) > corr_cutoff))
                                good_y = good_y[0:-1]
                            halo_radius_x = np.min([halo_radius_x,len(yvalues[good_y])-2])
                        else:
                            halo_radius_x = len(yvalues[good_y])-2
                        #print halo_radius_x
                        #print len(yvalues[good_y])-2
                        halo_radius_x = [np.max(halo_radius_x),np.max(halo_radius_x)+1]
                        xhalo_radius = xvalues[good_y][halo_radius_x]
                        yhalo_radius = yvalues[good_y][halo_radius_x]
                        slope = np.diff(yhalo_radius)/np.diff(xhalo_radius)
                        if yhalo_radius[0] > 0.0:
                            newy = corr_cutoff
                        else:
                            newy = -corr_cutoff
                            
                        halo_radius_final[j_band][j_nomad][j_mag] = np.min([(newy-yhalo_radius[0])/slope+xhalo_radius[0],xvalues[-1]])
                        #plt.scatter(halo_radius_final[j_band][j_nomad][j_mag],newy, \
                        #            marker='x',c='r',s=100)
                        #from itertools import cycle
                        #color_cycle = cycle(['g','b','r','c'])
                        line_color = line[0].get_color()
                        plt.axvline(x=halo_radius_final[j_band][j_nomad][j_mag],c=line_color,ls='--')
                        plt.xlim(xvalues[0],xvalues[-1])
                        plt.ylim([-1.3,3.0])
                        #        label='correlation: sources from bright-stars',marker='o')
                if plot_2pt:
                    plt.legend(loc = 'lower left',title=rmag_legend_title,framealpha=0.5,fontsize=10)
                    two_pt_pdf.savefig()
                else:
                    plt.legend(loc = 'upper right',title=rmag_legend_title,framealpha=0.5,fontsize=10)
                    cross_corr_pdf.savefig()

            if not plot_2pt:
                plt.clf()
                plt.semilogy(med_nomad,halo_radius_final[j_band],marker='o')
                bf = 280.*10**(0.16*(7.0-med_nomad))
                bf_jp = (med_nomad/26.0)**(-3.4)
                plt.semilogy(med_nomad,bf,marker='x',ls='--')
                legend_title = 'SSP ' + bands[j_band]+' band: mag [min,max](nsources)'
                legend_entries = all_mag_string
                legend_entries = np.append(legend_entries,'SSP approximation')
                plt.semilogy(med_nomad,bf_jp,marker='x',ls='--')
                legend_entries = np.append(legend_entries,'Coupon bf')
                plt.xlabel('Nomad magnitude')
                plt.ylabel('Halo Radius (arcseconds)')
                plt.title('Suggested bright-star radius for different source magnitudes.')
                plt.legend(legend_entries,loc = 'lower left',title= legend_title,framealpha=0.5)
                plt.ylim([3E-1,4E2])
                cross_corr_pdf.savefig()
                plt.clf()
                plt.semilogy(med_nomad,false_detection_max[j_band],marker='s')
                plt.xlabel('Nomad magnitude')
                plt.ylabel('Max. Correlation Coefficient')
                plt.title('False detection indicator')
                legend_title = 'SSP ' + bands[j_band]+' band: mag [min,max](nsources)'
                plt.legend(all_mag_string,loc = 'upper left',title= legend_title,framealpha=0.5)
                plt.ylim([5E-3,1E3])
                cross_corr_pdf.savefig()
        if plot_2pt:
            two_pt_pdf.close()
        else:
            for jj_mag in range(nsrc_mags):
                #halo_radius_25mag = halo_radius_final[0::,0::,np.where(np.array(min_mags) == 25.0)[0][0]]
                plt.clf()
                halo_radius_25mag = halo_radius_final[0::,0::,jj_mag]
                plt.semilogy(med_nomad,np.transpose(halo_radius_25mag),marker='o')
                legend_entries = bands
                plt.semilogy(med_nomad,bf,marker='x',ls='--')
                legend_entries.append('SSP approximation')
                plt.semilogy(med_nomad,bf_jp,marker='x',ls='--')
                legend_entries.append('Coupon bf')
                #plt.semilogy(med_nomad,bf,marker='x',ls='--')
                plt.xlabel('Nomad magnitude')
                plt.ylabel('Halo Radius (arcseconds)')
                plt.legend(legend_entries,loc = 'lower left',framealpha=0.5)
                plt.title('Suggested bright-star radius w/ bands, fixed mag ='+all_mag_string[jj_mag])
                plt.ylim([3E-1,400])
                cross_corr_pdf.savefig()
                plt.clf()
                false_detection_radius_25mag = false_detection_radius[0::,0::,jj_mag]
                plt.semilogy(med_nomad,np.transpose(false_detection_radius_25mag),marker='o')
                legend_entries = bands
                #plt.semilogy(med_nomad,bf,marker='x',ls='--')
                #legend_entries.append('SSP approximation')
                #plt.semilogy(med_nomad,bf_jp,marker='x',ls='--')
                #legend_entries.append('Coupon bf')
                #plt.semilogy(med_nomad,bf,marker='x',ls='--')
                plt.xlabel('Nomad magnitude')
                plt.ylabel('Arcseconds')
                plt.legend(legend_entries,loc = 'lower left',framealpha=0.5)
                plt.title('False detection radius '+all_mag_string[jj_mag])
                plt.ylim([3E-1,400])
                cross_corr_pdf.savefig()
                plt.clf()
                false_detection_max_25mag = false_detection_max[0::,0::,jj_mag]
                plt.semilogy(med_nomad,np.transpose(false_detection_max_25mag),marker='o')
                plt.xlabel('Nomad magnitude')
                plt.ylabel('Max. Correlation Coefficient')
                plt.title('False detection indicator '+all_mag_string[jj_mag])
                plt.legend(bands,loc = 'upper left',framealpha=0.5)#,title= legend_entries)
                plt.ylim([5E-3,1E2])
                cross_corr_pdf.savefig()
            cross_corr_pdf.close()
    print('\a')
    interact(local=locals())
    #corr = two_point_angular(RADec_sources, bins)
    return
'''
#Erase, just for debugging
#bbox_icrs = [wcs.pixelToSky(bbox[i].getX(),bbox[i].getY()).toIcrs() for i in range(4)] #wcs.pixelToSky(bbox[0].getX(),bbox[0].getY())
#I need to figure out how to create an Angle or coord in the lsst system
#bbox_icrs[0]#example
#afwCoordafwCoord(afwGeom.radians)
#The long way
#sample_icrs_coord = afwCoord.IcrsCoord(afwGeom.Angle(centerRA*afwGeom.degrees),afwGeom.Angle(centerDec*afwGeom.degrees))
#The short way
#sample_icrs_coord = afwCoord.IcrsCoord(centerRA*afwGeom.degrees,centerDec*afwGeom.degrees)
#sample_pixel_coord = wcs.skyToPixel(sample_icrs_coord)
######################## END OF PLOTTING IN DS9  ###################################
#Other mask functions:
#mask.getDimensions()#Output: Extent2I(2048, 4176)
#mask_labels = mask.getMaskPlaneDict()
#mask_labels.keys()#A better way of getting the keys
#mask_labels.values()#This is the number of the bit on the mask planes
#First, check that there is an available mask plane

#the specific maskneeds to be specified for this step
#crBitMask = mask.getPlaneBitMask('CR')
#mask.printMaskPlanes()

####For data downloading
#This is the folder that I want to download
#/lustre/Subaru/SSP/rerun/yasuda/SSP3.7.3_20150518/00817/HSC-I/qa
#These are the types of files that I want to download
#find . -name *1236*58*
#####################################################################################
### Notes on how to find the flux of sources:
    #This returns the same as the classification extendedness
    #nan values are predominantly edge pixels.
    ### psf_flux = sources.getPsfFlux()
    ###psf_index = ~np.isnan(psf_flux)
    ###psf_mag = zeropoint-2.5*np.log10(psf_flux)
    ###psf_index = psf_mag < 24.5
    ##temp = sources.get("flux.psf")
    ##source.get('flux.aperture')
    ##source.get('flux.aperture.nProfile') -> 8
    #key = sources.schema.find("flux.aperture").key
    #sources[0].get(key)
    #sources.getMetadata().get("flux_aperture_radii")
    #(3.0, 4.5, 6.0, 9.0, 12.0, 17.0, 25.0, 35.0, 50.0, 70.0)
    #source.get('flags.pixel.saturated.any')
    #source.get('flags.pixel.saturated.center')
#This plots the bright-star sources in the catalog
def plot_cat_psf_sources(ds9,sources,zeropoint):
    bs_sources = sources
    cmodel = sources.getModelFlux()
    psf = sources.getPsfFlux()
    good_cmodel = ~np.isnan(cmodel)
    cmodel = zeropoint-2.5*np.log10(cmodel[good_cmodel])
    psf = zeropoint-2.5*np.log10(psf[good_cmodel])
    bs_sources = bs_sources[good_cmodel]
    extendedness = psf-cmodel
    min_ext = -0.05
    max_ext = +0.05
    min_mag = 14
    max_mag = 20
    rect1 = Rectangle([min_mag,min_ext],max_mag-min_mag,max_ext-min_ext,alpha=0.5,color='red')
    # Make a selection on the catalog for our cut that's indicated in the shaded rectangle of the pyploy
    good_ext = (extendedness > min_ext) & (extendedness < max_ext)
    bs_ext = extendedness[good_ext]
    bs_cmodel=cmodel[good_ext]
    bs_sources = bs_sources[good_ext]
    good_mag = (bs_cmodel > min_mag) & (bs_cmodel < max_mag)
    bs_ext = bs_ext[good_mag]
    bs_cmodel = bs_cmodel[good_mag]
    bs_sources = bs_sources[good_mag]
#    with ds9.Buffering():
# Put a description of the what exactly we are plotting below because I don't think that it works.
    if 0:
        #for source in star_sources:
        symbol = "o"
        print "We found this many bright-star sources", len(bs_sources)    
        for source in bs_sources:
            ds9.dot(symbol,source.getX(),source.getY(),ctype=ds9.BLUE,size=10,frame=1,silent=True)
    ################ Check Our BS Selection Using the Plot ############################
    plt.figure(1)
    ax = plt.subplot(211)
    marker_size = 5
    plt.scatter(cmodel,extendedness,alpha=0.5,s=marker_size,edgecolors='none')
    plt.xlabel('cmodel mag')
    plt.ylabel('extendedness')
    rect1 = Rectangle([min_mag,min_ext],max_mag-min_mag,max_ext-min_ext,alpha=0.5,color='red')
    ax.add_patch(rect1)
    ax = plt.subplot(212)
    plt.scatter(cmodel,extendedness,alpha=0.5,s=marker_size,edgecolors='none')#,xlabel='cmodel mag',ylabel='extendedness')
    plt.scatter(bs_cmodel,bs_ext,alpha=0.5,s=4*marker_size,edgecolors='none',color='green')#,xlabel='cmodel mag',ylabel='extendedness')
    plt.axis([15,30,-0.5,0.5])
    rect1 = Rectangle([16,-0.05],6,0.1,alpha=0.5,color='red')
    ax.add_patch(rect1)
    plt.xlabel('cmodel mag')
    plt.ylabel('extendedness')
    #####################################################################################
    #### Aug 10,2015 Old Work
    if 0:
        psf = sources.get("flux.psf")
        #Note here: I'm not sure what kind of fluxes to use:
        kron = sources.get("flux.kron")
        ###############################################
        good_psf = ~np.isnan(psf)
        psf = psf[good_psf]
        kron = kron[good_psf]
        ext_sources = ext_sources[good_psf]
        ###############################################
        good_kron = ~np.isnan(kron)
        psf = psf[good_kron]
        kron = kron[good_kron]
        ext_sources = ext_sources[good_kron]
        good_kron = kron > 0.0
        psf = psf[good_kron]
        kron = kron[good_kron]
        ext_sources = ext_sources[good_kron]
    ###############################################
    #ratio = psf/kron
    #ratio = ratio[~np.isnan(ratio)]
    plt.savefig('figures/bs.png')
    return ds9
#####################################################################################
# PLOT THE panSTARRS CATALOG SOURCES (OBSOLETE SINCE WE'RE NOT USING THIS CATALOG)  #
#####################################################################################
def plot_panstarrs(ds9,mask_y,mask_x):
    symbol='o'
    all_mags = []
    astrom = measAstrom.astrom.Astrometry(measAstrom.config.MeasAstromConfig())
    cat = astrom.getReferenceSourcesForWcs(wcs,[mask_y,mask_x],'r')

    print "We found this many catalog sources", len(cat)    
    max_mag = 1000
    with ds9.Buffering():
        for cat_source in cat:
            #It doesn't look like the PanSTARSS mags overlaps with our extendedness parameter.
            #Make a plot of the extendedness vs. mg.       
            mag = -2.5*np.log10(cat_source.get('i'))#flux (looks the same)#g has a lot of NaN
            if mag<max_mag:
                max_mag=mag
            #mag = -2.5*np.log10(cat_source.get('i'))
            if np.isnan(mag): 
                mag = 1.0
                print("Is nan")
                all_mags.append(mag)
                #Rmag = 10*(0.0802*mag**2-1.860*mag+11.65)#*afwGeom.arcminutes#From Anderson, 2012
            mag_radius = 2.0*(-1.860*mag+60.00)#*afwGeom.arcminutes#From Anderson, 2012
            #            if Rmag<40:
            #This is a good limiting magnitude for identifying the bright stars.
            #As you can see, they are identified as extended sources.
            if mag<18.5:
#            if 1:
                print("I Mag: "+str(mag))
                #Rmag = 15
                #symbol = "@:{ixx},{ixy},{iyy}".format(ixx=Rmag,ixy=Rmag,iyy=Rmag)
                x,y = wcs.skyToPixel(cat_source.getCoord())
                ds9.dot(symbol,x,y,ctype=ds9.GREEN,size=mag_radius,frame=1,silent=True)
    print("Max_mag for panstarrs catalog: "+str(max_mag))#12.8
    return ds9

#I'm not sure what this is here, can probably be deleted.
#Stripe 82
#[min(Decs),max(Decs)] = [-1.265802, 1.264384]
#[min(RAs),max(RAs)] = [0.000201, 359.99985]
#[min(Decs),max(Decs)]= [2.2310007907275864, 2.327128144041483]
#[min(RAs),max(RAs)] = [149.82269529329847, 150.01820255991183]

