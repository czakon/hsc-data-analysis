import lsst.daf.persistence as dafPersist
import lsst.afw.coord as afwCoord
import lsst.afw.image as afwImage
import lsst.afw.geom as afwGeom
import os

dataDir = os.environ['runDir']
butler = dafPersist.Butler(dataDir)
#skyMap = butler.get("deepCoadd_skyMap", immediate=True)
visit =  int(os.environ['z_visit'])#works: g,r,i,z,y
ccd =  int(os.environ['test_ccd'])
dataId = {"visit" : visit, "ccd": ccd}
skyMap = butler.get("calexp",dataId)#exposure
                    
# your coordinates here
coord = afwCoord.IcrsCoord(ra, dec)
pixel = coadd.getWcs().skyToPixel(coord)
bbox = afwGeom.Box2I(pixel, pixel)   # 1-pixel box
bbox.grow(15)    # now a 31x31 pixel box
bbox.clip(coadd.getBBox(afwImage.PARENT))  # clip to overlap region (NGC what does it mean to overlap the region?)
if bbox.isEmpty():
    continue
subImage = lsst.afw.image.ExposureF(coadd, bbox, lsst.afw.image.PARENT)
# Save the subimage (or do something else with it)
# the saved WCS should be correct automatically.
subImage.writeFits("cutout.fits")
interact(local=locals())
