#The purpose of this is to take the fits files generated of the bs cutouts in Butler and to stack and analyze them
from astropy.io import fits
from code import interact
import matplotlib.pyplot as plt
image_directory = 'sample_cutouts/'
image_prefix = 'ds9_cutout_'
image_width = 61
for j in range(22):
    temp = fits.open(image_directory+image_prefix+str(j)+'.fits')
    #temp.info()
    #temp[0].header #This is the header for the image file
    #temp[1]#This is where the data is actually located.
    #Question about how to weight the images corrrectly...
    #Take a radial average?
    image = temp[1].data
    if image.shape[0]==image and image.shape[1]==61
    if j==0:
        sum_image = image
    else:
        sum_image += image
    interact(local=locals())
    temp.close()
    #image.shape
tmp = plt.imshow(sum_image)
plt.show()


