# MODIFICATION HISTORY:
# NGC 10/02/2015: Cut-and-pasted from "mask_coadds_09102015.py"
# This is temporary code to learn how to make cutouts of the HSC images.
def write_source_cutout(ds9,exposure,sources):
    # your coordinates here
    #coord = afwCoord.IcrsCoord(ra, dec)
    j=0
    for source in sources:
        #pixel0 = exposure.getWcs().skyToPixel(source.getCoord())
        #pixel = afwGeom.Point2I(pixel0)
        #Converted this to work for the nomad catalog
        source0 = afwGeom.Point2D(source.getX(),source.getY())
        pixel = afwGeom.Point2I(source0)
        #pixel = afwGeom.Point2I(source.getX(),source.getY())
        #pixel = exposure.getWcs().skyToPixel(source.getCoord())
        #bbox = afwGeom.Box2I(pixel, pixel)   # 1-pixel box
        bbox = afwGeom.Box2I(pixel, pixel)   # 1-pixel box
        bbox.grow(30)    # now a 31x31 pixel box
        bbox.clip(exposure.getBBox(afwImage.PARENT))  # clip to overlap region (NGC what does it mean to overlap the region?)
        if bbox.isEmpty():
            continue
        subImage = afwImage.ExposureF(exposure, bbox, afwImage.PARENT)
        #from lsst.pipe.tasks.imageDifference import ImageDifferenceTask
    
        # Save the subimage (or do something else with it)
        # the saved WCS should be correct automatically.
        ds9.mtv(subImage,frame=1,title='My Data')#settings=settings)
        #KEEP: This is if we want to write the file, which we won't actually do now.
        subImage.writeFits("ds9_cutout_"+str(j)+".fits")
        time.sleep(1)
        j+=1
    return
