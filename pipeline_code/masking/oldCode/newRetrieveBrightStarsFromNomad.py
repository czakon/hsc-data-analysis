#!/usr/bin/env python
#
# Created by Nicole Czkon 09212015
# Description of the Code:
# The purpose of this code is to retrieve the bright-stars from NOMAD
# And save them to a file.
#
# KEYWORDS:
#
# overwrite (I'm not sure if this has become obsolete)
# overwriteReg (This should overwrite the .Reg file)
# overwriteSQL (This should reinsert the data into the SQL (but I'm not sure if it clears the datbase or not).)
# insertSQL: this inserts the data into the SQL, by default selected if overwrite SQL is set. 
#
# USAGE:
#
# from pipeline_code.masking import newRetrieveBrightStarsFromNomad
#
# The default setting, loads the results from file.
# brightStars = newRetrieveBrightStarsFromNomad.FindNomadTask()
#
# The "test" option returns the dictionary
# brightStars = newRetrieveBrightStarsFromNomad.FindNomadTask(test=True)
#
# IF YOU ACTUALLY WANT TO ADD MORE STARS TO THE DATABASE.
# brightStars = newRetrieveBrightStarsFromNomad.FindNomadTask(test=True,insertSQL=True)
# The configuration phase, calls Vizier remotely.
# brightStars = newRetrieveBrightStarsFromNomad.FindNomadTask(exposure=exposure,
#                                                          mapCoord=mapCoord)
#
# Then, the catalog can be retrieved like this.
# nomadOut = brightStars.nomadOut
#
# OUTPUT:
# 
# TODO:
# Basically, you want to save the mapCoord's to confirm that you're opening
# the correct file.
#
import os
import glob
import re
################# For Debugging ###########################################
from code import interact
import time
########### Pipeline-Related Packages #####################################
import lsst.daf.persistence as dafPersist
import lsst.afw.coord as afwCoord
import lsst.afw.geom as afwGeom
import lsst.pex.config as pexConfig
############ General Python Packages #####################################
import astropy.units as u
import astropy.coordinates as coord
import numpy as np
from astropy.io import ascii,fits
from astropy import table
from astropy.table import vstack
from datetime import date
## Additional package needed in order to download the data from Vizier ###
from astroquery.vizier import Vizier
from copy import deepcopy
import cPickle as pickle
import sqlite3
########## My Own Python Packages #######################################
from pipeline_code import hscConfigurationFile as hscConfig
from pipeline_code.readFiles import readTractInfoFile
import loadTestExposure

__all__ = ["FindNomadTask"]

########## FINDING NOMAD SOURCES FOR VERY BRIGHT STARS  #################
class FindNomadTask:
    _DefaultName = "findNomad"

    def __init__(self,overwrite=None,searchWidth=None,exposure=None,mapCoord=None, \
                 test=None,insertSQL=None,overwriteSQL=None,overwriteReg=None,loadOnly=None,fields=None, \
                 loadFromFits=None):

        if loadFromFits is None:
            self.loadFromFits = False
        else:
            self.loadFromFits = True
            
        self.folderout = hscConfig.ssp_patch_folder

        self.folderout_exists = os.path.isdir(self.folderout)

        if fields is None:
            self.fields = ['D-COSMOS']
        else:
            self.fields = fields
            
        if loadOnly is None:
            self.loadOnly = False
        else:
            self.loadOnly = True
            self.load()
            
        if overwriteReg is None:
            self.overwriteReg = False
        else:
            self.overwriteReg = True

        if insertSQL is None:
            self.insertSQL = False
        else:
            self.insertSQL = True

        if overwriteSQL is None:
            self.overwriteSQL = False
        else:
            self.overwriteSQL = True
            if self.insertSQL is False:
                print('overwriteSQL keyword is, setting the insertSQL keyword to TRUE.')
                self.insertSQL = False

        if overwrite is None:
            self.overwrite = True
            if test:
                self.nomadOut = self.test()
            #else:
                #if self.folderout_exists:
                #    print('Overwrite keyword is set to False, loading data from file: ' + self.fileout)
                #    self.nomadOut = pickle.load(open(self.fileout,"rb"))
                #else:
                #    print('Output file: ' + self.fileout + ' does not exist, if you would like to create this,' + \
                #          ' please include "overwrite=True" as a keyword.')
        else:

            if test:
                print('The test configuration by design cannot overwrite the output file....')
                self.nomadOut = self.test()
                return self.nomadOut

            else:

                if exposure is None:
                    print('You must specify an exposure before querying Vizier.')
                    return
                else:
                    self.exposure = exposure

                if mapCoord is None:
                    print('You must specify an exposure before querying Vizier.')
                    return
                else:
                    self.mapCoord = mapCoord

                self.exposure = exposure    
                self.overwrite = True

        #The default search width is the size of the XMM-LSS field for now.
        if searchWidth is None:
            self.searchWidth = 2.8
        else:
            self.searchWidth = searchWidth

    def run(self):

        reg_file_open = False

        c = self.load()
        print('\nInserting all of the entries into the database, this will take a while...')
        tract_patch_completed = []
        if self.overwriteSQL:
            print('\nself.overwriteSQL is set to TRUE, erasing the old database...press Ctrl-D to continue...')
            interact(local=locals())
            c.execute('''DROP TABLE nomad''')
            # Create table
            c.execute('''CREATE TABLE nomad (nomad_id string, 
                                             RA_J2000_degrees float, Dec_J2000_degrees float,
                                             Bmag float, Vmag float, Rmag float,
                                             Jmag float, Hmag float, Kmag float,
                                             tract string, patch string,
                                             bright_star_radius float)''')
        if self.insertSQL and not self.overwriteSQL:
            # This counts all of the rows in the database
            for row in c.execute('''SELECT Count(*) FROM nomad'''):
                print(str(row[0])+' rows in the nomad sql database.')
            # This counts all of the different tracts in the database.
            # Only two distinct tracts for now, hopefully this changes when I make a new query.
            for row in c.execute('''SELECT Count(DISTINCT tract) FROM nomad'''):
                print(str(row[0])+' distinct tracts.')
            # Only two distinct tracts for now, hopefully this changes when I make a new query.
            for row in c.execute('''SELECT DISTINCT tract,patch FROM nomad GROUP BY tract,patch'''):
                tract_patch = {'tract':str(row[0]),'patch':str(row[1])}
                tract_patch_completed.append(tract_patch)
            #    print('Tract: '+ str(row[0]) + ' and Patch: ' + str(row[1]) + ' is included.')
            for row in c.execute('''SELECT DISTINCT tract FROM nomad GROUP BY tract'''):
                #tract_patch_completed.append(tract_patch)
                print('Tract: '+ str(row[0]) + ' is included.')
            
        for field in self.fields:
            tract_info_filename = self.folderout + field + '/new_tracts_patches_' + field + '.txt'
            tract_info_file = readTractInfoFile.data(tract_info_filename)

            if field[0]=='W':

                sub_directory = 'newWIDE/'

            elif field[0]=='D':

                sub_directory = 'newDEEP/'
                
            elif field[0]=='UD':

                sub_directory = 'newULTRADEEP/'

            else:

                print('Invalid format for the field name, must start with \'W\', \'D\' or \'UD\'.')
                interact(local=locals())

            # Open the fits file and see how that works
            if self.loadFromFits:

                pickleFitsFile = 'temp_v1.0.p'

                if os.path.isfile(pickleFitsFile):

                    [allIDs,allRAs,allDecs,allMags] = pickle.load(open(pickleFitsFiles,'rb'))

                else:

                    fits_file = '/array/users/czakon/data/ssp_patches/brightStar_NOMAD_HSC_all_v1.0.fits'
                    print('\nOpening the nomad fits file, this will take a while....\n')
                    hdulist = fits.open(fits_file)
                    all_data = hdulist[1].data
                    #allIDs allRAs allDecs allMags
                    allIDs = np.array([''.join(onedata[0].split('-')) for onedata in all_data])#~4E6 entries
                    allRAs = np.array([onedata[1] for onedata in all_data])#~4E6 entries
                    allDecs = np.array([onedata[2] for onedata in all_data])#~4E6 entries
                    allMags = np.array([onedata[3] for onedata in all_data])#~4E6 entries
                    goodMags = ~np.isnan(allMags)
                    allIDs = allIDs[goodMags]
                    allRAs = allRAs[goodMags]
                    allDecs = allDecs[goodMags]
                    allMags = allMags[goodMags]
                    all_data = []
                    hdulist.close()
                    #tHIS CLEARLY needs to have a better name
                    pickle.dump([allIDs,allRAs,allDecs,allMags],open(pickleFitsFile,'wb'))
                    print('\nFinished opening the fits file.\n')

            else:

                print('Will download the data directly from Vizier....')

            for tract_patch_full in tract_info_file:

                tract_patch = {'tract':tract_patch_full['tract'],'patch':tract_patch_full['patch']}

                if tract_patch in tract_patch_completed:
                    tract_patch_in_sql = True

                else:
                    tract_patch_in_sql = False
                #Start here!!! Search the directory and make sure that the file doesn't already exist.
                #We want to still perform the search if the reg file doesn't exist.
                # for band in hscConfig.bands: (this needs to go further down.)
                band = hscConfig.bands[0]

                directory_out = self.folderout + sub_directory + \
                                'deepCoadd/BrightObjectMasks/'+tract_patch['tract'] +'/'

                try:
                    os.stat(directory_out)
                except:
                    print(directory_out + ' does not exist, will create it....')
                    os.mkdir(directory_out)
                    
                reg_fileout = directory_out + 'BrightObjectMask-' + \
                              tract_patch['tract'] + '-' + tract_patch['patch'] + '-HSC-' + band + '.reg'

                print('\nWriting star mask to to reg_fileout: ' + reg_fileout)

                reg_file_exists = os.path.isfile(reg_fileout)

                if tract_patch_in_sql and self.insertSQL and reg_file_exists:

                    print('\nTract: ' + tract_patch['tract'] + \
                          ' Patch: ' + tract_patch['patch'] + \
                          ' already inserted in SQL and the .reg file exists, skipping...')

                elif not tract_patch_in_sql and not self.insertSQL and reg_file_exists:

                    print('\nTract: ' + tract_patch['tract'] + \
                          ' Patch: ' + tract_patch['patch'] + \
                          ' .reg file exists but the patch is not yet inserted into the SQL.'+ \
                          'As the \'insertSQL\' is set to False, will skip this tract patch...')

                else:
                    
                    CenterRA = tract_patch_full['CenterRA']
                    CenterDec = tract_patch_full['CenterDec']
                    Corner1RA = tract_patch_full['Corner1RA']
                    Corner1Dec = tract_patch_full['Corner1Dec']
                    Corner3RA = tract_patch_full['Corner3RA']
                    Corner3Dec = tract_patch_full['Corner3Dec']
                    extra_width = 0.2

                    if self.loadFromFits:

                        minRAs = np.where(allRAs < Corner3RA + extra_width)
                        maxRAs = np.where(allRAs > Corner1RA - extra_width)
                        minDecs = np.where(allDecs < Corner3Dec + extra_width)
                        maxDecs = np.where(allDecs > Corner1Dec - extra_width)

                        tempDecs = np.intersect1d(minDecs[0],maxDecs[0])
                        tempRAs = np.intersect1d(minRAs[0],maxRAs[0])

                        if len(tempRAs)==0 or len(tempDecs)==0:
                            print('\nTract: ' + tract_patch['tract'] + \
                                  ' Patch: ' + tract_patch['patch'] + \
                                  ' not included in Jean\'s file..')
                            continue

                        allStars = np.intersect1d(tempRAs,tempDecs)
                        starIDs =  allIDs[allStars]
                        starRAs = allRAs[allStars]
                        starDecs = allDecs[allStars]
                        starMags = allMags[allStars]

                        
                    else: #Download directly from Vizier

                        # This is from Jean
                        more_width = 0.10

                        w = (abs(Corner3RA - Corner1RA)+more_width)
                        h = (abs(Corner3Dec - Corner1Dec)+more_width)

                        time0  = time.clock()
                        vizCat = self.queryNOMADBox(Corner1RA,Corner1Dec,w,h)
                        time1  = time.clock()
                        timed = (time1-time0)*3600
                        print('\nThe program has taken ' + str(timed) + ' seconds to execute.\n')

                        interact(local=locals())
                        
                    #### START HERE ####
                    # Load the appropriate NOMAD catalogs
                    # Corner3Dec - Corner1Dec
                    # This will require opening several NOMAD catalogs
                    # nomad_home = hscConfig.catalog_folder+'NOMAD/'
                    # min_p_Dec = np.floor(Corner1Dec + 90.0)
                    #
                    # if min_p_Dec >= 80.0 and min_p_dec < 90.0:
                    #     print('Success!!!')
                    # else:
                    #    print('This didn\'t do what it was supposed to do.')
                    #
                    # max_p_Dec = np.ceil(Corner3Dec + 90.0)
                    # if max_p_Dec >= 80.0 and min_p_dec < 90.0:
                    #     print('Success!!!')
                    # else:
                    #    print('This didn\'t do what it was supposed to do.')
                    #
                    # nomad_dirs_a = ['130_139_nomad','080_089_nomad','090_099_nomad']
                    # nomad_dirs_ab = {'080_089_nomad':['nomad_083_txt']}#,'090_099_nomad','130_139_nomad']
                    # nomad_files = glob.glob(nomad_home + nomad_dir_a + '/' + nomad_dir_b+'/*')
                    ####################
                    ######################
                    # KEEP THE FOLLOWING BELOW.
                    #Corner3RA
                    #Corner3Dec
                    
                    #nomad_vizier_a['tract'] = tract_patch['tract'] 
                    #nomad_vizier_a['patch'] = tract_patch['patch'] 
                    #nomad_vizier_a['bright_star_radius'] = 0.0
                    print('Writing to: ' + reg_fileout)
                    f_t = open(reg_fileout,'w')

                    f_t.write('# BRIGHT STAR CATALOG: Nicole G. Czakon\n')
                    f_t.write('# GENERATED ON: ' + str(date.today()) + '\n')
                    f_t.write('# TRACT: ' + tract_patch['tract']  + '\n')
                    f_t.write('# PATCH: ' + tract_patch['patch']  + '\n')
                    f_t.write('# FILTER: HSC-' + band + '\n')
                    f_t.write('wcs; fk5 \n')
                                        
                    for starx in range(len(allStars)):
                        star = {}
     
                        bsr = hscConfig.bright_star_radius
                        radius = bsr['A0'] * 10**(bsr['B0'] * (bsr['C0'] - starMags[starx]))/3600. + \
                                 bsr['A1'] * 10**(bsr['B1'] * (bsr['C1'] - starMags[starx]))/3600.

                        star['bright_star_radius'] = radius

                        star['NOMAD1'] = starIDs[starx]
                        star['RAJ2000'] = starRAs[starx]
                        star['DEJ2000'] = starDecs[starx]
                        
                        f_t.write('circle(' + '{0:.7f}'.format(star['RAJ2000']) + ',' + \
                                  '{0:.7f}'.format(star['DEJ2000']) + ',' + \
                                  '{0:.6f}'.format(star['bright_star_radius']) + \
                                  'd) # ID: ' + star['NOMAD1'] + '\n')
                        #print('{0:.6f}'.format(star['bright_star_radius']))
                    f_t.close()
                    reg_file_open = False
                    # TRACT: XXXX
                    # PATCH: X,X
                    # FILTER HSC-I
                    # wcs; icrs
    def old_nomad(self):
        #see vizierNotes.txt for more information.
        print('\nStarting to query Vizier...')
        Vizier.VIZIER_SERVER = u'vizier.nao.ac.jp'
        nomad_cat = u'I/297' 
        #Even though I set this here, the magnitude does not change
        Vizier.ROW_LIMIT = -1#00
        #Alternative Filters: Bmag,Vmag,Rmag,Jmag,Hmag,Kmag
        nomad_vizier = []
        n_searches = 14
        #There is a bug in astroquery and it can only retrieve a maximum of 50 entries at a time.
        for i in range(n_searches):#Search through the RAs
            for j in range(n_searches):#Search through the Decs
                new_map_coord = deepcopy(self.mapCoord)
                new_map_coord.ra.deg = self.mapCoord.ra.deg + (i-n_searches/2+0.75)*0.2
                new_map_coord.dec.deg = self.mapCoord.dec.deg + (j-n_searches/2+0.75)*0.2
                new_map_coord = coord.SkyCoord(ra=float(new_map_coord.ra.deg),dec=float(new_map_coord.dec.deg), \
                                               unit=(u.deg,u.deg),frame='fk5')

                nomad_vizier_a = Vizier(column_filters={"Rmag":"5..19.0"}).query_region(new_map_coord,width="0.2d",catalog=["I/297"])
                                                                                         
                nomad_vizier_a = nomad_vizier_a['I/297/out']
                nomad_vizier_a.remove_column('_x')
                nomad_vizier_a.remove_column('_y')
                nomad_vizier_a.remove_column('_p')
                nomad_vizier_a.remove_column('_r')

                if j is 0 and i is 0:
                    nomad_vizier = deepcopy(nomad_vizier_a)
                else:
                    nomad_vizier = vstack([nomad_vizier,nomad_vizier_a])
        #In case you want more information
        print("NOMAD CATALOG INFORMATION RETRIEVED FROM VIZIER:")
        print(nomad_vizier)
        print('\r')
        #Now, we need to put the nomad catalog in the proper format to plot later on.
        nomadRA = nomad_vizier['_RAJ2000'].tolist()
        nomadDec = nomad_vizier['_DEJ2000'].tolist()
        nomadBmag = nomad_vizier['Bmag'].tolist()
        nomadVmag = nomad_vizier['Vmag'].tolist()
        nomadRmag = nomad_vizier['Rmag'].tolist()
        wcs = self.exposure.getWcs()
        interact(local=locals())
        nomad_pixel_out = [wcs.skyToPixel(afwCoord.IcrsCoord(nomadRA[i]*afwGeom.degrees, \
                                                             nomadDec[i]*afwGeom.degrees)) \
                           for i in range(len(nomadDec))]
        nomadOut = {'RA':nomadRA,'Dec':nomadDec,'pix_out':nomad_pixel_out,\
                     'Bmag':nomadBmag,'Vmag':nomadVmag,'Rmag':nomadRmag}
        if self.overwrite:
            interact(local=locals())
            pickle.dump(nomadOut,open(nomad_filename,'wb'))
        return nomadOut

    def load(self):
        print('\nLoading the SQL database...')

        # Setup the SQL datafile
        db_out_filename = hscConfig.bright_star_db
        self.conn = sqlite3.connect(db_out_filename)
        #Create a cursor object
        self.c = self.conn.cursor()
        return self.c
    
    def test(self):
        #START HERE!!! Make sure that the appropriate code is deleted. 
        #testExposure = loadTestExposure.LoadExposureTask(loadCoadd=True,filter='G',doPlot=False)
            #End of it single exposure or coadd conditional statement.
        #self.mapCoord = testExposure.mapCoord
        #self.exposure = testExposure.exposure
        print('\nStarting the test file...')
        self.overwrite = True
        junk = self.loadRegFiles()
        interact(local=locals())
        nomadOut = self.run()
        return nomadOut

    def loadRegFiles(self):

        junk = 0
        sub_directory = ''
        band = 'G'
        directory_out = self.folderout + sub_directory + \
                        'deepCoadd/BrightObjectMasks/*'
        all_tracts = glob.glob(directory_out)

        print('Starting loadRegFiles')

        for tract_in in all_tracts:
            
            reg_files_in = tract_in + '/BrightObjectMask-*-HSC-' + band + '.reg'

            all_files = glob.glob(reg_files_in)
            for file_in in all_files:
                print('Reading: ' + file_in)
                f = open(file_in,'rb')
                all_lines = f.readlines()
                all_lines = all_lines[6:-1]
                radius = []
                for line_in in all_lines:
                    temp = re.split('circle\(|,|d\)|\n',line_in)
                    radius.append(float(temp[3]))
                print('The maximum size of the radius is: ' + str(max(radius)))
                interact(local=locals())
        return junk
    
    def queryNOMADBox(self,minRA, minDec, w, h):
        Vizier.VIZIER_SERVER = u'vizier.nao.ac.jp'
        tables=[]
        #constraints={"Bmag":"<17.5", "Vmag":"<17.5", "Rmag":"<17.5", "Jmag":"<17.5"}
        constraints={ "Bmag":"<17.5", "Vmag":"<17.5", "Rmag":"<17.5" }
        steps_nomad = 1
        for k in constraints.keys():

            print k, constraints[k]

            for step_r in range(steps_nomad):

                for step_d in range(steps_nomad):

                    result = Vizier(column_filters={k:constraints[k]}, catalog=["NOMAD"], row_limit=-1).query_region(\
                                       coord.SkyCoord(ra=minRA + (0.5+step_r)*w/steps_nomad, \
                                                      dec=minDec + (0.5+step_d)*h/steps_nomad, \
                                                      unit=(u.deg, u.deg), frame='icrs'), \
                                       width="{0:f}d".format(w/steps_nomad), height="{0:f}d".format(h/steps_nomad))

                    #if len(result[0]) == 50:
                    #    print('Error! Result is 50, please increase the number of searches in Vizier...')
                    #    interact(local=locals())
                    #else:
                    if True:
                        print(str(len(result[0]))+ ' bright stars found in this section....')

                    tables.append(result[0])
                    
        print('Finished with the Vizier query')
        result = table.unique(table.vstack(tables), keys='NOMAD1')
        print('Finding the unique entries...')
        return result

if __name__ == 'main':
    print('Made it to main')
    brightStars = FindNomadTask(test=True)


                #The following below is old code and can be removed once we verify that the program
                #is working.
#            with open(tract_info_file,'r') as f:

#                for line in f:

#                    if line.find('Patch:') is not -1 and line.find('Corner1') is not -1:

#                        t_line = re.findall('[^(, )]+',line)
#                        Corner1RA = float(t_line[9])
#                        Corner1Dec = float(t_line[10])

#                    if line.find('Patch:') is -1 or line.find('Corner3') is -1:
#                        continue

#                    t_line = re.findall('[^(, )]+',line)
#                    tract = t_line[1]
#                    patch = t_line[3] + ',' + t_line[4]
