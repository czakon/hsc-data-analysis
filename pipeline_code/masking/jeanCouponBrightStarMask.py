#!/usr/bin/env python

"""
Jean coupon - 2015
script to build star masks
"""

import os, sys, errno
import numpy as np


import lsst.daf.persistence  as dafPersist
import lsst.afw.cameraGeom   as camGeom
import lsst.afw.coord        as afwCoord
import lsst.afw.geom         as afwGeom
import lsst.afw.image        as afwImage

import pickle

import datetime

import astropy.units as u
from astroquery.vizier import Vizier
import astropy.coordinates as coord

from astropy import table
from astropy.io import ascii, fits

EPS = np.finfo(np.float).eps

now = datetime.datetime.now()

# ----------------------------------------------------- #
# main
# ----------------------------------------------------- #

def main(args):


    function = getattr(sys.modules[__name__], args.option)(args, verbose=0)
    return

# ----------------------------------------------------- #
# Main functions
# ----------------------------------------------------- #

def brightStarPatch(args, verbose=0):

    skymap = pickle.load( open('../Pointings/data/skyMapWide.pickle', "rb" ) )
    tractList = ascii.read('../Pointings/data/tract.lst', format="no_header")

    for tract_id in tractList['col1']:

        print tract_id

        tract = skymap[tract_id]
        pixel_scale = tract.getWcs().pixelScale().asDegrees()

        diagonal = (max(tract.getPatchInnerDimensions()) + tract.getPatchBorder()) * pixel_scale * np.sqrt(2) / 2.0
        wcs = tract.getWcs()

        ra_star, dec_star, r_star, ID_star = starMask("tracts/"+"{0:d}".format(tract.getId())+".fits")

        mkdir_p("tracts/deepCoadd/BrightObjectMasks/{0:d}".format(tract.getId()))

        for patch in tract:

            [ra_cen, dec_cen] =  center(patch.getOuterBBox(), wcs)

            D = distAngSpherDeg(ra_cen, dec_cen, ra_star, dec_star)

            inPatch = D < diagonal + r_star

            for f in ["HSC-G", "HSC-R", "HSC-I", "HSC-Z", "HSC-Y"]:
                fileOutName="tracts/deepCoadd/BrightObjectMasks/{0:d}/BrightObjectMask-{0:d}-{1:d},{2:d}-{3:s}.reg".format(tract.getId(), patch.getIndex()[0], patch.getIndex()[1], f)
                writeMaskFile(ra_star[inPatch], dec_star[inPatch], r_star[inPatch], ID_star[inPatch], fileOutName, f, tract_id, patch.getIndex())



        #exit(-1)

    return

def writeMaskFile(ra, dec, r, ID, fileOutName, filter, tract, patch):

    f = open(fileOutName, "w+")

    f.write("# BRIGHT STAR CATALOG: Nicole G. Czakon\n")
    f.write("# GENERATED ON: {0:s}\n".format(now.strftime('%Y-%m-%d')))
    f.write("# TRACT: {0:d}\n".format(tract))
    f.write("# PATCH: {0:d},{1:d}\n".format(patch[0], patch[1]))

    f.write("# FILTER {0:s}\n".format(filter))
    f.write("wcs; fk5\n")

    for i in range(len(ra)):
        f.write("circle({0:f},{1:f},{2:f}d) # ID: {3:s}\n".format(ra[i], dec[i], r[i], ID[i].translate(None, '-')))

    f.close()
    return

    return

def center(bbox, wcs):
    """Get the center of a BBox and convert them to lists of RA and Dec."""

    p = afwGeom.Point2D((bbox.getMinX() + bbox.getMaxX())/2.0, (bbox.getMinY() + bbox.getMaxY())/2.0)
    center = wcs.pixelToSky(p).toIcrs()

    return center.getRa().asDegrees(), center.getDec().asDegrees()


def brightStarTract(args, verbose=0):

    skymap = pickle.load( open('../Pointings/data/skyMapWide.pickle', "rb" ) )

    tractList = ascii.read('../Pointings/data/tract.lst', format="no_header")

    for tract in skymap:
        if tract.getId() in tractList['col1']:

            center = [tract.getCtrCoord().getRa().asDegrees(), tract.getCtrCoord().getDec().asDegrees()]

            pixel_scale = tract.getWcs().pixelScale().asDegrees()
            w = (tract.getBBox().getMaxX() - tract.getBBox().getMinX()) * pixel_scale
            h = (tract.getBBox().getMaxY() - tract.getBBox().getMinY()) * pixel_scale

            table = queryNOMADBox(center, w+1.0, h+1.0)

            table.write("tracts/"+"{0:d}".format(tract.getId())+".fits", format='fits', overwrite=True)

    return

def queryNOMADBox(center, w, h):

    tables=[]
    #constraints={"Bmag":"<17.5", "Vmag":"<17.5", "Rmag":"<17.5", "Jmag":"<17.5"}
    constraints={ "Bmag":"<17.5", "Vmag":"<17.5", "Rmag":"<17.5" }

    for k in constraints.keys():
        print k, constraints[k]

        result = Vizier(column_filters={k:constraints[k]}, catalog=["NOMAD"], row_limit=-1).query_region(\
            coord.SkyCoord(ra=center[0], dec=center[1], unit=(u.deg, u.deg), frame='icrs'), \
            width="{0:f}d".format(w), height="{0:f}d".format(h))

        tables.append(result[0])

    result = table.unique(table.vstack(tables), keys='NOMAD1')

    return result




def buildBrightStarCatalog(args, verbose=0):
    "Merge, clean and compute mask radii"

    fileInName=args.input.split(',')

    ID=np.array([])
    RA=np.array([])
    DEC=np.array([])
    mag=np.array([])

    mag_key = ["Bmag", "Vmag", "Rmag"]

    for f in fileInName:
        fileIn = fits.open(f)
        data   = fileIn[1].data

        # set nan's to 99 so they don't get picked
        for m in mag_key:
            data[m][data[m] != data[m]] = +99.0

        brightest=np.array([min(B,V,R) for B,V,R in zip(data["Bmag"], data["Vmag"], data["Rmag"])])

        select = (brightest < 17.5)

        ID  = np.append(ID, data["NOMAD1"][select])
        mag = np.append(mag, brightest[select])
        RA  = np.append(RA, data["RAJ2000"][select])
        DEC = np.append(DEC, data["DEJ2000"][select])

    _, unique = np.unique(ID, return_index=True)

    tbhdu = fits.BinTableHDU.from_columns([
        fits.Column(name='NOMAD1', format='20A', array=ID[unique]),
        fits.Column(name='RAJ2000', format='E', array=RA[unique]),
        fits.Column(name='DEJ2000', format='E', array=DEC[unique]),
        fits.Column(name='mag', format='E', array=mag[unique])])
    tbhdu.writeto(args.output, clobber='True')

    return



    #mag_key = ["Bmag", "Vmag", "Rmag"]
    #for m in mag_key:
    #    result[result[m] != result[m]][m] = +99.0

def starMask(fileInName):

    mag_key = ["Bmag", "Vmag", "Rmag"]

    fileIn = fits.open(fileInName)
    data   = fileIn[1].data

    # set nan's to 99 so they don't get picked
    for m in mag_key:
        data[m][data[m] != data[m]] = +99.0

    mag = np.array([min(B,V,R) for B,V,R in zip(data["Bmag"], data["Vmag"], data["Rmag"])])

    A0=200.0
    B0=0.25
    C0=7.0
    A1=12.0
    B1=0.05
    C1=16.0

    r = (A0*pow(10.0, B0*(C0 - mag)) + A1*pow(10.0, B1*(C1 - mag))) / 3600.0

    select = (0.0 < mag) & (mag < 17.5)

    return data["RAJ2000"][select], data["DEJ2000"][select], r[select], data["NOMAD1"][select]

def buildMask(starTable):

    mag = [min(B,V,R) for B,V,R in zip(result["Bmag"], result["Vmag"], result["Rmag"])]

    A0=200.0
    B0=0.25
    C0=7.0
    A1=12.0
    B1=0.05
    C1=16.0

    r = A0*pow(10.0, B0*(C0 - mag)) + A1*pow(10.0, B1*(C1 - mag))
    r /= 3600.0

    #result.write('test.fits', format='fits', overwrite=True)

    return ra, dec, r


def query_NOMAD(args, verbose=0):
    """
    Queries a NOMAD catalogue
    ATTENTION: looks like there's a limit on width/height < 90.0
    """

    lim = [float(s) for s in args.lim.split(',')]

    dRA  = distAngSpherDeg(lim[0], 0.0, lim[1], 0.0)
    dDEC = distAngSpherDeg(0.0,    lim[2], 0.0, lim[3])

    #center=[((lim[0] + lim[1])%360)/2.0, ((lim[2] + lim[3])%360)/2.0]
    center=[(lim[1] - dRA / 2.0)%360, (lim[3] - dDEC / 2.0)%360]

    # looks like width -> dec and height -> ra (??)
    h = dRA
    w = dDEC

    #h = 2.0
    #w = 2.0

    tables=[]
    constraints={"Bmag":"<17.5", "Vmag":"<17.5", "Rmag":"<17.5", "Jmag":"<17.5"}
    #constraints={"Bmag":"<17.5"}

    #, "RAJ2000":"238.50..244.50", "DEJ2000":"51.00..57.00"

    for k in constraints.keys():
        print k, constraints[k]

        result = Vizier(column_filters={k:constraints[k]}, catalog=["NOMAD"], row_limit=-1).query_region(\
            coord.SkyCoord(ra=center[0], dec=center[1], unit=(u.deg, u.deg), frame='icrs'), \
            width="{0:f}d".format(w), height="{0:f}d".format(h))

        print len(result[0])

        #print result

        tables.append(result[0])

    t = table.vstack(tables)

    table.unique(t, keys='NOMAD1').write(args.output, format='fits', overwrite=True)


    return

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def distAngSpherDeg(ra1, dec1, ra2, dec2):

    sin2_ra  = 0.5*(1.0 - np.cos(ra1*np.pi/180.0)*np.cos(ra2*np.pi/180.0) - np.sin(ra1*np.pi/180.0)*np.sin(ra2*np.pi/180.0))
    sin2_dec = 0.5*(1.0 - np.cos(dec1*np.pi/180.0)*np.cos(dec2*np.pi/180.0) - np.sin(dec1*np.pi/180.0)*np.sin(dec2*np.pi/180.0))

    arg = sin2_dec + np.cos(dec1*np.pi/180.0)*np.cos(dec2*np.pi/180.0)*sin2_ra
    if hasattr(arg, '__iter__'):
        arg[arg < EPS] = EPS
    else:
        arg = max(EPS, arg)

    return 2.0*np.arcsin(np.sqrt(arg))*180.0/np.pi


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('option',                                      help="Which task to do")
    parser.add_argument('-i',  '--input',    default=None,             help='input file')
    parser.add_argument('-o',  '--output',   default=None,             help='output file')
    parser.add_argument('-lim',              default=None,             help='Coordinates boundaries in decimal degrees ramin,ramx,decmin,decmax')

    args = parser.parse_args()
    main(args)
