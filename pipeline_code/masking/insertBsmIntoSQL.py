#!/usr/bin/env python
#
# Created by Nicole Czkon 12102015
#
# Description of the Code:
# The purpose of this code is to retrieve the bright-stars from NOMAD that have been downloaded
# via bittorrent (www.ap-i.net/skychart/en/news/nomad) and insert them into the SQL.
#
# KEYWORDS:
#
# overwriteSQL (This removes the entire SQL database (very dangerous, should only be done in test mode.)
# insertSQL: this inserts the data into the SQL, by default selected if overwrite SQL is set. 
#
# USAGE:
#
# from pipeline_code.masking import insertBsmIntoSQL.py
#
# The default setting, loads the results from file.
# brightStars = insertBsmIntoSQL.FindNomadTask()
#
# IF YOU ACTUALLY WANT TO ADD MORE STARS TO THE DATABASE.
# brightStars = retrieveBrightStarsFromNomad.FindNomadTask(insertSQL=True)
#
# OUTPUT:
# 
# LIST OF FUNCTIONS:
#
#  insertRegIntoSql
#  insertNomadIntoSql
#  insertRandomsIntoSql
#  insertTychoIntoSql
#  insertUcacIntoSql
#  insertPicklesIntoSql
#  loadSql
#  loadTractPatchFile
#  modifyBsm
#
import os,re
import glob
################# For Debugging ###########################################
from code import interact
import time

########### Pipeline-Related Packages #####################################
#import lsst.daf.persistence as dafPersist
#import lsst.afw.coord as afwCoord
#import lsst.afw.geom as afwGeom
#import lsst.pex.config as pexConfig

############ General Python Packages #####################################
import numpy as np
from astropy.io import ascii#,fits
from astropy.coordinates import SkyCoord
from astropy import units as u

import sqlite3

########## My Own Python Packages #######################################
from pipeline_code import hscConfigurationFile as hscConfig
from pipeline_code.loadFiles import loadTractInfoFile
from pipeline_code.loadFiles import loadBrightStarSql

__all__ = ["InsertBsmTask"]

########## FINDING NOMAD SOURCES FOR VERY BRIGHT STARS  #################
class InsertBsmTask:

    _DefaultName = "insertBsm"

    def __init__(self,overwriteSql=None,insertNomad=None,insertReg=None,insertRandoms=None,insertTycho=None, \
                 test=True,dbFilename=None):
        #KEEP THIS FOR LATER FUNCTIONALITY loadOnly=None):
        
        if overwriteSql is None:
            self.overwriteSql = False
        else:
            self.overwriteSql = True
            
        if insertTycho is None:
            self.insertTycho = False
        else:
            self.insertTycho = True
                        
        if insertNomad is None:
            self.insertNomad = False
        else:
            self.insertNomad = True

        if insertReg is None:
            self.insertReg = False
        else:
            self.insertReg = True

        if insertRandoms is None:
            self.insertRandoms = False
        else:
            self.insertRandoms = True

        if dbFilename is None:
            self.dbFilename = hscConfig.brightStarDb
        else:
            print('\ndbFilename is set to: ' + dbFilename +'\n')
            self.dbFilename = hscConfig.sqlDirectory + dbFilename

        self.field = 'D-S15b-DR-2'

        print('\nStarting to insert the values into the Sql database...')

        junk = self.run()

        return junk

    def run(self):

        time0 = time.clock()
        c = self.loadSql()

        print('\nStarting to insert the selected entries into the database, this will take a while...\n')

        tract_info_file = loadTractInfoFile.data(fieldName=self.field)

        dtype = [('minRA','float'),('maxRA','float'),('minDec','float'),('maxDec','float')]
        #If the tract information is given, this is what you should use:

        #if 'Center' in tract_info_file.keys():
            
        self.tpRADec = np.array([(tract_x['Corner1']['RA'], tract_x['Corner3']['RA'], \
                             tract_x['Corner1']['Dec'], tract_x['Corner3']['Dec']) for key_x,tract_x in tract_info_file.items()],dtype=dtype)
            
        self.tpRADec.sort(order='minDec')

        if self.insertNomad:
            junk = self.insertNomadIntoSql()

        if self.insertTycho:
            junk = self.insertTychoIntoSql()
                        
        if self.insertReg:
            junk = self.insertRegIntoSql()

        if self.insertRandoms:
            junk = self.insertRandomsIntoSql()

        #self.conn.close()

        return
    
    def insertRegIntoSql(self):
        #START HERE! I WANT TO INCLUDE TRACT AND PATCH INFORMATION HERE.
        if self.overwriteSql:

            print('\nself.overwriteSql is set to TRUE, erasing the old reg database...')

            #Only drop the table if it exists...
            try:

                if self.insertReg:
                    self.c.execute('''DROP TABLE reg_all''')

            except:
                pass

            self.c.execute('''CREATE TABLE reg_all (RA_J2000_degrees float, Dec_J2000_degrees float,
                                                    bright_star_radius float, tract int, patch string)''')

        else:

            for row in self.c.execute('''SELECT Count(*) FROM reg_all'''):
                print(str(row[0])+' entries in the reg_all sql database.')


        #Where should the official ssp patches be located?
        regDir = hscConfig.sspPatchDirectory + self.field + '/deepCoadd/BrightObjectMasks/'
        allTractDirs = filter(os.path.isdir,glob.glob( regDir + '/*'))
        for tractDir in allTractDirs:
            
            allPatchFiles = glob.glob( tractDir + '/*HSC-G*.reg')
            tract = re.search(regDir+'(.*)',tractDir).group(1)
            print('Inserting the bright star masks for tract ' + tract + ' into the sql...')
            for patchFile in allPatchFiles:
                patch = re.search(r'\d,\d',patchFile).group()
                f = open(patchFile,'r')
                for line in f:

                    if line[0] != 'c':
                        continue
                    [tRa,tDec,tBsm] = re.findall('[-+]?\d+\.\d+',line)
                    bs_list = [float(tRa),float(tDec),float(tBsm)]
                    bs_list.append(int(tract))
                    bs_list.append(patch)
                    #print('Make sure that this was output correctly.')
                    #interact(local=locals())
                    self.c.execute('''INSERT INTO reg_all VALUES (?,?,?,?,?)''', bs_list)
                    
            
                    self.conn.commit()

                f.close()
        #start here 12/28/2015
        print('End of insertRegIntoSql')
        interact(local=locals())
        return

    def insertRandomsIntoSql(self):

        #START HERE! I WANT TO INCLUDE THE TRACT AND PATCH INFORMATION HERE!
        if self.overwriteSql:

            print('\nself.overwriteSql is set to TRUE, erasing the randoms_all table...')

            #Only drop the table if it exists...
            try:

                self.c.execute('''DROP TABLE randoms_all''')

            except:
                pass

            self.c.execute('''CREATE TABLE randoms_all (RA_J2000_degrees float, Dec_J2000_degrees float)''')

        else:

            for row in self.c.execute('''SELECT Count(*) FROM randoms_all'''):
                print(str(row[0])+' entries in the randoms_all sql database.')

        print('I want to find out how many files that we have pertaining to the randoms.')
        ###
        randomsDir = hscConfig.randomsDir
        #allTractDirs = filter(os.path.isdir,glob.glob( randomsDir + '/*'))
        allPatchFits = glob.glob( randomsDir + '*/*/*.fits')
        if len(allPatchFits)==0:

            print('No valid files found')
            interact(local=locals())
            
        for patchFile in allPatchFits:

            raRandoms, decRandoms = loadBrightStarSql.LoadBrightStarSqlTask().loadRandomsFitsTractPatch(patchFile)
            raDecRandoms = [(raRandoms[tempx],decRandoms[tempx]) for tempx in range(len(raRandoms))]

            self.c.executemany('''INSERT INTO randoms_all VALUES (?,?)''', raDecRandoms)
            self.conn.commit()

        self.conn.close()
        return

    def insertUcacIntoSql(self):

        #START HERE! I WANT TO INCLUDE THE TRACT AND PATCH INFORMATION HERE!
        if self.overwriteSql:
            print('\nself.overwriteSql is set to TRUE, erasing the old UCAC database...\n')

            #Only drop the table if it exists...
            try:
                
                self.c.execute('''DROP TABLE ucac_all''')

            except:
                
                pass

            self.c.execute('''CREATE TABLE ucac_all (ucac_id integer,       
                                                     RA_J2000_degrees float, Dec_J2000_degrees float,
                                                     mMag float, aMag float, Bmag float,Vmag float,
                                                     gMag float, rMag float, iMag float)''')
            #colnames = ['starid', 'ra', 'spd', 'magm', 'maga',
            #        'APASS-B', 'APASS-V', 'APASS-g', 'APASS-r', 'APASS-i']

        else:

            for row in self.c.execute('''SELECT Count(*) FROM ucac_all'''):
                print(str(row[0])+' entries in the ucac_all sql database.')

        with open(hscConfig.ucacDirFile) as fp:
        
            flist = [ f.strip() for f in fp.readlines() ]

            for f in flist:
                print('Inserting ' + f + ' out of 900....')
                #Start here, I don't think that any of this code has been developed yet...
                ucacData, colNames = loadBrightStarSql.LoadBrightStarSqlTask().loadUcac(f)
                self.c.executemany('''INSERT INTO ucac_all VALUES (?,?,?,?,?,?,?,?,?,?)''', ucacData)
                self.conn.commit()

                
        return
    
    def insertTychoIntoSql(self):

        # START HERE! I WANT TO INCLUDE THE TRACT AND PATCH INFORMATION HERE.
        if self.overwriteSql:

            print('\nself.overwriteSql is set to TRUE, erasing the old tycho database...')

            #Only drop the table if it exists...
            try:
                
                if self.insertReg:
                    self.c.execute('''DROP TABLE tycho_all''')

            except:
                
                pass

            self.c.execute('''CREATE TABLE tycho_all (tycho_id string,                                                    
                                                      RA_J2000_degrees float, Dec_J2000_degrees float,
                                                      btMag float, vtMag float)''')

        else:

            for row in self.c.execute('''SELECT Count(*) FROM tycho_all'''):
                print(str(row[0])+' entries in the tycho_all sql database.')

        tychoId, raTycho, decTycho, btMag, vtMag = loadBrightStarSql.LoadBrightStarSqlTask().loadTycho()

        bs_list = [(tychoId[tempx],raTycho[tempx],decTycho[tempx],btMag[tempx],vtMag[tempx]) for tempx in range(len(raTycho))]
        self.c.executemany('''INSERT INTO tycho_all VALUES (?,?,?,?,?)''', bs_list)
        self.conn.commit()

        return
    
    def insertNomadIntoSql(self):

        time0 = time.clock()
        
        if self.overwriteSql:

            print('\nself.overwriteSql is set to TRUE, erasing the old nomad database...')

            #Only drop the table if it exists...
            try:

                if self.insertReg:
                    self.c.execute('''DROP TABLE nomad_all''')

            except:
                pass

            self.c.execute('''CREATE TABLE nomad_all (nomad_id string, 
                                                 RA_J2000_degrees float, Dec_J2000_degrees float,
                                                 Bmag float, Bmag_ref char, Vmag float, Vmag_ref char, Rmag float, Rmag_ref char,
                                                 Jmag float, Hmag float, Kmag float,
                                                 bright_star_radius float)''')
        else:

            for row in self.c.execute('''SELECT Count(*) FROM nomad_all'''):
                print(str(row[0])+' entries in the nomad_all sql database.')

        nomad_home = hscConfig.catalogDirectory + 'NOMAD/'

        # We only need the directories that are linked to the HSC survey
        # nomad_dirs_a = os.listdir(nomad_home) # FULL DIRECTORY LIST
        nomad_dirs_a = ['130_139_nomad','080_089_nomad','090_099_nomad']
        nomad_dirs_ab = {'080_089_nomad':['nomad_083_txt']}
        
        # First make steps in Dec
        top_dirs = ['{:0>2d}'.format(int(temp)) for temp in np.unique(np.floor(0.1*(self.tpRADec['minDec']+90.0)))]

        for top_dir in top_dirs:
            #You need to find both where this is the maximum and where this is the minimum.

            goodDecs = np.where(self.tpRADec['minDec'] > float(top_dir)*10.0 - 90.0)
            tpRADec_top = self.tpRADec[goodDecs]
            goodDecs = np.where(tpRADec_top['minDec'] < float(top_dir)*10.0 - 80.0)
            tpRADec_top = tpRADec_top[goodDecs]

            nomad_dir_a = top_dir + '0_' + top_dir + '9_nomad'
            print('\nEntering ' + nomad_dir_a + '\n')

            nomad_dirs_b = filter(os.path.isdir,glob.glob(nomad_home+nomad_dir_a+'/*'))
            nomad_dirs_b.sort()
            #Just include the ones that haven't been inserted yet...
            #print('erase me')
            #nomad_dirs_b = nomad_dirs_b[8::]
            #nomad_dirs_b = nomad_dirs_ab[nomad_dir_a]

            if len(nomad_dirs_b) == 0:
                print('No valid folders')
            else:
                print(str(len(nomad_dirs_b)) + ' valid folders.')

            for nomad_dir_b in nomad_dirs_b:

                nomad_files = glob.glob(nomad_dir_b+'/*.txt')
                nomad_files.sort()
                print('\nEntering ' + nomad_dir_b + '\n')

                for nomad_file in nomad_files:
                    #nomad_file = '/array/users/czakon/catalogs/NOMAD/090_099_nomad/nomad_097_txt/0974_nomad.txt'
                    print(nomad_file)
                    ### 12/31/2015 Change this to simply read it line-by-line
                    with open(nomad_file) as f:
                        bs_list = []
                        #print('\nInserting ' + str(len(bright_stars)) + ' into the sql table.\n')
                        for bright_star in f:
                            ##bright_stars = ascii.read(nomad_file)
                            ##bright_stars.rename_column('col1','nomad_id')
                            ##bright_stars.rename_column('col3','RA  (J2000) Dec     r  sRA  sDE')
                            ##bright_stars.rename_column('col6','Bmag.r  Vmag.r  Rmag.r')# Integer 0.001 mag
                            ##bright_stars.rename_column('col7','Jmag   Hmag   Kmag')
                            ##bright_stars.remove_column('col8')# Flag indicating recommended
                            # Get rid of the columns below, keep the information here just in case
                            # it is useful in the future.
                            ##bright_stars.remove_column('col2')#,'BCTYM')#Flags for the source catalog?
                            ##bright_stars.remove_column('col4')#,'Ep.RA  EpDec') stdev in arcsecs
                            ##bright_stars.remove_column('col5')#,'pmRA     pmDE  spRA  spDE')
                            ##bright_stars.remove_column('col9')#,'r(")')
                    ##NOMAD1.0   |BCTYM|   RA  (J2000) Dec     r  sRA  sDE| Ep.RA  EpDec|    pmRA     pmDE  spRA  spDE| Bmag.r  Vmag.r  Rmag.r| Jmag   Hmag   Kmag |R| ;     r(")


                    # 12/31/2015 START HERE! TRY TO EXTRACT THE SOURCE MAGNITUDE INFORMATION AS WELL..
                            ##for bright_star in bright_stars:

                            #Format the unique id to exclude the hyphen
                            if bright_star[0] == '#':
                                continue
                            else:
                                ### NEW CODE BELOW
                                nomad_id = bright_star[0:12]
                                nomad1 = nomad_id.split('-')
                                star = {}
                                star['nomad_id'] = nomad1[0] + nomad1[1]
                                star['BCTYM'] = bright_star[13:18]
                                star['RA_J2000_degrees'] = float(bright_star[19:30])
                                star['Dec_J2000_degrees'] = float(bright_star[30:41])

                                try:
                                    star['Bmag'] = float(bright_star[98:104])
                                except:
                                    star['Bmag'] = np.nan
                                star['Bmag_ref'] = bright_star[104]

                                try:
                                    star['Vmag'] = float(bright_star[106:112])
                                except:
                                    star['Vmag'] = np.nan
                                star['Vmag_ref'] = bright_star[112]

                                try:
                                    star['Rmag'] = float(bright_star[114:120])
                                except:
                                    star['Rmag'] = np.nan
                                star['Rmag_ref'] = bright_star[120]

                                try:
                                    star['Jmag'] = float(bright_star[122:128])
                                except:
                                    star['Jmag'] = np.nan

                                try:
                                    star['Hmag'] = float(bright_star[129:135])
                                except:
                                    star['Hmag'] = np.nan

                                try:
                                    star['Kmag'] =  float(bright_star[136:142])
                                except:
                                    star['Kmag'] = np.nan
                                #The next part of the line has positional information bright_star[41:98]
                                
                                #member_catalogs
                                ### OLD CODE BELOW
                                #nomad1 = bright_star['nomad_id'].split('-')
                                #star = {}
                                #star['nomad_id'] = nomad1[0] + nomad1[1]
                                #ra_dec_col = bright_star['RA  (J2000) Dec     r  sRA  sDE']
                                #Hopefully this is standardized.
                                #star['RA_J2000_degrees'] = float(ra_dec_col[0:11])
                                #Let's keep all of the RAs positive...
                                #if star['RA_J2000_degrees'] < 0.0:
                                #    star['RA_J2000_degrees'] += 360.0
                                #star['Dec_J2000_degrees'] = float(ra_dec_col[11:22])
                                
                                #bs_B = re.findall('[-+]?\d+\.\d+',bright_star['Bmag.r  Vmag.r  Rmag.r'][0:7])
                                #bs_V = re.findall('[-+]?\d+\.\d+',bright_star['Bmag.r  Vmag.r  Rmag.r'][7:15])
                                #bs_R = re.findall('[-+]?\d+\.\d+',bright_star['Bmag.r  Vmag.r  Rmag.r'][15:21])

                                #bs_J = re.findall('[-+]?\d+\.\d+',bright_star['Jmag   Hmag   Kmag'][0:7])
                                #bs_H = re.findall('[-+]?\d+\.\d+',bright_star['Jmag   Hmag   Kmag'][7:14])
                                #bs_K = re.findall('[-+]?\d+\.\d+',bright_star['Jmag   Hmag   Kmag'][14:21])
                                #[bs_J,bs_H,bs_K] = bright_star['Jmag   Hmag   Kmag'].split()

                                #if bs_B == []:
                                #    bs_B = np.nan#None
                                #else:
                                #    bs_B = float(bs_B[0])

                                #if bs_V == []:
                                #    bs_V = np.nan#None
                                #else:
                                #    bs_V = float(bs_V[0])

                                #if bs_R == []:
                                #    bs_R = np.nan#None
                                #else:
                                #    bs_R = float(bs_R[0])

                                #if bs_J == []:
                                #    bs_J = np.nan#None
                                #else:
                                #    bs_J = float(bs_J[0])

                                #if bs_H == []:
                                #    bs_H = np.nan#None
                                #else:
                                #    bs_H = float(bs_H[0])

                                #if bs_K == []:
                                #    bs_K = np.nan#None
                                #else:
                                #    bs_K = float(bs_K[0])

                                #if bright_star['R'] is np.ma.masked:
                                #    bright_star['R'] = np.nan#None
                                #star['Bmag'] = bs_B
                                #star['Vmag'] = bs_V
                                #star['Rmag'] = bs_R
                                #star['Jmag'] = bs_J
                                #star['Hmag'] = bs_H
                                #star['Kmag'] = bs_K

                                min_mag = np.nanmin([star['Bmag'],star['Vmag'],star['Rmag']])
                
                                if np.isnan(min_mag) or min_mag > 17.5:

                                    continue

                                bsr = hscConfig.brightStarRadius['moderatelyConservative']
     
                                star['bright_star_radius'] = bsr['A0'] * 10**(bsr['B0'] * (bsr['C0'] - min_mag))/3600. + \
                                    bsr['A1'] * 10**(bsr['B1'] * (bsr['C1'] - min_mag))/3600.
                                
                                bs_list.append((star['nomad_id'],star['RA_J2000_degrees'],star['Dec_J2000_degrees'], \
                                           star['Bmag'], star['Bmag_ref'], star['Vmag'], star['Vmag_ref'], star['Rmag'], star['Rmag_ref'], \
                                           star['Jmag'], star['Hmag'], star['Kmag'], star['bright_star_radius']))

                                if (min_mag < 2.0) or (star['bright_star_radius'] > 1.0):
                                    print('Either the min_mag is less than 2.0 or the bright_star_radius is greater than one degree, check this out....')

                        self.c.executemany('''INSERT INTO nomad_all VALUES (?,?,?,
                                                                            ?,?,?,?,?,?,
                                                                            ?,?,?,?)''', bs_list)
            
                        self.conn.commit()

                    for row in self.c.execute('''SELECT Count(*) FROM nomad_all'''):
                        print(str(row[0])+' entries in the nomad_all sql database.')
                    time1  = time.clock()
                    timed = time1-time0
                    print('\nThe program has taken ' + str(timed/60.) + ' minutes to execute.\n')

        return

    def modifyBsm(self):
        print('modifyBsm This method takes to long to perform! Just write the data from scratch...')
        time0 = time.clock()
        bsr = hscConfig.brightStarRadius['moderatelyConservative']
        #This is a generic program in case we need to modify the database after it already has been created.
        #Feel free to modify this as the database needs to be tweaked.
        #interact(local=locals())
        nomadRa = []
        nomadDec = []
        nomadBmag = []
        nomadVmag = []
        nomadBsr = []
        tychoRa = []
        tychoDec = []
        tychoMinMag = []
        #tychoBtmag = []
        #tychoVtmag = []
        ###
        tychoAll = self.c.execute('''SELECT RA_J2000_degrees, Dec_J2000_degrees, btMag, vtMag FROM tycho_all
                                     WHERE (btMag < 11.0) OR (vtMag < 11.0)''')

        for row in tychoAll:
            tychoRa.append(row[0])
            tychoDec.append(row[1])
            if row[2] == None:
                tychoMinMag.append(row[3])
            elif row[3] == None:
                tychoMinMag.append(row[2])
            else:
                tychoMinMag.append(np.nanmin([row[2],row[3]]))
            #tychoBtmag.append(row[2])
            #tychoVtmag.append(row[3])
            
        tychoRa = np.array(tychoRa)
        tychoDec = np.array(tychoDec)
        tychoRadec = SkyCoord(ra=tychoRa*u.degree,dec=tychoDec*u.degree)
        tychoMinMag = np.array(tychoMinMag)
        tychoBsr = bsr['A0'] * 10**(bsr['B0'] * (bsr['C0'] - tychoMinMag))/3600. + \
                   bsr['A1'] * 10**(bsr['B1'] * (bsr['C1'] - tychoMinMag))/3600.

        newBsm = []
        nomadId = []
        deleteId = []
        for row in self.c.execute('''SELECT nomad_id, RA_J2000_degrees, Dec_J2000_degrees, 
                                                      Bmag, Vmag, bright_star_radius
                                     FROM nomad_all WHERE (Bmag < 11.0) OR (Vmag < 11.0) OR (Rmag < 11.0)'''):

            nomadRa.append(row[1])
            nomadDec.append(row[2])
            nomadId.append(row[0])
            nomadBsr.append(row[5])
        nomadId = np.array(nomadId)
        nomadBsr = np.array(nomadBsr)
        
        nomadRadec = SkyCoord(ra=nomadRa*u.degree,dec=nomadDec*u.degree)
        print('Matching the nomad catalog to the tycho one...')
        time4 = time.clock()
        idx,d2d,d3d = tychoRadec.match_to_catalog_sky(nomadRadec)
        time5 = time.clock()
        timed = time5-time4
        print('\nThis process has taken ' + str(timed/60.) + ' minutes to execute.\n')
        print(str(len(np.intersect1d(nomadBsr[idx],tychoBsr))) + ' entries already inserted.')
        #Hopefully, this gives the values in one array that are not in the other array.
        #print(len(np.in1d(nomadBsr[idx],tychoBsr,invert=True)))
        print('\nThe program has taken ' + str(timed/60.) + ' minutes to execute, will now start to insert into the arrays....\n')
        #interact(local=locals())
        #changeX = np.where(d2d.arcsecond < 1.0)[0]
        changeX = d2d.arcsecond < 1.0
        if True:
            #nomadId[idx[changeX]]
            #tychoBsr[changeX]
            #Only ten entries takes 1.16 minutes! Should simply modify this from the beginning.
            bs_list = [(tychoBsr[tempX],nomadId[idx[tempX]]) for tempX in changeX[0:9]]
            print('Updating the bright star radius to the default tycho ones....')
            self.c.executemany('''UPDATE nomad_all SET bright_star_radius=? WHERE nomad_id=?''', \
                               bs_list)
            time6 = time.clock()
            timed = time6-time5
            print('\nThe program has taken ' + str(timed/60.) + ' minutes to execute.\n')
            interact(local=locals())

        if True:
            #Note! This will remove all of the brighter than 11 stars that are outside of the
            #Dec range that I have inserted into the NOMAD catalog...
            # Oh well....
            print('Removing any of the nomad entires that do not have Tycho counterparts....')
            #Question, what about the Tycho values that do not have Nomad counterparts???
            self.c.executemany('''DELETE FROM nomad_all WHERE nomad_id=?''',(nomadId[idx[~changeX]]))
        self.conn.commit()
        self.conn.close()
        time1 = time.clock()
        timed = time1-time0
        #print(len(np.intersect1d(nomadBsr[idx],tychoBsr)))
        print('\nThe program has taken ' + str(timed/60.) + ' to completely execute.\n')

            #if d2d.arcsecond < 1.0:
            #If the source is good, then simply update the bright_star_radius
            ###
       #     if tychoBtmag[idx]==None:
       #         min_mag = tychoVtmag[idx]
       #     elif tychoVtmag[idx]==None:
       #         min_mag = tychoBtmag[idx]
       #     else:
       #         min_mag = np.nanmin([tychoBtmag[idx],tychoVtmag[idx]])
                
            #if tempBsm == row[5]:
            #    continue
            #else:
       #     newBsm.append(tempBsm)
       #     nomadId.append(row[0])
                #interact(local=locals())
                #print([newBsm,row[-1]])
                #print([newBsm,row[5]])
                #interact(local=locals())
        #else:
                #Otherwise,
                #print('Source has been classified as a bad source, will drop the star database....')
                #Although, there are no stars that have this large of a separation.
        #    deleteId.append(row[0])
        #interact(local=locals())
        #nomadRa.append(row[0])
        #nomadDec.append(row[1])
        #nomadBmag.append(row[2])
        #nomadVmag.append(row[3])

            
        ###
        #print('The maximum distance of the Tycho from the Nomad stars above 11.0 is: ' + str(max(d2d.arcsecond)))
        
        #print(str(len(badSources)) + ' sources in the nomad catalog are separate by more than one arcsecond from the tycho catalog....') 
        #interact(local=locals())
        #STEP 2:
        #Find where the NOMAD catalog overlaps with the Tycho catalog
    def insertPicklesSql(self):

        return
    
    def loadSql(self):

        print('\nLoading the Sql database: ' + self.dbFilename)
        self.conn = sqlite3.connect(self.dbFilename)

        #Create a cursor object
        self.c = self.conn.cursor()
        return self.c
    
########################################
    def loadTractPatchFile(self,field):
        #The purpose of this program is to load a particular patch file from the sp survey.

                
            for tract_patch_full in tract_info_file:

                tract_patch = {'tract':tract_patch_full['tract'],'patch':tract_patch_full['patch']}

                if tract_patch in tract_patch_completed:
                    tract_patch_in_sql = True

                else:
                    tract_patch_in_sql = False
                #Start here!!! Search the directory and make sure that the file doesn't already exist.
                #We want to still perform the search if the reg file doesn't exist.
                # for band in hscConfig.bands: (this needs to go further down.)
                band = hscConfig.bands[0]

                directory_out = self.folderout + sub_directory + \
                                'deepCoadd/BrightObjectMasks/'+tract_patch['tract'] +'/'

                try:
                    os.stat(directory_out)
                except:
                    print(directory_out + ' does not exist, will create it....')
                    os.mkdir(directory_out)
                    
                reg_fileout = directory_out + 'BrightObjectMask-' + \
                              tract_patch['tract'] + '-' + tract_patch['patch'] + '-HSC-' + band + '.reg'

                print('\nWriting star mask to to reg_fileout: ' + reg_fileout)

                reg_file_exists = os.path.isfile(reg_fileout)

                if tract_patch_in_sql and self.insertSql and reg_file_exists:

                    print('\nTract: ' + tract_patch['tract'] + \
                          ' Patch: ' + tract_patch['patch'] + \
                          ' already inserted in Sql and the .reg file exists, skipping...')

                elif not tract_patch_in_sql and not self.insertSql and reg_file_exists:

                    print('\nTract: ' + tract_patch['tract'] + \
                          ' Patch: ' + tract_patch['patch'] + \
                          ' .reg file exists but the patch is not yet inserted into the Sql.'+ \
                          'As the \'insertSql\' is set to False, will skip this tract patch...')

                else:
                    
                    Corner1RA = tract_patch_full['Corner1RA']
                    Corner1Dec = tract_patch_full['Corner1Dec']
                    Corner3RA = tract_patch_full['Corner3RA']
                    Corner3Dec = tract_patch_full['Corner3Dec']
                    n_searches = 15#11
                    extra_width = 0.2
                    total_width = np.max(np.abs(Corner3RA - Corner1RA),np.abs(Corner3Dec - Corner1Dec))*(1.0+ extra_width)
                    #t_width = hscConfig.patch_side * hscConfig.pix_scale/(n_searches+1.0)
                    t_width = total_width/n_searches


########################################
    def test(self):
        #START HERE!!! Make sure that the appropriate code is deleted. 
        #testExposure = loadTestExposure.LoadExposureTask(loadCoadd=True,filter='G',doPlot=False)
            #End of it single exposure or coadd conditional statement.
        #self.mapCoord = testExposure.mapCoord
        #self.exposure = testExposure.exposure
        print('\nStarting the test file...')
        print('If you don\'t understand what this is doing, then you should get rid of it...')
        interact(local=locals())
        self.overwrite = True
        nomadOut = self.run()
        return nomadOut

if __name__ == '__main__':
    print('Made it to main')
    brightStars = InsertBsmTask(test=True)#,overwriteSql=True)
