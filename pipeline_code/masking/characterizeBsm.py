#!/usr/bin/env python
#
# Created by Nicole Czkon 12102015
#
# Description of the Code:
# The purpose of this code is to characterize the bright star masks in a quantitative fashion. 
#
# KEYWORDS:
#
# USAGE:
#
# from pipeline_code.masking import insertNomadIntoSQL.py
#
# The default setting, loads the results from file.
# brightStars = insertNomadIntoSQL.FindNomadTask()
# brightStars = retrieveBrightStarsFromNomad.FindNomadTask(insertSQL=True)
#
# OUTPUT:
# 
# LIST OF FUNCTIONS:
#
#import os,re
#import glob
################# For Debugging ###########################################
from code import interact
import time
########### Pipeline-Related Packages #####################################
#import lsst.daf.persistence as dafPersist
#import lsst.afw.coord as afwCoord
#import lsst.afw.geom as afwGeom
#import lsst.pex.config as pexConfig
############ General Python Packages #####################################
#import astropy.units as u
#import astropy.coordinates as coord
#import numpy as np
#from astropy.io import ascii#,fits
#from astropy.table import vstack
#from datetime import date
## Additional package needed in order to download the data from Vizier ###
#from astroquery.vizier import Vizier
#from copy import deepcopy
#import cPickle as pickle
import sqlite3
########## My Own Python Packages #######################################
from pipeline_code import hscConfigurationFile as hscConfig
from pipeline_code.loadFiles import loadTractInfoFile
from pipeline_code.loadFiles.loadBrightStarSql import LoadBrightStarSQLTask as bsSql

__all__ = ["CharacterizeBsmTask"]

########## FINDING NOMAD SOURCES FOR VERY BRIGHT STARS  #################
class CharacterizeBsmTask:
    _DefaultName = "characterizeBsm"

    def __init__(self,dbFilename=None,test=None):

            
        if dbFilename is None:
            self.dbFilename = None
        else:
            print('\ndbFilename is set to: ' + dbFilename +'\n')
            self.dbFilename = dbFilename

        #conn = sqlite3.connect(self.dbFilename)

        #Create a cursor object
        #self.c = conn.cursor()
        
        if test is not None:
            #Then, this is run from the command line
            self.findCoverage()
        return 

    def findCoverage(self):

        #time0 = time.clock()
        #print('\nStarting to insert the selected entries into the database, this will take a while...')

        fields = hscConfig.fields

        for fieldName,fieldStats in fields.items():

            tractInfoFile = loadTractInfoFile.data(fieldName=fieldName)

            for tractName,tractStats in tractInfoFile.items():

                if tractName[0] != 'C':
                    
                    for patchName,patchStats in tractStats.items():

                        minRA  = patchStats['Corner1']['RA']
                        maxRA  = patchStats['Corner3']['RA']
                        minDec = patchStats['Corner1']['Dec']
                        maxDec = patchStats['Corner3']['Dec']
                        #raRandoms,decRandoms = \
                        #    bsSql(dbFilename=self.dbFilename).loadRandomsSqlRaDec(minRA,maxRA,minDec,maxDec)
                        interact(local=locals())
            # Step 1: Load all of the randoms.


if __name__ == '__main__':
    print('Made it to main')
    junk = CharacterizeBsmTask(dbFilename='bs_copy2.db',test=True)#,overwriteSQL=True)
