###############################################################################
# !/usr/bin/env python
# Created by Nicole Czakon 09212015
#
# Description of the Code:
# The purpose of this code is to characterize the extent of the bright-star
# halo. Currently, this is using sql catalogs from the public data release,
# although, it probably has to be done using the images directly...
#
# USAGE:
# import brightStarCorrelationFunctionNew
# temp = brightStarCorrelationFunctionNew.CalculateBrightStarCorrelation(
#             magModel='cmodel',plot=True)
# temp = brightStarCorrelationFunctionNew.CalculateBrightStarCorrelation(
#             magModel='cmodel',overwrite=True)
#
# OUTPUT:
#
########### General Python Packages ###########################################
import os,time
from code import interact
import numpy as np
from copy import copy,deepcopy
import cPickle as pickle
import sqlite3
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.backends.backend_pdf import PdfPages
import astropy.units as u
import astropy.coordinates as coord
from astropy.io import fits
from astropy.table import vstack
from astroquery.vizier import Vizier
from sklearn.neighbors import KDTree
from scipy.interpolate import UnivariateSpline

########## My Own Python Packages #######################################
from pipeline_code import hscConfigurationFile as hscConfig
from pipeline_code.hscConfigurationFile import brightStarRadius as bsrEstimate
import loadTestExposure
import loadTestExposure
from pipeline_code.loadFiles.loadBrightStarSql \
    import LoadBrightStarSqlTask as bsSql
#
# def two_pt_correlation_fxn:
#
# PURPOSE: to calculate the radius at which the sources counts fall off.
#
# ADDITIONAL INFORMATION:
#
#     sources: objects detected in the HSC images.
#     brightStar: bright-stars extracted from a brightStar catalog.
#                 Currently the default catalog is the UCAC one.
#
# OPTIONAL KEYWORDS:
#
#     plot_2pt: This is a kludge because I want to save the plots in different
#               files and I can't figure out how to do this without saving the
#               correlation functions in a different structure.
#     verbose: this gives a little more output in case we're interested in more
#              details.
#     overwrite: Since it takes a while to do these calculations,
#                it's better to pickle the results whenever you decide on a
#                configuration.
#                

__all__ = ["CalculateBrightStarCorrelationNew"]

class CalculateBrightStarCorrelationNew:

    _DefaultName = "CalculateBrightStarCorrelationNew"

    def __init__(self,magModel=None,overwrite=None,plot=None,verbose=None, \
                 brightStarDb=None,sourcesDb=None,randomsDb=None, \
                 doSourcesSq = None, *args,**kwargs):

        self.maxBinArcsec = 4E3#300.0
        mag_model_options = ['cmodel','kron']

        if doSourcesSq is None:
            self.doSourcesSq = True
        else:
            self.doSourcesSq = False
            
        if magModel in mag_model_options:
            self.magModel = magModel
        else:
            print('magModel is not specifed, will set to cmodel by default...')
            self.magModel = 'cmodel'

        self.plotFolder = hscConfig.maskPlotDirectory
        #fields = {'W-XMM':{'RA':[25,43],'Dec':[-10,1]}, \
            #          'W-GAMA09H':{'RA':[126,144],'Dec':[-5.0,7.0]}, \
            #          'W-GAMA15H':{'RA':[206,228],'Dec':[-6.0,6.0]}, \
            #          'W-VVDS':{'RA':[326,346],'Dec':[-4.0,6.0]}, \
            #          'W-WIDE12H':{'RA':[172,186],'Dec':[-5.0,5.0]}, \
            #          'W-HECTMAP':{'RA':[236,252],'Dec':[37.0,49.0]}}

        fields = hscConfig.fields

        self.fieldName = 'W-XMM'

        self.corrFilename = hscConfig.corrFilename + self.magModel + '_' + \
                            self.fieldName + '.p'

        self.corrFileExists = os.path.isfile(self.corrFilename)

        if brightStarDb is None:
            self.brightStarDb = hscConfig.brightStarDb
        else:
            self.brightStarDb = brightStarDb
            
        if sourcesDb is None:
            self.sourcesDb = hscConfig.sourcesDb
        else:
            self.sourcesDb = sourcesDb

        if randomsDb is None:
            self.randomsDb = hscConfig.randomsDb
        else:
            self.randomsDb = randomsDb

        if verbose:
            self.verbose = True
        else:
            self.verbose = False

        if overwrite or not self.corrFileExists:
            self.overwrite = True
            self.run(fields[self.fieldName])
        else:
            self.overwrite = False

        self.load()

        if plot:
            self.plot = True
            self.doPlotAll()
        else:
            self.plot = False

    def load(self):
        
        self.corrAll = pickle.load(open(self.corrFilename,"rb"))
        self.minMags = self.corrAll['minMags']
        self.maxMags = self.corrAll['maxMags']
        self.nSrcMags = len(self.minMags)
        self.minMagBrightStarBins = self.corrAll['minMagBrightStarBins']
        self.nBrightStarBins = len(self.minMagBrightStarBins)-1
        self.bins =  self.corrAll['bins']
        self.nBins = len(self.bins)
        self.bands =  self.corrAll['bands']
        self.nBands = len(self.bands)
        self.nSources =  self.corrAll['nSources']
        self.nBrightStar =  self.corrAll['nBrightStar']
        self.medBrightStar =  self.corrAll['medBrightStar']
        self.cntSrcSq = self.corrAll['corr']['countsSourcesSq']
        self.cntBsSq =  self.corrAll['corr']['countsBrightStarSq']
        self.cntSrcBs = self.corrAll['corr']['countsSourcesBrightStar']
        self.bestFitArcmin = self.corrAll['corr']['bestFitArcmin']
        self.corrSrcBs =  \
                                self.corrAll['corr']['corrSourcesBrightStar']
        #self.corrCutoff =  self.corrAll['corr']['cutoff']
        #corr_cutoff = 0.2

    def calculateHaloRadius(self,xvalues,yvalues,xArg,xCut):

        #This smooths the line to make it more of a continuous function.
        do_spline_fit = True

        # Do some checks on the input values:
        if len(xvalues) != len(yvalues):
            print('The number of radial bins does not match the number of '+ \
                  'correlation measurements.')
            interact(local=locals())

        good_y = np.isfinite(yvalues)
        xvalues = xvalues[good_y]
        yvalues = yvalues[good_y]

        if do_spline_fit:

            new_x = np.logspace(0,3,200)

            try:

                spl = UnivariateSpline(xvalues,yvalues,
                                       k=4,s=0.025,check_finite=True)
                spl_roots = spl.derivative().roots()
                
            except:

                spl_roots = []
                                    
            if len(spl_roots) > 0:
                
                max_root = np.argmax(spl(spl_roots))
                falseDetR = spl_roots[max_root]
                falseDetMax = spl(spl_roots[max_root])

            else:

                #Set the false detection radius to a very large value.
                falseDetR = 300.
                try:
                    falseDetMax = np.max(yvalues)
                except:
                    falseDetMax = np.max(yvalues)
        #END if do_spline_fit
        
        #Find the bright star radius.
        halo_radius_x = len(yvalues)

        # First find whether the maximum is above the bottom limit,
        # and the minimum is below the lower limt.
        if (np.max(yvalues) > xCut[0]) and (np.min(yvalues) < xCut[1]):
                                    
            #Find the maximum radius where the values stray beyond the limits
            try:

                halo_radius_x = np.max([np.max(np.where(yvalues > xCut[1])),
                                        np.max(np.where(yvalues < xCut[0]))])

            except:

                try:
                    halo_radius_x = np.max(np.where(yvalues < xCut[0]))
                except:
                    halo_radius_x = np.max(np.where(yvalues > xCut[1]))
        else:

            halo_radius_x = len(yvalues)-2

        # halo_radius_x = [np.max(halo_radius_x),np.max(halo_radius_x)+1]
        halo_radius_x = [halo_radius_x,halo_radius_x+1]
        xhalo_radius = xvalues[halo_radius_x]
        yhalo_radius = yvalues[halo_radius_x]
        slope = np.diff(yhalo_radius)/np.diff(xhalo_radius)

        if yhalo_radius[0] > 0.0:
                                    
            newy = xCut[1]

        else:
            
            newy = xCut[0]

        #Interpolate between the values.
        haloRadius = np.min([(newy-yhalo_radius[0])/slope+xhalo_radius[0],
                             xvalues[-1]])

        return haloRadius,falseDetR,falseDetMax
    
    def calculateHaloRadiusAll(self):

        # Initialize the dictionary for the final measurement.
        hrAll =  {
            'small':np.ndarray(shape=(self.nBands,self.nBrightStarBins,
                                      self.nSrcMags),dtype=float),
            'medium': np.ndarray(shape=(self.nBands,self.nBrightStarBins,
                                        self.nSrcMags),dtype=float),
            'large': np.ndarray(shape=(self.nBands,self.nBrightStarBins,
                                       self.nSrcMags),dtype=float)
        }
                                                
        frAll = np.ndarray(
            shape=(self.nBands,self.nBrightStarBins,self.nSrcMags),
            dtype=float)
            
        fmAll =  np.ndarray(
            shape=(self.nBands,self.nBrightStarBins,self.nSrcMags),
            dtype=float)

        # MAKE THE LOOP THROUGH WHICH WE WILL CALCULATE THE VALUES>
        for j_band in range(self.nBands):

            for j_mag in range(self.nSrcMags):

                for j_brightStar in range(self.nBrightStarBins):

                    xvalues = self.bins[1::]*3600
                    yvalues = self.corrSrcBs[j_band][j_brightStar][j_mag]

                    for xArg,nCut in hscConfig.corrCutoff.items():
                        
                        allRadii = self.calculateHaloRadius(xvalues,yvalues,
                                                            xArg,nCut)
                        hrAll[xArg][j_band][j_brightStar][j_mag] = allRadii[0]
                        frAll[j_band][j_brightStar][j_mag] = allRadii[1]
                        fmAll[j_band][j_brightStar][j_mag] = allRadii[2]
                                                                   
        return hrAll,frAll,fmAll

    def run(self,field):

        time0 = time.time()

        if self.magModel == 'kron':
            self.bands = ['g','r','i','z','y']
        else:
            self.bands = ['g']

        # Let's start at one arcsec bins for the correlation function and then
        # build up to 30 arcsec.
        minBinArcSec = 1.0
        self.nBins = 20

        #Need both a min and a max, otherwise, the datasets become too large.
        self.minMags = [21.0]#,22.0,23.00,24.00,25.00,26.00,26.50]
        #self.maxMags = [22.5,23.0,23.75,24.30,25.25,26.15,27.15]
        self.maxMags = {'kron':{'g':[23.0,24.0,23.75,24.50,25.25,26.15,27.15], \
                            'r':[23.0,23.5,23.75,24.30,25.25,26.25,27.15], \
                            'i':[23.0,23.0,23.75,24.30,25.25,26.5,27.5], \
                            'z':[22.5,23.0,23.75,24.30,25.40,26.5,27.5], \
                            'y':[22.5,23.0,23.75,24.30,25.40,26.6,28.0]}, \
                    'cmodel':{'g':[22.5,23.0,23.75,24.30,25.25,26.5,27.15], \
                              'r':[23.0,23.5,23.75,24.40,25.25,26.25,27.15], \
                              'i':[22.5,23.0,23.75,24.30,25.25,26.25,27.15], \
                              'z':[22.25,22.8,23.50,24.30,25.20,26.5,27.5], \
                              'y':[22.25,22.8,23.50,24.30,25.25,26.5,27.5]}}
                                                
        # For the bright-star mags, we only need to take the difference b/n
        # adjacent bins.
        self.minMagBrightStarBins = [1.0,6.0,7.5,7.75,8.5,9.0,
                                     10.0,11.0,12.5,14.5,17.5]
        self.nBrightStar = [0.0]*len(self.minMagBrightStarBins)

        #Step 1: Load the Randoms
        raRandoms,decRandoms = bsSql(brightStarDb=
                                     self.brightStarDb).loadRandomsSqlRaDec(\
                                        field['RA'][0],field['RA'][1], \
                                        field['Dec'][0],field['Dec'][1])
        
        #return idUcac,raUcac,decUcac,bsrUcac,bUcac,vUcac,rUcac,magUcac
        idBs,raBs,decBs,bsrUcac,bUcac,vUcac,rUcac,magBrightStar = \
            bsSql(brightStarDb=self.brightStarDb).loadUcacSqlRaDec(
                field['RA'][0],field['RA'][1],field['Dec'][0],field['Dec'][1])
        
        idBs = []        
        bUcac = []
        vUcac = []
        rUcac = []
        tempBins = np.array(0)
        
        self.bins = np.append(
            tempBins,np.logspace(np.log10(minBinArcSec/3600.),
                                 np.log10(self.maxBinArcsec/3600.),self.nBins))
                                 
        binsSqr = self.bins**2
        binsArea = np.pi*(binsSqr[1::]-binsSqr[0:-1])*(3600)**2
        self.nBins = len(self.bins)
        self.nBands = len(self.bands)
        nbrightStarBins = len(self.minMagBrightStarBins)-1
        nsrcMags = len(self.minMags)
        dataType = type(np.ndarray(self.nBins-1))
        # Technically, this doesn't have to be counted for every brightStar bin.
        # Consider removing if slowing the program down.
        cntSrcSq = np.ndarray(
            shape=(self.nBands,nbrightStarBins,nsrcMags),dtype=dataType)
        # Technically, this doesn't have to be counted for every band bin or
        # src bin.
        cntBsSq = np.ndarray(
            shape=(self.nBands,nbrightStarBins,nsrcMags),dtype=dataType)
        cntSrcBs = np.ndarray(
            shape=(self.nBands,nbrightStarBins,nsrcMags),dtype=dataType)
        self.corrSrcBs = np.ndarray(
            shape=(self.nBands,nbrightStarBins,nsrcMags),dtype=dataType)
        bestFitArcmin =  np.ndarray(
            shape=(self.nBands,nbrightStarBins,nsrcMags),dtype=float)
        nbrightStar =  np.ndarray(shape=(nbrightStarBins),dtype=float)
        self.medBrightStar =  np.ndarray(shape=(nbrightStarBins),dtype=float)
        self.nSources =  np.ndarray(shape=(self.nBands,nsrcMags),dtype=float)

        # Use the scikit to get started, this can be sophisticated if we look at
        # the examples in astroML (although it doesn't have the full
        # functionality).

        # Include these parts of the loop as you find appropriate.
        #END OF: if self.nSources[j_band][j_mag] != 0

        raDecRandoms = np.vstack((raRandoms,decRandoms)).T
        kdtreeRandoms = KDTree(raDecRandoms)
        #END OF: for j_mag in range(nsrcMags):
        j_mag = 0

        for j_band in range(self.nBands):
            
            raSources, decSources, MagCmodelSources = bsSql(
                verbose=True,band=self.bands[j_band]
            ).loadSourcesSspDownload(field['RA'][0],field['RA'][1], \
                                     field['Dec'][0],field['Dec'][1])
                                                        
            self.nSources[j_band][j_mag] = len(raSources)
            raDecSources = np.vstack((raSources,decSources)).T
            kdtreeSources = KDTree(raDecSources)

            for j_brightStar in range(nbrightStarBins):

                good_brightStar = np.where(
                    (magBrightStar \
                     >= self.minMagBrightStarBins[j_brightStar]) \
                    & (magBrightStar \
                       < self.minMagBrightStarBins[j_brightStar+1]))

                self.medBrightStar[j_brightStar] = \
                    np.median(magBrightStar[good_brightStar])
                self.nBrightStar[j_brightStar] = len(good_brightStar[0])
                raBs_bin = raBs[good_brightStar]
                decBs_bin = decBs[good_brightStar]
                raDecBrightStar_bin = np.vstack((raBs_bin,
                                                 decBs_bin)).T

                print('Using ' + str(len(good_brightStar[0])) \
                      + ' bright stars between ' \
                      + str(self.minMagBrightStarBins[j_brightStar]) + ' and ' \
                      + str(self.minMagBrightStarBins[j_brightStar+1]) + \
                      ' magnitudes...\n')

                kdtreeBrightStar = KDTree(raDecBrightStar_bin)
                
                #Now, perform the two_point correlation fxn
                bins = self.bins
            
                time1 = time.time()
                #max 3.0E8
                print('Determining the 2-pt correlation function of the'
                      ' bright stars themselves....')

                cntBsSq[j_band][j_brightStar][j_mag] =  \
                    np.diff(kdtreeBrightStar.two_point_correlation(
                        raDecBrightStar_bin,bins)) / binsArea

                time2 = time.time()
                timeTot = (time2 - time1) / 60.
                time1 = time2
                print('This segment has taken a total of ' + str(timeTot) + \
                      ' minutes to run.\n')
            
                #max 1.8E10
                print('Determining the 2-pt correlation function between the'
                      ' sources and the bright stars....')

                cntSrcBs[j_band][j_brightStar][j_mag] = \
                            np.diff(kdtreeSources.two_point_correlation(
                                raDecBrightStar_bin,bins)) / binsArea

                time2 = time.time()
                timeTot = (time2 - time1) / 60.
                time1 = time2
                print('Finished calculating the two-pt correlation function.\n')
                print('This segment has taken a total of ' + str(timeTot) + \
                      ' minutes to run.\n')

                if self.doSourcesSq:
                    #max 3.3E10
                    print('Determining the 2-pt correlation function of the'
                          ' sources themselves....\n')
                    cntSrcSq[j_band][j_brightStar][j_mag] = np.diff(
                        kdtreeSources.two_point_correlation(
                            raDecSources,bins
                        )) / binsArea
                                               
                    time2 = time.time()
                    timeTot = (time2 - time1) / 60.
                    time1 = time2
                    print('This segment has taken a total of ' + str(timeTot) \
                          + ' minutes to run.\n')

                #max 4.6E8
                # I don't think that I need to store the randoms right now.
                print('Determining the 2-pt correlation function between the'
                      ' randoms and the bright stars....\n')
            
                countsRandomsBrightStar = \
                    np.diff(kdtreeRandoms.two_point_correlation(
                        raDecBrightStar_bin,bins
                    )) / binsArea

                time2 = time.time()
                timeTot = (time2 - time1) / 60.
                time1 = time2
                print('This segment has taken a total of ' + str(timeTot) + \
                      ' minutes to run.\n')

                #max 3.3E8
                #This is simple used as a cross-check.
                #counts_sources_rbrightStar = \
                #    np.diff(kdtreeSources.two_point_correlation(
                #        raDecRandoms,bins)) / binsArea
                #W = D1*D2/(D2*R1)-1
                #print(len(raRandoms)/len(raSources)))
                #print(len(raRandoms))
                #print(len(raSources))
                #I need to normalize the output by the difference in the
                #number counts of the two samples.
                fudgeFactor = float(len(raRandoms))/len(raSources)#0.2
                self.corrSrcBs[j_band][j_brightStar][j_mag] =  \
                    cntSrcBs[j_band][j_brightStar][j_mag] \
                    / countsRandomsBrightStar*fudgeFactor-1
                #print('Investigate why the above is turning to zero.')
                #print('Perhaps we\'ll have to plot them up together and see'
                #      ' if they overlap in RA Dec space.....')
                #I've verified that this gives the same answer as well
                #W2 = cntSrcBs/counts_sources_rbrightStar-1
                #I've confirmed that these give the same outputs.
                #counts_brightStar_sources = np.diff(
                #    kdtreeBrightStar.two_point_correlation(raDecSources,bins))
                #Keep the following plot for the time being
                #print('Stop here to examine how to find the spline fit.')
                #Find the spot where the source counts equal approximately
                #80% of their overall value.
                #source_fraction = 0.55
                temp = self.bins*3600.>3.0
                xBins = np.where(temp==True)[0]
                goodBins = self.bins[xBins]
                #x_halo = np.abs(cntSrcBs - source_fraction*cntSrcBs[-1])
                tempMin = goodBins[np.argmax(
                    cntSrcBs[j_band][j_brightStar][j_mag][xBins[0:-1]])+1]*3600
                bestFitArcmin[j_band][j_brightStar][j_mag] = tempMin
                self.corrAll = {
                    'bins':self.bins, 'bands':self.bands, \
                    'minMags':self.minMags,'maxMags':self.maxMags,\
                    'nSources':self.nSources, \
                    'minMagBrightStarBins':self.minMagBrightStarBins,\
                    'nBrightStar':self.nBrightStar, \
                    'medBrightStar':self.medBrightStar, \
                    'corr':{'bestFitArcmin':bestFitArcmin, \
                            'countsBrightStarSq':cntBsSq, \
                            'countsSourcesSq':cntSrcSq, \
                            'countsSourcesBrightStar':cntSrcBs, \
                            'corrSourcesBrightStar':self.corrSrcBs
                    }}
                #Hopefully the above works properly.
                #self.corrAll = {'self.minMags':self.minMags,'max_mag':self.maxMags,''}    
                #END OF: for j_brightStar in range(nbrightStarBins)
                #END OF: if self.nSources[j_band][j_mag] != 0
                #END OF: for j_mag in range(nsrcMags):
        #END OF: for j_band in range(self.nBands):

        pickle.dump(self.corrAll,open(self.corrFilename,'wb'))

        # Close the sql connection
        #conn.close()
        #conn_bs.close()
        time2 = time.time()
        timeTot = (time2 - time0) / 60.
        print('This program has so far taken a total of ' + str(timeTot) + \
              ' minutes to run.\n')

    #END OF def run(self)

    def doPlot(self,plotParams,plot_pdf):
        
        #Put this in the appropriate section.
        #Move this to the plotting section.
        #plt.scatter(spl_roots[max_root],spl(spl_roots[max_root]),
        #            marker='x',c='k',s=60)
        plot_title_j_mag = plotParams['jmagTitle']
        all_mag_string = plotParams['all_mag_string']
        j_band = plotParams['j_band']
        j_mag = plotParams['j_mag']
        rmag_legend_title = ' BrightStar(Max B,V,R)\n' + \
                            'mag [min,max](nsources)'
        
        #Make a separate page for every range of source magnitudes.
        #PLOT 1: CROSS/2 PT CORRELATION FUNCTION
        plt.clf()
        plt.xlim([7E-1,6E3])
        plt.xlabel('arcseconds')
        plt.title(plot_title_j_mag)
        maxCrossCorr = 3.0#15.0

        #Set up the limits, labels, and reference lines.

        if plotParams['plot_2pt']:
            
            plt.ylabel('2_pt correlation function: counts/bin/arcsec$^2$')
            plt.ylim([1E-4,1E2])
        else:

            plt.ylabel('cross-correlation function: D1D2/D2R1 - 1')
            plt.ylim([-1.3,maxCrossCorr])
            #Plot horizontal lines to see where we make the cutoff.
            plt.axhline(y=hscConfig.corrCutoff['large'][1],ls=':',c='k')

            for xArg,nCut in hscConfig.corrCutoff.items():

                plt.axhline(y=nCut[0],ls='--',c='k')
                #Cycle through the different bright-star magnitude bins

        for j_brightStar in range(self.nBrightStarBins):

            xvalues = self.bins[1::]*3600
            yvalues = self.corrSrcBs[j_band][j_brightStar][j_mag]
            max_mag_string = \
                '['+ str(self.minMagBrightStarBins[j_brightStar])+ ','\
                + str(self.minMagBrightStarBins[j_brightStar+1])+']'
            max_mag_string += '(' + str(self.nBrightStar[j_brightStar]) + ')'

            if plotParams['plot_2pt']:
            
                plt.loglog(xvalues,
                           self.cntSrcBs[j_band][j_brightStar][j_mag],
                           label=max_mag_string,marker='o')
            else:

                line = plt.semilogx(xvalues,yvalues, \
                                    label=max_mag_string,marker='o')
        if plotParams['plot_2pt']:
            plt.legend(loc = 'lower left',title=rmag_legend_title,
                       framealpha=0.5,fontsize=10)
        else:
            plt.legend(loc = 'upper right',title=rmag_legend_title,
                       framealpha=0.5,fontsize=10)
        plot_pdf.savefig()

        #PlOT 2: best-fit halo radii with their estimates
            
        if not plotParams['plot_2pt']:

            plt.clf()
            legend_entries = np.array([])

            #Cycle once for the estimates
            for xArg,nCut in hscConfig.corrCutoff.items():
                #    for xArg,nCut in hscConfig.corrCutoff.items():
                    
                plt.semilogy(self.medBrightStar,
                             self.hrAll[xArg][j_band],
                             marker='o',alpha=0.5,
                             color=plotParams[xArg]['color'],
                             markersize=plotParams[xArg]['msize'],markeredgecolor=None)
                
                legend_entries = np.append(legend_entries,xArg)
            #Cycle once through the mask sizes for the measurements
            for xArg,nCut in hscConfig.corrCutoff.items():

                #Make a short-hand of the mask estimate.
                bfMask = bsrEstimate[xArg]['A0']* 10**(bsrEstimate[xArg]['B0'] \
                                                       * (bsrEstimate[xArg]['C0'] \
                                                          - self.medBrightStar)) \
                        + bsrEstimate[xArg]['A1']* 10**(bsrEstimate[xArg]['B1'] \
                                                        * (bsrEstimate[xArg]['C1'] \
                                                           - self.medBrightStar))

                #Plot the mask estimate.
                plt.semilogy(self.medBrightStar,bfMask,marker='x',ls='--', \
                             color=plotParams[xArg]['color'])

                legend_entries = np.append(legend_entries,xArg + \
                                           ' mask estimate')

            legend_title = 'SSP Archive ' + self.bands[j_band] + \
                           ' band: mag [min,max](nsources)'

            plt.xlabel('BrightStar magnitude')
            plt.ylabel('Halo Radius (arcseconds)')
            plt.title('Suggested mask radius for different bright-star magnitudes.')
            plt.legend(legend_entries,loc = 'upper right',
                       title= legend_title,framealpha=0.5,fontsize=10)
            plt.ylim([1E0,self.maxBinArcsec])
            plot_pdf.savefig()

            # PLOT 3: False detection radius
            plt.clf()
            plt.semilogy(self.medBrightStar,
                         self.fdMax[j_band],marker='s')
            plt.xlabel('BrightStar magnitude')
            plt.ylabel('Max. Correlation Coefficient')
            plt.title('False detection indicator')
            legend_title = 'SSP ' + self.bands[j_band] + \
                           ' band: mag [min,max](nsources)'
            plt.legend(all_mag_string,loc = 'upper left',
                       title = legend_title,framealpha=0.5)
            plt.ylim([5E-3,1E3])
            plot_pdf.savefig()

            # PLOT 4: Suggest Bright-Star Radius W/Bands
            for jj_mag in range(self.nSrcMags):

                plt.clf()
                halo_radius_25mag = self.hrAll[xArg][0::,0::,jj_mag]
                plt.semilogy(self.medBrightStar,
                             np.transpose(halo_radius_25mag),marker='o')
                legend_entries = self.bands
                #plt.semilogy(self.medBrightStar,bfMask['small'],
                #             marker='x',ls='--')
                #legend_entries.append('small mask radius')
                #plt.semilogy(self.medBrightStar,bfMask['large'],
                #             marker='x',ls='--')
                #legend_entries.append('large mask radius')
                plt.xlabel('BrightStar magnitude')
                plt.ylabel('Halo Radius (arcseconds)')
                plt.legend(legend_entries,loc = 'lower left',
                           framealpha=0.5)
                plt.title('Suggested bright-star radius w/ bands,'
                          ' fixed mag ='+all_mag_string[jj_mag])
                plt.ylim([3E-1,self.maxBinArcsec])
                plot_pdf.savefig()
                #PLOT 5: False Detection Radius
                plt.clf()
                fdAll_25mag = self.fdAll[0::,0::,jj_mag]
                plt.semilogy(self.medBrightStar,np.transpose(fdAll_25mag),
                             marker='o')
                legend_entries = self.bands
                plt.xlabel('BrightStar magnitude')
                plt.ylabel('Arcseconds')
                plt.legend(legend_entries,loc = 'lower left',
                           framealpha=0.5)
                plt.title('False detection radius '+all_mag_string[jj_mag])
                plt.ylim([3E-1,400])
                plot_pdf.savefig()
                plt.clf()
                #PLOT 6: False Detectection Indicator
                fdMax_25mag = self.fdMax[0::,0::,jj_mag]
                plt.semilogy(self.medBrightStar,np.transpose(fdMax_25mag),
                             marker='o')
                plt.xlabel('BrightStar magnitude')
                plt.ylabel('Max. Correlation Coefficient')
                plt.title('False detection indicator ' + \
                          all_mag_string[jj_mag])
                plt.legend(self.bands,loc = 'upper left',framealpha=0.5)
                plt.ylim([5E-3,1E2])
                plot_pdf.savefig()
            #else:
            #    plot_pdf.close()

        print('Finished plotting the cross-correlation function.')

        # PLOT 2: Show the distribution of these points.
        #plt.figure(2)
        do_plot = False
        if do_plot is True:
            plt.scatter(RA,Dec,s=2,alpha=0.5,edgecolor='None')
            plt.scatter(raBs_bin,decBs_bin,s=2,alpha=0.5,edgecolor='None',
                        color='red')
            plt.title(self.fieldName)
            plt.xlabel('RA (J2000) Degrees')
            plt.ylabel('Dec (J2000) Degrees')
            plt.gca().invert_xaxis()
            plot_filename = self.plotFolder + self.fieldName + \
                            '_lss_sky_coverage.png'
            plt.savefig(plot_filename)
        plot_pdf.close()
        #interact(local=locals())

    def doPlotAll(self):

        ######################################################################
        # PLOT 1: Plot the correlationfxn.
        # Generate a random version of both raDecSources and
        # alt_raDecBrightStar
        # self.minMagBrightStarBins = [12.0,14.4,15.6,16.4,17.0,17.0,17.51]
        # Cycle through the magnitudes again
        #######################################################################
        # Start here, we need to configure this to read in the pickles file.
        # This is an indicator whether or not to plot the two-point
        # correlation fxn.

        plotParams = {'small':{'color':'b','msize':2},
                      'medium':{'color':'g','msize':4},
                      'large':{'color':'r','msize':6},
                      'plot_2pt':False}

        self.hrAll,self.fdAll,self.fdMax = self.calculateHaloRadiusAll()
        #Set up the pdf files.
        if plotParams['plot_2pt']:
                
            two_pt_filename = self.plotFolder + self.magModel + '_' + \
                              self.fieldName + '_lss_2pt_corr.pdf'
            plot_pdf = PdfPages(two_pt_filename)

        else:
                
            cross_corrFilename = self.plotFolder + self.magModel + '_' + \
                                 self.fieldName +'_lss_cross_correlation.pdf'
            plot_pdf = PdfPages(cross_corrFilename)

        print('\nPlottting results to: ' + cross_corrFilename)
                
        for j_band in range(self.nBands):
                
            all_mag_string = np.array([])
            j_bx = self.bands[j_band]
                
            for j_mag in range(self.nSrcMags):

                check_corr = self.corrSrcBs[j_band][0][j_mag]

                if check_corr is None:
                    break

                minMag = self.minMags[j_mag]
                maxMag = self.maxMags[self.magModel][j_bx][j_mag]
                nSrc = self.nSources[j_band][j_mag]
                mini_title = ' sources with mag = [' + str(minMag) \
                                 + ',' + str(maxMag)+']'
                plotParams['altTitle'] = '[' + str(minMag) +',' \
                                             + str(maxMag) \
                                             +' ](' + str(nSrc)+')'
                plotParams['all_mag_string'] = \
                    np.append(all_mag_string,mini_title)
                plotParams['jmagTitle'] = \
                    self.magModel + ' ' + j_bx+' band: '+ \
                    str(self.nSources[j_band][j_mag]) + mini_title
                plotParams['j_band'] = j_band
                plotParams['j_mag'] = j_mag
                self.doPlot(plotParams,plot_pdf)
                    
        return
'''
#if __name__ == "__main__":
#    CalculateBrightStarCorrelation(sys.arv).__init__()
    

#I'm not sure what this does exactly, but I want to finish writing this for completeness.
#    def test(self):
#        testExposure = loadTestExposure.LoadExposureTask(loadCoadd=True,filter='G')

def old_queries():
        #This is the old code which is commented out for now until the program has been debugged.
            brightStars = retrieveBrightStarsFromBrightStar.FindBrightStarTask(loadOnly=True)
            c_brightStars = brightStars.load()
            #Question, we need to query whether or not the stars are within the field.
            #brightStars = retrieveBrightStarsFromBrightStar.FindBrightStarTask()
            brightStarOut = brightStars.brightStarOut
            Bmag_brightStar = np.array(brightStarOut['Bmag'])
            Vmag_brightStar = np.array(brightStarOut['Vmag'])
            Rmag_brightStar = np.array(brightStarOut['Rmag'])
            all_brightStar = np.vstack((Bmag_brightStar,Vmag_brightStar,Rmag_brightStar))
            magBrightStar = np.amin(all_brightStar,axis=0)
            raBs = np.array(brightStarOut['RA'])
        # and if you want to know a little more about the database, here is some more
        # output. Select verbose=True in the keywords.
        if self.verbose:
            for row in c_bs.execute(SELECT Count(*) FROM ssp'):
                print(str(row[0])+' rows in the SSP_sql.')
            # This counts all of the different tracts in the database.
            # Only two distinct tracts for now, hopefully this changes when I make a new query.
            for row in c_bs.execute('SELECT Count(DISTINCT tract) FROM ssp'):
                print(str(row[0])+' distinct tracts.')
            # There should be tracks 8524,8525, 8766, and 8767 in this field.
            for row in c_bs.execute('SELECT DISTINCT tract FROM ssp'):
                print('Tract '+str(row[0])+' is included.')
            #This is a good way to peruse the SQL database.
            print('Extracting all of the RA and Dec information from the table, this takes a while....')
            # Kind of chokes after 500,000, so, you'll have to be more clever with that dataset.
            # Create the bin array for the correlation function.
        ###
            decBs = np.array(brightStarOut['Dec'])


###
#Take a look here if this is loading the correct data
#Cycle through the GRIZY bands
        indices = {'RA':np.where(column_names == 'RA_J2000_degrees')[0][0], \
                   'Dec':np.where(column_names == 'Dec_J2000_degrees')[0][0], \
                   'gmag_cmodel':np.where(column_names == 'gmag_cmodel')[0][0], \
                   'gmag_cmodel_err':np.where(column_names == 'gmag_cmodel_err')[0][0], \
                   'rmag_cmodel':np.where(column_names == 'rmag_cmodel')[0][0], \
                   'rmag_cmodel_err':np.where(column_names == 'rmag_cmodel_err')[0][0], \
                   'imag_cmodel':np.where(column_names == 'imag_cmodel')[0][0], \
                   'imag_cmodel_err':np.where(column_names == 'imag_cmodel_err')[0][0], \
                   'zmag_cmodel':np.where(column_names == 'zmag_cmodel')[0][0], \
                   'zmag_cmodel_err':np.where(column_names == 'zmag_cmodel_err')[0][0], \
                   'ymag_cmodel':np.where(column_names == 'ymag_cmodel')[0][0], \
                   'ymag_cmodel_err':np.where(column_names == 'ymag_cmodel_err')[0][0]}


        for j_band in range(self.nBands):
            # First, insert the randoms, which are the same for all bands and magnitudes.
            column_names = np.array([])
            
                        
            #Cycle individually through the Rmags of the bright-sources and the mag bins of the HSC sources.
            for j_mag in range(nsrcMags):
                    sql_command = 'SELECT * FROM xmm_lss WHERE '+j_bx+'mag_'+self.magModel+'>=? AND ' + \
                                  j_bx+'mag_'+self.magModel+'<=? '
                    print('Loading the sources in the data archive...')
                    for row in c.execute(sql_command,[self.minMags[j_mag], \
                                                      self.maxMags[self.magModel][j_bx][j_mag]]):
                        if (len(RA_sources) % 10000) is 0:
                            print(str(len(RA_sources))+' entries inserted.')
                        RA_sources.append(row[indices['RA']])
                        Dec_sources.append(row[indices['Dec']])
                        mag_cmodel.append(row[indices[j_bx+'mag_'+self.magModel]])
                        mag_cmodel_err.append(row[indices[j_bx+'mag_'+self.magModel + '_err']])
                        #kron_flux.append(row[indices['kron']])
                        #kron_flux_err.append(row[indices['kron_err']])
                        raDecSources = np.append(raDecSources,[[row[indices['RA']],row[indices['Dec']]]],axis=0)
                    #END OF: for row in c.execute(sql_command,[self.minMags[j_mag],self.maxMags[j_mag]])
                    #print('Figure out how to extract the PFS information.')
                    #raDecSources_dict = {'RA':RA_sources,'Dec':Dec_sources}
                    self.nSources[j_band][j_mag] = len(RA_sources)
                    print('A total of '+ str(self.nSources[j_band][j_mag])+' entries have been inserted.')

                    ####
                    print('Saving to pickle....')
                    #pickle.dump(raDecSources_dict,open(radec_filename,'wb'))
                    pickle.dump([raDecSources,raDecRandoms,mag_cmodel,mag_cmodel_err], \
                                open(radec_filename,'wb'))
                    
                else:#if not saveToPickle
                    print('Unloading the following pickle file.')
                    print(radec_filename)
                    raDecSources,raDecRandoms, mag_cmodel, mag_cmodel_err = pickle.load(open(radec_filename,"rb"))
                if self.nSources[j_band][j_mag] == 0:
                    print('No valid sources returned...skipping to next set of parameters...')
                else:
                    #Remove the first entry.
                    #raDecSources = raDecSources[1::]

                                    if False:
                                        #Not really sure that this is doing here...
                                        while halo_radius_x > len(yvalues)-2 and halo_radius_x > 5:

                                            halo_radius_x = np.max(np.where(np.abs(yvalues) > nCut[1]))
                                            
                                            good_y = good_y[0:-1]

                                            #Make sure that the radius is shorter than the limits.
                                            halo_radius_x = np.min([halo_radius_x,len(yvalues)-2])

                                #Otherwise, just set the halo_radius to the second highest radius.
                                else:

                                    print('Consider increasing the size of your search')
                                    interact(local=locals())
        if False:
                            line_color = line[0].get_color()
                            plt.axvline(x=hrAll[xArg][j_band][j_brightStar][j_mag],c=line_color,ls='--')
                            plt.xlim(xvalues[0],xvalues[-1])
                            plt.ylim([-1.3,maxCrossCorr])

                            #plt.axvline(x=bestFitArcmin[j_brightStar][j_mag])
                            #,label='Estimated halo extent.')
                            #Keep these below in case they are interesting l
                            # ater on.
                            #plt.loglog(self.bins[1::]*3600,
                            #           cntSrcSq[j_brightStar][j_mag], 
                            #           label='2 pt: counts$^2$',marker='o')
                            #plt.loglog(self.bins[1::]*3600,
                            #           cntBsSq[j_brightStar][j_mag], 
                            #           label='2 pt: bright-stars$^2$',
                            #           marker='o')
                            #plt.loglog(self.bins[1::]*3600,
                            #           counts_brightStar_sources,'--')


'''
