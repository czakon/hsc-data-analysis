#!/usr/bin/env python
# Created by Nicole Czkon 09212015
# The purpose of this code is to load a default exposure that we can manipulate in other
# programs. This can probably be modified for general applications.
#
# USAGE:
# import loadTestExposure
# testExposure = loadTestExposure.LoadExposureTask(loadCoadd=True,filter='G')
# testExposure = loadTestExposure.LoadExposureTask(loadCoadd=True,filter='G',doPlot=True)
import os
########### For Debugging #################################################
from code import interact
import time
########### Pipeline-Related Packages #####################################
import lsst.daf.persistence as dafPersist
import lsst.afw.display.ds9 as ds9
#from lsst.coadd.utils.coadd import Coadd
#import lsst.meas.astrom.astrom as measAst
#import lsst.pex.config as pexConfig
### Another package in order to load the astrometry.net data #############
import lsst.meas.astrom as measAstrom
### General Python Packages ##############################################
import matplotlib.pyplot as plt
import astropy.units as u
import astropy.coordinates as coord
import numpy as np
#from matplotlib.backends.backend_pdf import PdfPages
from pipeline_code import hscConfigurationFile as hscConfig
#import hscConfigurationFile as hscConfig

__all__ = ["LoadExposureTask"]

class LoadExposureTask:
    _DefaultName = "loadTestExposure"
    def __init__(self,loadCoadd=None,filter=None,doPlot=None):
        if loadCoadd:
            self.loadCoadd = True
        else:
            #This is for a single CCD
            self.loadCoadd = False
        goodFilters = ('G','R','I','Z','Y')
        if filter:
            if filter in goodFilters:
                self.filter = filter
            else:
                print(filter+' not allowed as a filter input, please choose one of the following: ' + \
                      "".join(goodFilters))
        else:
            self.filter = goodFilters[0]
            print('Setting the default filter to: ' + self.filter)

        self.load()
        
        if doPlot:
            self.doPlot = True
            self.plot()
        else:
            self.doPlot = False

    def plot(self):
        ds9.setMaskPlaneColor('BRIGHT_STAR',color=ds9.BLUE)
        settings = {'scale':'zscale','zoom':'to fit','mask':'transparency 60'}
        ds9.mtv(self.exposure,frame=1,title='My Data',settings=settings)
        
    def load(self):
        dataDir = os.environ['runDir']

        butler = dafPersist.Butler(dataDir)

        if self.loadCoadd:
            tract =  hscConfig.test_tract
            patch =  hscConfig.test_patch
            dataId = {'filter':'HSC-' + self.filter,'tract':tract,'patch':patch}
            self.exposure = butler.get("deepCoadd",dataId,immediate=True)
            coadd = butler.get('deepCoadd',dataId)
            sources = butler.get('deepCoadd_meas',dataId,immediate=True)
        else:
            visit =  hscConfig.test_visit[self.filter]
            ccd = hscConfig.test_ccd
            dataId = {"visit" : visit, "ccd": ccd}
            self.exposure = butler.get("calexp",dataId)
            sources = butler.get('src',dataId)
            metadata = butler.get('calexp_md',dataId)
            zeropoint = 2.5*np.log10(metadata.get('FLUXMAG0'))
        ##############################################################################################
        ######################## STEP 1: FIND THE CORRELATION MATRIX  ################################
        ##############################################################################################
        sources_RA = []
        sources_Dec = []
        for source in sources:
            sources_RA.append(source.getRa().asDegrees())
            sources_Dec.append(source.getDec().asDegrees())
            #sources_RADec = np.append(sources_RADec,[[source.getRa().asDegrees(),source.getDec().asDegrees()]],axis=0)
        # Find the edges of the bounding box, we will use this information to search other catalogs with
        # For some reason, the values on the bbox are not correct
        use_bbox = 0
        if use_bbox:
            wcs = self.exposure.getWcs()
            bbox = self.exposure.getBBox().getCorners()
            bbox_icrs = [wcs.pixelToSky(bbox[i].getX(),bbox[i].getY()).toIcrs() for i in range(4)]
            Decs  =[bbox_icrs[i].getDec().asDegrees() for i in range(4)]
            RAs = [bbox_icrs[i].getRa().asDegrees() for i in range(4)]
            centerRA = np.mean(RAs)
            centerDec = np.mean(Decs)
        else:
            centerRA = np.mean(sources_RA)
            centerDec = np.mean(sources_Dec)
        self.mapCoord = coord.SkyCoord(ra=float(centerRA),dec=float(centerDec),unit=(u.deg,u.deg),frame='icrs')

