# !/usr/bin/env python
# Created by Nicole Czkon 09212015
# Description of the Code:
# The purpose of this code is to characterize the extent of the bright-star halo.
# Currently, this is using sql catalogs from the public data release, although,
# it probable has to be done using the images directly...
#
# USAGE:
# import brightStarCorrelationFunction
# temp = brightStarCorrelationFunction.CalculateBrightStarCorrelation(magModel='cmodel',plot=True)
# temp = brightStarCorrelationFunction.CalculateBrightStarCorrelation(magModel='cmodel',overwrite=True)
#
import os
########### For Debugging #################################################
from code import interact
import time
########### Pipeline-Related Packages #####################################
#import lsst.daf.persistence as dafPersist
#import lsst.afw.display.ds9 as ds9
#from lsst.coadd.utils.coadd import Coadd
#import lsst.meas.astrom.astrom as measAst
#import lsst.pex.config as pexConfig
### Another package in order to load the astrometry.net data #############
#import lsst.meas.astrom as measAstrom
### General Python Packages ##############################################
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import astropy.units as u
import astropy.coordinates as coord
import numpy as np
from astropy.io import fits
from astropy.table import vstack
## Additional package needed in order to download the data from Vizier ##
from astroquery.vizier import Vizier
#I'm not using the astroML version now, but it has some good pointers in case I want to make my
#program more sophisticated.
#from astroML.correlation import two_point_angular
from sklearn.neighbors import KDTree
import sqlite3
from copy import copy,deepcopy
import cPickle as pickle
from matplotlib.backends.backend_pdf import PdfPages
from scipy.interpolate import UnivariateSpline
import hscConfigurationFile as hscConfig
import loadTestExposure
import retrieveBrightStarsFromNomad
########## My Own Python Packages #######################################
from pipeline_code import hscConfigurationFile as hscConfig
from pipeline_code.masking import retrieveBrightStarsFromNomad
#Probably need to fix this up a bit.
import loadTestExposure

#
# def two_pt_correlation_fxn:
# PURPOSE: to calculate the radius at which the sources counts fall off.
# ADDITIONAL INFORMATION: "sources" refers to the sources detected in the HSC images.
#                         "nomad" refers to the bright-stars extracted from the nomad catalog.
#                         see "find_nomad" function. 
# OPTIONAL KEYWORDS:
# plot_2pt: This is a kludge because I want to save the plots in different files and I
#           can't figure out how to do this without saving the correlation functions in
#           a different structure.
# verbose: this gives a little more output in case we're interested in more
#          details.
# overwrite: Since it takes a while to do these calculations, it's better to
#            pickle the results whenever you decide on a configuration.

__all__ = ["CalculateBrightStarCorrelation"]

class CalculateBrightStarCorrelation:
    _DefaultName = "CalculateBrightStarCorrelation"

    def __init__(self,magModel=None,overwrite=None,plot=None,verbose=None,*args,**kwargs):

        mag_model_options = ['cmodel','kron']
        if magModel in mag_model_options:
            self.magModel = magModel
        else:
            print('magModel is not specifed, will set to cmodel by default...')
            self.magModel = 'cmodel'

        self.plotFolder = hscConfig.mask_plot_folder
        self.corrFilename = hscConfig.corr_filename + self.magModel + '.p'
        self.coreFileExists = os.path.isfile(self.corrFilename)
        self.corrCutoff =  0.2#self.corrAll['corr']['cutoff']

        if verbose:
            self.verbose = True
        else:
            self.verbose = False
        if overwrite:
            self.overwrite = True
            self.run()
        else:
            self.overwrite = False

        self.load()

        if plot:
            self.plot = True
            self.doPlot()
        else:
            self.plot = False
                    
    def load(self):
        self.corrAll = pickle.load(open(self.corrFilename,"rb"))
        self.minMags = self.corrAll['min_mags']
        self.maxMags = self.corrAll['max_mags']
        self.nSrcMags = len(self.minMags)
        self.minMagNomadBins = self.corrAll['min_mag_nomad_bins']
        self.nNomadBins = len(self.minMagNomadBins)-1
        self.bins =  self.corrAll['bins']
        self.nBins = len(self.bins)
        self.bands =  self.corrAll['bands']
        self.nBands = len(self.bands)
        self.nSources =  self.corrAll['nsources']
        self.nNomad =  self.corrAll['nnomad']
        self.medNomad =  self.corrAll['med_nomad']
        self.countsSourcesSq = self.corrAll['corr']['counts_sources_sq']
        self.countsNomadSq =  self.corrAll['corr']['counts_nomad_sq']
        self.countsSourcesNomad = self.corrAll['corr']['counts_sources_nomad']
        self.bestFitArcmin = self.corrAll['corr']['best_fit_arcmin']
        self.corrSourcesNomad =  self.corrAll['corr']['corr_sources_nomad']
        #self.corrCutoff =  self.corrAll['corr']['cutoff']
        #corr_cutoff = 0.2

    def run(self):
        #two_pt_correlation_fxn(nomad_all,verbose=False,overwrite=False):
        if self.magModel == 'kron':
            #I'm not sure why there is no 'g'
            self.bands = ['g','r','i','z','y']
        else:
            self.bands = ['g','r','i','z','y']            
        #Let's start at one arcsec and then build up to 30 arcsec.
        min_bin_arcsec = 1.0
        max_bin_arcsec = 300.0
        self.nBins = 20
        #Need both a min and a max, otherwise, the datasets become too large.
        self.minMags = [21.0,22.0,23.00,24.00,25.00,26.00,26.50]
        #self.maxMags = [22.5,23.0,23.75,24.30,25.25,26.15,27.15]
        self.maxMags = {'kron':{'g':[23.0,24.0,23.75,24.50,25.25,26.15,27.15], \
                            'r':[23.0,23.5,23.75,24.30,25.25,26.25,27.15], \
                            'i':[23.0,23.0,23.75,24.30,25.25,26.5,27.5], \
                            'z':[22.5,23.0,23.75,24.30,25.40,26.5,27.5], \
                            'y':[22.5,23.0,23.75,24.30,25.40,26.6,28.0]}, \
                    'cmodel':{'g':[22.5,23.0,23.75,24.30,25.25,26.5,27.15], \
                              'r':[23.0,23.5,23.75,24.40,25.25,26.25,27.15], \
                              'i':[22.5,23.0,23.75,24.30,25.25,26.25,27.15], \
                              'z':[22.25,22.8,23.50,24.30,25.20,26.5,27.5], \
                              'y':[22.25,22.8,23.50,24.30,25.25,26.5,27.5]}}
                                                
        #For the bright-star mags, we only need to take the difference b/n adjacent bins.
        self.minMagNomadBins = [5.0,7.5,10.0,12.5,14.5,17.5]
        #Convert the nomad values to something that's easier to manipulate in numpy.
        print('Finding the brightest NOMAD magnitude in the B/V/ and R bands.')
        #bring in the nomad file.
        brightStars = retrieveBrightStarsFromNomad.FindNomadTask(loadOnly=True)
        c_brightStars = brightStars.load()
        #Question, we need to query whether or not the stars are within the field.
        #brightStars = retrieveBrightStarsFromNomad.FindNomadTask()
        
        nomadOut = brightStars.nomadOut
        Bmag_nomad = np.array(nomadOut['Bmag'])
        Vmag_nomad = np.array(nomadOut['Vmag'])
        Rmag_nomad = np.array(nomadOut['Rmag'])
        all_nomad = np.vstack((Bmag_nomad,Vmag_nomad,Rmag_nomad))
        min_mag_nomad = np.amin(all_nomad,axis=0)
        RA_nomad = np.array(nomadOut['RA'])
        Dec_nomad = np.array(nomadOut['Dec'])
        sql_source_db = hscConfig.sql_source_db
        ##############################################################################
        # Open up the SQL database that I built elsewhere
        ##############################################################################
        #Create a connection object, this needs to be soft-coded!
        conn = sqlite3.connect(sql_source_db) 
        #Create a cursor object
        c = conn.cursor()
        column_names = np.array([])
        for row in c.execute('''PRAGMA table_info(xmm_lss)'''):
            column_names = np.append(column_names,row[1])
        indices = {'RA':np.where(column_names == 'RA_J2000_degrees')[0][0], \
                   'Dec':np.where(column_names == 'Dec_J2000_degrees')[0][0], \
                   #'shape_sdss_psf_r_det_fwhm':np.where(column_names == 'shape_sdss_psf_r_det_fwhm')[0][0], \
                   #'shape_sdss_psf':np.where(column_names == 'shape_sdss_psf')[0][0], \
                   'gmag_cmodel':np.where(column_names == 'gmag_cmodel')[0][0]}
        # and if you want to know a little more about the database, here is some more
        # output. Select verbose=True in the keywords.
        if self.verbose:
            for row in c.execute('''SELECT Count(*) FROM xmm_lss'''):
                print(str(row[0])+' rows in the XMM_LSS_sql.')
            # This counts all of the different tracts in the database.
            # Only two distinct tracts for now, hopefully this changes when I make a new query.
            for row in c.execute('''SELECT Count(DISTINCT tract) FROM xmm_lss'''):
                print(str(row[0])+' distinct tracts.')
            # There should be tracks 8524,8525, 8766, and 8767 in this field.
            for row in c.execute('''SELECT DISTINCT tract FROM xmm_lss'''):
                print('Tract '+str(row[0])+' is included.')
            #This is a good way to peruse the SQL database.
            print('Extracting all of the RA and Dec information from the table, this takes a while....')
            # Kind of chokes after 500,000, so, you'll have to be more clever with that dataset.
            # Create the bin array for the correlation function.
        temp_bins = np.array(0)
        self.bins = np.append(temp_bins,np.logspace(np.log10(min_bin_arcsec/3600.),np.log10(max_bin_arcsec/3600.),self.nBins))
        bins_sqr = self.bins**2
        bins_area = np.pi*(bins_sqr[1::]-bins_sqr[0:-1])*(3600)**2
        self.nBins = len(self.bins)
        self.nBands = len(self.bands)
        nnomad_bins = len(self.minMagNomadBins)-1
        nsrc_mags = len(self.minMags)
        data_type = type(np.ndarray(self.nBins-1))
        #Technically, this doesn't have to be counted for every nomad bin. Consider removing if slowing the program down.
        counts_sources_sq = np.ndarray(shape=(self.nBands,nnomad_bins,nsrc_mags),dtype=data_type)
        #Technically, this doesn't have to be counted for every band bin or src bin.
        counts_nomad_sq = np.ndarray(shape=(self.nBands,nnomad_bins,nsrc_mags),dtype=data_type)
        counts_sources_nomad = np.ndarray(shape=(self.nBands,nnomad_bins,nsrc_mags),dtype=data_type)
        self.corrSourcesNomad = np.ndarray(shape=(self.nBands,nnomad_bins,nsrc_mags),dtype=data_type)
        best_fit_arcmin =  np.ndarray(shape=(self.nBands,nnomad_bins,nsrc_mags),dtype=float)
        nnomad =  np.ndarray(shape=(nnomad_bins),dtype=float)
        self.medNomad =  np.ndarray(shape=(nnomad_bins),dtype=float)
        self.nSources =  np.ndarray(shape=(self.nBands,nsrc_mags),dtype=float)
        #Cycle through the GRIZY bands
        for j_band in range(self.nBands):
            #Cycle individually through the Rmags of the bright-sources and the mag bins of the HSC sources.
            for j_mag in range(nsrc_mags):        
                RADec_sources = np.array([[0,0]])
                RA_sources = []
                Dec_sources = []
                #This is a proxy for the seeing, I haven't decided whether it's worth including or not.
                #r_det_fwhm = []
                #for row in c.execute('''SELECT * FROM xmm_lss WHERE rmag_cmodel>=? AND rmag_cmodel<=?''', \
                    ###
                sql_command = 'SELECT * FROM xmm_lss WHERE '+self.bands[j_band]+'mag_'+self.magModel+'>=? AND ' + \
                              self.bands[j_band]+'mag_'+self.magModel+'<=? '
                for row in c.execute(sql_command,[self.minMags[j_mag], \
                                                  self.maxMags[self.magModel][self.bands[j_band]][j_mag]]):
                    if (len(RA_sources) % 10000) is 0:
                        print(str(len(RA_sources))+' entries inserted.')
                    #r_det_fwhm.append(row[indices['shape_sdss_psf_r_det_fwhm']])
                    RA_sources.append(row[indices['RA']])
                    Dec_sources.append(row[indices['Dec']])
                    RADec_sources = np.append(RADec_sources,[[row[indices['RA']],row[indices['Dec']]]],axis=0)
                #END OF: for row in c.execute(sql_command,[self.minMags[j_mag],self.maxMags[j_mag]])
                #print('Figure out how to extract the PFS information.')
                #interact(local=locals())
                RADec_sources_dict = {'RA':RA_sources,'Dec':Dec_sources}
                self.nSources[j_band][j_mag] = len(RA_sources)
                print('A total of '+ str(self.nSources[j_band][j_mag])+' entries have been inserted.')
                if self.nSources[j_band][j_mag] == 0:
                    print('No valid sources returned...skipping to next set of parameters...')
                else:
                    #Remove the first entry.
                    RADec_sources = RADec_sources[1::]
                    ##############################################################################
                    # Use the scikit to get started, this can be sophisticated if we look at
                    # the examples in astroML (although it doesn't have the full functionality).
                    ##############################################################################
                    for j_nomad in range(nnomad_bins):
                        good_nomad = np.where((min_mag_nomad >= self.minMagNomadBins[j_nomad]) & \
                                              (min_mag_nomad < self.minMagNomadBins[j_nomad+1]))
                        self.medNomad[j_nomad] = np.median(min_mag_nomad[good_nomad])
                        self.nNomad[j_nomad] = len(good_nomad[0])
                        RA_nomad_bin = RA_nomad[good_nomad]
                        Dec_nomad_bin = Dec_nomad[good_nomad]
                        RADec_nomad_bin = np.vstack((RA_nomad_bin,Dec_nomad_bin)).T
                        R_RADec_sources = copy(RADec_sources)
                        R_RADec_nomad_bin = copy(RADec_nomad_bin)
                        #setup -v -j -r /path/to/maskUtils
#                        drawRandoms.py DATADIR --rerun RERUNDIR --id tract=TRACT patch=PATCH filter=HSC-{G,R,I,Z,Y} --config Nden=100 fileOutName=ran.fits
                        #START HERE! FIGURE OUT A WAY TO MAKE BETTER RANDOMS.
                        np.random.shuffle(R_RADec_sources[:,0])
                        np.random.shuffle(R_RADec_nomad_bin[:,0])
                        #from sklearn.neighbors import KDTree
                        kdtree_sources = KDTree(RADec_sources)
                        R_kdtree_sources = KDTree(R_RADec_sources)
                        kdtree_nomad = KDTree(RADec_nomad_bin)
                        R_kdtree_nomad = KDTree(RADec_nomad_bin)
                        #Now, perform the two_point correlation fxn
                        #max 3.3E10
                        counts_sources_sq[j_band][j_nomad][j_mag] =  \
                                np.diff(kdtree_sources.two_point_correlation(RADec_sources,bins)) / bins_area
                        #max 3.0E8
                        counts_nomad_sq[j_band][j_nomad][j_mag] =  \
                                np.diff(kdtree_nomad.two_point_correlation(RADec_nomad_bin,bins)) / bins_area
                        #max 1.8E10
                        counts_sources_nomad[j_band][j_nomad][j_mag] = \
                                np.diff(kdtree_sources.two_point_correlation(RADec_nomad_bin,bins)) / bins_area
                        #max 4.6E8
                        # I don't think that I need to store the randoms right now.
                        counts_rsources_nomad = \
                                np.diff(R_kdtree_sources.two_point_correlation(RADec_nomad_bin,bins)) / bins_area
                        #max 3.3E8
                        #This is simple used as a cross-check.
                        #counts_sources_rnomad = \
                            #       np.diff(kdtree_sources.two_point_correlation(R_RADec_nomad_bin,bins)) / bins_area
                        #W = D1*D2/(D2*R1)-1
                        self.corrSourcesNomad[j_band][j_nomad][j_mag] =  \
                                counts_sources_nomad[j_band][j_nomad][j_mag]/counts_rsources_nomad-1
                        #I've verified that this gives the same answer as well
                        #W2 = counts_sources_nomad/counts_sources_rnomad-1
                        #I've confirmed that these give the same outputs.
                        #counts_nomad_sources = np.diff(kdtree_nomad.two_point_correlation(RADec_sources,bins))
                        #Keep the following plot for the time being
                        #print('Stop here to examine how to find the spline fit.')
                        #Find the spot where the source counts equal approximately 80% of their overall value.
                        #source_fraction = 0.55
                        temp = self.bins*3600.>3.0
                        x_bins = np.where(temp==True)[0]
                        good_bins = self.bins[x_bins]
                        #x_halo = np.abs(counts_sources_nomad - source_fraction*counts_sources_nomad[-1])
                        temp_min = good_bins[np.argmax(counts_sources_nomad[j_band][j_nomad][j_mag][x_bins[0:-1]])+1]*3600
                        best_fit_arcmin[j_band][j_nomad][j_mag] = temp_min
                        self.corrAll = {'bins':bins, 'bands':bands, \
                                    'min_mags':self.minMags,'max_mags':self.maxMags,'nsources':self.nSources, \
                                    'min_mag_nomad_bins':self.minMagNomadBins,'nnomad':nnomad,'med_nomad':self.medNomad, \
                                    'corr':{'best_fit_arcmin':best_fit_arcmin, \
                                            'counts_nomad_sq':counts_nomad_sq, \
                                            'counts_sources_sq':counts_sources_sq, \
                                            'counts_sources_nomad':counts_sources_nomad, \
                                            'corr_sources_nomad':self.corrSourcesNomad, \
                                    }}
                        #Hopefully the above works properly.
                        #self.corrAll = {'self.minMags':self.minMags,'max_mag':self.maxMags,''}    
                    #END OF: for j_nomad in range(nnomad_bins)
                #END OF: if self.nSources[j_band][j_mag] != 0
            #END OF: for j_mag in range(nsrc_mags):
        #END OF: for j_band in range(self.nBands):
        pickle.dump(self.corrAll,open(corr_filename,'wb'))
        ################################################################################################
        # Close the sql connection
        conn.close()
    #END OF def run(self)
    def doPlot(self):
    ########################################################################
    # PLOT 1: Plot the correlationfxn.
    # Generate a random version of both RADec_sources and alt_RADec_nomad
    # self.minMagNomadBins = [12.0,14.4,15.6,16.4,17.0,17.0,17.51]
    # Cycle through the magnitudes again
    # This is currently a kludge because I am too lazy to figure out how to plot
    # this to different pdf files simulatanteously.
    ########################################################################
    # Start here, we need to configure this to read in the pickles file.
    # This is an indicator whether or not to plot the two-point correlation fxn.
        all_plots = [False]
        halo_radius_final =  np.ndarray(shape=(self.nBands,self.nNomadBins,self.nSrcMags), \
                                        dtype=float)
        false_detection_radius =  np.ndarray(shape=(self.nBands,self.nNomadBins,self.nSrcMags), \
                                             dtype=float)
        false_detection_max =  np.ndarray(shape=(self.nBands,self.nNomadBins,self.nSrcMags), \
                                          dtype=float)
        for plot_2pt in all_plots:
            #Set up the pdf files.
            if plot_2pt:
                two_pt_filename = self.plotFolder + self.magModel + '_xmm_lss_2pt_corr.pdf'
                two_pt_pdf = PdfPages(two_pt_filename)
            else:
                cross_corr_filename = self.plotFolder + self.magModel + '_xmm_lss_cross_correlation.pdf'
                cross_corr_pdf = PdfPages(cross_corr_filename)
            for j_band in range(self.nBands):
                all_mag_string = np.array([])
                for j_mag in range(self.nSrcMags):
                    check_corr = self.corrSourcesNomad[j_band][0][j_mag]
                    if check_corr==None:
                        break
                    #Make a separate page for every range of source magnitudes.
                    plt.clf()
                    plt.xlabel('arcseconds')
                    temp_title = ' sources with mag = ['+str(self.minMags[j_mag])+','+ \
                                 str(self.maxMags[self.magModel][self.bands[j_band]][j_mag])+']'
                    alt_title = '['+str(self.minMags[j_mag])+','+ \
                                str(self.maxMags[self.magModel][self.bands[j_band]][j_mag])+']('+str(self.nSources[j_band][j_mag])+')'
                    all_mag_string = np.append(all_mag_string,alt_title)
                    plot_title_j_mag = self.magModel + ' ' + self.bands[j_band]+' band: '+ \
                                       str(self.nSources[j_band][j_mag])+ temp_title
                    plt.title(plot_title_j_mag)
                    if plot_2pt:
                        plt.ylabel('2_pt correlation function: counts/bin/arcsec$^2$')
                        plt.ylim([1E-4,1E2])
                    else:
                        plt.ylabel('cross-correlation function: D1D2/D2R1 - 1')
                        plt.ylim([-1.3,3.0])
                        #Plot horizontal lines to see where we make the cutoff.
                        plt.axhline(y=self.corrCutoff,ls='--',c='k')#,label='')
                        plt.axhline(y=-self.corrCutoff,ls='--',c='k')#,label='')
                        #Cycle through the different bright-star magnitude bins
                    for j_nomad in range(self.nNomadBins):
                        xvalues = self.bins[1::]*3600
                        yvalues = self.corrSourcesNomad[j_band][j_nomad][j_mag]
                        max_mag_string = '['+ str(self.minMagNomadBins[j_nomad])+','+ \
                                         str(self.minMagNomadBins[j_nomad+1])+']'
                        max_mag_string = max_mag_string + '(' + str(self.nNomad[j_nomad]) + ')'
                        rmag_legend_title = ' Nomad(Max B,V,R)\nmag [min,max](nsources)'
                        if plot_2pt:
                            plt.loglog(self.bins[1::]*3600,counts_sources_nomad[j_band][j_nomad][j_mag], \
                                       label=max_mag_string,marker='o')
                            #plt.axvline(x=best_fit_arcmin[j_nomad][j_mag])#,label='Estimated halo extent.')
                            #Keep these below in case they are interesting later on.
                            #plt.loglog(self.bins[1::]*3600,counts_sources_sq[j_nomad][j_mag], \
                                #           label='2 pt: counts$^2$',marker='o')
                            #plt.loglog(self.bins[1::]*3600,counts_nomad_sq[j_nomad][j_mag], \
                                #           label='2 pt: bright-stars$^2$',marker='o')
                            #plt.loglog(self.bins[1::]*3600,counts_nomad_sources,'--')
                        else:
                            if len(xvalues) != len(yvalues):
                                print('The number of radial bins does not match the number of '+ \
                                      'correlation measurements.')
                                interact(local=locals())
                            line = plt.semilogx(xvalues,yvalues, \
                                                label=max_mag_string,marker='o')
                            do_spline_fit = True
                            good_y = np.isfinite(yvalues)
                            if do_spline_fit:
                                new_x = np.logspace(0,3,200)
                                spl = UnivariateSpline(xvalues[good_y],yvalues[good_y],k=4,s=0.025, \
                                                       check_finite=True)
                                spl_roots = spl.derivative().roots()
                                if len(spl_roots) > 0:
                                    max_root = np.argmax(spl(spl_roots))
                                    #plt.plot(new_x,spl(new_x),c='k')
                                    plt.scatter(spl_roots[max_root],spl(spl_roots[max_root]), \
                                                marker='x',c='k',s=60)
                                    false_detection_radius[j_band][j_nomad][j_mag] = spl_roots[max_root]
                                    false_detection_max[j_band][j_nomad][j_mag] = spl(spl_roots[max_root])
                                else:
                                    false_detection_radius[j_band][j_nomad][j_mag] = 300.
                                    false_detection_max[j_band][j_nomad][j_mag] = np.max(yvalues[good_y])
                            halo_radius_x = len(yvalues[good_y])
                            if np.max(yvalues[good_y]) > -self.corrCutoff:
                                halo_radius_x = np.max(np.where(np.abs(yvalues[good_y]) > self.corrCutoff))
                                while halo_radius_x > len(yvalues[good_y])-2 and halo_radius_x > 5:
                                    halo_radius_x = np.max(np.where(np.abs(yvalues[good_y]) > self.corrCutoff))
                                    good_y = good_y[0:-1]
                                halo_radius_x = np.min([halo_radius_x,len(yvalues[good_y])-2])
                            else:
                                halo_radius_x = len(yvalues[good_y])-2
                            #print halo_radius_x
                            #print len(yvalues[good_y])-2
                            halo_radius_x = [np.max(halo_radius_x),np.max(halo_radius_x)+1]
                            xhalo_radius = xvalues[good_y][halo_radius_x]
                            yhalo_radius = yvalues[good_y][halo_radius_x]
                            slope = np.diff(yhalo_radius)/np.diff(xhalo_radius)
                            if yhalo_radius[0] > 0.0:
                                newy = self.corrCutoff
                            else:
                                newy = -self.corrCutoff
                            halo_radius_final[j_band][j_nomad][j_mag] = \
                                        np.min([(newy-yhalo_radius[0])/slope+xhalo_radius[0],xvalues[-1]])
                            #plt.scatter(halo_radius_final[j_band][j_nomad][j_mag],newy, \
                            #            marker='x',c='r',s=100)
                            #from itertools import cycle
                            #color_cycle = cycle(['g','b','r','c'])
                            line_color = line[0].get_color()
                            plt.axvline(x=halo_radius_final[j_band][j_nomad][j_mag],c=line_color,ls='--')
                            plt.xlim(xvalues[0],xvalues[-1])
                            plt.ylim([-1.3,3.0])
                            #        label='correlation: sources from bright-stars',marker='o')
                    if plot_2pt:
                        plt.legend(loc = 'lower left',title=rmag_legend_title,framealpha=0.5,fontsize=10)
                        two_pt_pdf.savefig()
                    else:
                        plt.legend(loc = 'upper right',title=rmag_legend_title,framealpha=0.5,fontsize=10)
                        cross_corr_pdf.savefig()
                if not plot_2pt:
                    plt.clf()
                    plt.semilogy(self.medNomad,halo_radius_final[j_band],marker='o')
                    #(self.medNomad/30.0)**(-4.0)
                    bf = 150.*10**(0.30*(7.0-self.medNomad)) + 12.*10**(0.05*(16.0-self.medNomad))
                    bf_jp = (self.medNomad/26.0)**(-3.4)
                    plt.semilogy(self.medNomad,bf,marker='x',ls='--')
                    legend_title = 'SSP ' + self.bands[j_band]+' band: mag [min,max](nsources)'
                    legend_entries = all_mag_string
                    legend_entries = np.append(legend_entries,'SSP approximation')
                    plt.semilogy(self.medNomad,bf_jp,marker='x',ls='--')
                    legend_entries = np.append(legend_entries,'Coupon bf')
                    plt.xlabel('Nomad magnitude')
                    plt.ylabel('Halo Radius (arcseconds)')
                    plt.title('Suggested bright-star radius for different source magnitudes.')
                    plt.legend(legend_entries,loc = 'lower left',title= legend_title,framealpha=0.5)
                    plt.ylim([3E-1,4E2])
                    cross_corr_pdf.savefig()
                    plt.clf()
                    plt.semilogy(self.medNomad,false_detection_max[j_band],marker='s')
                    plt.xlabel('Nomad magnitude')
                    plt.ylabel('Max. Correlation Coefficient')
                    plt.title('False detection indicator')
                    legend_title = 'SSP ' + self.bands[j_band]+' band: mag [min,max](nsources)'
                    plt.legend(all_mag_string,loc = 'upper left',title= legend_title,framealpha=0.5)
                    plt.ylim([5E-3,1E3])
                    cross_corr_pdf.savefig()
            if plot_2pt:
                two_pt_pdf.close()
            else:
                for jj_mag in range(self.nSrcMags):
                    #halo_radius_25mag = halo_radius_final[0::,0::,np.where(np.array(self.minMags)==25.0)[0][0]]
                    plt.clf()
                    halo_radius_25mag = halo_radius_final[0::,0::,jj_mag]
                    plt.semilogy(self.medNomad,np.transpose(halo_radius_25mag),marker='o')
                    legend_entries = self.bands
                    plt.semilogy(self.medNomad,bf,marker='x',ls='--')
                    legend_entries.append('SSP approximation')
                    plt.semilogy(self.medNomad,bf_jp,marker='x',ls='--')
                    legend_entries.append('Coupon bf')
                    #plt.semilogy(self.medNomad,bf,marker='x',ls='--')
                    plt.xlabel('Nomad magnitude')
                    plt.ylabel('Halo Radius (arcseconds)')
                    plt.legend(legend_entries,loc = 'lower left',framealpha=0.5)
                    plt.title('Suggested bright-star radius w/ bands, fixed mag ='+all_mag_string[jj_mag])
                    plt.ylim([3E-1,400])
                    cross_corr_pdf.savefig()
                    plt.clf()
                    false_detection_radius_25mag = false_detection_radius[0::,0::,jj_mag]
                    plt.semilogy(self.medNomad,np.transpose(false_detection_radius_25mag),marker='o')
                    legend_entries = self.bands
                    #plt.semilogy(self.medNomad,bf,marker='x',ls='--')
                    #legend_entries.append('SSP approximation')
                    #plt.semilogy(self.medNomad,bf_jp,marker='x',ls='--')
                    #legend_entries.append('Coupon bf')
                    #plt.semilogy(self.medNomad,bf,marker='x',ls='--')
                    plt.xlabel('Nomad magnitude')
                    plt.ylabel('Arcseconds')
                    plt.legend(legend_entries,loc = 'lower left',framealpha=0.5)
                    plt.title('False detection radius '+all_mag_string[jj_mag])
                    plt.ylim([3E-1,400])
                    cross_corr_pdf.savefig()
                    plt.clf()
                    false_detection_max_25mag = false_detection_max[0::,0::,jj_mag]
                    plt.semilogy(self.medNomad,np.transpose(false_detection_max_25mag),marker='o')
                    plt.xlabel('Nomad magnitude')
                    plt.ylabel('Max. Correlation Coefficient')
                    plt.title('False detection indicator '+all_mag_string[jj_mag])
                    plt.legend(self.bands,loc = 'upper left',framealpha=0.5)#,title= legend_entries)
                    plt.ylim([5E-3,1E2])
                    cross_corr_pdf.savefig()
                cross_corr_pdf.close()
        print('\a')
        interact(local=locals())
        #corr = two_point_angular(RADec_sources, bins)
        # PLOT 2: Show the distribution of these points.
        #plt.figure(2)
        do_plot = 0
        if do_plot:
            plt.scatter(RA,Dec,s=2,alpha=0.5,edgecolor='None')
            plt.scatter(RA_nomad_bin,Dec_nomad_bin,s=2,alpha=0.5,edgecolor='None',color='red')
            plt.title('XMM Deep')
            plt.xlabel('RA (J2000) Degrees')
            plt.ylabel('Dec (J2000) Degrees')
            plt.gca().invert_xaxis()
            plot_filename = self.plotFolder + 'xmm_lss_sky_coverage.png'
            plt.savefig(plot_filename)
            return

#if __name__ == "__main__":
#    CalculateBrightStarCorrelation(sys.arv).__init__()
    

#I'm not sure what this does exactly, but I want to finish writing this for completeness.
#    def test(self):
#        testExposure = loadTestExposure.LoadExposureTask(loadCoadd=True,filter='G')

