# Created by Nicole Czakon 10/05/2015
# The purpose of this program is to convert some of the SQL data entries to a pickle in order to speed up access.

# STEP 1: Open the SQL database.
# The purpose of this program is to read in all of the values created in "create_sources..."
# from astropy.io import ascii
#
# USAGE:
#
import numpy as np
import sqlite3
from code import interact
import hscConfigurationFile as hscConfig
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib.backends.backend_pdf import PdfPages
from scipy import ndimage
from scipy.interpolate import griddata,Rbf,interp2d
from scipy.stats import gaussian_kde,norm
import pickle
from copy import copy
import time

__all__ = ["MakePickleTask"]

class MakePickleTask:
    _DefaultName = "convertSQLToPickle"
    def __init__(self,srcParam=False,redo_downsample=None,do_sn=None):
                 
        time_1 = time.time()
        #This is the default pixel scale that I got from the images.
        self.pix_scale = 0.168#arcsec/pixel

        # VARIOUS SETTINGS FOR THE PROGRAM
        if srcParam:
            self.srcParam = srcParam
        else:
            print('srcParam must be defined in makeCharacterizationPlots.')
            interact(local=locals())


        if redo_downsample:
            self.redo_downsample = True
            # This is just to make a smaller version of the data so that when I test the code,
            # it doesn't take as long to run.
            self.downsample = 100
            #Need to change the name of this program.
            self.sql_pickle_file = hscConfig.characterization_sav_folder + 'characterization_' + str(downsample) + '.p'

        else:
            self.redo_downsample = False            

        if do_sn:
            self.do_sn = True
        else:
            self.do_sn = False            

        junk = self.loadSQL()
        #plt.savefig(hscConfig.characterization_plot_folder + 'xmm_lss_sky_coverage_2x2.png')
        pickle.dump([RA_new, Dec_new, shape_temp_new, downsample], open(self.sql_pickle_file, 'wb'))

        time_2 = time.time()
        time_tot = (time_2 - time_1) / 60.
        print('Program took a total of ' + str(time_tot) + ' minutes to execute.')
        print('\a')
        interact(local = locals())

    #if loadSQL:
    #This generally takes quite a bit longer than loading from the pickle.
    #This program automatically saves it to a pickle file and therefore
    #the second time when running this program, this is the option that
    #should be taken.
    def loadSQL(self):
        #Create a connection object
        #'/array/users/czakon/data/sql_downloads_XMMLSS/XMM_LSS_sql_psf_mod.db'') 
        conn = sqlite3.connect(hscConfig.sql_source_db)
        #Create a cursor object
        c = conn.cursor()
        ##################################################################################################
        column_names = np.array([])
        for row in c.execute('''PRAGMA table_info(xmm_lss)'''):
            column_names = np.append(column_names, row[1])
        indices = {'RA':np.where(column_names == 'RA_J2000_degrees')[0][0], \
                   'Dec':np.where(column_names == 'Dec_J2000_degrees')[0][0], \
                   'shape_sdss_psf_r_det_fwhm':np.where(column_names == \
                                                        'shape_sdss_psf_r_det_fwhm')[0][0], \
                   'gmag_cmodel':np.where(column_names == 'gmag_cmodel')[0][0], \
                   'gmag_cmodel_err':np.where(column_names == 'gmag_cmodel_err')[0][0], \
                   'gmag_kron':np.where(column_names == 'gmag_kron')[0][0], \
                   'gmag_kron_err':np.where(column_names == 'gmag_kron_err')[0][0], \
                   'gmag_psf':np.where(column_names == 'gmag_psf')[0][0], \
                   'gmag_psf_err':np.where(column_names == 'gmag_psf_err')[0][0], \
                   'rmag_cmodel':np.where(column_names == 'rmag_cmodel')[0][0], \
                   'rmag_cmodel_err':np.where(column_names == 'rmag_cmodel_err')[0][0], \
                   'rmag_kron':np.where(column_names == 'rmag_kron')[0][0], \
                   'rmag_kron_err':np.where(column_names == 'rmag_kron_err')[0][0], \
                   'rmag_psf':np.where(column_names == 'rmag_psf')[0][0], \
                   'rmag_psf_err':np.where(column_names == 'rmag_psf_err')[0][0], \
                   'imag_cmodel':np.where(column_names == 'imag_cmodel')[0][0], \
                   'imag_cmodel_err':np.where(column_names == 'imag_cmodel_err')[0][0], \
                   'imag_kron':np.where(column_names == 'imag_kron')[0][0], \
                   'imag_kron_err':np.where(column_names == 'imag_kron_err')[0][0], \
                   'imag_psf':np.where(column_names == 'imag_psf')[0][0], \
                   'imag_psf_err':np.where(column_names == 'imag_psf_err')[0][0], \
                   'zmag_cmodel':np.where(column_names == 'zmag_cmodel')[0][0], \
                   'zmag_cmodel_err':np.where(column_names == 'zmag_cmodel_err')[0][0], \
                   'zmag_kron':np.where(column_names == 'zmag_kron')[0][0], \
                   'zmag_kron_err':np.where(column_names == 'zmag_kron_err')[0][0], \
                   'zmag_psf':np.where(column_names == 'zmag_psf')[0][0], \
                   'zmag_psf_err':np.where(column_names == 'zmag_psf_err')[0][0], \
                   'ymag_cmodel':np.where(column_names == 'ymag_cmodel')[0][0], \
                   'ymag_cmodel_err':np.where(column_names == 'ymag_cmodel_err')[0][0], \
                   'ymag_kron':np.where(column_names == 'ymag_kron')[0][0], \
                   'ymag_kron_err':np.where(column_names == 'ymag_kron_err')[0][0], \
                   'ymag_psf':np.where(column_names == 'ymag_psf')[0][0], \
                   'ymag_psf_err':np.where(column_names == 'ymag_psf_err')[0][0]}
    
        # This counts all of the rows in the database
        interact(local=locals())
        for row in c.execute('''SELECT Count(*) FROM xmm_lss'''):
            print(str(row[0]) + ' rows in the XMM_LSS_sql.')
            # This counts all of the different tracts in the database.
            # Only two distinct tracts for now, hopefully this changes when I make a new query.
        for row in c.execute('''SELECT Count(DISTINCT tract) FROM xmm_lss'''):
            print(str(row[0]) + ' distinct tracts.')
            # Only two distinct tracts for now, hopefully this changes when I make a new query.
        for row in c.execute('''SELECT DISTINCT tract FROM xmm_lss'''):
            print('Tract ' + str(row[0]) + ' is included.')

        bands = ['g','r','i','z','y']
        mag_models = ['kron','cmodel']
        test_entries = [band + 'mag_' + mag_model for band in bands for mag_model in mag_models]
        test_entries.append('shape_sdss_psf_r_det_fwhm')
        minMags = [21.0, 22.0, 23.00, 24.00, 25.00, 26.00, 26.50]
        maxMags = {'kron' : {'g' : [23.00, 24.0, 23.75, 24.50, 25.25, 26.15, 27.15], \
                             'r' : [23.00, 23.5, 23.75, 24.30, 25.25, 26.25, 27.15], \
                             'i' : [23.00, 23.0, 23.75, 24.30, 25.25, 26.50, 27.50], \
                             'z' : [22.50, 23.0, 23.75, 24.30, 25.40, 26.50, 27.50], \
                             'y' : [22.50, 23.0, 23.75, 24.30, 25.40, 26.60, 28.00]}, \
                   'cmodel':{'g' : [22.50, 23.0, 23.75, 24.30, 25.25, 26.50, 27.15], \
                             'r' : [23.00, 23.5, 23.75, 24.40, 25.25, 26.25, 27.15], \
                             'i' : [22.50, 23.0, 23.75, 24.30, 25.25, 26.25, 27.15], \
                             'z' : [22.25, 22.8, 23.50, 24.30, 25.20, 26.50, 27.50], \
                             'y' : [22.25, 22.8, 23.50, 24.30, 25.25, 26.50, 27.50]}}

        #good_columns = [3, 4, 5, 6, 10, 11, 12, 16, 17, 18, 22, 23, 24, 28, 29, 30]#FULL
        #bad_columns = [4, 5, 6]
        #    if do_sn:
        good_columns = [10, 11, 12, \
                        16, 17, 18, \
                        22, 23, 24, \
                        28, 29, 30]
                    
        test_entries = column_names[good_columns]
        plot_filename_base = hscConfig.characterization_plot_folder + 'xmm_lss_hist'
                         
        for entry_name in test_entries:
            if entry_name == 'shape_sdss_psf_r_det_fwhm':
                norm_factor = pix_scale
            else:
                norm_factor = 1.0
            print('Testing the discrete number of ' + entry_name)
            sql_command_test  = 'SELECT Count(DISTINCT '+entry_name+') FROM xmm_lss'
            test_row = c.execute(sql_command_test)
            row = test_row.fetchone()
            if row[0] == 1:
                print('No distinct entries found, moving to next parameter...')
                continue
            print('Converting the SQL entries into arrays, this may take a while...')    
            #I would like to be able to test for null entries, however, this command does not work.
            #sql_command = '''SELECT * FROM xmm_lss WHERE ? > 0 AND ? IS NOT NULL AND TRIM(?) <> '' '''
            sql_command = '''SELECT * FROM xmm_lss WHERE ? > 0 '''
            if self.do_sn:
                err_entry = []
            entry = []#np.array([])
            RA = []
            Dec = []
            for row in c.execute(sql_command, [entry_name]):

                if row[indices[entry_name]] == '' or row[indices[entry_name]] == None:
                    continue
                if self.do_sn and (row[indices[entry_name + '_err']] == '' or \
                              row[indices[entry_name+'_err']] == None):
                    continue
                entry.append(float(row[indices[entry_name]]))
                RA.append(row[indices['RA']])
                Dec.append(row[indices['Dec']])
                if self.do_sn:
                    err_entry.append(row[indices[entry_name + '_err']])
            print('Done inserting ' + str(len(entry)) + ' ' + entry_name + ' entries....')
            if self.self.do_sn:
                err_entry = norm_factor * np.array(err_entry)
            entry = norm_factor * np.array(entry)
            RA = np.array(RA)
            Dec = np.array(Dec)
            out_filename = hscConfig.characterization_sav_folder + 'characterization_' + entry_name + add_string + '.p'
            pickle.dump([RA, Dec, entry], open(out_filename, 'wb'))

        #################################################################################################
        # Close the connection
        conn.close()
        print('\a')
        #interact(local=locals())
    
###############################################################
# THE FOLLOWING CODE IS OLD CODE THAT I JUST KEPT FOR REFERENCE
###############################################################
'''
def characterizationPlots(self):
        #This plots all of the files that were output to the pickle.
        if self.do_sn:
            add_string = '_SN'
        else:
            add_string = ''

        pdf_plot = PdfPages(plot_filename_base + add_string + '.pdf')
            if do_plot:
                if self.do_sn:
                    xlims = [15,30]
                    ylims = [1,1E5]#min(entry/err_entry),max(entry/err_entry)]
                    start_image(xlims,ylims)
                    scatter_plot(xlims,ylims,entry, entry/err_entry, entry_name)
                    do_hist(entry, entry_name + ' S/N', full_plot = False, SN_in = entry/err_entry)
                else:
                    do_hist(entry, entry_name, full_plot = True)
                pdf_plot.savefig()
                #plt.savefig(plot_filename_base + '_'+entry_name+'.png')
            pdf_plot.close()

####

 def start_image(xlims, ylims):
        #This program sets up the default plotting parameters. 
        plt.clf()
        font = {'size' : 10}
        matplotlib.rc('font', **font)#'ytick',labelsize=10
        ax1 = plt.subplot2grid((6,5), (0,0), rowspan = 4, colspan = 5)
        plt.xlabel('RA (J2000) (deg)')
        plt.ylabel('Dec (J2000) (deg)')
        plt.xlim(xlims)
        plt.ylim(ylims)
        plt.axis('equal')

    def scatter_plot(xlims,ylims,entry, hist_in, name):
        s_size = 5.0
        hist_max = 1E4
        plt.scatter(entry[0:hist_max], hist_in[0:hist_max], edgecolor = 'None', alpha = 0.1, s = s_size)
        plt.xlabel(name)
        plt.ylabel(name + ' S/N')
        plt.xlim(xlims)
        plt.ylim(ylims)
        plt.yscale('log')

    def density_plot():
        plt.colorbar(label = 'A.U.', format = '%2.1f')

    def seeing_plot():
        plt.colorbar(label = 'SDSS PSF Shape $r_{det}$\nFWHM')    

    def do_hist(hist_in, plot_title, full_plot = False, SN_in = None):

        if full_plot:
            plt.clf()
            font = {'size' : 20}
            matplotlib.rc('font', **font)
        else:
            if SN_in == None:
                ax2 = plt.subplot2grid((6, 5),(5,0),rowspan=2,colspan=4)
            else:
                ax2 = plt.subplot2grid((6, 5),(5,0),rowspan=2,colspan=5)

        if SN_in == None:
            n_histbins = 1E2
            temp_x = plt.hist(hist_in, bins = n_histbins)
        else:
            n_histbins = 1E4
            temp_x = plt.hist(SN_in, bins = n_histbins)

        #Find the gaussian best-fit
        param = norm.fit(hist_in)
        #Define out the 3-sigma mark, or three-times the median
        if SN_in==None:
            hist_xlims = [0, param[0]+3*param[1]]
        else:
            hist_xlims = [1, 6*np.median(SN_in)]
            plt.xscale('log')
        hist_x = np.linspace(hist_xlims[0], hist_xlims[1], n_histbins)
        plt.plot(hist_x, norm.pdf(hist_x, loc = param[0], scale = param[1]), 'r--')
        plt.xlim(hist_xlims)
        plt.ylim([0, 1.25*np.max(temp_x[0][1::])])
        plt.xlabel(plot_title)
        plt.ylabel('Counts')
        #Convert to scientific notation after a bit.
        plt.ticklabel_format(style = 'sci', axis = 'y', scilimits = (-3,3))
        if full_plot:
            #plt.subplots_adjust(left=0.1,right=0.9,top=0.9,bottom=0.1)
            plt.tight_layout()

    else:
        RA_new, Dec_new, shape_temp_new, downsample = pickle.load(open(sql_pickle_file, 'rb'))
    #The pixel scale can be obtained via the following:
    #import loadTestExposure
    #testExposure.exposure.getWcs().pixelScale().asArcseconds()
    xy = np.vstack([RA_new, Dec_new])
    new_pixelization = 512j
    x_flat = np.r_[RA_new.min() : RA_new.max() : new_pixelization]
    y_flat = np.r_[Dec_new.min() : Dec_new.max() : new_pixelization]
    x,y = np.meshgrid(x_flat, y_flat)
    grid_coords = np.append(x.reshape(-1, 1), y.reshape(-1, 1), axis = 1)
    # Below is the density function.
    if downsample == 100:
        z_kde = gaussian_kde(xy, bw_method = 0.01)#(xy)#'scott'
    else:
        z_kde = gaussian_kde(xy, bw_method = 0.05)#(xy)#'scott'
    z2_kde = z_kde(grid_coords.T)
    #Remove all of the values that correspond to zero density function.
    z3_kde = copy(z2_kde)
    #This histogram below is used for characterization.
    if False:
        plt.hist(z3, bins = 20)
        plt.show()
    kde_x = z2_kde < 1E-2
    z3_kde[kde_x] = 0.0
    z3_kde = z3_kde.reshape(new_pixelization, new_pixelization)
    # We need to make randoms based on the seeinf
    z_gd = griddata(xy.T, shape_temp_new, grid_coords, method = 'cubic')#nearest')
    z_gd[kde_x] = 0.0
    z_gd = z_gd.reshape(new_pixelization, new_pixelization)
    #from scipy.interpolate import griddata
    #from scipy.stats import gaussian_kde
    ####
    xlims = [RA_new.max(), RA_new.min()]
    ylims = [-6.2, -3.0]#[Dec_new.min(),Dec_new.max()]
    #fig,((ax1,ax2,ax3),(ax4,ax5,ax6)) = plt.subplots(2,3, \
        #sharex='col',sharey='row')
    #plt.figure(1,figsize=(20,20))
    #gs = GridSpec(12,8)#,bottom=0.1,left=0.1,right=0.1)
    if True:
        start_image(xlims, ylims)
        # PLOT 1A: Show the histogram results
        print('Making a 2d histogram of the distribution of sources...')
        #First, find where the density is zero in the image.
        vmin1 = 0.0
        vmax1 = 5.0
        nhist_bins = 200
        if final_plots:
            vmax1 *=downsample
            z_hist2d = plt.hist2d(RA, Dec, (nhist_bins, nhist_bins), \
                                  vmin = vmin1,vmax = vmax1)
        else:
            z_hist2d = plt.hist2d(RA_new, Dec_new,(nhist_bins, nhist_bins), \
                                  vmin = vmin1, vmax = vmax1)
        density_plot()
        plt.title('XMM Deep\n(2d Histogram)')
        plt.gca().invert_xaxis()
        # PLOT 1B: Show the histogram
        do_hist(z_hist2d[0].flatten(), 'Pixel Values')
        if do_pdf:
            pdf_plot.savefig()
        else:
            plt.savefig(plot_filename_base + '_2dhist.png')

        #plt.axes('equal','datalim')
    if False:
        # PLOT 2 (KDE)
        plt.clf()
        print('Making the kde approximation of the seeing data.')
        #plt.subplot(322)
        #vmax1 = 0.5*downsample
        #I'm not sure where this estimate came from...
        plt.imshow(20 * downsample * z3_kde[::-1, ::-1], \
                        extent = [x_flat[-1], x_flat[0], y_flat[0], y_flat[-1]], \
                                          vmin = vmin1,vmax = vmax1)
        #cbar_ax = plt.add_axes([0.85,0.15,0.05,0.7])
        density_plot()
        plt.title('XMM Deep\n(KDE)')
        start_image(xlims, ylims)
        #plt.gca().invert_xaxis()
        #plt.gca().invert_yaxis()
        plt.axis('equal')
        #plt.axis('scaled')
        if do_pdf:
            pdf_plot.savefig()
        else:
            plt.savefig(plot_filename_base + '_kde.png')

        #plt.show()

    if True:
        # PLOT 3: Finding a better way to interpolate the data
        print('Making the 2d interp')
        f_interp = interp2d(z_hist2d[1][0:-1], z_hist2d[2][0:-1], \
                            z_hist2d[0], kind = 'cubic')
        z_interp = f_interp(x_flat, y_flat)
        z_interp = z_interp.reshape(new_pixelization, new_pixelization)
        bad_z = z_interp < 0.0
        z_interp[bad_z] = 0.0
        #plt.subplot(323)
        #vmax1 = 5.0*downsample
        start_image(xlims, ylims)
        plt.title('XMM Deep\n(2d Spline Interp)')
        plt.imshow(z_interp[::-1,::-1].T, extent = [x_flat[-1], x_flat[0], y_flat[0], y_flat[-1]], \
                                          vmin = vmin1,vmax = vmax1)
        density_plot()
        do_hist(z_interp.flatten(), 'Pixel Values')
        if do_pdf:
            pdf_plot.savefig()
        else:
            plt.savefig(plot_filename_base + '_spline.png')
    vmax1 = 8.0 * pix_scale
    if True:
        # PLOT 1: Show the distribution of these points.
        print('Making the scatter plot including the seeing as a third dimension')
        #plt.subplot(324)
        #plt.scatter(RA_new,Dec_new,s=2,alpha=0.5,edgecolor='None')
        start_image(xlims, ylims)
        if final_plots:
            plt.scatter(RA, Dec, c = shape_temp, s = 1, edgecolor = '', \
                                vmin = vmin1, vmax = vmax1)
        else:
            plt.scatter(RA_new, Dec_new, c = shape_temp_new, s = 1, edgecolor = '', \
                        vmin = vmin1, vmax = vmax1)
        seeing_plot()
        plt.title('XMM Deep\n(Scatter plot)')
        #plt.gca().invert_xaxis()
        if final_plots:
            do_hist(shape_temp, 'Pixel Values')
        else:
            do_hist(shape_temp_new, 'Pixel Values')
        if do_pdf:
            pdf_plot.savefig()
        else:
            plt.savefig(plot_filename_base + '_scatter.png')
    if True:
        # PLOT 4: Show the griddata results
        print('Making the grid data interpolation of the seeing data.')
        start_image(xlims, ylims)
        vmax1 = 500
        #350:Perhaps 300 a bit too high and 400 perhaps a bit too high.
        #exponential factor, 1.1 didn't do anything
        norm_factor = 350.
        exp_factor = 1.0#-2.5
        plot_z = norm_factor*z_gd[::-1, ::-1] ** exp_factor
        plt.imshow(plot_z, \
                   extent = [x_flat[-1], x_flat[0], y_flat[0], y_flat[-1]], \
                   vmin = vmin1,vmax = vmax1)
        plt.colorbar(label = 'SDSS PSF Shape Scaled to Density (Grid Data)')
        plt.title('XMM Deep\n(GridData)')
        do_hist(plot_z.flatten(), 'Pixel Values')
        if do_pdf:
            pdf_plot.savefig()
        else:
            plt.savefig(plot_filename_base + '_GridData.png')

    if False:
        #This takes a little bit of a long time
        z_rbf = Rbf(RA_new, Dec_new, shape_temp_new, function = 'linear')
        di = z_rbf(x, y)
        plt.imshow(di)

    if False:
        sx = ndimage.sobel(im, axis = 0, mode = 'mirror')
        sy = ndimage.sobel(im, axis = 1, mode = 'mirror')
        sob = np.hypot(sx, sy)
        start_image(xlims, ylims)
        #plt.subplot(131)
        plt.imshow(im)#,cmap=plt.cm.gray)
        #plt.subplot(132)
        plt.imshow(sx)
        #plt.subplot(133)
        plt.imshow(sob)
        plt.title('XMM Deep')
        plt.savefig(plot_filename)
   
if do_plot:
    out_filename = hscConfig.characterization_sav_folder + 'characterization.p'
    if do_pdf:
        plot_filename = hscConfig.characterization_plot_folder + \
                        'xmm_lss_sky_coverage_'+ str(downsample)+'.pdf'
        pdf_plot = PdfPages(plot_filename)
    else:
        plot_filename_base = hscConfig.characterization_plot_folder + \
                        'xmm_lss_sky_coverage_'+ str(downsample) + '_v2'
    print('Starting to open the pickle file, this takes a while....')
    if final_plots:
        RA,Dec,shape_temp = pickle.load(open(out_filename,"rb"))
        shape_temp = np.array(shape_temp)
        shape_temp  = pix_scale*shape_temp
    if redo_downsample:
        new_size = len(RA) - len(RA) % downsample
        RA_new = np.array(RA)
        Dec_new = np.array(Dec)
        RA_new = RA_new[0:new_size]
        Dec_new = Dec_new[0:new_size]
        shape_temp_new = copy(shape_temp)
        shape_temp_new = shape_temp_new[0:new_size]
        print('Downsampling the arrays by a factor of ' + str(downsample) + '...')
        RA_new = RA_new.reshape(new_size / downsample, downsample)
        Dec_new = Dec_new.reshape(new_size / downsample, downsample)
        shape_temp_new = shape_temp_new.reshape(new_size / downsample, downsample)
        RA_new = RA_new[:,0]
        Dec_new = Dec_new[:,0]
        shape_temp_new  = pix_scale * shape_temp_new[:,0]
'''
