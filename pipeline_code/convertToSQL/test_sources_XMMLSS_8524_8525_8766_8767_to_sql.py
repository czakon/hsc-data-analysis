]#Ok, this database isn't done yet, I need to clean it up later.
#This does not work, I gave up and decided to try to code this to acfitsio file
#!/usr/bin/env python
#Create by Nicole Czakon 09/10/2015
#The purpose of this program is to read in all of the values created in "create_sources..."
#from astropy.io import ascii
import numpy as np
import sqlite3
import code
#Create a connection object
conn = sqlite3.connect('/array/users/czakon/data/sql_downloads_XMMLSS/XMM_LSS_sql.db')
#Create a cursor object
c = conn.cursor()
##########################################################################################################
#for row in c.execute('SELECT * FROM xmm_lss ORDER BY tract'):
#    print row
# This counts all of the rows in the database
for row in c.execute('''SELECT Count(*) FROM xmm_lss'''):
    print(str(row[0])+' rows in the XMM_LSS_sql.')
# This counts all of the different tracts in the database.
# Only two distinct tracts for now, hopefully this changes when I make a new query.
for row in c.execute('''SELECT Count(DISTINCT tract) FROM xmm_lss'''):
    print(str(row[0])+' distinct tracts.')
# Only two distinct tracts for now, hopefully this changes when I make a new query.
for row in c.execute('''SELECT DISTINCT tract FROM xmm_lss'''):
        print('Tract '+str(row[0])+' is included.')
#code.interact(local=locals())
##########################################################################################################
# Close the connection
conn.close()
#To open the data again
#conn,c
#Read in the table
#filename = '/home/hscpipe/czakon/data/sql_downloads_XMMLSS/8524_8525_8766_8767.csv'
#f = open(filename)
#The first line are the column values, remove the newline character and split along the commas.
#sql_columns = np.array(f.readline().strip().split(','))
#There are 77 columns in the original csv file that I downloaded. Here, I've decided to only enter in
#23 into the database, this can be changed when necessary.
#good_columns = ["# tract","ra2000","decl2000", \
#                "gmag_cmodel","gmag_psf","g_classification_extendedness","g_flags_pixel_saturated_any", \
#                "rmag_cmodel","rmag_psf","r_classification_extendedness","r_flags_pixel_saturated_any", \
#                "imag_cmodel","imag_psf","i_classification_extendedness","i_flags_pixel_saturated_any", \
#                "zmag_cmodel","zmag_psf","z_classification_extendedness","z_flags_pixel_saturated_any", \
#                "ymag_cmodel","ymag_psf","y_classification_extendedness","y_flags_pixel_saturated_any"]
#good_indices = []
#for entry in good_columns:
#    good_indices.extend(np.where(sql_columns==entry)[0])
                        
#good_indices.extend(np.where(np.in1d(sql_columns,good_columns))[0]

# Create table
#c.execute('''CREATE TABLE xmm_lss (tract real, RA_J2000_degrees real, Dec_J2000_degrees real,
#             gmag_cmodel real,gmag_psf real,g_classification_extendedness real,g_flags_pixel_saturate_any real, 
#             rmag_cmodel real,rmag_psf real,r_classification_extendedness real,r_flags_pixel_saturate_any real, 
#             imag_cmodel real,imag_psf real,i_classification_extendedness real,i_flags_pixel_saturate_any real, 
#             zmag_cmodel real,zmag_psf real,z_classification_extendedness real,z_flags_pixel_saturate_any real, 
#             ymag_cmodel real,ymag_psf real,y_classification_extendedness real,y_flags_pixel_saturate_any real)''')

