#Ok, this database isn't done yet, I need to clean it up later.
#This does not work, I gave up and decided to try to code this to acfitsio file
#!/usr/bin/env python
#Create by Nicole Czakon 09/10/2015
#The purpose of this program is to convert the catalog that I downloaded from STARS to sqlite3 si
#from astropy.io import ascii
import numpy as np
import sqlite3
from code import interact
import hscConfigurationFile as hscConfig
#Read in the table
#filename = '/home/hscpipe/czakon/data/sql_downloads_XMMLSS/8524_8525_8766_8767.csv'
filename = hscConfig.download_XMMLSS_sql
f = open(filename)
#The first line are the column values, remove the newline character and split along the commas.
sql_columns = np.array(f.readline().strip().split(','))
#There are 77 columns in the original csv file that I downloaded. Here, I've decided to only enter in
#23 into the database, this can be changed when necessary.
good_columns = ["# tract","ra2000","decl2000", "shape_sdss_psf",\
                "gmag_cmodel","gmag_psf","gmag_kron","gmag_cmodel_err","gmag_psf_err","gmag_kron_err",\
                "rmag_cmodel","rmag_psf","rmag_kron","rmag_cmodel_err","rmag_psf_err","rmag_kron_err",\
                "imag_cmodel","imag_psf","imag_kron","imag_cmodel_err","imag_psf_err","imag_kron_err",\
                "zmag_cmodel","zmag_psf","zmag_kron","zmag_cmodel_err","zmag_psf_err","zmag_kron_err",\
                "ymag_cmodel","ymag_psf","ymag_kron","ymag_cmodel_err","ymag_psf_err","ymag_kron_err"]
good_indices = []
for entry in good_columns:
    good_indices.extend(np.where(sql_columns==entry)[0])
                        
#good_indices.extend(np.where(np.in1d(sql_columns,good_columns))[0]

#Create a connection object
#home_folder = '/array/users/czakon/data/sql_downloads_XMMLSS/'
db_out_filename = hscConfig.sql_source_db
conn = sqlite3.connect(db_out_filename)
#conn = sqlite3.connect(home_folder+'XMM_LSS_sql_psf_mod.db')
#Create a cursor object
c = conn.cursor()
#Drop the table so that we can create it again.
c.execute('''DROP TABLE xmm_lss''')
# Create table
c.execute('''CREATE TABLE xmm_lss (tract integer, RA_J2000_degrees real, Dec_J2000_degrees real, 
             shape_sdss_psf_r_det_fwhm float, 
             gmag_cmodel real,gmag_psf real,gmag_kron real,gmag_cmodel_err real,gmag_psf_err real,gmag_kron_err real,
             rmag_cmodel real,rmag_psf real,rmag_kron real,rmag_cmodel_err real,rmag_psf_err real,rmag_kron_err real,
             imag_cmodel real,imag_psf real,imag_kron real,imag_cmodel_err real,imag_psf_err real,imag_kron_err real,
             zmag_cmodel real,zmag_psf real,zmag_kron real,zmag_cmodel_err real,zmag_psf_err real,zmag_kron_err real,
             ymag_cmodel real,ymag_psf real,ymag_kron real,ymag_cmodel_err real,ymag_psf_err real,ymag_kron_err real)''')

print('Inserting all of the entries into the database, this will take a while...')
for line in f:
    #first_column = np.array(f.readline().strip().split(','))
    #first, split along the double quotations to find the shape measurements.
    column1 = np.array(line.strip().split('"'))
    column = np.array([])
    for subcolumn in column1:
        #If there is no '[', split the column as usual
        if subcolumn.find('['):
            temp_column = np.array(subcolumn.strip().split(','))
            #For some reason, this keeps the comma at the end, which I want to get rid of.
            column = np.append(column,temp_column[0:-1])
            #print('A')
            #print(subcolumn.find('['))
            #print(column)
            #interact(local=locals())
        #Otherwise, leave it in tact.
        else:
            #print('B')
            #print(subcolumn)
            #print(subcolumn.find('['))
            #Technically, this could be done at the stage when the elements are inserted into the sql database
            shape_sdss_psf = subcolumn.strip('[]').split(', ')
            for psf_x in range(len(shape_sdss_psf)):
                if shape_sdss_psf[psf_x] == 'None':
                    #print('Converting None')
                    shape_sdss_psf[psf_x] = None
                else:
                    shape_sdss_psf[psf_x] = np.float(shape_sdss_psf[psf_x]) 
            #Alternative
            #r_trace = (0.5*(shape_sdss_psf[0]+shape_sdss_psf[1]))**0.5
            #r_det is preferred as the area of the allipse is pi*r_det**2
            #Convert from sigma to FWHM.
            if shape_sdss_psf[0] == None:
                subcolumn = None
            else:
                temp= shape_sdss_psf[0]*shape_sdss_psf[1]-shape_sdss_psf[2]**2
                if temp < 0.0:
                    subcolumn = -1
                    print('A')
                    print(line)
                    print(column1)
                    print(subcolumn)
                    print(shape_sdss_psf)
                    print('\a')
                    print('For some reason there is a negative here.')
                    #interact(local=locals)
                else:
                    subcolumn = 2.0*np.sqrt(2.0*np.log(2.0))*(shape_sdss_psf[0]*shape_sdss_psf[1]-shape_sdss_psf[2]**2)**0.25
            column = np.append(column,subcolumn)
            #print(column)
            #interact(local=locals())
    #column[np.where(column == '')] = 'nan'
    #column = np.array(f.readline().strip().split(','))
    # Insert a row of data
    #print('B')
    #print(sql_columns)
    #print(column)
    #print(column[good_indices])
    #interact(local=locals())
    c.execute('''INSERT INTO xmm_lss VALUES (?,?,?,?,
                                             ?,?,?,?,?,?,
                                             ?,?,?,?,?,?,
                                             ?,?,?,?,?,?,                                                                        
                                             ?,?,?,?,?,?, 
                                             ?,?,?,?,?,?)''',column[good_indices])
#    if column[good_indices][10] == '':
#        print('No rmag_cmodel entry...')
#    else:#
#        print('good rmag_cmodel entry...')
        #interact(local=locals())
#for row in c.execute('SELECT * FROM xmm_lss ORDER BY tract'):
#    print row
# Commit the changes and close the connection
# for row in c.execute('''SELECT Count(*) FROM xmm_lss'''):
#...     print row
# This counts all of the rows in the database
for row in c.execute('''SELECT Count(*) FROM xmm_lss'''):
        print(str(row[0])+' rows in the XMM_LSS_sql.')
# This counts all of the different tracts in the database.
# Only two distinct tracts for now, hopefully this changes when I make a new query.
for row in c.execute('''SELECT Count(DISTINCT tract) FROM xmm_lss'''):
    print(str(row[0])+' distinct tracts.')
        # Only two distinct tracts for now, hopefully this changes when I make a new query.
for row in c.execute('''SELECT DISTINCT tract FROM xmm_lss'''):
    print('Tract '+str(row[0])+' is included.')
conn.commit()
# Close the connection
conn.close()
#To open the data again
#conn,c
f.close()
interact(local=locals())
