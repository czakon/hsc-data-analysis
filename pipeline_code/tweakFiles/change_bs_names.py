#!/usr/bin/env python

from pipeline_code import hscConfigurationFile as hscConfig
import os
import glob
import shutil
from copy import copy
import numpy as np
from code import interact

band = hscConfig.bands[0]
obands = hscConfig.bands[1:4]
folderout = hscConfig.ssp_patch_folder

#sub_directory = 'WIDE/'
sub_directory = 'DEEP2/'
#sub_directory = 'ULTRADEEP/'
tracts = ['8766','9812']
for tract in tracts:

    directory_out = folderout + sub_directory + \
                    'deepCoadd/BrightObjectMasks/' + tract +'/'

    reg_fileout = directory_out + 'BrightObjectMask-' + \
                  '*-HSC-' + band + '.reg'
    all_files = glob.glob(reg_fileout)

    for file_in in all_files:

        for oband in obands:

            file_out = file_in.replace('HSC-'+band,'HSC-'+oband)
            print('Copying file: ' + file_in + ' to:' + file_out)
            shutil.copyfile(file_in,file_out)
        #START HERE AND FIGURE OUT HOW TO CHANGE THE FILENAMES.
#        os.listdir(reg_fileout)
#              tract_patch['tract'] + '-' + tract_patch['patch'] + '-HSC-' + band + '.reg'

                                                                                    
