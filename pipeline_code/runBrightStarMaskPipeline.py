#!/usr/bin/env python
# Created by Nicole Czakon 10/05/2015
# The purpose of this script is to run the bright star mask pipeline from
# beginning to end
#
# USAGE:  ./runBrightStarMaskPipeline.py --options
#
# Optional Arguments:
# -h: help.                                                                     
#
#
# GENERAL OPTIONS
#
# --field: specify which ssp field.
# -brightStarDb: override the default database, useful if you're having
#                issues accessing the default database.
#
#
# INSERT DATA, MEASURE CORRELATION FUNCTION, WRITE OUTPUT
#
# --insertBrightStarsSql: insert the bright star catalog into SQL.
# --makeRandomFits: make fits files for the randoms
# --makeRandomsSql: write the randoms to SQL
# --makeSspSql: write the SSP soures to SQL
# --measureCorrelationFunction: determine what the size of the bright-star      
#                               masks should be.                                
# --makeBrightStarRegFiles: this creates the reg files using Jean Coupon's code
# -maskSize: specify whether you want the small, medium, or large mask size.    
# --insertBsrRegIntoSql: this inserts the region files into the sql database.   
#
#
# PLOTTING OPTIONS
#
# (not complete, ucac needs tract and patch information)
# --verifyBrightStarRegFiles: this verifies the reg files with ds9 images.
# (tested)                                          
# --compareCatMags: compare the various magnitudes of the different star        
#                   catalogs.
#   Two of the following catalogs need to be chosen
# --plotNomad: plot the nomad catalog.  
# --plotTycho: plot the tycho catalog.
# --plotUcac: plot the ucac catalog.
#
# --plotBrightStarMasks: plot the bright-star masks.                            
# --makeRandomPlots: include the randoms in the plotting procedure.
# --sql: load the data from the sql file.                                       
# --cp: generate data characterization plots.                                   
#
# Standard Python Packages
import argparse, subprocess
from code import interact
# User Packages
from plotting import makeCharacterizationPlots
from pipeline_code.plotting.makeCharacterizationPlots import MakePlotsTask
from convertToPickle import convertSQLToPickle
from pipeline_code.masking import retrieveBrightStarsFromNomad
from pipeline_code.masking.insertBsmIntoSQL import InsertBsmTask
from pipeline_code.masking import brightStarCorrelationFunctionNew as bsCorr
from pipeline_code import hscConfigurationFile as hscConfig
from pipeline_code.masking import jeanCouponBrightStarMaskEdit as jcBS

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description= 'The purpose of this script is to run the bright star '
                     'mask pipeline from beginning to end')

    parser.add_argument('-field',help='Specify which field',
                        default=None)
    parser.add_argument('-brightStarDb', \
                        help='Override the default database, useful if '
                             'you\'re having issues accessing the database.',
                        default=None)
    parser.add_argument('--insertBrightStarsSql', \
                        help='Insert the bright star catalog into SQL',
                        action='store_true',default=None)
    parser.add_argument('--makeRandomFits',\
                        help='make fits files for the randoms.',
                        action='store_true',default=None)
    parser.add_argument('--makeRandomSql',\
                        help='Write the randoms to SQL.',
                        action='store_true',default=None)
    parser.add_argument('--makeSspSql',\
                        help='Write the SSP sources to SQL.',
                        action='store_true',default=None)
    parser.add_argument('--measureCorrelationFunction',
                        help='This is the method that we use to determine the '
                             'size of the bright-star masks', \
                        action='store_true',required=False)
    parser.add_argument('--makeBrightStarRegFiles',
                        help='This creates the reg files using Jean Coupon\'s '
                             'code', \
                        action='store_true',required=False)
    parser.add_argument('-maskSize', \
                        help='Here is the option to specify whether you want '
                             'the small,medium, or large mask size', \
                        default=None)
    parser.add_argument('--insertBsrRegIntoSql',
                        help='This inserts the region files into the '
                             'sql database.',action='store_true',required=False)
    parser.add_argument('--verifyBrightStarRegFiles',
                        help='This verifies the reg files with ds9 images.',
                        action='store_true',required=False)
    parser.add_argument('--compareCatMags',
                        help='Compare the various magnitudes of the different '
                             'star catalogs.',
                        action='store_true',required=False)
    parser.add_argument('--plotNomad',help='Plot the nomad catalog',
                        action='store_true',required=False)
    parser.add_argument('--plotTycho',help='Plot the tycho catalog',
                        action='store_true',required=False)
    parser.add_argument('--plotUcac',help='Plot the ucac catalog',
                        action='store_true',required=False)
    parser.add_argument('--plotBrightStarMasks',
                        help='Plot the bright-star masks',
                        action='store_true',required=False)
    parser.add_argument('--makeRandomPlots',\
                        help='Include the randoms in the plotting procedure.',
                        action='store_true',default=None)
    parser.add_argument('--sql',help='Load the data from the SQL file.',
                        action='store_true',required=False)
    parser.add_argument('--cp',help='Generate data characterization plots',
                        action='store_true',required=False)

    args = parser.parse_args()

    if args.field and args.field not in hscConfig.fields.keys():
        print(args.field + ' is not a value field in hscConfig.fields, '
              'please check your input configuration.')
        interact(local=locals())
    elif args.field==None:
        args.field = hscConfig.fields.keys()
    else:
        args.field = [args.field]
        
    dbFilename = hscConfig.brightStarDb

    ##### START THE MAIN BODY OF THE PROGRAM ####
    # 1a. Insert the Bright Stars into the sql database
    # NGC 04/22/2016: It would be nice to have a characterization of this.
    if args.insertBrightStarsSql:
        junk = InsertBsmTask(dbFilename='BrightStar.db',
                             overwriteSql=True).insertUcacIntoSql()
    
    # 1b. Generate the randoms from the data. You actually have to go inside
    #     of this program and set the patches that you want to cycle through
    #     by hand.
    if args.makeRandomFits:
        subprocess.call(['convertToSQL/convertButlerToFits',hscConfig.sspDir])

    # 1c. Open the fits files and insert the randoms into a specific randoms
    #     database.        
    if args.makeRandomSql:
        junk = InsertBsmTask(dbFilename=hscConfig.randomsDb,
                             overwriteSql=True).insertRandomsIntoSql()

    # 1d. Insert the SSP source into the sql database
    if args.makeSspSql:
        subprocess.call(['convertToSQL/convertButlerToSQL',hscConfig.sspDir])
        
    # 2. Find the correlation of the data...
    if args.measureCorrelationFunction:
        temp = bsCorr.CalculateBrightStarCorrelationNew(magModel='cmodel', \
                   plot=True,overwrite=False,doSourcesSq=False)

    # 3. Make the bright-star masks reg files. feas
    if args.makeBrightStarRegFiles:
        if args.maskSize==None:
            args.maskSize='large'
        jcBS.brightStarPatch(brightStarDb=args.brightStarDb,masks=args.maskSize)

    # Insert the Bright Star Mask Radii in Sql
    if args.insertBsrRegIntoSql:
        junk = InsertBsmTask(dbFilename=args.brightStarDb,insertReg=True,
                             overwriteSql=True)#.insertUcacIntoSql()

    # 4. Quickly plot up some of the reg files to see that they didn't get
    #    messed up.
    if args.verifyBrightStarRegFiles:
        MakePlotsTask(plotUcac=True,plotSources=None,plotRandoms=None,
                      plotPatch=True).plotRaDec()
    # 4a.
    if args.compareCatMags:
        
        if args.plotNomad + args.plotTycho + args.plotUcac < 2:
            print('\nError, at least two catalogs must be set to compare...\n')
            interact(local=locals())

        MakePlotsTask(plotNomad=args.plotNomad,plotTycho=args.plotTycho,
                      plotUcac=args.plotUcac,dbFilename=None).compareCatMags()
    # 4b.
    if args.plotBrightStarMasks:
        MakePlotsTask(plotNomad=True,plotUcac=True,plotTycho=True,
                      dbFilename=None,fields=args.field).plotRaDec()
    # 4c.        
    if args.makeRandomPlots:
        MakePlotsTask(plotSources=None,plotRandoms=True,plotPatch=None,
                      dbFilename='randoms.db').plotRaDec()
        # 4d. I'm not sure if this still works, needs to be tested.
        bands = ['g','r','i','z','y']
        mag_models = ['kron','cmodel']
        test_entries = [band + 'mag_' + mag_model
                        for band in bands for mag_model in mag_models]
        test_entries.append('shape_sdss_psf_r_det_fwhm')
        for test_entry in test_entries:
            if args.sql:
                junk = convertSQLToPickle.MakePickleTask(srcParam=test_entry)
            if args.cp:
                junk = makeCharacterizationPlots.MakePlotsTask(
                    srcParam=test_entry)
