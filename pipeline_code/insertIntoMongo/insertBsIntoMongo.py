################################################################################# 
# MODIFICATION HISTORY: 
# 01/12/2015 NGC Created
#################################################################################### 
#!/usr/bin/env python2.7
#from pymongo import ReturnDocument
import pymongo
#import json
from pymongo import MongoClient
#import unicodedata
import time
#from configuration_files.sample_cluster_json import cluster_json
#from astropy.io import fits
#from astropy.cosmology import FlatLambdaCDM
#from astropy import units as u
#from astropy.coordinates import SkyCoord
#import numpy as np
#import os
#import sys
from code import interact
########## My Own Python Packages #######################################
from pipeline_code import hscConfigurationFile as hscConfig
#There are two places where I want to replicate the data
#Master: ~/data/catalogs/clusterDB (this is on mongodb, port 27000)
#Slave: ~/data/catalogs/clusterDB2 (this is on smaip, port 27000)
#client = MongoClient('localhost',27000)
################CATALOG_TO_JSON ORDER################################### 
#NOMAD (not implemented)
#PICKLES (not implemented)
#UCAC4 (not implemented)
#USNOB (not implemented)
########################################################################
__all__ = ["InsertBsmMongoTask"]

########## FINDING NOMAD SOURCES FOR VERY BRIGHT STARS  #################
class InsertBsmMongoTask:

    _DefaultName = "insertBsMongo"

    def __init__(self,field=None):

        if field is None:
        
            self.field = 'D-S15b-DR-2'

        else:

            self.field = field

        return
    def __name__(self):
        print('Success!')

'''
def find_matches(clusters,temp_ra,temp_decl):
	limits = [temp_ra-search_radius,temp_ra+search_radius, \
                 temp_decl-search_radius,temp_decl+search_radius]
	if limits[2]<0:
           matches = clusters.find({"$and":[{"nominal.decl":{"$lt":'{:f}'.format(limits[2])}}, \
                                  {"nominal.decl":{"$gt":'{:f}'.format(limits[3])}},\
                                  {"nominal.ra":{"$gt":'{:f}'.format(limits[0])}},\
                                  {"nominal.ra":{"$lt":'{:f}'.format(limits[1])}}]})
        else:
           matches = clusters.find({"$and":[{"nominal.decl":{"$gt":'{:f}'.format(limits[2])}}, \
                                  {"nominal.decl":{"$lt":'{:f}'.format(limits[3])}},\
                                  {"nominal.ra":{"$gt":'{:f}'.format(limits[0])}},\
                                  {"nominal.ra":{"$lt":'{:f}'.format(limits[1])}}]})
	return matches
def insert(catalog,matches,cluster_j):
	if matches.count() is 0:
            cluster_j['nominal'] = cluster_j[catalog]
            clusters.insert_one(cluster_j)
            print clusters.count()
        #If there is already an entry for the cluster, we will update the ami information.
#        elif matches.count() is 1:
#            clusters.find_one_and_update({'nominal.cluster_id':matches[0]['nominal']['cluster_id']},\
#                                        {'$set':{matches[0][catalog]: cluster_j[catalog]}})
            #matches[0][catalog] = cluster_j[catalog]
#            print 'found 1 match', clusters.count(),cluster_j[catalog]['cluster_id'],matches[0][catalog]['cluster_id']#, matches[0]['nominal']['cluster_id']
        #If there is more than one cluster, we will allow the program to crash and decide what to do based 
        # on the frequency on which this happens.
        else:
	    k=0
            for i in range(matches.count()):
		#For duplicate matches, first, try to use redshift_cut to seperate them.
                if abs(float(matches[i]['nominal']['redshift'])-float(cluster_j[catalog]['redshift']))<redshift_cut: 
#                    if i == 0 :
                        #clusters.find_one_and_update({'nominal.cluster_id':matches[0]['nominal']['cluster_id']},\
                        #                          {'$set':{cluster_j['nominal']['cluster_id']:cluster_j[catalog]['cluster_id']}})
#                        print 'found 1',clusters.count(),matches[0]['nominal']['cluster_id']#,matches[0]['nominal']['subcatalog_name']
                    if i > 0:
                        print 'duplicate clusters', matches[i]['nominal']['cluster_id']#  matches[i]['nominal']['subcatalog_name']
			#If there are more than one matches in the search radius, the program will update the multiple_system to 'Y'
                        clusters.find_one_and_update({'nominal.cluster_id':matches[i]['nominal']['cluster_id']},\
						     {'$set':{'nominal.multiple_system': 'Y'}})
			#print('There are '+str(matches.count())+' clusters within the search radius.')
		    k+=1
            if k>1:
		print 'There are ',k,' clusters within the search radius.'
	return
'''
def NomadToJson(brightStars):

    #Record how long it takes to generate this database....
    time0 = time.clock()

    nomad_home = hscConfig.catalog_folder+'NOMAD/'

    #Consider putting this in an initialization file
    tractInfoFile = loadTractInfoFile.data(fieldName=self.field)

        dtype = [('minRA','float'),('maxRA','float'),('minDec','float'),('maxDec','float')]
        #If the tract information is given, this is what you should use:

        #if 'Center' in tract_info_file.keys():
            
        self.tpRADec = np.array([(tract_x['Corner1']['RA'], tract_x['Corner3']['RA'], \
                             tract_x['Corner1']['Dec'], tract_x['Corner3']['Dec']) for key_x,tract_x in tract_info_file.items()],dtype=dtype)
            
        self.tpRADec.sort(order='minDec')

    time1 = time.clock()
    return brightStars
'''
#########################################################################
# Since this is the largest database, this will be the first step for inserting
# the clusters.
def mcxc_to_json(clusters):
    m_x = load_xray.mcxc()
    m_x["MASS_500"] = m_x["MASS_500"]*1E-14
    for j in range(len(m_x)):
        #Need to make a deepcopy, otherwise, cluster_json will be treated as a pointer.
        cluster_j = deepcopy(cluster_json)
        cluster_j['nominal']['cluster_id'] = m_x[j]['NAME']
        cluster_j['nominal']['alt_cluster_id'] = m_x[j]['ALT_NAME']
        cluster_j['nominal']['redshift'] = '{:f}'.format(m_x[j]['REDSHIFT'])
        cluster_j['nominal']['ra'] = '{:f}'.format(m_x[j]['RA'])#degrees
        cluster_j['nominal']['decl'] = '{:f}'.format(m_x[j]['DEC'])#degrees
        cluster_j['nominal']['mass']['500']['bf'] = '{:f}'.format(m_x[j]['MASS_500']*cosmo.h)#convert to 1E14/h SOLAR_MASS
        cluster_j['nominal']['ysz']['500']['bf'] = '{:f}'.format(m_x[j]['RADIUS_500'])#MPC
        cluster_j['nominal']['lx']['500']['bf'] = '{:f}'.format(m_x[j]['LX_500'])#ERG/S
        cluster_j['mcxc'] = cluster_j['nominal']
        clusters.insert_one(cluster_j)
    return clusters
####################################################################################
def bcs_to_json(clusters):
    bcs_data = load_xray.bcs()
    j=0
    catalog = 'bcs'
    for row in bcs_data:
	matches = find_matches(clusters,float(row[2]),float(row[3])) #degrees
	cluster_j = deepcopy(cluster_json)
        cluster_j[catalog]['cluster_id'] = row[1]
        cluster_j[catalog]['redshift'] = row[11]
        cluster_j[catalog]['ra'] = row[2]
        cluster_j[catalog]['decl'] = row[3]
        #eliminate the extra "e" in row[10](kT)
        if float(row[10]) == 0: #row is string
            if row[10][4]:  #xx.xe
                row[10] = row[10][0:4]
            elif row[10][3]: #x.xe
                row[10] = row[10][0:3]
        cluster_j[catalog]['tx']['bf'] = row[10]   #keV
        cluster_j[catalog]['lx']['bf'] = row[13]  #which radius??
        cluster_j[catalog]['radius']['bf'] = row[7] #r_VTP #arcmin
        insert(catalog,matches,cluster_j)
	j+=1
    return clusters
def ebcs_to_json(clusters):
    ebcs_data = load_xray.ebcs()
    catalog = 'ebcs'
    j=0
    for row in ebcs_data:
        matches = find_matches(clusters,float(row[2]),float(row[3])) #degrees
	cluster_j = deepcopy(cluster_json)
        cluster_j[catalog]['cluster_id'] = row[1]
        cluster_j[catalog]['redshift'] = row[11]
        cluster_j[catalog]['ra'] = row[2] #degree
        cluster_j[catalog]['decl'] = row[3] #degree
        cluster_j[catalog]['tx']['bf'] = row[10]  #keV
        cluster_j[catalog]['lx']['bf'] = row[13]  #which radius??
        insert(catalog,matches,cluster_j)
        j+=1
    return clusters

def reflex_to_json(clusters):
    reflex_data = load_xray.reflex()
    catalog = 'reflex'
    j=0
    for row in reflex_data:
	temp_coord = SkyCoord(" ".join(row[1:3]),unit=(u.hourangle,u.deg))
    	#print SkyCoord.to_string(temp_coord).split()[0]
	matches = find_matches(clusters,float(SkyCoord.to_string(temp_coord).split()[0]),float(SkyCoord.to_string(temp_coord).split()[1]))
	cluster_j = deepcopy(cluster_json)
        cluster_j[catalog]['cluster_id'] = row[0]
        cluster_j[catalog]['redshift'] = row[6]
        cluster_j[catalog]['ra'] = SkyCoord.to_string(temp_coord).split()[0]
        cluster_j[catalog]['decl'] = SkyCoord.to_string(temp_coord).split()[1]
        cluster_j[catalog]['lx']['bf'] = row[7]   #unit??
        insert(catalog,matches,cluster_j)
        j+=1
    return clusters

def WL_to_json(clusters):
    j=0
    merten_data = load_masses.merten2014_N()
    catalog = 'merten2014'
    for row in merten_data:
        matches = find_matches(clusters,float(row[17]),float(row[18])) #degrees
	cluster_j = deepcopy(cluster_json)
        cluster_j[catalog]['cluster_id'] = row[0]
        cluster_j[catalog]['ra'] = '{:f}'.format(row(17))
        cluster_j[catalog]['decl'] = '{:f}'.format(row(18))
        cluster_j[catalog]['mass']['2500']['bf'] = '{:f}'.format(float(row[1])*10)  #convert from 1E15/h SOLAR_MASS to 1E14/h SOLAR_MASS
        cluster_j[catalog]['mass']['2500']['sig']['upper'] = '{:f}'.format(float(row[2])*10)
        cluster_j[catalog]['mass']['2500']['sig']['lower'] = '{:f}'.format(float(row[2])*10)
        cluster_j[catalog]['mass']['500']['bf'] = '{:f}'.format(float(row[5])*10)
        cluster_j[catalog]['mass']['500']['sig']['upper'] = '{:f}'.format(float(row[6])*10)
        cluster_j[catalog]['mass']['500']['sig']['lower'] = '{:f}'.format(float(row[6])*10)
        cluster_j[catalog]['mass']['200']['bf'] = '{:f}'.format(float(row[9])*10)
        cluster_j[catalog]['mass']['200']['sig']['upper'] = '{:f}'.format(float(row[10])*10)
        cluster_j[catalog]['mass']['200']['sig']['lower'] = '{:f}'.format(float(row[10])*10)
        cluster_j[catalog]['mass']['vir']['bf'] = '{:f}'.format(float(row[13])*10)
        cluster_j[catalog]['mass']['vir']['sig']['upper'] = '{:f}'.format(float(row[14])*10)
        cluster_j[catalog]['mass']['vir']['sig']['lower'] = '{:f}'.format(float(row[14])*10)
        insert(catalog,matches,cluster_j)
        j+=1
    return clusters
def comalit_to_json(clusters):
    comalit_data = load_masses.comalit()
    catalog = 'comalit'
    j=0
    for row in comalit_data:
	temp_coord = SkyCoord(" ".join(row[2:4]),unit=(u.hourangle,u.deg))
        matches = find_matches(clusters,float(SkyCoord.to_string(temp_coord).split()[0]),float(SkyCoord.to_string(temp_coord).split()[1]))
	cluster_j = deepcopy(cluster_json)
        cluster_j[catalog]['cluster_id'] = row[0]+row[1]
        cluster_j[catalog]['redshift'] = row[4]
        cluster_j[catalog]['ra'] = SkyCoord.to_string(temp_coord).split()[0]
        cluster_j[catalog]['decl'] = SkyCoord.to_string(temp_coord).split()[1]
        cluster_j[catalog]['mass']['2500']['bf'] = row[13]
        cluster_j[catalog]['mass']['500']['bf'] = row[15]
        cluster_j[catalog]['mass']['200']['bf'] = row[17]
        cluster_j[catalog]['mass']['vir']['bf'] = row[19]
        cluster_j[catalog]['mass']['2500']['sig']['median'] = row[14]
        cluster_j[catalog]['mass']['500']['sig']['median'] = row[16]  
        cluster_j[catalog]['mass']['200']['sig']['median'] = row[18]
        cluster_j[catalog]['mass']['vir']['sig']['median'] = row[20]
        insert(catalog,matches,cluster_j)
        j+=1
    return clusters

def relics_to_json(cluster):
    catalog = 'relics'
    relics_data = load_masses.relics()
    j=0
    for row in relics_data:
        matches = find_matches(clusters,float(row[8]),float(row[9])) #degrees
	cluster_j = deepcopy(cluster_json)
        cluster_j[catalog]['redshift'] = row[3]
        cluster_j[catalog]['ra'] = row[8]
        cluster_j[catalog]['dec'] = row[9]
        cluster_j[catalog]['alt_cluster_id'] = row[6]
        insert(catalog,matches,cluster_j)
        j+=1
    return clusters

def okabe2010_to_json(clusters):
    catalog = 'okabe2010'
    okabe2010_data = load_masses.Okabe2010()
    j=0
    for row in okabe2010_data:
	    temp_coord = SkyCoord(" ".join(row[2:8]),unit=(u.hourangle,u.deg))
            matches = find_matches(clusters,float(SkyCoord.to_string(temp_coord).split()[0]),float(SkyCoord.to_string(temp_coord).split()[1]))            
	    cluster_j = deepcopy(cluster_json)
            cluster_j[catalog]['cluster_id'] = row[0]+row[1]
            cluster_j[catalog]['ra'] = SkyCoord.to_string(temp_coord).split()[0]
            cluster_j[catalog]['decl'] = SkyCoord.to_string(temp_coord).split()[1]
            if len(row)>=9:
                cluster_j[catalog]['redshift'] = row[8]
                cluster_j[catalog]['lx']['bf'] = row[9]#x-ray measured at 0.1-2.4keV(obtained from ROSAT) in 10^44erg*s^-1
            if len(row)>=12:
                cluster_j[catalog]['mass']['vir']['bf'] = row[10]
                cluster_j[catalog]['mass']['vir']['sig']['upper'] = row[11]
                cluster_j[catalog]['mass']['vir']['sig']['lower'] = '{:f}'.format(float(row[12])*(-1))
            if len(row)>=15:   
                cluster_j[catalog]['mass']['200']['bf'] = row[13]
                cluster_j[catalog]['mass']['200']['sig']['upper'] = row[14]
                cluster_j[catalog]['mass']['200']['sig']['lower'] = '{:f}'.format(float(row[15])*(-1))
            if len(row)>=21:
                cluster_j[catalog]['mass']['500']['bf'] = row[16]
                cluster_j[catalog]['mass']['500']['sig']['upper'] = row[17]
                cluster_j[catalog]['mass']['500']['sig']['lower'] = '{:f}'.format(float(row[18])*(-1))
                cluster_j[catalog]['mass']['2500']['bf'] = row[19]
                cluster_j[catalog]['mass']['2500']['sig']['upper'] = row[20]
                cluster_j[catalog]['mass']['2500']['sig']['lower'] = '{:f}'.format(float(row[21])*(-1))
            insert(catalog,matches,cluster_j)    
            j+=1
    return clusters

def okabe2015_to_json(clusters):
	catalog = 'okabe2015'
	locuss_data = load_masses.Okabe2015()
	j=0
	for row in locuss_data:
                temp_coord = SkyCoord(" ".join(row[2:4]),unit=(u.hourangle,u.deg))
        	matches = find_matches(clusters,float(SkyCoord.to_string(temp_coord).split()[0]),float(SkyCoord.to_string(temp_coord).split()[1]))
		cluster_j = deepcopy(cluster_json)
		cluster_j[catalog]['cluster_id'] = row[0]         
		cluster_j[catalog]['redshift'] = row[1]
		cluster_j[catalog]['ra'] = SkyCoord.to_string(temp_coord).split()[0]   #degree
		cluster_j[catalog]['decl'] = SkyCoord.to_string(temp_coord).split()[1]   #degree
		cluster_j[catalog]['mass']['vir']['bf'] = row[4]           #10^14 Msol*h-1
		cluster_j[catalog]['mass']['vir']['sig']['upper'] = row[6]   
		cluster_j[catalog]['mass']['vir']['sig']['lower'] = row[5]
		cluster_j[catalog]['mass']['200']['bf'] = row[7]
		cluster_j[catalog]['mass']['200']['sig']['upper'] = row[9]
		cluster_j[catalog]['mass']['200']['sig']['lower'] = row[8]
		cluster_j[catalog]['mass']['500']['bf'] = row[10]
		cluster_j[catalog]['mass']['500']['sig']['upper'] = row[12]
		cluster_j[catalog]['mass']['500']['sig']['lower'] = row[11]
		cluster_j[catalog]['mass']['2500']['bf'] = row[16]
		cluster_j[catalog]['mass']['2500']['sig']['upper'] = row[18]
		cluster_j[catalog]['mass']['2500']['sig']['lower'] = row[17]
		insert(catalog,matches,cluster_j)	
		j+=1
	return clusters

def cccp_to_json(clusters):
    catalog = 'cccp'
    cccp_data = load_masses.CCCP()
    j=0
    for row in cccp_data:
	temp_coord = SkyCoord(" ".join(row[4:10]),unit=(u.hourangle,u.deg))
        matches = find_matches(clusters,float(SkyCoord.to_string(temp_coord).split()[0]),float(SkyCoord.to_string(temp_coord).split()[1]))
	cluster_j = deepcopy(cluster_json)
        cluster_j[catalog]['cluster_id'] = row[1]+row[2]
        cluster_j[catalog]['redshift'] = row[3]
        cluster_j[catalog]['ra'] = SkyCoord.to_string(temp_coord).split()[0]
        cluster_j[catalog]['decl'] = SkyCoord.to_string(temp_coord).split()[1]
        cluster_j[catalog]['mass']['vir']['bf'] = row[18]
        cluster_j[catalog]['mass']['vir']['sig']['upper'] = row[19]
        cluster_j[catalog]['mass']['vir']['sig']['lower'] = row[20]
        cluster_j[catalog]['mass']['2500']['bf'] = row[21]
        cluster_j[catalog]['mass']['2500']['sig']['upper'] = row[22]
        cluster_j[catalog]['mass']['2500']['sig']['lower'] = row[23]
        cluster_j[catalog]['mass']['500']['bf'] = row[24]
        cluster_j[catalog]['mass']['500']['sig']['upper'] = row[25]
        cluster_j[catalog]['mass']['500']['sig']['lower'] = row[26]
        insert(catalog,matches,cluster_j)
        j+=1
    return clusters

def umetsu2015_to_json(clusters):
    catalog = 'umetsu2015'
    data = load_masses.umetsu2015()
    j=0
    for row in data:
        temp_coord = SkyCoord(" ".join(row[2:4]),unit=(u.hourangle,u.deg))
        matches = find_matches(clusters,float(SkyCoord.to_string(temp_coord).split()[0]),float(SkyCoord.to_string(temp_coord).split()[1]))
        cluster_j = deepcopy(cluster_json)
        cluster_j[catalog]['cluster_id'] = row[0]
        cluster_j[catalog]['redshift'] = row[1]
        cluster_j[catalog]['ra'] = SkyCoord.to_string(temp_coord).split()[0]   #degree
        cluster_j[catalog]['decl'] = SkyCoord.to_string(temp_coord).split()[1]
        cluster_j[catalog]['mass']['200']['bf'] = '{:f}'.format(float(row[4])*cosmo.h)                  #convert 10^14Msol to 10^14Msol/h
        cluster_j[catalog]['mass']['200']['sig']['upper'] = '{:f}'.format(float(row[5])*cosmo.h) 
        cluster_j[catalog]['mass']['200']['sig']['lower'] = '{:f}'.format(float(row[5])*cosmo.h) 
        cluster_j[catalog]['mass']['2500']['bf'] = '{:f}'.format(float(row[6])*cosmo.h) 
        cluster_j[catalog]['mass']['2500']['sig']['upper'] = '{:f}'.format(float(row[7])*cosmo.h) 
        cluster_j[catalog]['mass']['2500']['sig']['lower'] = '{:f}'.format(float(row[7])*cosmo.h) 
        cluster_j[catalog]['mass']['500']['bf'] = '{:f}'.format(float(row[10])*cosmo.h) 
        cluster_j[catalog]['mass']['500']['sig']['upper'] = '{:f}'.format(float(row[11])*cosmo.h) 
        cluster_j[catalog]['mass']['500']['sig']['lower'] = '{:f}'.format(float(row[11])*cosmo.h) 
        cluster_j[catalog]['mass']['vir']['bf'] = '{:f}'.format(float(row[12])*cosmo.h) 
        cluster_j[catalog]['mass']['vir']['sig']['upper'] = '{:f}'.format(float(row[13])*cosmo.h) 
        cluster_j[catalog]['mass']['vir']['sig']['lower'] = '{:f}'.format(float(row[13])*cosmo.h) 
        insert(catalog,matches,cluster_j)
	j+=1
    return clusters

def WtG_to_json(clusters):
    catalog = 'wtg'
    data = load_masses.wtg()
    j=0
    for row in data:
        temp_coord = SkyCoord(" ".join(row[2:4]),unit=(u.hourangle,u.deg))
        matches = find_matches(clusters,float(SkyCoord.to_string(temp_coord).split()[0]),float(SkyCoord.to_string(temp_coord).split()[1]))
	cluster_j = deepcopy(cluster_json)
        cluster_j[catalog]['cluster_id'] = row[0]
        cluster_j[catalog]['redshift'] = row[1]
        cluster_j[catalog]['ra'] = SkyCoord.to_string(temp_coord).split()[0]   #degree
        cluster_j[catalog]['decl'] = SkyCoord.to_string(temp_coord).split()[1]
        cluster_j[catalog]['radius']['scale']['bf'] = '{:f}'.format(float(row[4]))         #Mpc     
        cluster_j[catalog]['radius']['scale']['sig']['upper'] = '{:f}'.format(float(row[5])) 
        cluster_j[catalog]['radius']['scale']['sig']['lower'] = '{:f}'.format(float(row[6])) 
        cluster_j[catalog]['mass']['special']['bf'] = '{:f}'.format(float(row[7])*cosmo.h)           #M(<1.5Mpc)
        cluster_j[catalog]['mass']['special']['sig']['upper'] = '{:f}'.format(float(row[8])*cosmo.h) 
        cluster_j[catalog]['mass']['special']['sig']['lower'] = '{:f}'.format(float(row[9])*cosmo.h) #convert 10^14Msol to 10^14Msol/h 
	insert(catalog,matches,cluster_j)
    return clusters
'''
'''
def hst2015_to_json(clusters):
    filename = mass_files_config.hst2015
    hst2015 = open(filename)
#    hst2015_json = {"analysis_name":"hst2015_analysis","galaxy_clusters":{}}
    hst2015_line = hst2015.readline()
    j=0
    while hst2015_line:
        if hst2015_line[0:5]!='INDEX':
            hst2015_column = hst2015_line.split('	')
            temp_decl = float(hst2015_column[7]) #degree
            temp_ra = float(hst2015_column[6]) #degree
            matches = find_matches(clusters,temp_coord)
            
            cluster_j = deepcopy(cluster_json)
            cluster_j['hst2015']['cluster_id'] = hst2015_column[4]
            cluster_j['hst2015']['redshift'] = hst2015_column[3]
            cluster_j['hst2015']['ra'] = hst2015_column[6]
            cluster_j['hst2015']['decl'] = hst2015_column[7]
            cluster_j['hst2015']['mass']['bf'] = hst2015_column[2]   #unit??
            #If there is no entry for the cluster, then we will create a new version. 
            if matches.count() is 0:
                cluster_j['nominal'] = cluster_j['hst2015']
                clusters.insert_one(cluster_j)
            #If there is already an entry for the cluster, we will update the ami information.
            elif matches.count() is 1:
                matches[0]['hst2015'] = cluster_j['hst2015']
            #If there is more than one cluster, we will allow the program to crash and decide what to do based 
            # on the frequency on which this happens.
            else:
                count=0
                for i in range(matches.count()):
#                print type(float(matches[i]['nominal']['redshift']))
                
                    if abs(float(matches[i]['nominal']['redshift'])-float(cluster_j['hst2015']['redshift']))<redshift_cut:
                        if count == 0 :
                            matches[0]['hst2015'] = cluster_j['hst2015'] #update the information with 1 mathches
                            print 'found 1'
                        elif count > 0:
                            print 'duplicate clusters in redshift'
                            cluster_j['hst2015']['multiple_system'] = 'Y'
                        count+=1
                print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                print('There are '+str(matches.count())+' clusters within the search radius.')
                print('The current plan is to copy the hst2015 data to both of these entries.')
                print('Type Ctrl-D to continue...')
                print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                for document in matches:
                    document['hst2015'] = cluster_j['hst2015']
            j+=1
        hst2015_line = hst2015.readline()
    hst2015.close()
    return clusters
'''
'''
def mantz2010_to_json(clusters):
    mantz_data = load_masses.mantz2010()
    catalog = 'mantz2010'
    j=0
    for row in mantz_data:
        temp_coord = SkyCoord(" ".join(row[3:7]),unit=(u.hourangle,u.deg))
	matches = find_matches(clusters,float(SkyCoord.to_string(temp_coord).split()[0]),float(SkyCoord.to_string(temp_coord).split()[1]))
	cluster_j = deepcopy(cluster_json)
        cluster_j[catalog]['cluster_id'] = row[0]+row[1]
        cluster_j[catalog]['redshift'] = row[2]
        cluster_j[catalog]['ra'] = SkyCoord.to_string(temp_coord).split()[0]
        cluster_j[catalog]['decl'] = SkyCoord.to_string(temp_coord).split()[1]
        cluster_j[catalog]['mass']['500']['bf'] = row[7]
        insert(catalog,matches,cluster_j)
        j+=1
    return clusters

def planck_union_to_json(clusters):
    #This has all of the useful information (columns)
    union_fits = load_sze.planck_union()
    catalog = 'union'
    #Here are the columns that we want to keep for the ami analysis: (ami.columns)
    conversion_factor = union_fits[0]
    union_fits = union_fits[1]
    nunion = len(union_fits.data)
    all_clusters = []
    for j in range(nunion):
         ### TODO: CHECK TO SEE IF THERE ARE ANY CLUSTERS WITHIN THE RANGE OF THE EXISTING CLUSTERS
        matches = find_matches(clusters,union_fits.data['RA'][j],union_fits.data['Dec'][j])
        cluster_j = deepcopy(cluster_json)
        cluster_j[catalog]['cluster_id'] = union_fits.data['Name'][j]
        cluster_j[catalog]['redshift'] = '{:f}'.format(union_fits.data['REDSHIFT'][j])
        cluster_j[catalog]['ra'] = '{:f}'.format(union_fits.data['RA'][j])#degree
        cluster_j[catalog]['decl'] = '{:f}'.format(union_fits.data['Dec'][j])#degree
        cluster_j[catalog]['ysz']['500']['bf'] = '{:f}'.format(union_fits.data['Y5R500'][j]*0.001*conversion_factor)#convert to arcmin^2
        cluster_j[catalog]['ysz']['500']['sig']['upper'] = '{:f}'.format(union_fits.data['Y5R500_ERR'][j]*0.001*conversion_factor)#convert to arcmin^2
        cluster_j[catalog]['ysz']['500']['sig']['lower'] = '{:f}'.format(union_fits.data['Y5R500_ERR'][j]*(-0.001)*conversion_factor)#convert to arcmin^2
        cluster_j[catalog]['mass']['500']['bf'] = '{:f}'.format(union_fits.data['MSZ'][j]/0.676)#convert 10^14 Msol to 10^14Msol/h   #didn't mention the 'radius'?500?
        cluster_j[catalog]['mass']['500']['sig']['upper'] = '{:f}'.format(union_fits.data['MSZ_ERR_UP'][j]/0.676)#10^14 Msol/h
        cluster_j[catalog]['mass']['500']['sig']['lower'] = '{:f}'.format(union_fits.data['MSZ_ERR_LOW'][j]/0.676)#10^14 Msol/h
        insert(catalog,matches,cluster_j)
    return clusters
'''
'''
def planck_mmf3_to_json(clusters):        #MMF3 IS ALREADY INCLUDED IN UNIO CATALOG!!!!
    mmf3_fits = load_sze.planck_mmf3()
#    n=0
    nmmf3 = len(mmf3_fits.data)
    for j in range(nmmf3):
        matches = find_matches(clusters,mmf3_fits.data['RA'][j],mmf3_fits.data['Dec'][j])
        cluster_j = deepcopy(cluster_json)
        cluster_j['mmf3']['cluster_id'] = mmf3_fits.data['NAME'][j]
        cluster_j['mmf3']['ra'] = '{:f}'.format(mmf3_fits.data['RA'][j]) #degree
        cluster_j['mmf3']['decl'] ='{:f}'.format(mmf3_fits.data['DEC'][j])  #degree
        cluster_j['mmf3']['ysz_min'] = '{:f}'.format(mmf3_fits.data['Y_MIN'][j]) #arcmin^2
        cluster_j['mmf3']['ysz_max'] ='{:f}'.format(mmf3_fits.data['Y_MAX'][j]) #arcmin^2
        insert(catalog,matches,cluster_j)
    return clusters
'''
'''
def ami_to_json(clusters):
    ami_fits = load_sze.ami()                                      
    #Here are the columns that we want to keep for the ami analysis: (ami.columns)
    catalog = 'ami'
    nami = len(ami_fits.data)
    all_clusters = []
    for j in range(nami):
         ### TODO: CHECK TO SEE IF THERE ARE ANY CLUSTERS WITHIN THE RANGE OF THE EXISTING CLUSTERS
        matches = find_matches(clusters,ami_fits.data['AMI_RA'][j],ami_fits.data['AMI_Dec'][j])
        cluster_j = deepcopy(cluster_json)
        cluster_j[catalog]['cluster_id'] = ami_fits.data['Planck_ID'][j]
        cluster_j[catalog]['redshift'] = '{:f}'.format(ami_fits.data['Planck_z'][j])
        cluster_j[catalog]['ra'] = '{:f}'.format(ami_fits.data['AMI_RA'][j])#degree
        cluster_j[catalog]['decl'] = '{:f}'.format(ami_fits.data['AMI_Dec'][j])#degree
        cluster_j[catalog]['radius']['500']['bf'] = '{:f}'.format(ami_fits.data['AMI_THETA_500'][j]) #arcmin
        cluster_j[catalog]['radius']['500']['sig']['median'] = '{:f}'.format(ami_fits.data['AMI_ETHETA_500'][j]) #arcmin
        cluster_j[catalog]['radius']['500']['sig']['upper'] = '{:f}'.format(ami_fits.data['AMI_ETHETA_500_U'][j]) #arcmin
        cluster_j[catalog]['radius']['500']['sig']['lower'] = '{:f}'.format(ami_fits.data['AMI_ETHETA_500_L'][j]) #arcmin
        cluster_j[catalog]['ysz']['500']['bf'] = '{:f}'.format(ami_fits.data['AMI_Y500'][j])#arcmin^2
        cluster_j[catalog]['ysz']['500']['sig']['median'] = '{:f}'.format(ami_fits.data['AMI_ETHETA_500'][j])#arcmin^2
        cluster_j[catalog]['ysz']['500']['sig']['upper'] = '{:f}'.format(ami_fits.data['AMI_EY500_U'][j])#arcmin^2
        cluster_j[catalog]['ysz']['500']['sig']['lower'] = '{:f}'.format(ami_fits.data['AMI_EY500_L'][j])#arcmin^2
        insert(catalog,matches,cluster_j)
    return clusters

def czakon2015_to_json(clusters):
    catalog = 'czakon2015'
    bolo_data = load_sze.czakon2015_N()
    j=0
    for row in bolo_data:
        temp_coord = SkyCoord(" ".join(row[1:3]),unit=(u.hourangle,u.deg))
        matches = find_matches(clusters,float(SkyCoord.to_string(temp_coord).split()[0]),float(SkyCoord.to_string(temp_coord).split()[1]))
	cluster_j = deepcopy(cluster_json)
        cluster_j[catalog]['cluster_id'] = row[0]
        cluster_j[catalog]['redshift'] = row[4]
        cluster_j[catalog]['ra'] = SkyCoord.to_string(temp_coord).split()[0]
        cluster_j[catalog]['decl'] = SkyCoord.to_string(temp_coord).split()[1]
        cluster_j[catalog]['radius']['2500']['bf'] = row[5]  #Mpc
        cluster_j[catalog]['radius']['2500']['sig']['upper'] = row[6]  #Mpc
        cluster_j[catalog]['radius']['2500']['sig']['lower'] = row[7]  #Mpc
        cluster_j[catalog]['mass']['2500']['bf'] = '{:f}'.format(float(row[11])*cosmo.h)    #convert M^14Msol to M^14Msol/h 
        cluster_j[catalog]['mass']['2500']['sig']['upper'] = '{:f}'.format(float(row[12])*cosmo.h)
        cluster_j[catalog]['mass']['2500']['sig']['lower'] = '{:f}'.format(float(row[13])*cosmo.h)
        cluster_j[catalog]['tx']['bf'] = row[14] #KeV
        cluster_j[catalog]['tx']['sig']['upper'] = row[15] #KeV
        cluster_j[catalog]['tx']['sig']['lower'] = '{:f}'.format(float(row[15])*(-1))
        cluster_j[catalog]['ysz']['2500']['bf'] = '{:f}'.format(float(row[16])*(1E-10)*((180*60/np.pi)**2))  #convert 1E-10 ster to arcmin^2
        cluster_j[catalog]['ysz']['2500']['sig']['upper'] = '{:f}'.format(float(row[17])*(1E-10)*((180*60/np.pi)**2))  
        cluster_j[catalog]['ysz']['2500']['sig']['lower'] = '{:f}'.format(float(row[18])*(1E-10)*((180*60/np.pi)**2)) 
        insert(catalog,matches,cluster_j)
        j+=1
    return clusters
'''
############################# MAIN PROGRAM #######################################
# Initialize the port to communicate with the database
client = MongoClient('mongodb.asiaa.sinica.edu.tw',27000)
db = client.brightStars#Can call db.collection_names

#DEBUGGING: Keep this command only until the program has been fully debugged.
#db.drop_collection('clusters')

#Step 1: Count the number of entries, if 0 start by filling in the database with the MCXC clusters.
brightStars = db.brightStars

if brightStars.count() is 0:
    print 'No bright stars in database, will start to insert NOMAD stars....this will take a while...'
    brightStars = NomadToJson(brightStars)
else:
    print (brightStars.count()),' entries in the MongoDB bright star catalog.\n'

interact(local=locals())
search_radius = 5/60.#*u.deg #5 arcmin
redshift_cut2 = 100
redshift_cut = 0.1   #dz
#print 'inserting bcs'
#clusters = bcs_to_json(clusters)
#print 'inserting ebcs'
#clusters = ebcs_to_json(clusters)
'''
print 'inserting reflex'
clusters = reflex_to_json(clusters)
print 'inserting WL'
clusters = WL_to_json(clusters)
print 'inserting okabe2010'
clusters = okabe2010_to_json(clusters)
'''
#print 'inserting okabe2015'
#clusters = okabe2015_to_json(clusters)
#print 'inserting WtG'
#clusters = WtG_to_json(clusters)

    #This one has problems because of duplicate clusters
#print 'inserting mantz2010'
#clusters = mantz2010_to_json(clusters)
    #Check to make sure that this runs correctly
#print 'inserting umetsu2015'
#clusters = umetsu2015_to_json(clusters)
#print 'inserting CCCP'
#clusters = cccp_to_json(clusters)

#print 'Inserting RELICS clusters:\n'
#clusters = relics_to_json(clusters)

#print 'inserting comalit'
#clusters = comalit_to_json(clusters)
##print 'inserting hst2015'
##hst2015_clusters = hst2015_to_json(clusters)
print 'inserting planck_union'
clusters = planck_union_to_json(clusters) 
##print 'inserting planck_mmf3'
##clusters = planck_mmf3_to_json(clusters)  
#print 'inserting planck_ami'
#clusters = ami_to_json(clusters)
#print 'inserting czakon2015'
#clusters = czakon2015_to_json(clusters)

