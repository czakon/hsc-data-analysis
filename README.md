# README #

This is a public repository for the code that I have been using to characterize the extent of the bright star halo in HyperSuprime camera images. Please see the wiki for more information on what this code does.