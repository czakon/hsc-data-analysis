from setuptools import setup

setup(name='pipeline_code', version='0.1',description='This is a set of programs that have been developed in relation to the HSC pipeline and specifically for the bright star mask project.',author='Nicole Czakon',packages=['pipeline_code'])
