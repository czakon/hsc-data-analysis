#!/usr/bin/env python

import numpy as np
import lsst.daf.persistence as dafPersist
import lsst.afw.geom as afwGeom
import lsst.meas.astrom as measAstrom

def main():

    # pick your favourite frame
    d      = "/lustre/Subaru/SSP/rerun/yasuda/SSP3.7.3_20150518/"
    butler = dafPersist.Butler(d)
    dataId = {"visit": 1236, "ccd": 50}
    
    exposure = butler.get('calexp', dataId)
    wcs      = exposure.getWcs()
    sources  = butler.get('src', dataId)

    # grab a random ra,dec
    n   = len(sources)
    ra  = sources[n/2].getRa()
    dec = sources[n/2].getDec()

    # get the nearby sources from the installed catalog (currently either sdss or pan-starrs)

    # get all objects within this distance of the above ra,dec
    rad = 0.05*afwGeom.degrees
    astrom = measAstrom.astrom.Astrometry(measAstrom.config.MeasAstromConfig())
    cat = astrom.getReferenceSources(ra, dec, rad, "r")

    # how many did we get?
    print "We found this many catalog sources", len(cat)    


    # now lets add them to the mask plane
    msk = exposure.getMaskedImage().getMask()

    # create our own new mask plane
    myPlane = msk.addMaskPlane("ME_ME_ME")
    myBit = 1 << myPlane
    # we'll use the np array as it's easier to do slicing operations
    msk_np = msk.getArray()
    nx, ny = msk.getWidth(), msk.getHeight()  # image dimensions
    dx, dy = 100, 100                         # box half-size to use to mask each source
    for c in cat:

        x,y = wcs.skyToPixel(c.getCoord())
        print "masking at: ", x,y

        # make sure we don't try to set pixels off the edge of the frame
        xmin = max(x-dx, 0)
        ymin = max(y-dy, 0)
        xmax = min(x+dx, nx)
        ymax = min(y+dy, ny)

        # OR with our mask plane value to set the bit without changing existing bits
        msk_np[ymin:ymax,xmin:xmax] |= myBit

    # write it (or send it to ds9 to view)
    exposure.writeFits("foo.fits")

    
if __name__ == '__main__':
    main()

    
