import lsst.daf.persistence as dafPersist
import lsst.afw.coord as afwCoord
import lsst.afw.image
import lsst.afw.geom
import os

dataDir = os.environ['runDir']
butler = dafPersist.Butler(dataDir)
#skyMap = butler.get("deepCoadd_skyMap", immediate=True)
visit =  int(os.environ['z_visit'])#works: g,r,i,z,y
ccd =  int(os.environ['test_ccd'])
dataId = {"visit" : visit, "ccd": ccd}
skyMap = butler.get("calexp",dataId)#exposure
                    
# your coordinates here
coord = afwCoord.IcrsCoord(ra, dec)

# Iterate over tracts and patches that overlap the point.
# Note that there may be multiple coadds of a single point, because we
# intentionally overlap them.
for tract, patch in skyMap.findClosestTractPatchList([coord]):
    coadd = butler.get("deepCoadd", tract=tract.getId(),
                       patch="%d,%d" % patch.getIndex(),
                       filter="HSC-I", immediate)  # your filter here
    pixel = coadd.getWcs().skyToPixel(coord)
    bbox = lsst.afw.geom.Box2I(pixel, pixel)   # 1-pixel box
    bbox.grow(15)    # now a 31x31 pixel box
    bbox.clip(coadd.getBBox(lsst.afw.image.PARENT))  # clip to overlap region
    if bbox.isEmpty():
        continue
    subImage = lsst.afw.image.ExposureF(coadd, bbox, lsst.afw.image.PARENT)
    # Save the subimage (or do something else with it)
    # the saved WCS should be correct automatically.
    subImage.writeFits("cutout.fits")
